<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<!doctype html>
<html>
    <head>
        <!-- ESTILOS PICKER -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <link rel="stylesheet" href="../css/zebra_datepicker.min.css" type="text/css">
        <link rel="stylesheet" href="../css/examples.css" type="text/css">       

        <!-- LIBRERIAS DE JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <!--<script src="https://cdn.jsdelivr.net/npm/zebra_pin@2.0.0/dist/zebra_pin.min.js"></script>-->
        <script src="../js/zebra_datepicker.min.js"></script>
        <script src="../js/examples.js"></script>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>2.</strong> Allow future dates only, starting one day in the future</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-future-tomorrow" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>3. </strong> Allow dates from a dynamic interval</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-dynamic-interval" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>4.</strong> Allow dates from an interval between 2 dates</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-dates-interval" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>5.</strong> Allow only dates that come after a certain date</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-after-date" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>6.</strong> Disable dates</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-disabled-dates" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>
                    <!-- =========================================================================================== -->

                    <h3><strong>7.</strong> Date ranges (sort of)</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-range-start" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                            <div class="col-sm-3">
                                <input id="datepicker-range-end" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>8.</strong> Date formats</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-formats" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>9.</strong> Enabling the time picker</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-time" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>10.</strong> Partial date formats</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-partial-date-formats" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>11.</strong> Showing week numbers</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-week-number" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>12.</strong> Changing the starting view</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-starting-view" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>13.</strong> Custom classes</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-custom-classes" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>14.</strong> Handling the "onChange" event</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-on-change" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>15.</strong> Always-visible date pickers</h3>
                    <div id="container" style="margin: 10px 0 15px 0; height: 255px; position: relative"></div>

                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-always-visible" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>16.</strong> RTL support</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-rtl-support" type="text" class="form-control" data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <!-- =========================================================================================== -->

                    <h3><strong>17.</strong> Data attributes</h3>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker-data-attributes" type="text" class="form-control" data-zdp_direction="1" data-zdp_disabled_dates='["* * * 0,6"]' data-zdp_readonly_element="false">
                            </div>
                        </div>
                    </div>

                    <br>
                </div>
            </div>
        </div>
    </body>
</html>


