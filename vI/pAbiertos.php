<?php 
    session_start();
    date_default_timezone_set("America/Mexico_City");
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
        $depto = $_SESSION['depto'];    
        //$depto = 'l638';    
    } else {
        $userName = '';
        $name = '';
        $typeUser = 0;
        echo '<script>location.href = "../index.php";</script>';
    }    
?>
<html lang="es">
<head>
    <title>Administradores</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!--<script src="../js/main.js"></script>--> 
    <script src="../js/iOffice.js"></script>
    
    <?php 
        include './modals/informativos/help.php';
        
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MODAL INFORMATIVOS
        include './modals/accesos/login.php';
        include './modals/accesos/cerrarSesion.php';
        include './modals/informativos/rules.php';  
        include './modals/informativos/NoneAuditoria.php';
        include './modals/informativos/InfoAuditoria.php';
        include './modals/informativos/TipoAuditoria.php';
        
        include './modals/auditoria/maintenance/mSelect.php';
        include './modals/auditoria/maintenance/mOrder.php';
        include './modals/auditoria/maintenance/mOrder2.php';
        include './modals/auditoria/maintenance/mOrder3.php';
        include './modals/auditoria/maintenance/mClean.php';
        include './modals/auditoria/maintenance/mClean2.php';
        include './modals/auditoria/maintenance/mStandar.php';
        include './modals/auditoria/maintenance/mStandar2.php';   
        include './modals/auditoria/maintenance/mDisc.php';
        
        include './modals/auditoria/office/mSelect.php';
        include './modals/auditoria/office/mOrder.php';                
        include './modals/auditoria/office/mClean.php';                
        include './modals/auditoria/office/mStandar.php';   
        include './modals/auditoria/office/mDisc.php';
        
        //DEACUERDO AL USUARIO VAMOS A PODER HACER LA AUDITORIA Y SE VAN ABRIR LOS MODAL        
        include './modals/auditoria/line/mSelect.php';
        include './modals/auditoria/line/mOrder.php';
        include './modals/auditoria/line/mOrder2.php';
        include './modals/auditoria/line/mOrder3.php';
        include './modals/auditoria/line/mClean.php';
        include './modals/auditoria/line/mClean2.php';
        include './modals/auditoria/line/mStandar.php';
        include './modals/auditoria/line/mStandar2.php';   
        include './modals/auditoria/line/mDisc.php';
        
        //MODALS PARA OPL
        include './modals/opl/mUOpl.php';
        
        //INCLUIMOS LA CONEXION AL ARCHIVO DE LAS CONSULTAS
        include '/db/funciones.php';       
        include './imagenes/opl/';        
        
        //LLAMAMOS LAS CONSLTAS A LA BASE DE DATOS 
        $f = date('Y-m-d');
        $nuevafecha = date ( 'm/d/Y' , strtotime('-30 day',strtotime($f)));
        $anio = date("Y"); 
        
        $cOplAbiertos = cOPLAbiertosArea($depto, $nuevafecha);      
        
    ?>
    
</head>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
           <div class="logo full-reset all-tittles">                
               <img src="../assets/img/icono.png" style="width: 100%">
<!--               <img src="../files/imagenes/opl/opl0.png" style="width: 100%">-->
            </div>
            
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">                    
                    <li>
                        <a href="index.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a>
                    </li>                    
                    <?php if ($typeUser == 1) { ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-case zmdi-hc-fw"></i>&nbsp;&nbsp; Administración <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="aUAdmin.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp; Usuarios</a></li>
                            <li><a href="aAAdmin.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Línea / Área</a></li>
                            <li><a href="aAuditoria.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de Auditoría </a></li>
                            <li><a href="aCVAdmin.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Cadenas de valor </a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php if ($typeUser != 0) { ?>
                    <li class="btn-new-audit" data-placement="bottom">
                        <a> <i class="zmdi zmdi-assignment-o zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Auditoria </a>
                    </li>
                    <?php } ?>
                    <li>
                        <!--<a href="report.html"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>-->   
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-trending-up  zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="rGeneral.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; General</a></li>
                            <li><a href="rArea.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Por área</a></li>
                        </ul>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-calendar zmdi-hc-fw"></i>&nbsp;&nbsp; Calendario <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li>
                                <a href="cGeneral.php"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i>&nbsp;&nbsp; Vista general </a>
                            </li>
                            <li>
                                <a href="cArea.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de auditoría </a>
                            </li>
                        </ul>
                    </li> 
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-info zmdi-hc-fw"></i>&nbsp;&nbsp; Información general <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="iFiles.php"><i class="zmdi zmdi-folder zmdi-hc-fw"></i>&nbsp;&nbsp; Información general </a></li>
                            <li>
                                <a href="iLayout.php"><i class="zmdi zmdi-grid zmdi-hc-fw"></i>&nbsp;&nbsp; Layouts </a>
                            </li>
                        </ul>
                    </li>
                    <?php if ($typeUser != 0) { ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-assignment-check zmdi-hc-fw"></i>&nbsp;&nbsp; OPL <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="opl/pCerrados.php"><i class="zmdi zmdi-alert-polygon zmdi-hc-fw"></i>&nbsp;&nbsp; Puntos abiertos </a></li>
                            <li><a href="opl/pAbiertos.php"><i class="zmdi zmdi-check-all zmdi-hc-fw"></i>&nbsp;&nbsp; Puntos Cerrados </a></li>
                            <li><a href="opl/pListado.php"><i class="zmdi zmdi-view-list zmdi-hc-fw"></i>&nbsp;&nbsp; Todos </a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">                
                <?php if ($typeUser != 0){ ?>
                    <li style="color:#fff; cursor:default;">
                        <span class="all-tittles"> <?php echo $name ?></span>
                    </li>
                <?php } else { ?>
                    <li  class="tooltips-general btn-login" data-placement="bottom" >
                        <span class="all-tittles">Inicar Sesion</span>
                    </li>  
                <?php } ?>
                
                <?php if ($typeUser != 0) { ?>
                    <li  class="tooltips-general exit-system-button"   data-placement="bottom" title="Salir del sistema">
                        <i class="zmdi zmdi-power"></i>
                    </li>
                <?php } ?>
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles"><small>OPL </small></h1>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">
                <li role="presentation" class="active"><a href="pAbiertos.php">Puntos Abiertos </a></li>
                <li role="presentation"><a href="pCerrados.php">Puntos Cerrados</a></li>
                <li role="presentation"><a href="pListado.php">Todos</a></li>
            </ul>
        </div>        
        <!--APARTADO DE TABLA DE LISTADO-->
        <div class="container-fluid" style="height: 100% ">
            <div class="div-table" >
                <table class="table table-striped table-bordered table-hover" >
                    <thead style="text-align: center;" >                        
                        <th colspan="2">Fechas</th> 
                        <th rowspan="2">Hallazgo</th>
                        <th rowspan="2">Accion</th>
                        <th rowspan="2">Responsable</th>
                        <th rowspan="2">Soporte</th>
                        <th rowspan="2">Estado</th>
                        <th rowspan="2">Acción</th>
                        <tr>
                            <th>Registro</th>
                            <th>Compromiso</th>                        
                        </tr>
                    </thead>
                    <tbody >
                        <?php
                            for($i = 0; $i < count($cOplAbiertos); $i++) {
                        ?>
                            <tr>
                                <td width = '0%' style = "display:none" ><?php echo $cOplAbiertos[$i][0]; ?></td>
                                <td width = '4%' ><?php echo $cOplAbiertos[$i][1]; ?></td>
                                <td width = '4%' ><?php echo $cOplAbiertos[$i][2]; ?></td>
                                <td width = '20%' ><?php echo $cOplAbiertos[$i][3]; ?></td>
                                <td width = '20%' ><?php echo $cOplAbiertos[$i][4]; ?></td>
                                <td width = '10%' ><?php echo $cOplAbiertos[$i][5]; ?></td>
                                <td width = '10%' ><?php echo $cOplAbiertos[$i][6]; ?></td>
                                <td width = '4%' >                                    
                                    <?php                                     
                                        switch ($cOplAbiertos[$i][7]){
                                            case '0';
                                                ?>
                                                <img src="vI/imagenes/opl/opl0.png" style="width: 3vh">
                                                <?php
                                                break;
                                            case '1';
                                                ?>
                                                <img src="vI/imagenes/opl/opl1.png" style="width: 3vh">
                                                <?php
                                                break;
                                            case '2';
                                                ?>
                                                <img src="vI/imagenes/opl/opl2.png" style="width: 3vh">
                                                <?php
                                                break;
                                            case '3';
                                                ?>
                                                <img src="vI/imagenes/opl/opl3.png" style="width: 3vh" >
                                                <?php
                                                break;                                                                                      
                                        }
                                    ?>
                                </td>
                                <td width='12%'>                        
                                    <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#mUOpl" 
                                            data-hallazgo1 = "<?php echo $cOplAbiertos[$i][0]?>" 
                                            data-hallazgo2 = "<?php echo $cOplAbiertos[$i][1]?>" 
                                            data-hallazgo3 = "<?php echo $cOplAbiertos[$i][2]?>" 
                                            data-hallazgo = "<?php echo $cOplAbiertos[$i][3]?>" 
                                            data-accion = "<?php echo $cOplAbiertos[$i][4]?>" 
                                            data-responsable = "<?php echo $cOplAbiertos[$i][5]?>" 
                                            data-soporte = "<?php echo  $cOplAbiertos[$i][6]?>" 
                                            data-estado = "<?php echo  $cOplAbiertos[$i][7]?>" >
                                        <i class='glyphicon glyphicon-edit'></i> Modificar</button>
                                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#dataDelete" data-id = "<?php echo $cOplAbiertos[$i][0] ?>" ><i class='glyphicon glyphicon-trash'></i> Eliminar</button>
                                </td>
                            </tr>                 
                       <?php 
                            }
                       ?>
                    </tbody>
                </table>              
            </div> 
            
        </div>        
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Areli Pérez Calixto | Auditoría 5Ss | 2018 </p>
                        <p>© SEG Automotive México Service, S. de R. L. de C.V. 2018</p>           
                    </div>
                </div>
            </div>            
        </footer>
    </div>
</body>
</html>