<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<form id="logOut" method="post">
<div class="modal" tabindex="-1" role="dialog" id="ModalLogOut">
    <div class="modal-dialog modal-sm ">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title text-center all-tittles">CERRAR SESION</h4>
            </div>
            <div class="modal-body">
                <div id="datos_ajax"></div> 
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <H2>¿Estás seguro?</H2>    
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Quieres salir del sistema y cerrar la sesión actual</h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" >Aceptar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" >Cancelar</button>
            </div>
        </div>
    </div>
</div>
</form>


