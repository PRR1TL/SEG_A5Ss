<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

?>

<style>
    .no {
        display:none;
    }
    .si {
        display:block;
    }
</style>

<form id="fICadValor">
    <div class="modal fade" id="mICadValor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel" align="center">NUEVA CADENA DE VALOR</h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto; ">
                    <div id="datos_ajaxIU"></div>                                          
                    <div class="form-group">                 
                        <label class="control-label" >Tipo de área: </label>                                                           
                        <select id="depto" name="depto" type ="text" class="form-control" style=" width: 30%">
                            <option value="0" selected disabled> - </option>
                            <option value="1"> ALMACEN</option>
                            <option value="2"> LINEA</option>
                            <option value="3"> LABORATORIO</option>
                            <option value="4"> MANTENIMIENTO</option>
                            <option value="5"> OFICINA</option>
                        </select> 

                        <label for="nombre0" class="control-label" style="margin-left: 39%; margin-top: -8%">Acronimo:</label>
                        <input type="text" class="form-control" id="acronimo" name="acronimo" minlength="5" maxlength="15" style=" width: 55%; margin-left: 38%; margin-top: -3.5%" >
                        
                    </div>                              
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>



