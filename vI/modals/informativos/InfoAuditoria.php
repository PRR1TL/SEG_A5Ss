<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $fInicio = date('d/m/Y');
    $fNow = date('Y-m-d');
    $fProp = date('d/m/Y' ,strtotime('+0 day', strtotime($fNow)));

?>
<form id="fInfoAuditoria" method="post"> 
    <div class="modal fade" tabindex="-1" role="dialog" id="mInfoAuditoria">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">INFO AUDITORIA</h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto; ">
                    <input id="idAuditoria" name="idAuditoria" type="hidden" />
                    <input id="tipoEv" name="tipoEv" type="hidden" />
                    <div class="row " >
                        <div class="col-sm-4" >                        
                            <span ><b> Fecha: </b></span> 
                            <input id="fecha" name="" maxlength="150" style=" width: 59%;" value="<?php echo $fProp?>" type="text" readonly />
                        </div>
                        <div class="col-sm-4" >
                            <span ><b> Area/Linea: </b></span>
                            <input id="areaAuditoria" name="areaAuditoria" maxlength="150" style=" width: 50%;" placeholder="areaAuditoria" readonly />
                        </div>
                        <div class="col-sm-4" >
                            <span ><b> Tipo: </b></span>
                            <input id="tipo" name="tipo" maxlength="150" style=" width: 60%;" placeholder="tipo" readonly />
                        </div>
                        <br>                    
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12" >    
                            <span ><b> Auditado: </b></span>    
                            <input id="auditado" name="auditado" maxlength="175" style=" width: 85%;" value="" type="text" readonly= />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-infAuditoria btn-success" > INICIAR &nbsp;</button>
                </div>
            </div>
        </div>
    </div>
</form>






