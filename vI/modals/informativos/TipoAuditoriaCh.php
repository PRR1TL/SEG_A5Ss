<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<form id="fTipoAuditoriaCh" method="post"> 
    <div class="modal fade" tabindex="-1" role="dialog" id="mTipoAuditoriaCh">
        <div class="modal-dialog modal-xsm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">TIPO AUDITORIA</h4>
                </div>
                <div class="modal-body" >
                    <div class="row " >
                        <div class="col-sm-12"  >
                            <span ><b> Tienes asignada y/o pendiente más de una Auditoria.  </b></span> 
                            <span ><b><br> Selecciona la que desees realizar.  <br></b></span> 
                            <!--<input id="responsable17" name="responsable17" maxlength="150" style=" width: 45%;" type="text" />-->
                        </div>
                        <br><br><br>
                        <div id="a12" name="a12" class="col-sm-12" align="center">
                            <span ><b> Opcion: </b></span>
                            <select id="cmbTipoAuditoria" name="cmbTipoAuditoria" >
                                <option value="1">Checklist</option>
                                <option value="3">Mensual</option>
                            </select>                        
                        </div>                                       
                        <br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-tipoAuditoria btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"></i></button>
                </div>
            </div>
        </div>
    </div>
</form>
