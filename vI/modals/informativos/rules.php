<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="modal fade" tabindex="-1" role="dialog" id="ModalRules">
    <div class="modal-dialog modal-xsm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center all-tittles">Instrucciones</h4>
            </div>
            <div class="modal-body">
                Antes de iniciar la auditoría: 
                <p>Lee cuidadosamente las preguntas en cada sección <br>
                La sección la podrás encontrar en la parte superiror de cada venta</p>
                <br>
                <h5 style="text-transform: uppercase; font-weight: bold"> EVALUACION </h5>
                    0 = Existen más de 7 desviaciones / No se cumple <br>	
                    1 = De 6 a 7 desviaciones <br>	
                    2 = De 4 a 5 desviaciones <br>	
                    3 = De 1 a 3 desviaciones <br>	
                    4 = No existe desviacion. Se cumple completamente
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-rules btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"></i></button>
            </div>
        </div>
    </div>
</div>