<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//PARA FECHAS EN TIEMPO REAL
    $fInicio = date('d/m/Y');
    $fNow = date('Y-m-d');
    $fProp = date('d/m/Y' ,strtotime('+1 day', strtotime($fNow)));
?>

<script>
    
    //VALIDACIONES PARA EL FORMATO DEL INPUT
    function IsNumeric(valor){ 
        var log=valor.length; 
        var sw="S"; 
        
        for (x=0; x<log; x++){ 
            v1=valor.substr(x,1); 
            v2 = parseInt(v1); 
            //Compruebo si es un valor numérico 
            if (isNaN(v2)) { 
                sw = "N";
            } 
            console.log(sw);
        } 
        
        if (sw=="S") {
            return true;
        } else {
            return false; 
        } 
    } 
    
    var fNow = new Date().toISOString().slice(0,10);    
    var fEntra;
    
    var primerslap=false; 
    var segundoslap=false; 
    function formateafecha(fecha) { 
        var long = fecha.length; 
        //console.log(long);
        var dia; 
        var mes; 
        var ano; 
        if ((long>=2) && (primerslap==false)) { 
            dia=fecha.substr(0,2); 
            if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { 
                fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); 
                primerslap=true; 
            } else { 
                fecha=""; 
                primerslap=false;
            } 
        } else { 
            dia = fecha.substr(0,1); 
            if (IsNumeric(dia)==false){ 
                fecha="";
            } 
            if ((long<=2) && (primerslap=true)) {
                fecha=fecha.substr(0,1); 
                primerslap=false; 
            } 
        } 
        
        if ((long>=5) && (segundoslap==false)) { 
            mes = fecha.substr(3,2); 
            if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { 
                fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); 
                segundoslap=true; 
            } else { 
                fecha=fecha.substr(0,3); 
                segundoslap=false;
            } 
        } else { 
            if ((long <= 5) && (segundoslap = true)) { 
                fecha=fecha.substr(0,4); 
                segundoslap=false; 
            } 
        } 
        
        if (long >= 7) { 
            ano=fecha.substr(6,4); 
            if (IsNumeric(ano)==false) { 
                fecha=fecha.substr(0,6); 
            } else { 
                if (long==10) { 
                    if ((ano==0) || (ano<1900) || (ano>2100)) { 
                        fecha=fecha.substr(0,6); 
                    } 
                } 
            } 
        } 
        
        if (long >= 10) {        
            fecha=fecha.substr(0,10); 
            dia=fecha.substr(0,2); 
            mes=fecha.substr(3,2); 
            ano=fecha.substr(6,4); 
            
            fEntra = ano+'-'+mes+'-'+dia;
            //console.log(fEntra);
            
            if (fEntra > fNow){
                console.log("FECHA ENTRADA ES MAYOR A FECHA ACTUAL :) ");
            } else {
                console.log("FECHA ACTUAL ES MAYOR A FECHA ENRADA :( ");
            }
            // Año no viciesto y es febrero y el dia es mayor a 28 
            if ( (ano%4 != 0) && (mes == 02) && (dia > 28) ) { 
                fecha=fecha.substr(0,2)+"/"; 
            } 
        } 
        return (fecha); 
    }
    
    function cpM11() {
        $('#hallazgo11').val("");
        $('#accion11').val("");
        $('#responsable11').val("");
        $('#soporte11').val("");
        var x = document.getElementById("pM11").value;
        if (x <= 2 ){
            pnlOplM11.className = 'si';
        } else {             
            pnlOplM11.className = 'no';
        }
    }
    
    function cpM12() {
        $('#hallazgo12').val("");
        $('#accion12').val("");
        $('#responsable12').val("");
        $('#soporte12').val("");
        var x = document.getElementById("pM12").value;
        if (x <= 2 ){
            pnlOplM12.className = 'si';
        } else {             
            pnlOplM12.className = 'no';
        }
    }
    
</script>

<form id="fLClean2M" method="post"> 
    <div class="modal fade" tabindex="-1" role="dialog" id="mLClean2M">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title text-center all-tittles">LIMPIAR (MANTENIMIENTO)</h4>
                    <h6 class="modal-title text-left all-tittles" style="top: -15px">*Evaluación: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0 = No se cumple &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        1 = De 6 a 7 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        2 = De 4 a 5 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        3 = De 1 a 3 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        4 = Se cumple
                    </h6>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto; ">
                    <div id="datos_ajaxCLM2"></div>
                    <div class="row">
                        <div class="col-sm-11">
                            <label>11. Los productos y materiales de limpieza están ubicados en un área de fácil acceso y se encuentran disponibles.<a style="font-size: 9px" data-toggle="modal" href="#iM11">INFO</a></label>
                            <br>
                        </div>
                        <div class="col-sm-1">
                            <select id="pM11" name="p11" onchange="cpM11()">
                                <option value="5" selected disabled > - </option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="panel no" id="pnlOplM11" style = "margin-top: 5vh">
                            <div class="panel panel-info" style="width: 95%; margin-left: 3%">
                                <div class="panel-heading">Opl 11</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" >
                                        <div class="btn-group" role="group">
                                            <span ><b> Hallazgo: </b></span> 
                                            <input id="hallazgo11" name="hallazgo11" maxlength="150" style="width: 84%;" placeholder="Hallazgo" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Acción: </b></span>
                                            <input id="accion11" name="accion11" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                        </div>                                        
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px">
                                        <div class="btn-group" role="group">
                                            <span ><b> Responsable: </b></span> 
                                            <input id="responsable11" name="responsable11" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Soporte: </b></span>
                                            <input id="soporte11" name="soporte11" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                        </div>                                        
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Inicio: </b></span>    
                                            <input id="fInicio11" name="fInicio11" maxlength="171" style=" width: 45%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Compromiso: </b></span>
                                            <input id="fCompromiso11" name="fCompromiso11" maxlength="150" style=" width: 45%;" value="<?php echo $fProp?>" onKeyUp = "this.value=formateafecha(this.value);" />
                                        </div>                                        
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>               
                    <div class="row">
                        <div class="col-sm-11">
                            <label>12. La identificación de materiales (piso, etiquetas, marbetes) están limpios y en buenas condiciones.<a style="font-size: 9px" data-toggle="modal" href="#iM12">INFO</a></label>
                            <br>
                        </div>
                        <div class="col-sm-1">
                            <select id="pM12" name="p12" onchange="cpM12()">
                                <option value="5" selected disabled > - </option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="panel no" id="pnlOplM12" style = "margin-top: 5vh">
                            <div class="panel panel-info" style="width: 95%; margin-left: 3%">
                                <div class="panel-heading">Opl 12</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" >
                                        <div class="btn-group" role="group">
                                            <span ><b> Hallazgo: </b></span> 
                                            <input id="hallazgo12" name="hallazgo12" maxlength="150" style="width: 84%;" placeholder="Hallazgo" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Acción: </b></span>
                                            <input id="accion12" name="accion12" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                        </div>                                        
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px">
                                        <div class="btn-group" role="group">
                                            <span ><b> Responsable: </b></span> 
                                            <input id="responsable12" name="responsable12" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Soporte: </b></span>
                                            <input id="soporte12" name="soporte12" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                        </div>                                        
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Inicio: </b></span>    
                                            <input id="fInicio12" name="fInicio12" maxlength="171" style=" width: 45%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Compromiso: </b></span>
                                            <input id="fCompromiso12" name="fCompromiso12" maxlength="150" style=" width: 45%;" value="<?php echo $fProp?>" onKeyUp = "this.value=formateafecha(this.value);" />
                                        </div>                                        
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>
    
<!--APARTADO PARA MODALS INFORMATIVOS-->
<div class="modal fade" tabindex="-1" id="iM11" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Punto a Revisar</h4>
            </div>
            <div class="modal-body">
                Verificar que el estante de material de limpieza se encuentre el material en buen estado: escobas, trapos, limpiadores, trapeadores.
            </div>            
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="iM12" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Punto a Revisar</h4>
            </div>
            <div class="modal-body">
                Etiquetas, cintas y marbetes conforme al estándar y en buena condición.
            </div>            
        </div>
    </div>
</div>
