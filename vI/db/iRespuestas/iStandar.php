<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    $typeEv = $_SESSION["tipoEv"];    

    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];
    $fInicio = date('Y-m-d');

    //SEGUNDA
    if (isset($_POST["p13"])){
	$p13 = $_POST["p13"];
        $hallazgo13 = $_POST["hallazgo13"];
        $accion13 = $_POST["accion13"];
        $responsable13 = $_POST["responsable13"];
        $soporte13 = $_POST["soporte13"];
        $fCompromiso13 = $_POST["fCompromiso13"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 13";
    }
    
    if (isset($_POST["p14"])){
	$p14 = $_POST["p14"];
        $hallazgo14 = $_POST["hallazgo14"];
        $accion14 = $_POST["accion14"];
        $responsable14 = $_POST["responsable14"];
        $soporte14 = $_POST["soporte14"];
        $fCompromiso14 = $_POST["fCompromiso14"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 14";
    }    

    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);
    
    //INSERT OPL
    if (( isset($hallazgo13) && isset($accion13) && isset($responsable13) && isset($soporte13) && isset($fCompromiso13)) && 
       ( !empty($hallazgo13) && !empty($accion13) && !empty($responsable13) && !empty($soporte13) && !empty($fCompromiso13))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso13)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 13)";
        }          
    } else {
        if(!empty($fCompromiso13)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso13)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 13)";
            }
            if ($p13 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 13 ";
            } else
            if(!empty($hallazgo13) || !empty($accion13) || !empty($responsable13) || !empty($soporte13)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(13) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }

    if (( isset($hallazgo14) && isset($accion14) && isset($responsable14) && isset($soporte14) && isset($fCompromiso14)) && 
       ( !empty($hallazgo14) && !empty($accion14) && !empty($responsable14) && !empty($soporte14) && !empty($fCompromiso14))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso13)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 13)";
        }          
    } else {
        if(!empty($fCompromiso14)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso14)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 14)";
            }
            if ($p14 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 14 ";
            } else
            if(!empty($hallazgo14) || !empty($accion14) || !empty($responsable14) || !empty($soporte14)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(14) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }

if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else {    

    //INSERT EN BASE DE DATOS
    $queryIPuntos = "UPDATE a_puntosEvaluacion SET p13 = '$p13', p14 = '$p14' WHERE id = '$id' ";
    $resultIPuntos = sqlsrv_query($conn, $queryIPuntos);
    
    if ((isset($hallazgo13) && isset($accion13) && isset($responsable13) && isset($soporte13) && isset($fCompromiso13)) && 
       ( !empty($hallazgo13) && !empty($accion13) && !empty($responsable13) && !empty($soporte13) && !empty($fCompromiso13))){
        $queryIOpl13 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso13', NULL, '$hallazgo13', '$accion13', '$responsable13', '$soporte13', 1);";
        $resultIOp13 = sqlsrv_query($conn,$queryIOpl13);   
    }

    if ((isset($hallazgo14) && isset($accion14) && isset($responsable14) && isset($soporte14) && isset($fCompromiso14)) && 
       ( !empty($hallazgo14) && !empty($accion14) && !empty($responsable14) && !empty($soporte14) && !empty($fCompromiso14))){
        $queryIOpl14 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso14', NULL, '$hallazgo14', '$accion14', '$responsable14', '$soporte14', 1);";
        $resultIOpl14 = sqlsrv_query($conn,$queryIOpl14); 
    }
    
    echo "Bien";
     
}




