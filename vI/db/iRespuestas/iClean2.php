<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    session_start();
    $typeEv = $_SESSION["tipoEv"];   

    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];
    $fInicio = date('Y-m-d');

    //SEGUNDA
    if (isset($_POST["p11"])){
	$p11 = $_POST["p11"];
        $hallazgo11 = $_POST["hallazgo11"];
        $accion11 = $_POST["accion11"];
        $responsable11 = $_POST["responsable11"];
        $soporte11 = $_POST["soporte11"];
        $fCompromiso11 = $_POST["fCompromiso11"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 11";
    }    

    if (isset($_POST["p12"])){
	$p12 = $_POST["p12"];
        $hallazgo12 = $_POST["hallazgo12"];
        $accion12 = $_POST["accion12"];
        $responsable12 = $_POST["responsable12"];
        $soporte12 = $_POST["soporte12"];
        $fCompromiso12 = $_POST["fCompromiso12"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 12";
    }
    

    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    //INSERT OPL
    if (( isset($hallazgo11) && isset($accion11) && isset($responsable11) && isset($soporte11) && isset($fCompromiso11)) && 
       ( !empty($hallazgo11) && !empty($accion11) && !empty($responsable11) && !empty($soporte11) && !empty($fCompromiso11))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso11)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 11)";
        }          
    } else {
        if(!empty($fCompromiso11)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso11)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 11)";
            }
            if ($p11 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 11 ";
            } else
            if(!empty($hallazgo11) || !empty($accion11) || !empty($responsable11) || !empty($soporte11)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(11) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }   

    if (( isset($hallazgo12) && isset($accion12) && isset($responsable12) && isset($soporte12) && isset($fCompromiso12)) && 
       ( !empty($hallazgo12) && !empty($accion12) && !empty($responsable12) && !empty($soporte12) && !empty($fCompromiso12))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso12)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 12)";
        }          
    } else {
        if(!empty($fCompromiso12)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso12)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 12)";
            }
            if ($p12 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 12 ";
            } else
            if(!empty($hallazgo12) || !empty($accion12) || !empty($responsable12) || !empty($soporte12)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(12) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }

if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else {    
    
    //INSERT EN BASE DE DATOS
    $queryIPuntos = "UPDATE a_puntosEvaluacion SET p11 = '$p11', p12 = '$p12' WHERE id = '$id' ";
    $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 
    
    if (( isset($hallazgo11) && isset($accion11) && isset($responsable11) && isset($soporte11) && isset($fCompromiso11)) && 
       ( !empty($hallazgo11) && !empty($accion11) && !empty($responsable11) && !empty($soporte11) && !empty($fCompromiso11))){
        $queryIOpl11 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso11', NULL, '$hallazgo11', '$accion11', '$responsable11', '$soporte11', 1);";
        $resultIOp11 = sqlsrv_query($conn,$queryIOpl11);   
    }

    if (( isset($hallazgo12) && isset($accion12) && isset($responsable12) && isset($soporte12) && isset($fCompromiso12)) && 
       ( !empty($hallazgo12) && !empty($accion12) && !empty($responsable12) && !empty($soporte12) && !empty($fCompromiso12))){
        $queryIOpl12 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso12', NULL, '$hallazgo12', '$accion12', '$responsable12', '$soporte12', 1);";
        $resultIOpl12 = sqlsrv_query($conn,$queryIOpl12); 
    }
    
    echo "Bien";
}













