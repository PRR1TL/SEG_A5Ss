<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    $typeEv = $_SESSION["tipoEv"];   

    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];
    $fInicio = date('Y-m-d');

    //SEGUNDA
    if (isset($_POST["p8"])){
        $p8 = $_POST["p8"];
        $hallazgo8 = $_POST["hallazgo8"];
        $accion8 = $_POST["accion8"];
        $responsable8 = $_POST["responsable8"];
        $soporte8 = $_POST["soporte8"];
        $fCompromiso8 = $_POST["fCompromiso8"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 8";
    }    

    if ($typeEv == 5 ){
        if (isset($_POST["p9"])){
            $p9 = $_POST["p9"];
            $hallazgo9 = $_POST["hallazgo9"];
            $accion9 = $_POST["accion9"];
            $responsable9 = $_POST["responsable9"];
            $soporte9 = $_POST["soporte9"];
            $fCompromiso9 = $_POST["fCompromiso9"];
        } else {
            $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 9";
        }        
    } else {
        //PRIMER
        if (isset($_POST["p7"])){
            $p7 = $_POST["p7"];
            $hallazgo7 = $_POST["hallazgo7"];
            $accion7 = $_POST["accion7"];
            $responsable7 = $_POST["responsable7"];
            $soporte7 = $_POST["soporte7"];
            $fCompromiso7 = $_POST["fCompromiso7"];
        } else {
            $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 7";
        }        
    }

    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    //INSERT EN BASE DE DATOS
    //echo $typeEv;
    if ($typeEv != 5 ){        

        //INSERT OPL
        if (( isset($hallazgo7) && isset($accion7) && isset($responsable7) && isset($soporte7) && isset($fCompromiso7)) && 
             ( !empty($hallazgo7) && !empty($accion7) && !empty($responsable7) && !empty($soporte7) && !empty($fCompromiso7))){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso7)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 7)";
            }          
        } else {
            if(!empty($fCompromiso7)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso7)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p7 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 7 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 7)";
                }
                if(!empty($hallazgo7) || !empty($accion7) || !empty($responsable7) || !empty($soporte7)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(7) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }
        
        if (( isset($hallazgo8) && isset($accion8) && isset($responsable8) && isset($soporte8) && isset($fCompromiso8)) && 
            ( !empty($hallazgo8) && !empty($accion8) && !empty($responsable8) && !empty($soporte8) && !empty($fCompromiso8))){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso8)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 8)";
            }          
        } else {
            if(!empty($fCompromiso8)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso8)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p8 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 8 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 8)";
                }
                if(!empty($hallazgo8) || !empty($accion8) || !empty($responsable8) || !empty($soporte8)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(8) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }        
    } else {  
        //INSERT OPL
        if (( isset($hallazgo8) && isset($accion8) && isset($responsable8) && isset($soporte8) && isset($fCompromiso8)) && 
            ( !empty($hallazgo8) && !empty($accion8) && !empty($responsable8) && !empty($soporte8) && !empty($fCompromiso8))){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso8)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 8)";
            }          
        } else {
            if(!empty($fCompromiso8)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso8)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p8 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 8 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 8)";
                }
                if(!empty($hallazgo8) || !empty($accion8) || !empty($responsable8) || !empty($soporte8)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(8) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }

        if (( isset($hallazgo9) && isset($accion9) && isset($responsable9) && isset($soporte9) && isset($fCompromiso9)) && 
            ( !empty($hallazgo9) && !empty($accion9) && !empty($responsable9) && !empty($soporte9) && !empty($fCompromiso9))){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso9)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 9)";
            }          
        } else {
            if(!empty($fCompromiso9)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso9)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p9 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 9 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 9)";
                }
                if(!empty($hallazgo9) || !empty($accion9) || !empty($responsable9) || !empty($soporte9)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(9) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }
    }

//    MODULO PARA IMPRIMIR ERRORES
if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else {  
    
    if ($typeEv != 5 ){
        
        $queryIPuntos = "UPDATE a_puntosEvaluacion SET p7 = '$p7', p8 = '$p8' WHERE id = '$id' ";
        $resultIPuntos = sqlsrv_query($conn, $queryIPuntos);  

        //INSERT OPL
        if (( isset($hallazgo7) && isset($accion7) && isset($responsable7) && isset($soporte7) && isset($fCompromiso7)) && 
             (!empty($hallazgo7) && !empty($accion7) && !empty($responsable7) && !empty($soporte7) && !empty($fCompromiso7))){
            
            $queryIOpl7 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso7', NULL, '$hallazgo7', '$accion7', '$responsable7', '$soporte7', 1);";
            $resultIOpl7 = sqlsrv_query($conn, $queryIOpl7);   
        }

        if (( isset($hallazgo8) && isset($accion8) && isset($responsable8) && isset($soporte8) && isset($fCompromiso8)) && 
             (!empty($hallazgo8) && !empty($accion8) && !empty($responsable8) && !empty($soporte8) && !empty($fCompromiso8))){
            $queryIOpl8 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso8', NULL, '$hallazgo8', '$accion8', '$responsable8', '$soporte8', 1);";
            $resultIOpl8 = sqlsrv_query($conn,$queryIOpl8);   
        }
    } else {
        $queryIPuntos = "UPDATE a_puntosEvaluacion SET p8 = '$p8', p9 = '$p9' WHERE id = '$id' ";
        $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 

        //INSERT OPL
        if ((isset($hallazgo8) && isset($accion8) && isset($responsable8) && isset($soporte8) && isset($fCompromiso8)) && 
             ( !empty($hallazgo8) && !empty($accion8) && !empty($responsable8) && !empty($soporte8) && !empty($fCompromiso8))){
            $queryIOpl8 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso8', NULL, '$hallazgo8', '$accion8', '$responsable8', '$soporte8', 1);";
            $resultIOpl8 = sqlsrv_query($conn,$queryIOpl8);   
        }

        if (( isset($hallazgo9) && isset($accion9) && isset($responsable9) && isset($soporte9) && isset($fCompromiso9)) && 
             (!empty($hallazgo9) && !empty($accion9) && !empty($responsable9) && !empty($soporte9) && !empty($fCompromiso9))){
            $queryIOpl9 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso9', NULL, '$hallazgo9', '$accion9', '$responsable9', '$soporte9', 1);";
            $resultIOpl9 = sqlsrv_query($conn,$queryIOpl9); 
        }   
    }
    echo "Bien";
}








