<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    include '../../db/funciones.php';

    $tipoEv = 0;
    if (isset($_REQUEST["area"]) && !empty($_REQUEST["area"])){
        if($_REQUEST["area"] == 2){
            $tipoEv = 5;
        } 
    } else {        
        $errors []= "SE DEBE SELECCIONAR UNA AREA";
    }
    
    if (isset($_REQUEST["acronimo"]) && !empty($_REQUEST["acronimo"])){
        $depto = $_REQUEST["acronimo"];
    } else {
        $errors []= "SE DEBE INGRESAR UN ACRONIMO";
    }
    
    if (isset($_REQUEST["descripcion"]) && !empty($_REQUEST["descripcion"])){
        $descripcion = $_REQUEST["descripcion"];
    } else {
        $errors []= "SE DEBE INGRESAR UNA DESCRIPCION";
    }
    
    if (isset($_REQUEST["cadValor"]) && !empty($_REQUEST["cadValor"])){        
        if ($_REQUEST["cadValor"] == "N/A"){
            $cadValor = $_REQUEST["acronimo"];
            if ($tipoEv != 5){
                if (isset($_REQUEST["depto"]) && !empty($_REQUEST["depto"])){        
                    $tipoEv = $_REQUEST["depto"];                    
                } else {
                    $errors []= "SE DEBE SELECCIONAR UNA OPCION DE TIPO DE AREA";
                }
            }           
        } else {
            $cadValor = $_REQUEST["cadValor"];
            $tEv = cTipoEv_CadValor($cadValor);
            for ($i = 0; $i < count($tEv); $i++){
                $tipoEv = $tEv[$i][0];
            }            
        }
    } else {
        $errors []= "SE DEBE SELECCIONAR UNA OPCION DE CADENA DE VALOR";
    }
        
    if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else { 
    
     iDepto($tipoEv, $depto, $descripcion, $cadValor);
    
?>
        <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>
            <?php 
                echo "REGISTRO GUARDADO CORRECTAMENTE",'<BR>';
            ?> </strong>     
        </div>
<?php
}





