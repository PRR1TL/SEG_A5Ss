<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../funciones.php';
    date_default_timezone_set("America/Mexico_City");  
    
    $fNow = date('Y-m-d');    
    $y = date('Y');
    $m = date('m');
    $d = date('d');
    $nuevafecha = date ( 'm/d/Y' , strtotime('+1 day',strtotime($fNow)));
    $fecha = new DateTime(date($nuevafecha));
    $semana = $fecha->format('W');    
    $bnContador = 0;
    
    # Obtenemos el día de la semana de la fecha dada
    $diaSemana = date("w",mktime(0,0,0,$m,$d,$y));
    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
    $primerDia = date("Y-m-d",mktime(0,0,0,$m,$d-$diaSemana,$y));
    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
    $ultimoDia = date("Y-m-d",mktime(0,0,0,$m,$d+(6-$diaSemana),$y));
   
    session_start();
    $userName = $_SESSION['userName'];
    $dep = $_POST["cmbDeptoAuditoria"];
    
    $dept = str_replace("+", "", $dep);
    $depto = trim($dept);
    //$depto = 'ICO';
    
    $auditoriasUsuario = cAuditoriaSUPandADMIN($userName);
    for ($i = 0; $i < count($auditoriasUsuario); $i++){
        $id = $auditoriasUsuario[$i][0];
    }          
    
    $cUsuario = cUsuarioAuditarSup_Admin ($depto);
    for($i = 0; $i < count($cUsuario); $i++){
        $auditado = $cUsuario[$i][0];
        $nombreAuditado = $cUsuario[$i][1];
    } 
    
    uAuditoriaSup_Admin($primerDia, $ultimoDia, $y, $m, $semana, $depto, $auditado, $id);    
    
    //CONSULTA PARA LA INFORMACION QUE VA IR AL MODAL
    $cAuditoriaId = cAuditoriaId($id);
    for($i = 0; $i < count($cAuditoriaId); $i++){
        $uAditado = $cAuditoriaId[$i][0];
        $tipoEv = $cAuditoriaId[$i][1];
    }
    
    echo $id,',',$depto,',',$uAditado,',',3,',',$tipoEv;
    