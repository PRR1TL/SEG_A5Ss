<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'conexion.php';
//Devuelve un array multidimensional con el resultado de la consulta
function getArraySQL($sql) {
    $connectionObj = new ServerConfig();
    $connectionStr = $connectionObj -> serverConnection();
    
    if (!$result = sqlsrv_query($connectionStr, $sql)) die();
    $rawdata = array();
    $i = 0;
    while ($row = sqlsrv_fetch_array($result)) {
        $rawdata[$i] = $row;
        $i++;
    }
    $connectionObj ->serverDisconnect();
    return $rawdata;
}

//CONSULTA PARA LOGIN
function cUsuarioContrasena($usuario, $contrasena){
    $sql = "SELECT usuario, nombre, privilegio, estado, tipoEv, depto FROM a_usuarios WHERE usuario = '$usuario' AND contrasenia = '$contrasena'";
    return getArraySQL($sql);
}

//CONSULTA PARA USUARIOS
function cUAdministradores(){
    $sql = "SELECT depto, tipoEv, usuario, nombre FROM a_usuarios WHERE privilegio = 1";
    return getArraySQL($sql);
}

//EVENTOS SEMANALES POR USUARIO 
function cEvenSemanalUsuario($usuario, $fIni, $fFin){
    $sql = "SELECT id, nWeek, depAuditado, auditado, auditor, tipoEvaluacion FROM a_progAuditoria WHERE auditor = '$usuario' AND fIni = '$fIni' AND fFin = '$fFin' AND estado = 0";
    return getArraySQL($sql);
}

//EVENTOS MENSUALES POR USUARIO
function cEvenMensualUsuario($usuario, $fIni, $fFin){
    $sql = "SELECT id, nWeek, depAuditado, auditado, auditor, tipoEvaluacion FROM a_progAuditoria WHERE auditor = '$usuario' AND fIni > '$fIni' AND fFin = '$fFin' AND estado = 0 ";
    return getArraySQL($sql);
}

//AUDITORIAS
function cUAuditoriaChamp(){
    $sql = "SELECT depto,usuario, nombre, tipoEv FROM a_usuarios WHERE privilegio = 3";
    return getArraySQL($sql);
}

function cUAuditoriaChampPiso(){
    $sql = "SELECT depto,usuario, nombre, tipoEv FROM a_usuarios WHERE privilegio = 3 AND tipoEv BETWEEN 1 AND 4";
    return getArraySQL($sql);
}

function cUAuditoriaChampOficina(){
    $sql = "SELECT depto,usuario, nombre, tipoEv FROM a_usuarios WHERE privilegio = 3 AND tipoEv = 5 ";
    return getArraySQL($sql);
}

function cUAuditoriaSupPiso(){
    $sql = "SELECT depto,usuario, nombre, tipoEv FROM a_usuarios WHERE privilegio = 2 AND tipoEv BETWEEN 1 AND 4 ";
    return getArraySQL($sql);
}

function cUAuditoriaSupOficina(){
    $sql = "SELECT depto,usuario, nombre, tipoEv FROM a_usuarios WHERE privilegio = 2 AND tipoEv = 5";
    return getArraySQL($sql);
}

//CONSULTA PARA LAS AREAS
function cDepartamentosPiso(){
    $sql = "SELECT * FROM a_departamento WHERE tipoEv BETWEEN 1 AND 4 ORDER BY tipoEv";
    return getArraySQL($sql);
}

function cDepartamentosOficina(){
    $sql = "SELECT * FROM a_departamento WHERE tipoEv = 5 ";
    return getArraySQL($sql);
}

//CONSULTAS PARA LAS AUDITORIAS (ADMINISTRADOR)
function cAuditoriaAdmin($semana,$anio){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au where au.usuario = pa.auditor AND nWeek >= '$semana' AND anio >= '$anio' ORDER BY pa.depAuditado, pa.tipoAuditoria";
    return getArraySQL($sql);
}

function cAuditoriaId($id){
    $sql = "SELECT us.nombre, us.tipoEv FROM a_progAuditoria as prog, a_usuarios as us WHERE prog.id = '$id' AND prog.auditado = us.usuario";
    return getArraySQL($sql);
}

//CONSULTA DE AUDITORIAS
function cAuditoriasProgramadasPiso($semana,$anio){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), concat(us.nombre,' (',us.depto,')'), concat(au.nombre,' (',au.depto,')' ), pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au, a_usuarios as us WHERE au.usuario = pa.auditado AND us.usuario = pa.auditor AND pa.nWeek >= '$semana' AND anio >= '$anio' AND pa.tipoEvaluacion BETWEEN 1 AND 4 AND pa.tipoAuditoria >= 2 order by us.depto";
    return getArraySQL($sql);
}

function cAuditoriasProgramadasOficina($semana,$anio){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), us.depto, us.nombre , concat(au.nombre,' (',au.depto,')' ), pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au, a_usuarios as us WHERE au.usuario = pa.auditado AND us.usuario = pa.auditor AND pa.nWeek >= '$semana' AND anio >= '$anio' AND pa.tipoEvaluacion = 5 AND pa.tipoAuditoria >= 2 order by us.depto";
    return getArraySQL($sql);
}

function cAuditoriaUser($semana,$anio, $usuario){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au where au.usuario = pa.auditado AND nWeek >= '$semana' AND anio >= '$anio' AND auditor LIKE '$usuario' ORDER BY pa.depAuditado, pa.tipoAuditoria";
    return getArraySQL($sql);
}

function cAuditorUser($semana,$anio, $usuario){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), au.depto,au.nombre, pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au WHERE au.usuario = pa.auditor AND nWeek >= '$semana' AND anio >= '$anio' AND auditado LIKE '%$usuario%' AND tipoAuditoria = 2 ORDER BY pa.depAuditado, pa.tipoAuditoria";
    return getArraySQL($sql);
}

//CONSULTAS PARA PROGRAMACION DE AUDITORIAS
function cUUsuariosSup (){
    $sql = "SELECT usuario, depto FROM a_usuarios WHERE estado = 1 AND tipoEv < 7 AND privilegio = 2 ORDER BY depto,usuario ASC";
    return getArraySQL($sql);
}

function cUUsuariosChampPiso (){
    $sql = "SELECT usuario, depto, tipoEv FROM a_usuarios WHERE tipoEv BETWEEN 1 AND 4 AND privilegio = 3  ORDER BY usuario ASC";
    return getArraySQL($sql);
}

function cUUsuariosChampMantenimiento (){
    $sql = "SELECT usuario, depto, tipoEv FROM a_usuarios WHERE estado = 1 AND tipoEv = 4 AND privilegio = 3 ORDER BY tipoEv,usuario ASC";
    return getArraySQL($sql);
}

function cUUsuariosChampOficina (){
    $sql = "SELECT usuario, depto, tipoEv FROM a_usuarios WHERE estado = 1 AND tipoEv = 5 AND privilegio = 3 ORDER BY tipoEv,usuario ASC";
    return getArraySQL($sql);
}

function cUsuarioPAuditoriaSup ($cValor) {
    $sql = "SELECT u.usuario FROM a_usuarios as u, a_departamento AS d WHERE U.privilegio = 3  AND d.depto = u.depto AND d.cValor = '$cValor'";
    return getArraySQL($sql);
}

function cDeptosArea () {
    $sql = "SELECT tipoEv, depto FROM a_departamento GROUP BY tipoEv, depto ORDER BY tipoEv, depto";
    return getArraySQL($sql);
}

function cUsuarioDeptos ($depto) {
    $sql = "SELECT usuario FROM a_usuarios WHERE depto LIKE '$depto' AND privilegio > 2 AND tipoEv <> 6 AND estado <> 0 GROUP BY usuario ORDER BY usuario ASC";
    return getArraySQL($sql);
}

function cUltimoUsuarioAuditorDeptos ($depto, $fInicio) {
    $sql = "SELECT auditor FROM a_progAuditoria WHERE depAuditado LIKE '$depto' AND fIni = '$fInicio' AND tipoAuditoria = 1";
    return getArraySQL($sql);
}

function cUltimoUsuarioAuditadoDepto ($depto, $fInicio) {
    $sql = "SELECT auditado FROM a_progAuditoria WHERE depAuditado LIKE '$depto' AND fIni = '$fInicio' AND tipoAuditoria = 2";
    return getArraySQL($sql);
}

function cUsuarioPAuditoriaChampionPiso ($usuario, $anio, $fInicio){
    $sql = "SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
                SELECT a.auditor FROM a_progAuditoria as a WHERE u.usuario = a.auditor AND a.fIni = '$fInicio' AND a.tipoAuditoria = 2
            ) AND u.usuario NOT LIKE '%$usuario%' AND u.tipoEv BETWEEN 1 AND 4 AND u.privilegio = 3 AND estado <> 0 GROUP BY u.usuario
            UNION
            SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
                SELECT b.auditor FROM a_progAuditoria as b WHERE u.usuario = b.auditor AND b.anio = '$anio' AND b.tipoAuditoria = 2
            ) AND u.usuario NOT LIKE '%$usuario%' AND u.tipoEv BETWEEN 1 AND 4 AND u.privilegio = 3 AND estado <> 0 GROUP BY u.usuario"; 
    return getArraySQL($sql);    
}

function cUsuarioPAuditoriaChampionOficina($usuario, $anio, $fInicio) {
    $sql = "SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
                SELECT a.auditor FROM a_progAuditoria as a WHERE u.usuario = a.auditor AND a.fIni = '$fInicio' AND a.tipoAuditoria = 2
            ) AND u.usuario NOT LIKE '%$usuario%' AND u.tipoEv = 5 AND u.privilegio = 3 AND estado <> 0 GROUP BY u.usuario
            UNION
            SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
                SELECT b.auditor FROM a_progAuditoria as b WHERE u.usuario = b.auditor AND b.anio = '$anio' AND b.tipoAuditoria = 2
            ) AND u.usuario NOT LIKE '%$usuario%' AND u.tipoEv = 5 AND u.privilegio = 3 AND estado <> 0 GROUP BY u.usuario";
    return getArraySQL($sql);
}

//function cLastAuditProgram ($anio){
//    $sql = "SELECT max(nWeek) FROM a_progAuditoria WHERE anio LIKE '$anio'";
//    return getArraySQL($sql);
//}


function cLastAuditProgram ($anio){
    $sql = "SELECT max(nWeek) FROM a_progAuditoria WHERE anio LIKE '$anio'";
    return getArraySQL($sql);
}

//function iProgAuditoria ($fIni, $fFin, $anio, $mes, $sem, $depto, $auditado, $auditor, $tipoAuditoria, $tipoEv ){
//    $sql = "INSERT INTO a_progAuditoria (fIni, fFin, anio, mes, nWeek, depAuditado, auditado, auditor, tipoAuditoria, tipoEvaluacion, estado) VALUES ('$fIni', '$fFin', '$anio', '$mes', '$sem', '$depto', '$auditado', '$auditor', '$tipoAuditoria', '$tipoEv', '0');";
//    return getArraySQL($sql);
//}

function iProgAuditoria ($fIni, $fFin, $anio, $mes, $sem, $depto, $auditado, $auditor, $tipoAuditoria, $tipoEv ){
    $sql = "INSERT INTO a_progAuditoria (fIni, fFin, anio, mes, nWeek, depAuditado, auditado, auditor, tipoAuditoria, tipoEvaluacion, estado) VALUES ('$fIni', '$fFin', '$anio', '$mes', '$sem', '$depto', '$auditado', '$auditor', '$tipoAuditoria', '$tipoEv', '0');";
    return getArraySQL($sql);
}

function cAuditoriaSUPandADMIN ($userName){
    $sql = "SELECT id FROM a_progAuditoria WHERE auditor = '$userName'  AND estado = 0";
    return getArraySQL($sql);
}

//function iNewAuditoriaSUPandADMIN ($fIni, $fFin, $anio, $mes, $sem, $userName, $tipoAuditoria, $tipoEv ){
//    $sql = "INSERT INTO a_progAuditoria (fIni, fFin, anio, mes, nWeek, auditor, tipoAuditoria, tipoEvaluacion, estado) VALUES ('$fIni', '$fFin', '$anio', '$mes', '$sem', '$userName', '$tipoAuditoria', '$tipoEv', '0');";
//    return getArraySQL($sql);
//}

//AUDITORIAS PROGRAMADAS / PENDIENTES POR USUARIO
function cAuditoriasTipoUsuario($fInicio, $usuario){
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.nWeek, CAST(CONVERT(date, pa.fIni, 103) as VARCHAR(15)) as fini, pa.tipoEvaluacion FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fFin >= '$fInicio' AND pa.auditor LIKE '%$usuario%' AND pa.estado = 0 ORDER BY pa.tipoAuditoria, pa.fFin";
    return getArraySQL($sql);
}

function cAuditoriasUsuario($fInicio, $usuario){
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.nWeek, CAST(CONVERT(date, pa.fIni, 103) as VARCHAR(15)) as fini, pa.tipoEvaluacion FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fFin >= '$fInicio' AND pa.auditor LIKE '%$usuario%' AND pa.estado = 0 ORDER BY pa.tipoAuditoria, pa.fFin";
    return getArraySQL($sql);
}

//AUDITORIA PENDIENTE POR USUARIO
function cDatAuditoriaUsuario($fInicio, $usuario){
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.tipoAuditoria FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fIni >= '$fInicio' AND pa.auditor LIKE '%$usuario%' AND pa.estado = 0";
    return getArraySQL($sql);
}

function cAuditoriaFechaUsuario($fInicio, $usuario, $tipoAuditoria){
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.tipoAuditoria, CAST(CONVERT(date, pa.fIni, 103) as VARCHAR(15)) as fIni, CAST(CONVERT(date, pa.fFin, 103) as VARCHAR(15)) as fFin FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fFin >= '$fInicio' AND pa.auditor LIKE '%$usuario%' AND pa.tipoAuditoria LIKE '%$tipoAuditoria%' AND pa.estado = 0";
    return getArraySQL($sql);
}

function cExistRegistroAuditoria($id){
    $sql = "SELECT id FROM a_puntosEvaluacion WHERE id = '$id'";
    return getArraySQL($sql);
}

/************************* CONSULTAS PARA REPORTTES ***************************/
function cCadValorPiso(){
    $sql = "SELECT cValor, tipoEv FROM a_cadValor WHERE tipoEv BETWEEN 1 AND 4 ORDER BY cValor ASC";
    return getArraySQL($sql);
}

function cCadValorOficina(){
    $sql = "SELECT cValor, tipoEv FROM a_cadValor WHERE tipoEv = 5 ORDER BY cValor ASC";
    return getArraySQL($sql);
}

function cTotalDeptos(){
    $sql = "SELECT depto FROM a_departamento GROUP BY depto ORDER BY depto ASC";
    return getArraySQL($sql);
}

function cDeptosPiso(){
    $sql = "SELECT depto FROM a_departamento WHERE tipoEv BETWEEN 1 AND 4 GROUP BY depto ORDER BY depto ASC";
    return getArraySQL($sql);
}

function cDeptosOficina(){
    $sql = "SELECT depto FROM a_departamento WHERE tipoEv = 5 GROUP BY depto ORDER BY depto ASC";
    return getArraySQL($sql);
}

function cDeptoRegistro($anio){
    $sql = "SELECT depto FROM a_puntajeAuditoria WHERE anio = '$anio' GROUP BY depto";
    return getArraySQL($sql);
}

function cDeptoPuntosMes($anio, $depto){
    $sql = "SELECT month(fecha) AS mes, puntaje FROM a_puntajeAuditoria WHERE anio = '$anio' AND depto = '$depto' " ;
    return getArraySQL($sql);
}

//CONSULTAS PARA GRAFICAS
function cPAreaMes($anio){
    //$sql = "SELECT depto, month(fecha) AS mes, AVG(puntaje) FROM a_puntajeAuditoria WHERE anio = '$anio' GROUP BY depto, month(fecha)";
    $sql = "SELECT prog.depAuditado, month(prog.fIni) AS mes, AVG(point.puntaje) AS puntos FROM a_progAuditoria AS prog, a_puntajeAuditoria as point WHERE prog.depAuditado = point.depto AND prog.id = point.id AND prog.anio = '$anio' GROUP BY prog.depAuditado, month(prog.fIni)";
    return getArraySQL($sql);
}

function cAPlanAreaMes($anio){
    $sql = "SELECT depAuditado, month(fFin) AS mes, COUNT(id) FROM a_progAuditoria WHERE anio = '$anio' GROUP BY depAuditado, month(fFin)";
    return getArraySQL($sql);
}

function cARealAreaMes($anio){
    $sql = "SELECT depAuditado, month(fFin) AS mes, COUNT(id) FROM a_progAuditoria WHERE anio = '$anio' AND estado = '1' GROUP BY depAuditado, month(fFin)";
    return getArraySQL($sql);
}

//GRAFICA GENERAL
function cPuntosPisoGY($anio){
    $sql = "SELECT depto, AVG(puntaje) FROM a_puntajeAuditoria WHERE tipoEval BETWEEN 1 AND 4 AND anio = '$anio' GROUP BY depto";
    return getArraySQL($sql);
}

function cAPlanPisoGY($anio){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoEvaluacion BETWEEN 1 AND 4 AND anio = '$anio' GROUP BY depAuditado";
    return getArraySQL($sql);
}

function cARealPisoGY($anio){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoEvaluacion BETWEEN 1 AND 4 AND anio = '$anio' AND estado = '1' GROUP BY depAuditado";
    return getArraySQL($sql);
}

function cPuntosOficinaGY($anio){
    $sql = "SELECT depto, AVG(puntaje) FROM a_puntajeAuditoria WHERE tipoEval = 5 AND anio = '$anio' GROUP BY depto";
    return getArraySQL($sql);
}

function cAPlanOficinaGY($anio){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoEvaluacion BETWEEN 1 AND 4 AND anio = '$anio' GROUP BY depAuditado";
    return getArraySQL($sql);
}

function cARealOficinaGY($anio){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoEvaluacion BETWEEN 1 AND 4 AND anio = '$anio' AND estado = '1' GROUP BY depAuditado";
    return getArraySQL($sql);
}

//CONSULTAS PARA OPL
function cOPLAbiertosArea($depto, $fCompromiso){
    $sql = "SELECT id, convert (varchar(15), fInicio) as inicio, convert (varchar(15),fCompromiso) as compromiso, hallazgo, accion, responsable, soporte, estado FROM a_opl WHERE depto LIKE '%$depto%' AND fCompromiso >= '$fCompromiso' AND estado <> 4";
    return getArraySQL($sql);
}

function cOPLCerradosArea($depto, $fCompromiso){
    $sql = "SELECT id, convert (varchar(15), fInicio) as inicio, convert (varchar(15),fCompromiso) as compromiso, convert (varchar(15),fTerminacion) as terminacion, hallazgo, accion, responsable, soporte, estado FROM a_opl WHERE depto LIKE '%$depto%' AND fCompromiso >= '$fCompromiso' AND estado = 4";
    return getArraySQL($sql);
}

function cOPLTodosArea($depto, $fCompromiso){
    $sql = "SELECT id, convert (varchar(15), fInicio) as inicio, convert (varchar(15),fCompromiso) as compromiso, convert (varchar(15),fTerminacion) as terminacion, hallazgo, accion, responsable, soporte, estado FROM a_opl WHERE depto LIKE '%$depto%' AND fCompromiso >= '$fCompromiso' ";
    return getArraySQL($sql);
}

//CONSULTA DE AUDITORIAS PROGRAMADAS 
function listadoAuditores($week, $anio){
    $sql = "SELECT prog.auditor, u.nombre , u.correo FROM a_usuarios as u, a_progAuditoria as prog WHERE u.usuario = prog.auditor AND prog.nWeek > '$week' AND prog.anio = '$anio' GROUP BY prog.auditor, u.nombre, u.correo";
    return getArraySQL($sql);
}

function auditoriasProgramadasUsuario($usuario, $year, $week){
    $sql = "SELECT CONCAT(prog.fIni,' al ',prog.fFin), CONCAT(us.nombre,' (',prog.depAuditado,') ') AS auditado, prog.tipoAuditoria FROM a_progAuditoria as prog, a_usuarios as us WHERE prog.auditado = us.usuario AND prog.auditor LIKE '%$usuario%' AND prog.nWeek > '$week' AND prog.anio = '$year' ";
    return getArraySQL($sql);
}

// LISTADOS PARA LINEAS DE SUPERVISORES
function listadoDeptosSup_Gerentes($usuario){
    $sql = "SELECT d.depto FROM a_usuarios AS u, a_departamento AS d WHERE u.depto = d.cValor AND u.usuario LIKE '%$usuario%' OR u.depto = d.depto AND u.usuario LIKE '%$usuario%'";
    return getArraySQL($sql);
}

//DATOS DE LA AUDITORIA DE ACUERDO A EL AREA SELECCIONADA SUP-ADMINASTRAR
function cUsuarioAuditarSup_Admin ($depto){
    $sql = "SELECT usuario, nombre FROM a_usuarios WHERE depto LIKE '%$depto%'";
    return getArraySQL($sql);
}

function uAuditoriaSup_Admin($fIn, $fFin, $anio, $mes, $week, $depto, $auditado, $id){
    $sql = "UPDATE a_progAuditoria SET fIni = '$fIn', fFin = '$fFin', anio = '$anio', mes = '$mes', nWeek = '$week', depAuditado = '$depto', auditado = '$auditado' WHERE id = '$id'";
    return getArraySQL($sql);
}

//APARTADO PARA EL MODULO DE USUARIOS
function iUsuario($tipoEv, $depto,$usuario,$nombre,$correo,$contrasenia,$privilegio ){
    $sql = "INSERT INTO a_usuarios (tipoEv, depto, usuario, nombre, correo, contrasenia, privilegio, estado) VALUES ('$tipoEv','$depto','$usuario','$nombre','$correo','$contrasenia','$privilegio',1)";
    return getArraySQL($sql);
}

function iCadValor($tipoEv, $cadValor ){
    $sql = "INSERT INTO a_cadValor (tipoEv, cValor) VALUES ('$tipoEv','$cadValor')";
    return getArraySQL($sql);
}

function iDepto($tipoEv, $depto, $descripcion, $cValor ){
    $sql = "INSERT INTO a_departamento (tipoEv, depto, descripcion, cValor) VALUES ('$tipoEv','$depto','$descripcion','$cValor')";
    return getArraySQL($sql);
}

//TipoEv de acuerdo a cadena de valor
function cTipoEv_CadValor($cadValor ){
    $sql = "SELECT tipoEv FROM a_cadValor WHERE cValor = '$cadValor'";
    return getArraySQL($sql);
}

function cTipoEv_Depto($depto ){
    $sql = "SELECT tipoEv FROM a_departamento WHERE depto = '$depto'";
    return getArraySQL($sql);
}

//INSERT EN OPL
function iOPL($id, $depto, $hallazgo, $accion, $responsable, $soporte, $fIni, $fFin ){
    $sql = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fIni', '$fFin', NULL, '$hallazgo', '$accion', '$responsable', '$soporte', 1);";
    return getArraySQL($sql);
}







