<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require("../../smtp/class.phpmailer.php");
date_default_timezone_set("America/Mexico_City");

//CONFIGURACION DE CORREO ELECTRONICO
$mensaje = "";
$mail = new PHPMailer();

$mail->isSMTP();
$mail->SMTPDebug = 2;
$mail->Debugoutput = 'html';
//CONFIGURACION DE HOST DE SEG
$mail->Host = "smtp.sg.lan";
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->SMTPSecure = false;
$mail->setFrom("no-reply@seg-automotive.com", "Portal auditoria 5S's"); 
        
// APARTADO PARA LOS PUNTAJES DE LA AUDITORIA
session_start();
$typeEv = $_SESSION["tipoEv"]; 

//OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
$id = $_SESSION["idAuditoria"];
$depto = $_SESSION["deptoAuditoria"];
$tipo = $_SESSION["tipoAuditoria"];
$fInicio = date('Y-m-d');
$anio = date("Y"); 
//APARTADO PARA LA CONVERSION DE LA SEMANA
$f = date('Y-m-d');
$nuevafecha = date ( 'm/d/Y' , strtotime('+1 day',strtotime($f)));
$fecha = new DateTime(date($nuevafecha));
$semana = $fecha->format('W');
$puntaje = 0;

//SEGUNDA
if (isset($_POST["p10"])){
    $p10 = $_POST["p10"];
    $hallazgo10 = $_POST["hallazgo10"];
    $accion10 = $_POST["accion10"];
    $responsable10 = $_POST["responsable10"];
    $soporte10 = $_POST["soporte10"];
    $fCompromiso10 = $_POST["fCompromiso10"];
} else {
    $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 10";
}

if ($typeEv != 5 ){
    if (isset($_POST["p9"])){
        $p9 = $_POST["p9"];
        $hallazgo9 = $_POST["hallazgo9"];
        $accion9 = $_POST["accion9"];
        $responsable9 = $_POST["responsable9"];
        $soporte9 = $_POST["soporte9"];
        $fCompromiso9 = $_POST["fCompromiso9"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 9";
    }
} 

//CONEXION A LA BASE DE DATOS
$serverName = "SGLERSQL01\sqlexpress, 1433"; 
$options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
$conn = sqlsrv_connect($serverName, $options);

//INSERT EN BASE DE DATOS
//echo $typeEv;
if ($typeEv != 5 ){    
    if (( isset($hallazgo9) && isset($accion9) && isset($responsable9) && isset($soporte9) && isset($fCompromiso9)) && 
        ( !empty($hallazgo9) && !empty($accion9) && !empty($responsable9) && !empty($soporte9) && !empty($fCompromiso9))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso9)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 9)";
        }          
    } else {
        if(!empty($fCompromiso9)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso9)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($p9 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 9 ";
            }
            
            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 9)";
            }
            if(!empty($hallazgo9) || !empty($accion9) || !empty($responsable9) || !empty($soporte9)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(9) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }
    
    if (( isset($hallazgo10) && isset($accion10) && isset($responsable10) && isset($soporte10) && isset($fCompromiso10)) && 
        ( !empty($hallazgo10) && !empty($accion10) && !empty($responsable10) && !empty($soporte10) && !empty($fCompromiso10))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso10)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 10)";
        }          
    } else {
        if(!empty($fCompromiso10)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso10)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($p10 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 10 ";
            }
            
            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 10)";
            }
            if(!empty($hallazgo10) || !empty($accion10) || !empty($responsable10) || !empty($soporte10)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(10) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }
} else {
    if (( isset($hallazgo10) && isset($accion10) && isset($responsable10) && isset($soporte10) && isset($fCompromiso10)) && 
        ( !empty($hallazgo10) && !empty($accion10) && !empty($responsable10) && !empty($soporte10) && !empty($fCompromiso10))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso10)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 10)";
        }          
    } else {
        if(!empty($fCompromiso10)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso10)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($p10 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 10 ";
            }  
            
            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 10)";
            }
            
            if(!empty($hallazgo10) || !empty($accion10) || !empty($responsable10) || !empty($soporte10)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(10) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }
}

//    MODULO PARA IMPRIMIR ERRORES
if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else {   
    
    //echo  $typeEv.': ';
    
    if ($typeEv != 5 ){
        $queryIPuntos = "UPDATE a_puntosEvaluacion SET p9 = '$p9', p10 = '$p10' WHERE id = '$id' ";
        $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 

        //INSERT OPL
        if (( isset($hallazgo9) && isset($accion9) && isset($responsable9) && isset($soporte9) && isset($fCompromiso9)) && 
            ( !empty($hallazgo9) && !empty($accion9) && !empty($responsable9) && !empty($soporte9) && !empty($fCompromiso9))){
            
            $d = $fCompromiso9;
            $fCompromiso9 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
            
            $queryIOpl9 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso9', NULL, '$hallazgo9', '$accion9', '$responsable9', '$soporte9', 1);";
            $resultIOpl9 = sqlsrv_query($conn,$queryIOpl9); 
        }

        if (( isset($hallazgo10) && isset($accion10) && isset($responsable10) && isset($soporte10) && isset($fCompromiso10)) && 
            (!empty($hallazgo10) && !empty($accion10) && !empty($responsable10) && !empty($soporte10) && !empty($fCompromiso10))){
            
            $d = $fCompromiso10;
            $fCompromiso10 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
            
            $queryIOpl10 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso10', NULL, '$hallazgo10', '$accion10', '$responsable10', '$soporte10', 1);";
            $resultIOp10 = sqlsrv_query($conn,$queryIOpl10);   
        }
        
        echo "Bien, ";
    } else {        
        $queryIPuntos = "UPDATE a_puntosEvaluacion SET p10 = '$p10' WHERE id = '$id' ";
        $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 
        
        $uEstadoAuditoria = "UPDATE a_progAuditoria SET estado = 1 WHERE id = '$id' ";
        $resultUEstadoAuditoria = sqlsrv_query($conn, $uEstadoAuditoria);  
        
        //APARTADO EL TOTAL DE PUNTOS OBTENIDOS EN AUDITORIA
        $cPuntosAuditoria = "SELECT sum(p1+p2+p3+p4+p5+p6+p7+p8+p9+p10) AS puntaje FROM a_puntosEvaluacion WHERE id = '$id'";
        $resultCPuntosAuditoria = sqlsrv_query($conn,$cPuntosAuditoria);
        
        while($row = sqlsrv_fetch_array($resultCPuntosAuditoria, SQLSRV_FETCH_ASSOC)){
            $sumPuntos = isset($row['puntaje']) ? $row['puntaje'] : 0 ; //contador de registros que trae despues de la consulta        
        }
        
        if ($sumPuntos > 0){
            $puntaje = round(($sumPuntos / 40) * 100, 2);
        } else {
            $puntaje = 0;
        }       
        
        //CONSULTA PARA NO RECRIBIR PUNTAJES DE AUDITORIAS 
        $cExistePuntaje = "SELECT id FROM a_puntajeAuditoria WHERE id = '$id'";
        $resultCExistePuntaje = sqlsrv_query($conn,$cExistePuntaje);
        
        if ($row = sqlsrv_fetch_array($resultCExistePuntaje, SQLSRV_FETCH_ASSOC)){
            $iPuntosAuditoria = "UPDATE a_puntajeAuditoria SET puntaje = '$puntaje' WHERE id = '$id' )" ;
            $resultIPuntosAuditoria = sqlsrv_query($conn,$iPuntosAuditoria);  
        } else {
            $iPuntosAuditoria = "INSERT INTO a_puntajeAuditoria(id, puntaje, depto, tipoEval, semana, anio, fecha) VALUES ('$id','$puntaje','$depto','$tipo','$semana','$anio','$fInicio')" ;
            $resultIPuntosAuditoria = sqlsrv_query($conn,$iPuntosAuditoria);
        }
        
        //INSERT OPL
        if (( isset($hallazgo10) && isset($accion10) && isset($responsable10) && isset($soporte10) && isset($fCompromiso10)) && 
            (!empty($hallazgo10) && !empty($accion10) && !empty($responsable10) && !empty($soporte10) && !empty($fCompromiso10))){            
            $d = $fCompromiso10;
            $fCompromiso10 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
            
            $queryIOpl10 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso10', NULL, '$hallazgo10', '$accion10', '$responsable10', '$soporte10', 1);";
            $resultIOpl10 = sqlsrv_query($conn, $queryIOpl10); 
        }        
        
        //CONSULTA DE E-MAIL DEL AIDITADO
        $queryCorreo = "SELECT u.nombre, u.correo  FROM a_usuarios as u, a_progAuditoria as prog where prog.id = '$id' AND prog.auditor = u.usuario;";
        $resultCorreo = sqlsrv_query($conn, $queryCorreo);    

        while($row = sqlsrv_fetch_array($resultCorreo, SQLSRV_FETCH_ASSOC)){
            $nombreAuditado = $row['nombre']; //contador de registros que trae despues de la consulta        
            $correoAuditado = $row['correo']; //contador de registros que trae despues de la consulta        
        }
        
        $mensaje = $nombreAuditado. "<br> El puntaje de la auditoria que se te realizo es: ".$puntaje;
        
        //CONSULTA OPL
        $query = "SELECT hallazgo FROM a_opl WHERE idAud = '$id' ;";
        $result = sqlsrv_query($conn, $query);
        $cont = 0;
        $mensaje2 = "<br>";
        while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
            $cont++;
            $mensaje2 = $mensaje2.$cont.'.- '.$row['hallazgo'].'<br>' ; //contador de registros que trae despues de la consulta        
        }
        
        if ($cont > 0){
            $mensaje = $mensaje." <br><br> En esta auditoria surgieron los siguientes puntos: <br>";
            $mensaje = $mensaje.'<br>'.$mensaje2;
        }
        
        $mensaje = $mensaje."<br><br><br><br><br> *** EN CASO DE ALGUNA INCONFORMIDAD, FAVOR DE HACERLO SABER AL DEPARTAMENTO DE VSE ***";
        //ENVIAMOS LA NOTIFICACION DE LA AUDITORIA
        $mail->addAddress("$correoAuditado", "Recepient Name");
        $mail->isHTML(true);

        $mail->Subject = "Evaluacion 5S's";
        $mail->Body = "$mensaje";
        $mail->AltBody = "This is the plain text version of the email content";

        //echo $mensaje;
        if(!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Bien, ";
        }        
    }
    echo $puntaje;
}
