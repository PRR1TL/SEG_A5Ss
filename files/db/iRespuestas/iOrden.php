<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();    
    $typeEv = $_SESSION["tipoEv"];
    
    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];
    $fInicio = date('Y-m-d');

    //SEGUNDA
    if (isset($_POST["p4"])){
        $p4 = $_POST["p4"];
        $hallazgo4 = $_POST["hallazgo4"];
        $accion4 = $_POST["accion4"];
        $responsable4 = $_POST["responsable4"];
        $soporte4 = $_POST["soporte4"];
        $fCompromiso4 = $_POST["fCompromiso4"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 4";
    }   

    if ($typeEv == 5 ){
        if (isset($_POST["p5"])){
            $p5 = $_POST["p5"];
            $hallazgo5 = $_POST["hallazgo5"];
            $accion5 = $_POST["accion5"];
            $responsable5 = $_POST["responsable5"];
            $soporte5 = $_POST["soporte5"];
            $fCompromiso5 = $_POST["fCompromiso5"];
        } else {
            $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 5";
        }        
    } else {
        if (isset($_POST["p3"])){
            $p3 = $_POST["p3"];
            $hallazgo3 = $_POST["hallazgo3"];
            $accion3 = $_POST["accion3"];
            $responsable3 = $_POST["responsable3"];
            $soporte3 = $_POST["soporte3"];
            $fCompromiso3 = $_POST["fCompromiso3"];
        } else {
            $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 3";
        } 
    }

    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    //INSERT EN BASE DE DATOS
    //echo $typeEv;
    if ($typeEv != 5 ) {
        //INSERT OPL
        if (( isset($hallazgo3) && isset($accion3) && isset($responsable3) && isset($soporte3) && isset($fCompromiso3)) && 
            ( !empty($hallazgo3) && !empty($accion3) && !empty($responsable3) && !empty($soporte3) && !empty($fCompromiso3))){        

            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso3)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 3)";
            }
        } else {
            if(!empty($fCompromiso3)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso3)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p3 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR UN ACCION EN OPL 3 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 3)";
                }
                if(!empty($hallazgo3) || !empty($accion3) || !empty($responsable3) || !empty($soporte3)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(3) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }

        if (( isset($hallazgo4) && isset($accion4) && isset($responsable4) && isset($soporte4) && isset($fCompromiso4)) && 
            ( !empty($hallazgo4) && !empty($accion4) && !empty($responsable4) && !empty($soporte4) && !empty($fCompromiso4))){

            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso4)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 4)";
            }          
        } else {
            if(!empty($fCompromiso4)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso4)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];
                
                if ($p4 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR UN ACCION EN OPL 4 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 4)";
                }
                if(!empty($hallazgo4) || !empty($accion4) || !empty($responsable4) || !empty($soporte4)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(4) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }    
    } else {
        //INSERT OPL
        if (( isset($hallazgo4) && isset($accion4) && isset($responsable4) && isset($soporte4) && isset($fCompromiso4)) && 
            ( !empty($hallazgo4) && !empty($accion4) && !empty($responsable4) && !empty($soporte4) && !empty($fCompromiso4))){
            //$fi = DateTime::createFromFormat('d/m/Y', $fInicio)->format('Y-m-d');
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso4)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 4)";
            }            
        } else {
            if(!empty($fCompromiso4)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso4)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p4 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 4";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 4)";
                }
                if(!empty($hallazgo4) || !empty($accion4) || !empty($responsable4) || !empty($soporte4)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(4) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }

        if (( isset($hallazgo5) && isset($accion5) && isset($responsable5) && isset($soporte5) && isset($fCompromiso5)) && 
            ( !empty($hallazgo5) && !empty($accion5) && !empty($responsable5) && !empty($soporte5) && !empty($fCompromiso5))){        
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso5)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];
            
            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 5)";    
            }        
        } else {
            if(!empty($fCompromiso5)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso5)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];
                
                if ($p5 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 5 ";
                }

                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 5)";
                }
                if(!empty($hallazgo5) || !empty($accion5) || !empty($responsable5) || !empty($soporte5)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(5) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }
    }
    
//    MODULO PARA IMPRIMIR ERRORES
if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else {    
    if ($typeEv == 5) {
        
        $queryIPuntos = "UPDATE a_puntosEvaluacion SET p4 = '$p4', p5 = '$p5' WHERE id = '$id' ";
        $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 
        
        //INSERT OPL
        if (( isset($hallazgo4) && isset($accion4) && isset($responsable4) && isset($soporte4) && isset($fCompromiso4)) && 
            ( !empty($hallazgo4) && !empty($accion4) && !empty($responsable4) && !empty($soporte4) && !empty($fCompromiso4))){
            
            $d = $fCompromiso4;
            $fCompromiso4 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');

            $queryIOpl4 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso4', NULL, '$hallazgo4', '$accion4', '$responsable4', '$soporte4', 1);";
            $resultIOpl4 = sqlsrv_query($conn,$queryIOpl4); 
        } 

        if (( isset($hallazgo5) && isset($accion5) && isset($responsable5) && isset($soporte5) && isset($fCompromiso5)) && 
            ( !empty($hallazgo5) && !empty($accion5) && !empty($responsable5) && !empty($soporte5) && !empty($fCompromiso5))){        
            
            $d = $fCompromiso5;
            $fCompromiso5 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');

            $queryIOpl5 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso5', NULL, '$hallazgo5', '$accion5', '$responsable5', '$soporte5', 1);";
            $resultIOp5 = sqlsrv_query($conn, $queryIOpl5);  
        }       
    } else {        
        $queryIPuntos = "UPDATE a_puntosEvaluacion SET p3 = '$p3', p4 = '$p4' WHERE id = '$id' ";
        $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 
        
        if (( isset($hallazgo3) && isset($accion3) && isset($responsable3) && isset($soporte3) && isset($fCompromiso3)) && 
            ( !empty($hallazgo3) && !empty($accion3) && !empty($responsable3) && !empty($soporte3) && !empty($fCompromiso3))){        
            
            $d = $fCompromiso3;
            $fCompromiso3 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');

            $queryIOpl3 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso3', NULL, '$hallazgo3', '$accion3', '$responsable3', '$soporte3', 1);";
            $resultIOp3 = sqlsrv_query($conn, $queryIOpl3);                      
        } 

        if (( isset($hallazgo4) && isset($accion4) && isset($responsable4) && isset($soporte4) && isset($fCompromiso4)) && 
            ( !empty($hallazgo4) && !empty($accion4) && !empty($responsable4) && !empty($soporte4) && !empty($fCompromiso4))){

            $d = $fCompromiso4;
            $fCompromiso4 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');

            $queryIOpl4 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso4', NULL, '$hallazgo4', '$accion4', '$responsable4', '$soporte4', 1);";
            $resultIOpl4 = sqlsrv_query($conn,$queryIOpl4);
        }            
    }
    echo "Bien";    
}     

