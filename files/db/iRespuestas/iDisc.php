<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    require("../../smtp/class.phpmailer.php");

    $mail = new PHPMailer();

    $mail->isSMTP();
    $mail->SMTPDebug = 2;
    $mail->Debugoutput = 'html';
    //CONFIGURACION DE HOST DE SEG
    $mail->Host = "smtp.sg.lan";
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->SMTPSecure = false;
    $mail->setFrom("no-reply@seg-automotive.com", "Portal auditoria 5S's");
    session_start();
    $typeEv = $_SESSION["tipoEv"];   

    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];
    $tipo = $_SESSION["tipoAuditoria"];
    $fInicio = date('Y-m-d');
    
    $anio = date("Y"); 
    //APARTADO PARA LA CONVERSION DE LA SEMANA
    $f = date('Y-m-d');
    $nuevafecha = date ( 'm/d/Y' , strtotime('+1 day',strtotime($f)));
    $fecha = new DateTime(date($nuevafecha));
    $semana = $fecha->format('W');

    //SEGUNDA
    if (isset($_POST["p17"])){
	$p17 = $_POST["p17"];
        $hallazgo17 = $_POST["hallazgo17"];
        $accion17 = $_POST["accion17"];
        $responsable17 = $_POST["responsable17"];
        $soporte17 = $_POST["soporte17"];
        $fCompromiso17 = $_POST["fCompromiso17"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 17";
    }
    
    if (isset($_POST["p18"])){
        $p18 = $_POST["p18"];
        $hallazgo18 = $_POST["hallazgo18"];
        $accion18 = $_POST["accion18"];
        $responsable18 = $_POST["responsable18"];
        $soporte18 = $_POST["soporte18"];
        $fCompromiso18 = $_POST["fCompromiso18"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 18";
    }    

    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    //INSERT OPL
    if (( isset($hallazgo17) && isset($accion17) && isset($responsable17) && isset($soporte17) && isset($fCompromiso17)) && 
       ( !empty($hallazgo17) && !empty($accion17) && !empty($responsable17) && !empty($soporte17) && !empty($fCompromiso17))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso17)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 17)";
        }          
    } else {
        if(!empty($fCompromiso17)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso17)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 17)";
            }
            if ($p17 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 17 ";
            } else
            if(!empty($hallazgo17) || !empty($accion17) || !empty($responsable17) || !empty($soporte17)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(17) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }

    if (( isset($hallazgo18) && isset($accion18) && isset($responsable18) && isset($soporte18) && isset($fCompromiso18)) && 
       ( !empty($hallazgo18) && !empty($accion18) && !empty($responsable18) && !empty($soporte18) && !empty($fCompromiso18))){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso18)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 18)";
        }          
    } else {
        if(!empty($fCompromiso18)){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso18)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 18)";
            }
            if ($p18 <= 2 ){
                $errors []= "SE DEBE ASIGNAR ACCION EN OPL 18 ";
            } else
            if(!empty($hallazgo18) || !empty($accion18) || !empty($responsable18) || !empty($soporte18)){
                $errors[] = "TODOS LOS CAMPOS DE LA OPL(18) DEBEN ESTAR LLENOS CORRECTAMENTE";
            }                
        }
    }

if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else {    
    //INSERT EN BASE DE DATOS
    
    $queryIPuntos = "UPDATE a_puntosEvaluacion SET p17 = '$p17', p18 = '$p18' WHERE id = '$id' ";
    $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 
    
    //HACEMOS EL CAMBIO DE ESTADO DE REGISTRO DE AUDITORIA
    $uEstadoAuditoria = "UPDATE a_progAuditoria SET estado = 1 WHERE id = '$id' ";
    $resultUEstadoAuditoria = sqlsrv_query($conn, $uEstadoAuditoria);  
    
    //APARTADO EL TOTAL DE PUNTOS OBTENIDOS EN AUDITORIA
    $cPuntosAuditoria = "SELECT sum(p1+p2+p3+p4+p5+p6+p7+p8+p9+p10+p11+p12+p13+p14+p15+p16+p17+p18) AS puntaje FROM a_puntosEvaluacion WHERE id = '$id'";
    $resultCPuntosAuditoria = sqlsrv_query($conn,$cPuntosAuditoria);

    while($row = sqlsrv_fetch_array($resultCPuntosAuditoria, SQLSRV_FETCH_ASSOC)){
        $sumPuntos = isset($row['puntaje']) ? $row['puntaje'] : 0 ; //contador de registros que trae despues de la consulta        
    }    
    $puntaje = ($sumPuntos / 72) * 100;     
        
    //CONSULTA PARA NO RECRIBIR PUNTAJES DE AUDITORIAS 
    $cExistePuntaje = "SELECT id FROM a_puntajeAuditoria WHERE id = '$id'";
    $resultCExistePuntaje = sqlsrv_query($conn,$cExistePuntaje);

    if ($row = sqlsrv_fetch_array($resultCExistePuntaje, SQLSRV_FETCH_ASSOC)){
        $iPuntosAuditoria = "UPDATE a_puntajeAuditoria SET puntaje = '$puntaje' WHERE id = '$id' )" ;
        $resultIPuntosAuditoria = sqlsrv_query($conn,$iPuntosAuditoria);  
    } else {
        $iPuntosAuditoria = "INSERT INTO a_puntajeAuditoria(id, puntaje, depto, tipoEval, semana, anio, fecha) VALUES ('$id','$puntaje','$depto','$tipo','$semana','$anio','$fInicio')" ;
        $resultIPuntosAuditoria = sqlsrv_query($conn,$iPuntosAuditoria);
    }

    //INSERT OPL
    if (( isset($hallazgo17) && isset($accion17) && isset($responsable17) && isset($soporte17) && isset($fCompromiso17)) && 
       ( !empty($hallazgo17) && !empty($accion17) && !empty($responsable17) && !empty($soporte17) && !empty($fCompromiso17))){
        $queryIOpl17 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso17', NULL, '$hallazgo17', '$accion17', '$responsable17', '$soporte17', 1);";
        $resultIOp17 = sqlsrv_query($conn, $queryIOpl17);   
    }

    if (( isset($hallazgo18) && isset($accion18) && isset($responsable18) && isset($soporte18) && isset($fCompromiso18)) && 
       ( !empty($hallazgo18) && !empty($accion18) && !empty($responsable18) && !empty($soporte18) && !empty($fCompromiso18))){
        $queryIOpl18 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso18', NULL, '$hallazgo18', '$accion18', '$responsable18', '$soporte18', 1);";
        $resultIOpl18 = sqlsrv_query($conn,$queryIOpl18); 
    }  
    
    //CONSULTA DE E-MAIL DEL AIDITADO
    $queryCorreo = "SELECT u.nombre, u.correo FROM a_usuarios as u, a_progAuditoria as prog where prog.id = '$id' AND prog.auditado = u.usuario;";
    $resultCorreo = sqlsrv_query($conn, $queryCorreo);    

    while($row = sqlsrv_fetch_array($resultCorreo, SQLSRV_FETCH_ASSOC)){
        $nombreAuditado = $row['nombre']; //contador de registros que trae despues de la consulta        
        $correoAuditado = $row['correo']; //contador de registros que trae despues de la consulta        
    }

    //echo "Id: ",$id,' auditado: ',$nombreAuditado,' correo:',$correoAuditado;
    $mensaje = $nombreAuditado. "<br> El puntaje de la auditoria que se te realizo es: ".$puntaje;

    //CONSULTA OPL
    $query = "SELECT hallazgo FROM a_opl WHERE idAud = '$id' ;";
    $result = sqlsrv_query($conn, $query);
    $cont = 0;
    $mensaje2 = "<br>";
    while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
        $cont++;
        $mensaje2 = $mensaje2.$cont.'.- '.$row['hallazgo'].'<br>' ; //contador de registros que trae despues de la consulta        
    }

    if ($cont > 0){
        $mensaje = $mensaje." <br><br> En esta auditoria surgieron los siguientes puntos: <br>";
        $mensaje = $mensaje.'<br>'.$mensaje2;
    }

    $mensaje = $mensaje."<br><br><br><br><br> *** EN CASO DE ALGUNA INCONFORMIDAD, FAVOR DE HACERLO SABER AL DEPARTAMENTO DE VSE ***";
    //ENVIAMOS LA NOTIFICACION DE LA AUDITORIA
    $mail->addAddress("$correoAuditado", "Recepient Name");
    $mail->isHTML(true);

    $mail->Subject = "Evaluacion 5S's";
    $mail->Body = "$mensaje";
    $mail->AltBody = "This is the plain text version of the email content";

    //echo $mensaje;
    if(!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Bien, ";
    }
    
   echo $puntaje;
}



