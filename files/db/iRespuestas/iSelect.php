<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    $typeEv = $_SESSION["tipoEv"];    

    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];

    //echo 'id: ',$id,'<br>';
    //echo $depto;

    //$fInicio = $_POST["fInicio1"];
    $fInicio = date('Y-m-d');
    //$fInicio = DateTime::createFromFormat('d/m/Y', $fInicio)->format('Y-m-d');

    //PRIMER
    if (isset($_POST["p1"])){
        $p1 = $_POST["p1"];
        $hallazgo1 = $_POST["hallazgo1"];
        $accion1 = $_POST["accion1"];
        $responsable1 =  $_POST["responsable1"];
        $soporte1 = $_POST["soporte1"];
        $fCompromiso1 = $_POST["fCompromiso1"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 1";
    }

    //SEGUNDA
    if (isset($_POST["p2"])){
        $p2 = $_POST["p2"];
        $hallazgo2 = $_POST["hallazgo2"];
        $accion2 = $_POST["accion2"];
        $responsable2 = $_POST["responsable2"];
        $soporte2 = $_POST["soporte2"];
        $fCompromiso2 = $_POST["fCompromiso2"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 2";
    }
    //TERCER
    if ($typeEv == 5 ){
        if(isset($_POST["p3"])){
            $p3 = $_POST["p3"];
            $hallazgo3 = $_POST["hallazgo3"];
            $accion3 = $_POST["accion3"];
            $responsable3 = $_POST["responsable3"];
            $soporte3 = $_POST["soporte3"];
            $fCompromiso3 = $_POST["fCompromiso3"];
        } else {
            $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 3";
        }    
    }

    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    $query = "SELECT id FROM a_puntosEvaluacion WHERE id = '$id';";
    $result = sqlsrv_query($conn,$query);

    if ($result === false ){
        echo "error <br>";
    } else {
        if ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
            $countRegAuditoria = 1 ; //contador de registros que trae despues de la consulta        
        } else {
            $countRegAuditoria = 0;
        }
    }

    //INSERT EN BASE DE DATOS
    if ($typeEv != 5 ){
        //INSERT OPL
        if (( isset($hallazgo1) && isset($accion1) && isset($responsable1) && isset($soporte1) && isset($fCompromiso1))  && 
            ( !empty($hallazgo1) && !empty($accion1) && !empty($responsable1) && !empty($soporte1) && !empty($fCompromiso1)) ){              
            
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           
            
            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso1)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];
            
            if ($fIn > $fIc){              
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";
            }            
        }  else {                       
            if( !empty($fCompromiso1)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso1)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];
                
                if ($p1 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 1 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1)";
                }
                if(!empty($hallazgo1) || !empty($accion1) || !empty($responsable1) || !empty($soporte1)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(1) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }            
        }

        if ((isset($hallazgo2) && isset($accion2) && isset($responsable2) && isset($soporte2) && isset($fCompromiso2)) && 
            (!empty($hallazgo2) && !empty($accion2) && !empty($responsable2) && !empty($soporte2) && !empty($fCompromiso2))){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           
            
            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso2)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];
                                   
            if ($fIn > $fIc){              
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";
            }   
        } else {
            if (!empty($fCompromiso2)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi);
                $fIn = $iFI[0].$iFI[1].$iFI[2];

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso2)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p2 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 2 ";
                }             
                
                if ($fIn > $fIc){              
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";//. $fCompromiso1." es menor a ".$fInicio;
                }
                
                if(!empty($hallazgo2) || !empty($accion2) || !empty($responsable2) || !empty($soporte2)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(2) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }             
        }
    } else {        
        //INSERT OPL                
        if (( isset($hallazgo1) && isset($accion1) && isset($responsable1) && isset($soporte1) && isset($fCompromiso1))  && 
            ( !empty($hallazgo1) && !empty($accion1) && !empty($responsable1) && !empty($soporte1) && !empty($fCompromiso1))){     
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           
            
            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso1)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];
            if ($fIn > $fIc){              
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";
            }            
        }  else {            
            if(!empty($fCompromiso1)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso1)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];
                
                if ($p1 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 1 ";
                }             
                
                if ($fIn > $fIc){              
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";
                }
                if(!empty($hallazgo1) || !empty($accion1) || !empty($responsable1) || !empty($soporte1)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(1) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }            
        }

        if (( isset($hallazgo2) && isset($accion2) && isset($responsable2) && isset($soporte2) && isset($fCompromiso2)) && 
            ( !empty($hallazgo2) && !empty($accion2) && !empty($responsable2) && !empty($soporte2) && !empty($fCompromiso2))){
            
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           
            
            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso2)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];
            
            if ($fIn > $fIc){              
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";//. $fCompromiso1." es menor a ".$fInicio;
            }    
        } else {
            if (!empty($fCompromiso2)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso2)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p2 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 3 ";
                }            
                
                if ($fIn > $fIc){              
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";
                }
                if( !empty($hallazgo2) || !empty($accion2) || !empty($responsable2) || !empty($soporte2) ){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(2) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }            
        }

        if ((isset($hallazgo3) && isset($accion3) && isset($responsable3) && isset($soporte3) && isset($fCompromiso3)) && 
            (!empty($hallazgo3) && !empty($accion3) && !empty($responsable3) && !empty($soporte3) && !empty($fCompromiso3))){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           
            
            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso3)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];
            
            if ($fIn > $fIc){              
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";
            }            
        } else {
            if (!empty($fCompromiso3)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso3)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];
                
                if ($p3 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 3 ";
                }             
                
                if ($fIn > $fIc){              
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 1) ";
                }
                if(!empty($hallazgo3) || !empty($accion3) || !empty($responsable3) || !empty($soporte3) ){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(3) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            } 
        }
    }

//    MODULO PARA IMPRIMIR ERRORES
if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else {    
    if ($typeEv == 5){      
        
        if($countRegAuditoria != 0 ){
            $queryIPuntos = "UPDATE a_puntosEvaluacion SET p1 = '$p1', p2 = '$p2', p3 = '$p3' WHERE id = '$id' ";
            $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 
        } else {
            $queryIPuntos = "INSERT INTO a_puntosEvaluacion (id, p1, p2, p3, tipoEv) VALUES ('$id','$p1','$p2','$p3','$typeEv');";
            $resultIPuntos = sqlsrv_query($conn, $queryIPuntos);
        }         
        
        if (( isset($hallazgo1) && isset($accion1) && isset($responsable1) && isset($soporte1) && isset($fCompromiso1))  && 
            ( !empty($hallazgo1) && !empty($accion1) && !empty($responsable1) && !empty($soporte1) && !empty($fCompromiso1))){                          
                $d = $fCompromiso1;
                $fCompromiso1 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
                
                $queryIOpl1 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso1', NULL, '$hallazgo1', '$accion1', '$responsable1', '$soporte1', 1);";
                $resultIOpl1 = sqlsrv_query($conn,$queryIOpl1);                      
        }  

        if (( isset($hallazgo2) && isset($accion2) && isset($responsable2) && isset($soporte2) && isset($fCompromiso2)) && 
            ( !empty($hallazgo2) && !empty($accion2) && !empty($responsable2) && !empty($soporte2) && !empty($fCompromiso2))){            
                $d = $fCompromiso2;
                $fCompromiso2 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
                
                $queryIOpl2 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso2', NULL, '$hallazgo2', '$accion2', '$responsable2', '$soporte2', 1);";
                $resultIOpl2 = sqlsrv_query($conn,$queryIOpl2);            
        } 

        if ((isset($hallazgo3) && isset($accion3) && isset($responsable3) && isset($soporte3) && isset($fCompromiso3)) && 
            (!empty($hallazgo3) && !empty($accion3) && !empty($responsable3) && !empty($soporte3) && !empty($fCompromiso3))){
                $d = $fCompromiso3;
                $fCompromiso3 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
                
                $queryIOpl3 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso3', NULL, '$hallazgo3', '$accion3', '$responsable3', '$soporte3', 1);";
                $resultIOp3 = sqlsrv_query($conn, $queryIOpl3);                             
        }        
    } else {
        
        if($countRegAuditoria != 0 ){
            $queryIPuntos = "UPDATE a_puntosEvaluacion SET p1 = '$p1', p2 = '$p2' WHERE id = '$id' ";
            $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 
        } else {
            $queryIPuntos = "INSERT INTO a_puntosEvaluacion (id, p1, p2, tipoEv) VALUES ('$id','$p1','$p2','$typeEv');";
            $resultIPuntos = sqlsrv_query($conn, $queryIPuntos);
        }
            
        if (( isset($hallazgo1) && isset($accion1) && isset($responsable1) && isset($soporte1) && isset($fCompromiso1))  && 
            ( !empty($hallazgo1) && !empty($accion1) && !empty($responsable1) && !empty($soporte1) && !empty($fCompromiso1))){
		$d = $fCompromiso1;
                $fCompromiso1 = date('Y-m-d', strtotime($d));
                
                $queryIOpl1 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso1', NULL, '$hallazgo1', '$accion1', '$responsable1', '$soporte1', 1);";
                $resultIOpl1 = sqlsrv_query($conn,$queryIOpl1); 
        }

        if (( isset($hallazgo2) && isset($accion2) && isset($responsable2) && isset($soporte2) && isset($fCompromiso2)) && 
            ( !empty($hallazgo2) && !empty($accion2) && !empty($responsable2) && !empty($soporte2) && !empty($fCompromiso2))){
                $d = $fCompromiso2;
                $fCompromiso2 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
                $queryIOpl2 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso2', NULL, '$hallazgo2', '$accion2', '$responsable2', '$soporte2', 1);";
                $resultIOpl2 = sqlsrv_query($conn,$queryIOpl2);               
        }         
    }
    echo "Bien";    
} ?>









