<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();
$typeEv = $_SESSION["tipoEv"];   

//OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
$id = $_SESSION["idAuditoria"];
$depto = $_SESSION["deptoAuditoria"];
$fInicio = date('Y-m-d');

//SEGUNDA
if (isset($_POST["p15"])){
    $p15 = $_POST["p15"];
    $hallazgo15 = $_POST["hallazgo15"];
    $accion15 = $_POST["accion15"];
    $responsable15 = $_POST["responsable15"];
    $soporte15 = $_POST["soporte15"];
    $fCompromiso15 = $_POST["fCompromiso15"];
} else {
    $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 15";
}

if (isset($_POST["p16"])){
    $p16 = $_POST["p16"];
    $hallazgo16 = $_POST["hallazgo16"];
    $accion16 = $_POST["accion16"];
    $responsable16 = $_POST["responsable16"];
    $soporte16 = $_POST["soporte16"];
    $fCompromiso16 = $_POST["fCompromiso16"];
} else {
    $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 16";
}


//CONEXION A LA BASE DE DATOS
$serverName = "SGLERSQL01\sqlexpress, 1433"; 
$options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
$conn = sqlsrv_connect($serverName, $options);

//INSERT OPL
if (( isset($hallazgo15) && isset($accion15) && isset($responsable15) && isset($soporte15) && isset($fCompromiso15)) && 
   ( !empty($hallazgo15) && !empty($accion15) && !empty($responsable15) && !empty($soporte15) && !empty($fCompromiso15))){
    $fi = date('Y-m-d', strtotime($fInicio));
    $iFI = split ("-", $fi); 
    $fIn = $iFI[0].$iFI[1].$iFI[2];           

    $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso15)->format('Y-m-d');
    $iFC = split ("-", $fc); 
    $fIc = $iFC[0].$iFC[1].$iFC[2];

    if ($fIn > $fIc){
        $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 15)";
    }          
} else {
    if(!empty($fCompromiso15)){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso15)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 15)";
        }
        if ($p15 <= 2 ){
            $errors []= "SE DEBE ASIGNAR ACCION EN OPL 15 ";
        } else
        if(!empty($hallazgo15) || !empty($accion15) || !empty($responsable15) || !empty($soporte15)){
            $errors[] = "TODOS LOS CAMPOS DE LA OPL(15) DEBEN ESTAR LLENOS CORRECTAMENTE";
        }                
    }
}

if (( isset($hallazgo16) && isset($accion16) && isset($responsable16) && isset($soporte16) && isset($fCompromiso16)) && 
   ( !empty($hallazgo16) && !empty($accion16) && !empty($responsable16) && !empty($soporte16) && !empty($fCompromiso16))){
    $fi = date('Y-m-d', strtotime($fInicio));
    $iFI = split ("-", $fi); 
    $fIn = $iFI[0].$iFI[1].$iFI[2];           

    $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso16)->format('Y-m-d');
    $iFC = split ("-", $fc); 
    $fIc = $iFC[0].$iFC[1].$iFC[2];

    if ($fIn > $fIc){
        $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 16)";
    }          
} else {
    if(!empty($fCompromiso16)){
        $fi = date('Y-m-d', strtotime($fInicio));
        $iFI = split ("-", $fi); 
        $fIn = $iFI[0].$iFI[1].$iFI[2];           

        $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso16)->format('Y-m-d');
        $iFC = split ("-", $fc); 
        $fIc = $iFC[0].$iFC[1].$iFC[2];

        if ($fIn > $fIc){
            $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 16)";
        }
        if ($p16 <= 2 ){
            $errors []= "SE DEBE ASIGNAR ACCION EN OPL 16 ";
        } else
        if(!empty($hallazgo16) || !empty($accion16) || !empty($responsable16) || !empty($soporte16)){
            $errors[] = "TODOS LOS CAMPOS DE LA OPL(16) DEBEN ESTAR LLENOS CORRECTAMENTE";
        }                
    }
}

if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else {    
    
    //INSERT EN BASE DE DATOS
    $queryIPuntos = "UPDATE a_puntosEvaluacion SET p15 = '$p15', p16 = '$p16' WHERE id = '$id' ";
    $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 

    //INSERT OPL
    if (( isset($hallazgo15) && isset($accion15) && isset($responsable15) && isset($soporte15) && isset($fCompromiso15)) && 
       ( !empty($hallazgo15) && !empty($accion15) && !empty($responsable15) && !empty($soporte15) && !empty($fCompromiso15))){
        $queryIOpl15 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso15', NULL, '$hallazgo15', '$accion15', '$responsable15', '$soporte15', 1);";
        $resultIOp15 = sqlsrv_query($conn, $queryIOpl15);   
    }

    if (( isset($hallazgo16) && isset($accion16) && isset($responsable16) && isset($soporte16) && isset($fCompromiso16)) && 
       ( !empty($hallazgo16) && !empty($accion16) && !empty($responsable16) && !empty($soporte16) && !empty($fCompromiso16))){
        $queryIOpl16 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso16', NULL, '$hallazgo16', '$accion16', '$responsable16', '$soporte16', 1);";
        $resultIOpl16 = sqlsrv_query($conn,$queryIOpl16); 
    }    
    
    echo "Bien";
}







