<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    $typeEv = $_SESSION["tipoEv"];   

    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];
    $fInicio = date('Y-m-d');

    //SEGUNDA
    if (isset($_POST["p6"])){
        $p6 = $_POST["p6"];
        $hallazgo6 = $_POST["hallazgo6"];
        $accion6 = $_POST["accion6"];
        $responsable6 = $_POST["responsable6"];
        $soporte6 = $_POST["soporte6"];
        $fCompromiso6 = $_POST["fCompromiso6"];
    } else {
        $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 6 ";
    }

    if ($typeEv == 5 ){
        if (isset($_POST["p7"])){
            $p7 = $_POST["p7"];
            $hallazgo7 = $_POST["hallazgo7"];
            $accion7 = $_POST["accion7"];
            $responsable7 = $_POST["responsable7"];
            $soporte7 = $_POST["soporte7"];
            $fCompromiso7 = $_POST["fCompromiso7"];
        } else {
            $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 7";
        }        
    } else {
        //PRIMER
        if (isset($_POST["p5"])){
            $p5 = $_POST["p5"];
            $hallazgo5 = $_POST["hallazgo5"];
            $accion5 = $_POST["accion5"];
            $responsable5 = $_POST["responsable5"];
            $soporte5 = $_POST["soporte5"];
            $fCompromiso5 = $_POST["fCompromiso5"];
        } else {
            $errors []= "DEBES SELECCIONAR LOS PUNTOS DEL PUNTO 5";
        }
    }

    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    //INSERT EN BASE DE DATOS
    //echo $typeEv;
    if ($typeEv != 5 ){         

        //INSERT OPL
        if (( isset($hallazgo5) && isset($accion5) && isset($responsable5) && isset($soporte5) && isset($fCompromiso5)) && 
            ( !empty($hallazgo5) && !empty($accion5) && !empty($responsable5) && !empty($soporte5) && !empty($fCompromiso5))){

            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso5)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 5)";
            }
        } else {
            if(!empty($fCompromiso5)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso5)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];
                
                if ($p5 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 5 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 5)";
                }
                if(!empty($hallazgo5) || !empty($accion5) || !empty($responsable5) || !empty($soporte5)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(5) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }

        if (( isset($hallazgo6) && isset($accion6) && isset($responsable6) && isset($soporte6) && isset($fCompromiso6)) &&
            ( !empty($hallazgo6) && !empty($accion6) && !empty($responsable6) && !empty($soporte6) && !empty($fCompromiso6)) ){  
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso6)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 6)";
            }
        } else {
            if(!empty($fCompromiso6)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso6)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p6 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 6 ";
                }
                
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 6)";
                }
                if(!empty($hallazgo6) || !empty($accion6) || !empty($responsable6) || !empty($soporte6)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(6) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }
    } else {
        //INSERT OPL
        if (( isset($hallazgo6) && isset($accion6) && isset($responsable6) && isset($soporte6) && isset($fCompromiso6)) &&
            ( !empty($hallazgo6) && !empty($accion6) && !empty($responsable6) && !empty($soporte6) && !empty($fCompromiso6)) ){
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso6)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 6)";
            }
        } else {
            if(!empty($fCompromiso6)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso6)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];

                if ($p6 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 6 ";
                }
                //if ($fCompromiso1 < $fInicio){
                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 6)";
                }
                if(!empty($hallazgo6) || !empty($accion6) || !empty($responsable6) || !empty($soporte6)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(6) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }
        if (( isset($hallazgo7) && isset($accion7) && isset($responsable7) && isset($soporte7) && isset($fCompromiso7)) &&
            ( !empty($hallazgo7) && !empty($accion7) && !empty($responsable7) && !empty($soporte7) && !empty($fCompromiso7)) ){  
            $fi = date('Y-m-d', strtotime($fInicio));
            $iFI = split ("-", $fi); 
            $fIn = $iFI[0].$iFI[1].$iFI[2];           

            $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso7)->format('Y-m-d');
            $iFC = split ("-", $fc); 
            $fIc = $iFC[0].$iFC[1].$iFC[2];

            if ($fIn > $fIc){
                $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 7)";
            }
        } else {
            if(!empty($fCompromiso7)){
                $fi = date('Y-m-d', strtotime($fInicio));
                $iFI = split ("-", $fi); 
                $fIn = $iFI[0].$iFI[1].$iFI[2];           

                $fc = DateTime::createFromFormat('d/m/Y', $fCompromiso7)->format('Y-m-d');
                $iFC = split ("-", $fc); 
                $fIc = $iFC[0].$iFC[1].$iFC[2];
                
                if ($p7 <= 2 ){
                    $errors []= "SE DEBE ASIGNAR ACCION EN OPL 7 ";
                }

                if ($fIn > $fIc){
                    $errors []= "REVISAR CAMPO DE FECHA COMPROMISO (OPL 7)";
                }
                
                if(!empty($hallazgo7) || !empty($accion7) || !empty($responsable7) || !empty($soporte7)){
                    $errors[] = "TODOS LOS CAMPOS DE LA OPL(7) DEBEN ESTAR LLENOS CORRECTAMENTE";
                }                
            }
        }
    }

if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>
            <?php foreach ($errors as $error) {
                echo $error,'<BR>';
            } ?> 
        </strong>     
        </div>
<?php } else {  
    
    if ($typeEv != 5 ){
        
        $queryIPuntos = "UPDATE a_puntosEvaluacion SET p5 = '$p5', p6 = '$p6' WHERE id = '$id' ";
        $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 

        //INSERT OPL
        if (( isset($hallazgo5) && isset($accion5) && isset($responsable5) && isset($soporte5) && isset($fCompromiso5)) && 
            ( !empty($hallazgo5) && !empty($accion5) && !empty($responsable5) && !empty($soporte5) && !empty($fCompromiso5))){

            $d = $fCompromiso5;
            $fCompromiso5 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
            
            $queryIOpl5 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso5', NULL, '$hallazgo5', '$accion5', '$responsable5', '$soporte5', 1);";
            $resultIOpl5 = sqlsrv_query($conn,$queryIOpl5); 
        }

        if ( isset($hallazgo6) && isset($accion6) && isset($responsable6) && isset($soporte6) && isset($fCompromiso6) &&
           ( !empty($hallazgo6) && !empty($accion6) && !empty($responsable6) && !empty($soporte6) && !empty($fCompromiso6))){
            $d = $fCompromiso6;
            $fCompromiso6 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
            
            $queryIOpl6 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso6', NULL, '$hallazgo6', '$accion6', '$responsable6', '$soporte6', 1);";
            $resultIOpl6 = sqlsrv_query($conn,$queryIOpl6);   
        }
    } else {
        //echo "aqui";
        $queryIPuntos = "UPDATE a_puntosEvaluacion SET p6 = '$p6', p7 = '$p7' WHERE id = '$id' ";
        $resultIPuntos = sqlsrv_query($conn, $queryIPuntos); 
        //INSERT OPL
        if ( isset($hallazgo6) && isset($accion6) && isset($responsable6) && isset($soporte6) && isset($fCompromiso6) &&
           ( !empty($hallazgo6) && !empty($accion6) && !empty($responsable6) && !empty($soporte6) && !empty($fCompromiso6))){
            $d = $fCompromiso6;
            $fCompromiso6 = DateTime::createFromFormat('d/m/Y', $d)->format('Y-m-d');
            
            $queryIOpl6 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso6', NULL, '$hallazgo6', '$accion6', '$responsable6', '$soporte6', 1);";
            $resultIOpl6 = sqlsrv_query($conn,$queryIOpl6);   
        }

        if ( isset($hallazgo7) && isset($accion7) && isset($responsable7) && isset($soporte7) && isset($fCompromiso7) &&
           ( !empty($hallazgo7) && !empty($accion7) && !empty($responsable7) && !empty($soporte7) && !empty($fCompromiso7))){
            $queryIOpl7 = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado) VALUES ('$id', '$depto', '$fInicio', '$fCompromiso7', NULL, '$hallazgo7', '$accion7', '$responsable7', '$soporte7', 1);";
            $resultIOpl7 = sqlsrv_query($conn, $queryIOpl7);   
        }
    }    
    echo "Bien";
}
    
    


















