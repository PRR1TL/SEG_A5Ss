<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once './funciones.php';
    
    session_start();
    $usuario = strtoupper($_POST["username"]);
    $password = strtoupper($_POST["password"]);
    
    //MANDAMOS TRAER LOS DATOS DE LAS AUDITORIAS POR USUARIO
    $datConsultaLogin = cUsuarioContrasena($usuario, $password);
        
    if (count($datConsultaLogin) > 0) {
        $user = $datConsultaLogin[0][0];
        $name = $datConsultaLogin[0][1];
        $tipo = $datConsultaLogin[0][2];
        $estado = $datConsultaLogin[0][3];
        $tipoEv = $datConsultaLogin[0][4];
        $depto = $datConsultaLogin[0][5];
        
        if ($estado != 0 ){
            $_SESSION["userName"] = $user;
            $_SESSION["name"] = $name;
            $_SESSION["tipo"] = $tipo;
            $_SESSION["ev"] = $tipoEv;
            $_SESSION["depto"] = $depto;
        } else {
            $errors []= "Favor de ir con tu VSE: <br> Tu usuario esta deshabilitado";
        }                      
    } else {
        $errors []= "Verifica Usuario y Contraseña";
    }
//    MODULO PARA IMPRIMIR ERRORES
    if (isset($errors)){
    ?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error;
        } ?> </strong>     
        </div>
    <?php } else {
        ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>BIENVENIDO</strong>     
        </div>
        <script>
            location.href = "./index.php";
        </script>        
        <?php
    } 
?>
