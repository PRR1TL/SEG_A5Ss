<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../funciones.php';

    //RECIBIMOS LOS PARAMETROS DE LOS CAMPOS DEL FORMULARIO
    $privilegio = $_REQUEST["privilegio"];
    switch ($privilegio){
        case 1:
            $tipoAdmin = $_REQUEST["tipoA"];
            if ($tipoAdmin == 1){
                $tipoEv = 6;
            } else {
                $tipoEv = 5;
            }
            
            $depto = 'VSE';
            
            $usuario = $_REQUEST["usuarioA"];
            $nombre = $_REQUEST["nombreA"];
            $correo = $_REQUEST["correoA"];
            $pass = $_REQUEST["passA"];
            break;
        case 2: 
            $tipoEv = $_REQUEST["areaS"];
            if ($_REQUEST["cadValor"] == 'N/A'){
                $depto = $_REQUEST["deptoS"];
                $tEv = cTipoEv_Depto($depto);
                for ($i = 0; $i < count($tEv); $i++){
                    $tipoEv = $tEv[$i][0];
                }
            } else {
                $depto = $_REQUEST["cadValor"];
                $tEv = cTipoEv_CadValor($depto);
                for ($i = 0; $i < count($tEv); $i++){
                    $tipoEv = $tEv[$i][0];
                }                
            }
            $usuario = $_REQUEST["usuarioS"];
            $nombre = $_REQUEST["nombreS"];
            $correo = $_REQUEST["correoS"];
            $pass = $_REQUEST["passS"];            
            break;
        case 3:
            $tipoEv = $_REQUEST["areaCh"];            
            $depto = $_REQUEST["deptoCh"];             
            $usuario = $_REQUEST["usuarioCh"];
            $nombre = $_REQUEST["nombreCh"];
            $correo = $_REQUEST["correoCh"];
            $pass = $_REQUEST["passCh"];             
            break;
    }    
    
    if (empty($tipoEv)){
        $errors []= "SE DEBE SELECCIONAR UNA ÁREA";
    } else if (empty ($depto)){
        $errors []= "SE DEBE SELECCIONAR UN DEPARTAMENTO";
    } else if (empty ($usuario)){
        $errors []= "SE DEBE INGRESAR UN USUARIO";
    } else if (empty ($nombre)){
        $errors []= "SE DEBE INGRESAR NOMBRE DEL USUARIO";
    } else if (empty ($correo)){
        $errors []= "SE DEBE INGRESARR CORREO";
    } else if (empty ($pass)){
        $errors []= "SE DEBE INGRESAR UNA CONTRASEÑA";
    } 
    
    if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else { 
    
    //MANDAMOS EL INSERT A LA BASE DE DATOS    
    //iUsuario($tipoEv, $depto, $usuario, $nombre, $correo, $pass, $privilegio);
    echo $privilegio,': ',$tipoEv,', ', $depto,', ', $usuario,', ', $nombre,', ',$correo,', ',$pass;
    
    ?>
        <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php  echo "REGISTRO GUARDADO CORRECTAMENTE",'<BR>'; ?> </strong>     
        </div>
<?php
}
    
    
    
    
