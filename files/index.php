<html lang="es">
<head>
<?php

    session_start();
    date_default_timezone_set("America/Mexico_City");    
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
        $typeEv = $_SESSION['ev'];    
    } else {
        $userName = '';
        $name = '';
        $typeUser = 0;
        $typeEv = 0;
    }
    
?>
    <title>INICIO</title>
    <meta charset="UTF-8">   
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    
    <!-- ESTILOS PICKER -->
    <link rel="stylesheet" href="../css/zebra_datepicker.min.css" type="text/css">
    <link rel="stylesheet" href="../css/examples.css" type="text/css">

    <!-- LIBRERIAS DE JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
    <script src="../js/zebra_datepicker.min.js" ></script>
    <script src="../js/examples.js" ></script>
    
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" >
    <link rel="stylesheet" href="../css/style.css" >
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <?php 
        switch ($typeUser) { 
            case 0: //PAGINA PRINCIPAL SIN LOGIN
    ?>
                <script src="../js/iOffice.js" > </script>
    <?php 
                break;
            case 1: //ADMINISTRADOR
    ?>    
                <script src="../js/admin.js" > </script>
    <?php 
                break; 
            case 2: //SUPERVISOR Y GERENTE
    ?>
                <script src="../js/supeGerente.js" > </script>
    <?php
            break;
            case 3: //CHAMPION           
    ?>
                <script src="../js/champion.js" > </script>
                <script src="../js/checkList.js" > </script>               
    <?php
            break;
        }
    ?>
    <?php 
        include './modals/informativos/help.php';
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MOD AL INFORMATIVOS
        include './modals/accesos/login.php';
        include './modals/accesos/cerrarSesion.php';
        include './modals/informativos/rules.php';
        
        //MODALS PARA LA SELECCION DE AUDITORIAS
        //DE ACUERDO A LOS PRIVILEGIOS DE USUARIOS
        switch ($typeUser) {
            case 1:
                include './modals/vse/AreaAudit.php';
                include './modals/vse/DeptoAudit.php';                
                break;
            case 2: 
                include './modals/supervisor/DeptoAudit.php';
                include './modals/informativos/InfoAuditoria.php';
                break;
            case 3:
                include './modals/informativos/NoneAuditoria.php';
                include './modals/informativos/InfoAuditoria.php';
                include './modals/informativos/TipoAuditoriaCh.php';
                break;
        }
        
        include './modals/auditoria/daily/daily.php';
        include './modals/informativos/PuntosAuditoriaB.php';
        include './modals/informativos/PuntosAuditoriaR.php';
        include './modals/informativos/PuntosAuditoriaM.php';
        
        include './modals/auditoria/maintenance/mSelect.php';
        include './modals/auditoria/maintenance/mOrder.php';
        include './modals/auditoria/maintenance/mOrder2.php';
        include './modals/auditoria/maintenance/mOrder3.php';
        include './modals/auditoria/maintenance/mClean.php';
        include './modals/auditoria/maintenance/mClean2.php';
        include './modals/auditoria/maintenance/mStandar.php';
        include './modals/auditoria/maintenance/mStandar2.php';   
        include './modals/auditoria/maintenance/mDisc.php';
        
        include './modals/auditoria/office/mSelect.php';
        include './modals/auditoria/office/mOrder.php';                
        include './modals/auditoria/office/mClean.php';                
        include './modals/auditoria/office/mStandar.php';   
        include './modals/auditoria/office/mDisc.php';
        
        //DEACUERDO AL USUARIO VAMOS A PODER HACER LA AUDITORIA Y SE VAN ABRIR LOS MODAL        
        include './modals/auditoria/line/mSelect.php';
        include './modals/auditoria/line/mOrder.php';
        include './modals/auditoria/line/mOrder2.php';
        include './modals/auditoria/line/mOrder3.php';
        include './modals/auditoria/line/mClean.php';
        include './modals/auditoria/line/mClean2.php';
        include './modals/auditoria/line/mStandar.php';
        include './modals/auditoria/line/mStandar2.php';   
        include './modals/auditoria/line/mDisc.php'; 
        
        //AYUDAS VISUALES DE PREGUNTAS DE DAILY
        include './modals/informativos/daily/mDP1.php';        
        include './modals/informativos/daily/mDP2.php';
        include './modals/informativos/daily/mDP3.php';
        include './modals/informativos/daily/mDP4.php';
        include './modals/informativos/daily/mDP5.php';
        include './modals/informativos/daily/mDP6.php';
        include './modals/informativos/daily/mDP7.php';
        include './modals/informativos/daily/mDP8.php';
        include './modals/informativos/daily/mDP9.php';
        include './modals/informativos/daily/mDP10.php';
        
        //OPL
        include './modals/opl/mIOPL.php';
        
    ?>    
</head>
<body>
    <div class="navbar-lateral full-reset" >
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers" >
            <div class="logo full-reset all-tittles">                
                <img src="../assets/img/icono.png" style="width: 100%">
            </div>
            
            <div class="full-reset nav-lateral-list-menu" >
                <ul class="list-unstyled" >                    
                    <li>
                        <a><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a>
                    </li>                    
                    <?php if ($typeUser == 1) { ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-case zmdi-hc-fw"></i>&nbsp;&nbsp; Administración <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="aUAdmin.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp; Usuarios</a></li>
                            <li><a href="aAAdmin.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Línea / Área</a></li>
                            <li><a href="aAuditoria.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de Auditoría </a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php if ($typeUser != 0) { ?>
                    <li class="btn-new-audit" data-placement="bottom">
                        <a> <i class="zmdi zmdi-assignment-o zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Auditoria </a>
                    </li>                    
                    <?php } ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-trending-up  zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="rGeneral.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; General</a></li>
                            <li><a href="rArea.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Por área</a></li>
                        </ul>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-calendar zmdi-hc-fw"></i>&nbsp;&nbsp; Calendario <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li>
                                <a href="cGeneral.php"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i>&nbsp;&nbsp; Vista general </a>
                            </li>
                            <li>
                                <a href="cArea.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de auditoría </a>
                            </li>
                        </ul>
                    </li> 
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-info zmdi-hc-fw"></i>&nbsp;&nbsp; Información general <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="iFiles.php"><i class="zmdi zmdi-folder zmdi-hc-fw"></i>&nbsp;&nbsp; Información general </a></li>
                            <li>
                                <a href="iLayout.php"><i class="zmdi zmdi-grid zmdi-hc-fw"></i>&nbsp;&nbsp; Layouts </a>
                            </li>
                        </ul>
                    </li>
                    <?php if ($typeUser != 0) { ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-assignment-check zmdi-hc-fw"></i>&nbsp;&nbsp; OPL <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="pAbiertos.php"><i class="zmdi zmdi-alert-polygon zmdi-hc-fw"></i>&nbsp;&nbsp; Puntos abiertos </a></li>
                            <li><a href="pCerrados.php"><i class="zmdi zmdi-check-all zmdi-hc-fw"></i>&nbsp;&nbsp; Puntos Cerrados </a></li>
                            <li><a href="pListado.php"><i class="zmdi zmdi-view-list zmdi-hc-fw"></i>&nbsp;&nbsp; Todos </a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">                
                <?php if ($typeUser != 0){ ?>
                    <li style="color:#fff; cursor:default;">
                        <span class="all-tittles"> <?php echo $name ?></span>
                    </li>
                <?php } else { ?>
                    <li  class="tooltips-general btn-login" data-placement="bottom" >
                        <span class="all-tittles">Inicar Sesion</span>
                    </li>  
                <?php } ?>                
                <?php if ($typeUser != 0) { ?>
                    <li  class="tooltips-general exit-system-button"   data-placement="bottom" title="Salir del sistema">
                        <i class="zmdi zmdi-power"></i>
                    </li>
                <?php } ?>
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles" align="center" ><small>¡Bienvenido! </small></h1>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12" align="center">
                <img src="../assets/img/5sss.png" style="width: 73vh;">                    
            </div>
        </div>
        <br>
        <footer class="footer full-reset" >
            <div class="container-fluid">
                <div class="col-sm-12">
                    <p>Areli Pérez Calixto | Auditoría 5Ss | 2018 </p>
                    <p>© SEG Automotive México Service, S. de R. L. de C.V. 2018</p>           
                </div>                
            </div>            
        </footer>
    </div>
</body>
</html>
