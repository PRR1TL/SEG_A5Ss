<html lang="es">
<head>
<?php 
    session_start();
    date_default_timezone_set("America/Mexico_City");    
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
        $typeEv = $_SESSION['ev'];    
    } else {
        $userName = '';
        $name = '';
        $typeUser = 0;
        $typeEv = 0;
    }    
?>
    <title>REPORTE GENERAL</title>
    <meta charset="UTF-8">
   
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../js/jquery-1.11.2.min.js"><\/script>')</script>    
    
    <!-- LIBRERIAS PARA HIGHCHART -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/iOffice.js"></script>
       
    <?php 
        include './modals/informativos/help.php';
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MODAL INFORMATIVOS
        include './modals/accesos/login.php';
        include './modals/accesos/cerrarSesion.php';
        include './modals/informativos/rules.php';  
        include './modals/informativos/NoneAuditoria.php';
        include './modals/informativos/InfoAuditoria.php';
        include './modals/informativos/TipoAuditoria.php';
        
        include './modals/auditoria/maintenance/mSelect.php';
        include './modals/auditoria/maintenance/mOrder.php';
        include './modals/auditoria/maintenance/mOrder2.php';
        include './modals/auditoria/maintenance/mOrder3.php';
        include './modals/auditoria/maintenance/mClean.php';
        include './modals/auditoria/maintenance/mClean2.php';
        include './modals/auditoria/maintenance/mStandar.php';
        include './modals/auditoria/maintenance/mStandar2.php';   
        include './modals/auditoria/maintenance/mDisc.php';
        
        include './modals/auditoria/office/mSelect.php';
        include './modals/auditoria/office/mOrder.php';                
        include './modals/auditoria/office/mClean.php';                
        include './modals/auditoria/office/mStandar.php';   
        include './modals/auditoria/office/mDisc.php';
        
        //DEACUERDO AL USUARIO VAMOS A PODER HACER LA AUDITORIA Y SE VAN ABRIR LOS MODAL        
        include './modals/auditoria/line/mSelect.php';
        include './modals/auditoria/line/mOrder.php';
        include './modals/auditoria/line/mOrder2.php';
        include './modals/auditoria/line/mOrder3.php';
        include './modals/auditoria/line/mClean.php';
        include './modals/auditoria/line/mClean2.php';
        include './modals/auditoria/line/mStandar.php';
        include './modals/auditoria/line/mStandar2.php';   
        include './modals/auditoria/line/mDisc.php';
        
        include 'db/funciones.php';
    
        $anio = date("Y"); 
        //CONSULTAS DE RESULTADOS
        $cDepto = cTotalDeptos();
        //CONTADOR DE DEPARTAMENTOS    
        for ($i = 0; $i < count($cDepto); $i++){
            $depto[$i+1] = $cDepto[$i][0];
            for ($j = 1; $j < 13; $j++){
                $pDeptoMes[$i+1][$j] = 0;
                $cantAuditPlan[$i+1][$j] = 0;
                $cantAuditReal[$i+1][$j] = 0;
                $cumplimiento[$i+1][$j] = 0;
            } 
        }
        
        $month[1]= "JAN";
        $month[2]= "FEB";
        $month[3]= "MAR";
        $month[4]= "APR";
        $month[5]= "MAY";
        $month[6]= "JUN";
        $month[7]= "JUL";
        $month[8]= "AUG";
        $month[9]= "SEP";
        $month[10]= "OCT";
        $month[11]= "NOV";
        $month[12]= "DEC";        
        
        //PUNTOS OBTENIDOS POR LA AUDITORIA 
        $cPuntosAuditoria = cPAreaMes($anio);
        for ($i = 0; $i < count($cPuntosAuditoria); $i++ ){
            for ($j = 1; $j <= count($cDepto); $j++){                
                if ($cPuntosAuditoria[$i][0] == $depto[$j]){
                    $m = $cPuntosAuditoria[$i][1]; ; 
                    $pDeptoMes[$j][$m] = $cPuntosAuditoria[$i][2]; 
                }
            }
        }        
        
        //PUNTOS POR LA CANTIDAD DE AUDITORIAS PLANEADAS CONTRA LAS REALES
        $cAuditPlaneadas = cAPlanAreaMes($anio);
        for($i = 0; $i < count($cAuditPlaneadas); $i++ ){
            for ($j = 1; $j <= count($cDepto); $j++){                
                if ($cAuditPlaneadas[$i][0] == $depto[$j]){
                    $m = $cAuditPlaneadas[$i][1]; 
                    $cantAuditPlan[$j][$m] = $cAuditPlaneadas[$i][2];
                }
            }
        }
        
        $cAuditReales = cARealAreaMes($anio);
        for($i = 0; $i < count($cAuditReales); $i++ ){
            for ($j = 1; $j <= count($cDepto); $j++){                
                if ($cAuditReales[$i][0] == $depto[$j]){
                    $m = $cAuditReales[$i][1]; 
                    $cantAuditReal[$j][$m] = $cAuditReales[$i][2];
                    $cumplimiento[$j][$m] = ( $cantAuditReal[$j][$m] * 100 ) / $cantAuditPlan[$j][$m];
                }
            }
        }    
        
    ?>    
</head>
<body>
    
    <div class="navbar-lateral full-reset" >
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers" >
            <div class="logo full-reset all-tittles">                
                <img src="../assets/img/icono.png" style="width: 100%">
            </div>
            
            <div class="full-reset nav-lateral-list-menu" >
                <ul class="list-unstyled" >                    
                    <li>
                        <a><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a>
                    </li>                    
                    <?php if ($typeUser == 1) { ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-case zmdi-hc-fw"></i>&nbsp;&nbsp; Administración <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="aUAdmin.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp; Usuarios</a></li>
                            <li><a href="admin/aAAdmin.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Línea / Área</a></li>
                            <li><a href="admin/aAuditoria.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de Auditoría </a></li>
                            <li><a href="admin/aPreguntas.php"><i class="zmdi zmdi-format-list-numbered zmdi-hc-fw"></i>&nbsp;&nbsp; Preguntas</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php if ($typeUser != 0) { ?>
                    <li class="btn-new-audit" data-placement="bottom">
                        <a> <i class="zmdi zmdi-assignment-o zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Auditoria </a>
                    </li>                    
                    <?php } ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-trending-up  zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="rGeneral.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; General</a></li>
                            <li><a href="rArea.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Por área</a></li>
                        </ul>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-calendar zmdi-hc-fw"></i>&nbsp;&nbsp; Calendario <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li>
                                <a href="cGeneral.php"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i>&nbsp;&nbsp; Vista general </a>
                            </li>
                            <li>
                                <a href="cArea.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de auditoría </a>
                            </li>
                        </ul>
                    </li> 
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-info zmdi-hc-fw"></i>&nbsp;&nbsp; Información general <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="iFiles.php"><i class="zmdi zmdi-folder zmdi-hc-fw"></i>&nbsp;&nbsp; Información general </a></li>
                            <li>
                                <a href="iLayout.php"><i class="zmdi zmdi-grid zmdi-hc-fw"></i>&nbsp;&nbsp; Layouts </a>
                            </li>
                        </ul>
                    </li>
                    <?php if ($typeUser != 0) { ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-assignment-check zmdi-hc-fw"></i>&nbsp;&nbsp; OPL <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="pCerrados.php"><i class="zmdi zmdi-alert-polygon zmdi-hc-fw"></i>&nbsp;&nbsp; Puntos abiertos </a></li>
                            <li><a href="pAbiertos.php"><i class="zmdi zmdi-check-all zmdi-hc-fw"></i>&nbsp;&nbsp; Puntos Cerrados </a></li>
                            <li><a href="pListado.php"><i class="zmdi zmdi-view-list zmdi-hc-fw"></i>&nbsp;&nbsp; Todos </a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">                
                <?php if ($typeUser != 0){ ?>
                    <li style="color:#fff; cursor:default;">
                        <span class="all-tittles"> <?php echo $name ?></span>
                    </li>
                <?php } else { ?>
                    <li  class="tooltips-general btn-login" data-placement="bottom" >
                        <span class="all-tittles">Inicar Sesion</span>
                    </li>  
                <?php } ?>
                
                <?php if ($typeUser != 0) { ?>
                    <li  class="tooltips-general exit-system-button"   data-placement="bottom" title="Salir del sistema">
                        <i class="zmdi zmdi-power"></i>
                    </li>
                <?php } ?>
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles"><small>REPORTES</small></h1>
            </div>
            
            <div class="container-fluid">
                <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">
                    <li role="presentation" ><a href="rGeneral.php">Reporte general</a></li>
                    <li role="presentation" class="active" ><a href="rArea.php">Reporte por área </a></li>
                </ul>
            </div>            
            <div  class="container-fluid">
                <br>
                <form>
                    ANIO
                    <select>

                    </select>
                    &nbsp&nbsp&nbsp&nbsp MES
                    <select >
                        <?php for ($i = 1; $i < 13; $i++){ ?>
                        <option value="<?PHP ECHO $i ?>" ><?php echo $month[$i] ?></option>
                        <?php } ?>
                    </select>
                    <button> ACTUALIZAR </button>
                </form>
                
                
                <table style="width: 100%">
                    <?php                     
                    for ($i = 1; $i <= count($cDepto); $i++ ){ 
                        if (($i % 2) != 0){ ?>
                            <tr>
                    <?php } ?>
                                <td >
                                    <div id = "<?php echo "chartdiv".$i ?>" style="width: 72vh; height: 250px;">
                                        <script>
                                            var chart = AmCharts.makeChart("<?php echo "chartdiv".$i ?>", {
                                                "type": "serial",
                                                "theme": "light", 
                                                "titles": [{
                                                    "text": "<?php echo " Área: ".$depto[$i] ?>"
                                                }],
                                                "dataProvider": [
                                                <?PHP for($j = 1; $j < 13; $j++){ ?>
                                                    {
                                                    "month": "<?php echo $month[$j] ?>",
                                                    "madurez": <?php echo $pDeptoMes[$i][$j];?>,
                                                    "cumplimiento": <?php echo $cumplimiento[$i][$j];?>,
                                                    "meta": 85
                                                    },
                                                <?PHP } ?>],
                                                "graphs": [{
                                                    "fillAlphas": 0.9,
                                                    "lineAlpha": 0.2,
                                                    "type": "column",
                                                    "title": "Madurez",
                                                    "valueField": "madurez"
                                                }, {    
                                                    "bullet": "round",
                                                    "bulletBorderAlpha": 1,
                                                    "bulletColor": "#FFFFFF",
                                                    "bulletSize": 5,
                                                    "hideBulletsCount": 50,
                                                    "lineThickness": 2,
                                                    "lineColor": "#43A66E",
                                                    "type": "smoothedLine",
                                                    "title": "Cumplimiento",
                                                    "useLineColorForBulletBorder": true,
                                                    "valueField": "cumplimiento",
                                                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
                                                }, {  
                                                    "bullet": "round",
                                                    "bulletBorderAlpha": 1,
                                                    "bulletColor": "#FFFFFF",
                                                    "bulletSize": 5,
                                                    "lineThickness": 2,
                                                    "lineColor": "#E82C0C",
                                                    "type": "smoothedLine",
                                                    "dashLength": 5,
                                                    "title": "Meta",
                                                    "useLineColorForBulletBorder": true,
                                                    "valueField": "meta",
                                                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"     
                                                }],
                                                "categoryField": "month",
                                                "legend": {
                                                  "equalWidths": false,
                                                  "useGraphSettings": true,
                                                  "valueAlign": "left",
                                                  "valueWidth": 50
                                                }
                                            });
                                        </script>
                                    </div>
                                </td>
                    <?php } ?>
                </table>
            </div>            
        </div>
        
        <footer class="footer full-reset" >
            <div class="container-fluid">
                <div class="col-sm-12">
                    <p>Areli Pérez Calixto | Auditoría 5Ss | 2018 </p>
                    <p>© SEG Automotive México Service, S. de R. L. de C.V. 2018</p>           
                </div>                
            </div>            
        </footer>
    </div>
</body>
</html>