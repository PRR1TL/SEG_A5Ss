<html lang="es">
<head>
<?php 
    session_start();

    date_default_timezone_set("America/Mexico_City");
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
    } else {
        $userName = '';
        $name = '';
        $typeUser = 0;
        echo '<script>location.href = "../index.php";</script>';
    }    
?>

    <title>INICIO</title>
    <meta charset="UTF-8">
   
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <script src="../../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../../css/sweet-alert.css">
    <link rel="stylesheet" href="../../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../../css/normalize.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../../js/modernizr.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <?php 
        //INTEGRAMOS LOS MODALS DE AYUDA Y CERRAR SESION
        include '../modals/informativos/help.php';
        
        //INCLUIMOS LAS LIBRERIA DE CONSULTAS A LA BASE DE DATOS
        include '../db/funciones.php';
        
        //CONSULTAS A LA BASE DE DATOS
        $cUAdmin = cUAdministradores();
        $cUAdminSup = cUAuditoriaSup();
        $cUAdminChamp = cUAuditoriaChamp();        
    ?>
    
</head>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
           <div class="logo full-reset all-tittles">                
                <img src="../../assets/img/icono.png" style="width: 100%">
            </div>
            
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">                    
                    <li>
                        <a href="../index.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a>
                    </li>                    
                    <?php if ($typeUser == 1) { ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-case zmdi-hc-fw"></i>&nbsp;&nbsp; Administración <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="aUAdmin.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp; Usuarios</a></li>
                            <li><a href="aAAdmin.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Línea / Área</a></li>
                            <li><a href="aAuditoria.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de Auditoría </a></li>
                            <li><a href="aCVAdmin.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Cadenas de valor </a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php if ($typeUser != 0) { ?>
                    <li>
                        <a> <i class="zmdi zmdi-assignment-o zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Auditoria </a>
                    </li>
                    <?php } ?>
                    <li>
                        <!--<a href="report.html"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>-->   
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-trending-up  zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="rGeneral.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; General</a></li>
                            <li><a href="rArea.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Por área</a></li>
                        </ul>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-calendar zmdi-hc-fw"></i>&nbsp;&nbsp; Calendario <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li>
                                <a href="cGeneral.php"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i>&nbsp;&nbsp; Vista general </a>
                            </li>
                            <li>
                                <a href="cArea.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de auditoría </a>
                            </li>
                        </ul>
                    </li> 
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-info zmdi-hc-fw"></i>&nbsp;&nbsp; Información general <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="iFiles.php"><i class="zmdi zmdi-folder zmdi-hc-fw"></i>&nbsp;&nbsp; Información general </a></li>
                            <li>
                                <a href="iLayout.php"><i class="zmdi zmdi-grid zmdi-hc-fw"></i>&nbsp;&nbsp; Layouts </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">                
                <?php if ($typeUser != 0){ ?>
                    <li style="color:#fff; cursor:default;">
                        <span class="all-tittles"> <?php echo $userName ?></span>
                    </li>
                <?php } else { ?>
                    <li  class="tooltips-general btn-login" data-placement="bottom" >
                        <span class="all-tittles">Inicar Sesion</span>
                    </li>  
                <?php } ?>
                    
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles"><small>Administración</small></h1>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">
                <li role="presentation" class="active"><a href="aUAdmin.php">Usuarios</a></li>
                <li role="presentation" ><a href="aAAdmin.php">Línea / Área </a></li>
                <li role="presentation"><a href="aAuditoria.php">Auditoría</a></li>
                <li role="presentation"><a href="aPreguntas.php">Preguntas</a></li>
            </ul>
        </div>
        
        <!--APARTADO DE TABLA DE LISTADO-->
        <div class="container-fluid">
            <h2 class="text-center all-tittles">Lista de administradores</h2>
            <div class="div-table">
                
                <div class="div-table-row div-table-head">
                    <div class="div-table-cell">Dep</div>
                    <!--<div class="div-table-cell">Área</div>-->
                    <div class="div-table-cell">Usuario</div>
                    <div class="div-table-cell">Nombre</div>
                    <!--<div class="div-table-cell">Tipo Usuario</div>-->
                    <div class="div-table-cell">Actualizar</div>
                    <div class="div-table-cell">Eliminar</div>
                </div>  
                
                <?PHP for ($i = 0; $i < count($cUAdmin); $i++) { ?>
                <div class="div-table-row">
                    <div class="div-table-cell"><?php echo $cUAdmin[$i][0] ?></div>
                    <div class="div-table-cell"><?php echo $cUAdmin[$i][1] ?></div>
                    <div class="div-table-cell"><?php echo $cUAdmin[$i][2] ?></div>
                    <!--<div class="div-table-cell"><?php echo $cUAdmin[$i][1] ?></div>-->
                    <!--<div class="div-table-cell"><?php echo 'AMINISTRADOR'?></div>-->
                    
                    <div class="div-table-cell">
                        <button class="btn btn-success"><i class="zmdi zmdi-refresh"></i></button>
                    </div>
                    <div class="div-table-cell">
                        <button class="btn btn-danger"><i class="zmdi zmdi-delete"></i></button>
                    </div>
                </div>
                <?php }?>                
            </div>                  
        </div>
        
        <!--APARTADO PARA SUPERVISORES-->
        <div class="container-fluid">
            <h2 class="text-center all-tittles">Lista de Supervisores</h2>
            <div class="div-table">
                
                <div class="div-table-row div-table-head">
                    <div class="div-table-cell">Cadena de valor</div>
                    <!--<div class="div-table-cell">Área</div>-->
                    <div class="div-table-cell">Usuario</div>
                    <div class="div-table-cell">Nombre</div>
                    <!--<div class="div-table-cell">Tipo Usuario</div>-->
                    <div class="div-table-cell">Actualizar</div>
                    <div class="div-table-cell">Eliminar</div>
                </div>  
                
                <?PHP 
                for ($i = 0; $i < count($cUAdminSup); $i++) { 
                    ?>
                <div class="div-table-row">
                    <div class="div-table-cell"><?php echo $cUAdminSup[$i][0] ?></div>
                    <div class="div-table-cell"><?php echo $cUAdminSup[$i][1] ?></div>
                    <div class="div-table-cell"><?php echo $cUAdminSup[$i][2] ?></div>
                    <!--<div class="div-table-cell"><?php echo $cUAdminSup[$i][1] ?></div>-->
                    <!--<div class="div-table-cell"><?php echo 'AMINISTRADOR'?></div>-->
                    
                    <div class="div-table-cell">
                        <button class="btn btn-success"><i class="zmdi zmdi-refresh"></i></button>
                    </div>
                    <div class="div-table-cell">
                        <button class="btn btn-danger"><i class="zmdi zmdi-delete"></i></button>
                    </div>
                </div>
                <?php }?>                
            </div>                  
        </div>        
        
        <!--APARTADO DE TABLA DE LISTADO-->
        <div class="container-fluid">
            <h2 class="text-center all-tittles">Lista de usuarios (Champion / T. Leader)</h2>
            <div class="div-table">                
                <div class="div-table-row div-table-head">
                    <div class="div-table-cell">Dep</div>
                    <!--<div class="div-table-cell">Área</div>-->
                    <div class="div-table-cell">Usuario</div>
                    <div class="div-table-cell">Nombre</div>
                    <div class="div-table-cell">Tipo Auditoria</div>   
                    <div class="div-table-cell">Actualizar</div>
                    <div class="div-table-cell">Eliminar</div>
                </div>  
                
                <?PHP for ($i = 0; $i < count($cUAdminChamp); $i++){?>
                <div class="div-table-row ">
                    <div class="div-table-cell"><?php echo $cUAdminChamp[$i][0] ?></div>
                    <div class="div-table-cell"><?php echo $cUAdminChamp[$i][1] ?></div>
                    <div class="div-table-cell"><?php echo $cUAdminChamp[$i][2] ?></div>
                    <!--<div class="div-table-cell"><?php echo $cUAdminChamp[$i][1] ?></div>-->
                    <div class="div-table-cell">
                        <?php 
                        switch ($cUAdminChamp[$i][3]){ 
                            case 1:
                                echo 'Almacen' ;
                                break;
                            case 2:
                                echo 'Laboratorio';
                                break;
                            case 3:
                                echo 'Línea';
                                break;
                            case 4:
                                echo 'Mantenimiento';
                                break;
                            case 5:
                                echo 'Oficia';
                                break;
                        }
                        ?>
                    </div>
                    
                    <div class="div-table-cell">
                        <button class="btn btn-success"><i class="zmdi zmdi-refresh"></i></button>
                    </div>
                    <div class="div-table-cell">
                        <button class="btn btn-danger"><i class="zmdi zmdi-delete"></i></button>
                    </div>
                </div>
                <?php }?>                
            </div>                  
        </div>
        
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Areli Pérez Calixto | Auditoría 5Ss | 2018 </p>
                        <p>© SEG Automotive México Service, S. de R. L. de C.V. 2018</p>           
                    </div>
                </div>
            </div>            
        </footer>
    </div>
</body>
</html>