<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<form id="logiin" method="post" >
    <div class="modal" tabindex="-1" role="dialog" id="ModalLogin" >
        <div class="modal-dialog" style=" width: 60vh" >
            <div class="modal-content" >
                <div class="modal-header" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true" >&times;</span></button>
                    <h4 class="modal-title text-center all-tittles" >INICIO DE SESSION</h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;" >
                    <div id="datos_ajax" ></div>
                    <div class="row" >
                        <div class="col-sm-12" >
                            <div class="col-sm-4" >
                                <span style="margin-left: 8vh" > Usuario: </span>
                            </div>
                            <div class="col-sm-6" >
                                <input id="username" name="username" type="text" >
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row" >
                        <div class="col-sm-12" >
                            <div class="col-sm-4" >
                                <span style="margin-left: 4vh" >Contraseña: </span>
                            </div>
                            <div class="col-sm-6" >
                                <input id="password" name="password" type="password" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Aceptar </button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" >Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</form>
