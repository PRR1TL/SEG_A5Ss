<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script>
    
    //VALIDACIONES PARA EL FORMATO DEL INPUT
    function IsNumeric(valor){ 
        var log=valor.length; 
        var sw="S"; 
        
        for (x=0; x<log; x++){ 
            v1 = valor.substr(x,1); 
            v2 = parseInt(v1); 
        //Compruebo si es un valor numérico 
            if (isNaN(v2)) { 
                sw= "N";
            } 
            console.log(sw);
        } 
        
        if (sw=="S") {
            return true;
        } else {
            return false; 
        } 
    } 
    
    var fNow = new Date().toISOString().slice(0,10);    
    var fEntra;
    
    var primerslap=false; 
    var segundoslap=false; 
    function formateafecha(fecha) { 
        var long = fecha.length; 
        //console.log(long);
        var dia; 
        var mes; 
        var ano; 
        if ((long>=2) && (primerslap==false)) { 
            dia=fecha.substr(0,2); 
            if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { 
                fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); 
                primerslap=true; 
            } else { 
                fecha=""; 
                primerslap=false;
            } 
        } else { 
            dia = fecha.substr(0,1); 
            if (IsNumeric(dia)==false){ 
                fecha="";
            } 
            if ((long<=2) && (primerslap=true)) {
                fecha=fecha.substr(0,1); 
                primerslap=false; 
            } 
        } 
        
        if ((long>=5) && (segundoslap==false)) { 
            mes = fecha.substr(3,2); 
            if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { 
                fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); 
                segundoslap=true; 
            } else { 
                fecha=fecha.substr(0,3); 
                segundoslap=false;
            } 
        } else { 
            if ((long <= 5) && (segundoslap = true)) { 
                fecha=fecha.substr(0,4); 
                segundoslap=false; 
            } 
        } 
        
        if (long >= 7) { 
            ano=fecha.substr(6,4); 
            if (IsNumeric(ano)==false) { 
                fecha=fecha.substr(0,6); 
            } else { 
                if (long==10) { 
                    if ((ano==0) || (ano<1900) || (ano>2100)) { 
                        fecha=fecha.substr(0,6); 
                    } 
                } 
            } 
        } 
        
        if (long >= 10) {        
            fecha=fecha.substr(0,10); 
            dia=fecha.substr(0,2); 
            mes=fecha.substr(3,2); 
            ano=fecha.substr(6,4); 
            
            fEntra = ano+'-'+mes+'-'+dia;
            //console.log(fEntra);
            
            if (fEntra > fNow){
                console.log("FECHA ENTRADA ES MAYOR A FECHA ACTUAL :) ");
            } else {
                console.log("FECHA ACTUAL ES MAYOR A FECHA ENRADA :( ");
            }
            // Año no viciesto y es febrero y el dia es mayor a 28 
            if ( (ano%4 != 0) && (mes == 02) && (dia > 28) ) { 
                fecha=fecha.substr(0,2)+"/"; 
            } 
        } 
        return (fecha); 
    }
    
    function cpO10() {
        $('#hallazgo10').val("");
        $('#accion10').val("");
        $('#responsable10').val("");
        $('#soporte10').val(""); 
        var x = document.getElementById("pO10").value;
        if (x <= 2 ){
            pnlOplO10.className = 'si'; 
        } else {
            pnlOplO10.className = 'no'; 
        }
    }
    
</script>

<form id="fLDiscO" method="post"> 
    <div class="modal fade" tabindex="-1" role="dialog" id="mLDiscO">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title text-center all-tittles">DISCIPLINA (OFICINA)</h4>
                    <h6 class="modal-title text-left all-tittles" style="top: -15px">*Evaluación: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0 = No se cumple &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        1 = De 6 a 7 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        2 = De 4 a 5 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        3 = De 1 a 3 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        4 = Se cumple
                    </h6>
                </div>
                <div class="modal-body">
                    <div id="datos_ajaxDIO"></div>
                    <div class="row">
                        <div class="col-sm-11">
                            <label>10. Se realizaron las acciones definidas trás la última auditoría 5S's en el plazo establecido.<a style="font-size: 9px" data-toggle="modal" href="#iO10">INFO</a></label>
                            <br>
                        </div>
                        <div class="col-sm-1" style="margin-top: -4px">
                            <select id="pO10" name="p10" onchange="cpO10()">
                                <<option value="5" selected disabled > - </option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="panel no" id="pnlOplO10" style = "margin-top: 4vh">
                            <div class="panel panel-info" style="width: 95%; margin-left: 3%">
                                <div class="panel-heading">Opl 10</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" >
                                        <div class="btn-group" role="group">
                                            <span ><b> Hallazgo: </b></span> 
                                            <input id="hallazgo10" name="hallazgo10" maxlength="150" style="width: 84%;" placeholder="Hallazgo" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Acción: </b></span>
                                            <input id="accion10" name="accion10" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                        </div>                                        
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px">
                                        <div class="btn-group" role="group">
                                            <span ><b> Responsable: </b></span> 
                                            <input id="responsable10" name="responsable10" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Soporte: </b></span>
                                            <input id="soporte10" name="soporte10" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                        </div>                                        
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Inicio: </b></span>    
                                            <input id="fInicio10" name="fInicio10" maxlength="1710" style=" width: 45%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Compromiso: </b></span>
                                            <input id="fCompromiso10" name="fCompromiso10" maxlength="150" style=" width: 45%;" value="<?php echo $fProp?>" onKeyUp = "this.value=formateafecha(this.value);" />
                                        </div>                                        
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>                            
                    <!--Descripción rápida-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" > Finalizar &nbsp;<i class="zmdi zmdi-check zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>    
    
<!--APARTADO PAR LOS MODAL INFORMATIVOS-->
<div class="modal fade" tabindex="-1" id="iO10" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Punto a Revisar</h4>
            </div>
            <div class="modal-body">
                Verificar que el departamento tenga un rol de 5S's para mobiliario/áreas compartidas y que se lleve a cabo.
                <br>
                <img src="./imagenes/oficinas/IMG-20181127-WA0003.jpg">
            </div>            
        </div>
    </div>
</div>
