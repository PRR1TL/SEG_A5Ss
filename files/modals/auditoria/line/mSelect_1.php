<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('d/m/Y');
$fNow = date('Y-m-d');
$fProp = date('d/m/Y' ,strtotime('+1 day', strtotime($fNow)));

?>

<script>
    
    function cambioOpcionesIEvento(){ 
            var id = document.getElementById('p1').value;
            var dataString = 'action='+ id;
        if (id === 4){
            btnOpl1.className = 'si';
        }
    }

     //VALIDACIONES PARA EL FORMATO DEL INPUT
    function IsNumeric(valor){ 
        var log = valor.length; 
        var sw = "S"; 
        
        for (x=0; x<log; x++){ 
            v1=valor.substr(x,1); 
            v2 = parseInt(v1); 
        //Compruebo si es un valor numérico 
            if (isNaN(v2)) { 
                sw= "N";
            } 
            console.log(sw);
        } 
        
        if (sw=="S") {
            return true;
        } else {
            return false; 
        } 
    } 
    
    var fNow = new Date().toISOString().slice(0,10);    
    var fEntra;
    
    var primerslap=false; 
    var segundoslap=false; 
    
    function formateafecha(fecha) { 
        var long = fecha.length; 
        //console.log(long);
        var dia; 
        var mes; 
        var ano; 
        if ((long>=2) && (primerslap==false)) { 
            dia=fecha.substr(0,2); 
            if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { 
                fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); 
                primerslap=true; 
            } else { 
                fecha=""; 
                primerslap=false;
            } 
        } else { 
            dia = fecha.substr(0,1); 
            if (IsNumeric(dia)==false){ 
                fecha="";
            } 
            if ((long<=2) && (primerslap=true)) {
                fecha=fecha.substr(0,1); 
                primerslap=false; 
            } 
        } 
        
        if ((long>=5) && (segundoslap==false)) { 
            mes = fecha.substr(3,2); 
            if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { 
                fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); 
                segundoslap=true; 
            } else { 
                fecha=fecha.substr(0,3); 
                segundoslap=false;
            } 
        } else { 
            if ((long <= 5) && (segundoslap = true)) { 
                fecha=fecha.substr(0,4); 
                segundoslap=false; 
            } 
        } 
        
        if (long >= 7) { 
            ano=fecha.substr(6,4); 
            if (IsNumeric(ano)==false) { 
                fecha=fecha.substr(0,6); 
            } else { 
                if (long==10) { 
                    if ((ano==0) || (ano<1900) || (ano>2100)) { 
                        fecha=fecha.substr(0,6); 
                    } 
                } 
            } 
        } 
        
        if (long >= 10) {        
            fecha = fecha.substr(0,10); 
            dia = fecha.substr(0,2); 
            mes = fecha.substr(3,2); 
            ano = fecha.substr(6,4); 
            
            fEntra = ano+'-'+mes+'-'+dia;
            //console.log(fEntra);
            
            if (fEntra > fNow){
                console.log("FECHA ENTRADA ES MAYOR A FECHA ACTUAL :) ");
            } else {
                console.log("FECHA ACTUAL ES MAYOR A FECHA ENRADA :( ");
            }
            // Año no viciesto y es febrero y el dia es mayor a 28 
            if ( (ano%4 != 0) && (mes == 02) && (dia > 28) ) { 
                fecha=fecha.substr(0,2)+"/"; 
            } 
        } 
        return (fecha); 
    }
    
    function cpL1() {
        $('#hallazgo1').val("");
        $('#accion1').val("");
        $('#responsable1').val("");
        $('#soporte1').val("");
        var x = document.getElementById("pL1").value;
        if (x <= 2 ){
            pnlOplL1.className = 'si';
        } else {             
            pnlOplL1.className = 'no';           
        }
    }
    
    function cpL2() {
        $('#hallazgo2').val("");
        $('#accion2').val("");
        $('#responsable2').val("");
        $('#soporte2').val(""); 
        var x = document.getElementById("pL2").value;
        if (x <= 2 ){
            pnlOplL2.className = 'si';
        } else {            
            pnlOplL2.className = 'no';
        }
    }

</script>

<form id="fLSelectL" method="post">
    <div class="modal fade" tabindex="-1" role="dialog" id="mLSelectL">
        <div class="modal-dialog" style="width: 180vh">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title text-center all-tittles">SELECCIONAR (LINEA) </h4>
                    <h6 class="modal-title text-left all-tittles" style=" font-size: 18px; top: -15px">*Evaluación: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0 = No se cumple &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        1 = De 6 a 7 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        2 = De 4 a 5 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        3 = De 1 a 3 desviaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
                        4 = Se cumple
                    </h6>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="datos_ajaxSEL"></div>
                    <div class="row">
                        <div class="col-sm-11">
                            <label>1. El asociado conoce las 5S´s y cuáles son los beneficios de su aplicación en el área de trabajo. <a style="font-size: 9px" data-toggle="modal" href="#iL1">INFO</a></label>
                            <br>                                                    
                        </div>
                        <div class="col-sm-1">
                            <select id="pL1" name="p1" onchange="cpL1()" >
                                <option value="5" selected disabled > - </option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="panel no" id="pnlOplL1" style="margin-top: 5vh; height: 170px">
                            <div class="panel panel-info" style="width: 95%; margin-left: 3%">
                                <div class="panel-heading">Opl 1</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" >
                                        <div class="btn-group" role="group">
                                            <span ><b> Hallazgo: </b></span> 
                                            <input id="hallazgo1" name="hallazgo1" maxlength="150" style="width: 84%;" placeholder="Hallazgo" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Acción: </b></span>
                                            <input id="accion1" name="accion1" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                        </div>                                        
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px">
                                        <div class="btn-group" role="group">
                                            <span ><b> Responsable: </b></span> 
                                            <input id="responsable1" name="responsable1" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Soporte: </b></span>
                                            <input id="soporte1" name="soporte1" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                        </div>                                        
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Inicio: </b></span>    
                                            <input id="fInicio1" name="fInicio1" maxlength="171" style=" width: 45%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Compromiso: </b></span>
                                            <input id="fCompromiso1" name="fCompromiso1" maxlength="150" style=" width: 45%;" value="<?php echo $fProp?>" onKeyUp = "this.value=formateafecha(this.value);" />
                                        </div>                                        
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>    
                    <hr size="50px" width="95%" align="center" color="black"/>
                    <div class="row" style="margin-top: 20px">
                        <div class="col-sm-11">
                            <label> 2. Las áreas de trabajo están libres de piezas, máquinas/objetos innecesarios y/o defectuosos, incluyendo artículos personales no necesarios.<a style="font-size: 9px" data-toggle="modal" href="#iL2">INFO</a></label>
                            <br>
                        </div>
                        <div class="col-sm-1">
                            <select id="pL2" name="p2" onchange="cpL2()">
                                <option value="5" selected disabled > - </option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="panel no" id="pnlOplL2" style = "margin-top: 5vh; ">
                            <div class="panel panel-info" style="width: 95%; margin-left: 3%">
                                <div class="panel-heading">Opl 2</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group" >
                                        <div class="btn-group" role="group">
                                            <span ><b> Hallazgo: </b></span> 
                                            <input id="hallazgo2" name="hallazgo2" maxlength="150" style="width: 84%;" placeholder="Hallazgo" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Acción: </b></span>
                                            <input id="accion2" name="accion2" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                        </div>                                        
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px">
                                        <div class="btn-group" role="group">
                                            <span ><b> Responsable: </b></span> 
                                            <input id="responsable2" name="responsable2" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Soporte: </b></span>
                                            <input id="soporte2" name="soporte2" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                        </div>                                        
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Inicio: </b></span>    
                                            <input id="fInicio2" name="fInicio2" maxlength="172" style=" width: 45%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                        </div>
                                        <div class="btn-group" role="group">
                                            <span ><b> Fecha Compromiso: </b></span>
                                            <input id="fCompromiso2" name="fCompromiso2" maxlength="150" style=" width: 45%;" value="<?php echo $fProp?>" onKeyUp = "this.value=formateafecha(this.value);" />
                                        </div>                                        
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Descripción rápida-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>

<!--APARTADO PARA LOS MODAL INFORMATIVOS-->
<div class="modal fade" tabindex="-1" id="iL1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Punto a Revisar</h4>
            </div>
            <div class="modal-body">
                Máquinas, áreas de trabajo, gavetas, vitrinas, contenedores para piezas, cable, pisos, acrílicos, perfiles, fugas, etc.
                
            </div>            
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="iL2" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Punto a Revisar</h4>
            </div>
            <div class="modal-body">
                Suelo, paredes, ventanas, puertas, piezas en el piso, equipos de trabajo, acrílicos cubre materiales, no cables sueltos, etc.
                <br>
                <img src="./imagenes/linea/2.png" style="max-width:100%;width:auto;height:auto;" >
            </div>            
        </div>
    </div>
</div>

