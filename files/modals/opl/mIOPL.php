<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('d/m/Y');
$fNow = date('Y-m-d');
$fProp = date('d/m/Y' ,strtotime('+1 day', strtotime($fNow)));

?>

<script>    
    function cambioOpcionesIEvento(){ 
            var id = document.getElementById('p1').value;
            var dataString = 'action='+ id;
        if (id === 4){
            btnOpl1.className = 'si';
        }
    }

     //VALIDACIONES PARA EL FORMATO DEL INPUT
    function IsNumeric(valor){ 
        var log = valor.length; 
        var sw = "S"; 
        
        for (x=0; x<log; x++){ 
            v1=valor.substr(x,1); 
            v2 = parseInt(v1); 
        //Compruebo si es un valor numérico 
            if (isNaN(v2)) { 
                sw= "N";
            } 
            console.log(sw);
        } 
        
        if (sw=="S") {
            return true;
        } else {
            return false; 
        } 
    } 
    
    var fNow = new Date().toISOString().slice(0,10);    
    var fEntra;
    
    var primerslap=false; 
    var segundoslap=false; 
    
    function formateafecha(fecha) { 
        var long = fecha.length; 
        //console.log(long);
        var dia; 
        var mes; 
        var ano; 
        if ((long>=2) && (primerslap==false)) { 
            dia=fecha.substr(0,2); 
            if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { 
                fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); 
                primerslap=true; 
            } else { 
                fecha=""; 
                primerslap=false;
            } 
        } else { 
            dia = fecha.substr(0,1); 
            if (IsNumeric(dia)==false){ 
                fecha="";
            } 
            if ((long<=2) && (primerslap=true)) {
                fecha=fecha.substr(0,1); 
                primerslap=false; 
            } 
        } 
        
        if ((long>=5) && (segundoslap==false)) { 
            mes = fecha.substr(3,2); 
            if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { 
                fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); 
                segundoslap=true; 
            } else { 
                fecha=fecha.substr(0,3); 
                segundoslap=false;
            } 
        } else { 
            if ((long <= 5) && (segundoslap = true)) { 
                fecha=fecha.substr(0,4); 
                segundoslap=false; 
            } 
        } 
        
        if (long >= 7) { 
            ano=fecha.substr(6,4); 
            if (IsNumeric(ano)==false) { 
                fecha=fecha.substr(0,6); 
            } else { 
                if (long==10) { 
                    if ((ano==0) || (ano<1900) || (ano>2100)) { 
                        fecha=fecha.substr(0,6); 
                    } 
                } 
            } 
        } 
        
        if (long >= 10) {        
            fecha = fecha.substr(0,10); 
            dia = fecha.substr(0,2); 
            mes = fecha.substr(3,2); 
            ano = fecha.substr(6,4); 
            
            fEntra = ano+'-'+mes+'-'+dia;
            //console.log(fEntra);
            
            if (fEntra > fNow){
                console.log("FECHA ENTRADA ES MAYOR A FECHA ACTUAL :) ");
            } else {
                console.log("FECHA ACTUAL ES MAYOR A FECHA ENRADA :( ");
            }
            // Año no viciesto y es febrero y el dia es mayor a 28 
            if ( (ano%4 != 0) && (mes == 02) && (dia > 28) ) { 
                fecha=fecha.substr(0,2)+"/"; 
            } 
        } 
        return (fecha); 
    }
    
    function cpL1() {
        $('#hallazgo1').val("");
        $('#accion1').val("");
        $('#responsable1').val("");
        $('#soporte1').val("");
        var x = document.getElementById("pL1").value;
        if (x <= 2 ){
            pnlOplL1.className = 'si';
        } else {             
            pnlOplL1.className = 'no';           
        }
    }
    
    function cpL2() {
        $('#hallazgo2').val("");
        $('#accion2').val("");
        $('#responsable2').val("");
        $('#soporte2').val(""); 
        var x = document.getElementById("pL2").value;
        if (x <= 2 ){
            pnlOplL2.className = 'si';
        } else {            
            pnlOplL2.className = 'no';
        }
    }
</script>

<form id="fIOPL" method="post" >
    <div class="modal fade" tabindex="-1" role="dialog" id="mIOPL">
        <div class="modal-dialog" style="width: 180vh">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center all-tittles"> OPL </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="datos_ajaxSEL"></div>
                    <div class="panel no" id="pnlOplL1" >
                        <div class="panel panel-info" style="width: 95%; margin-left: 3%">
                            <div class="panel-heading" >Opl </div>
                            <div class="panel-body" >
                                <div class="btn-group btn-group-justified" role="group" >
                                    <div class="btn-group" role="group" >
                                        <span ><b> Hallazgo: </b></span> 
                                        <input id="hallazgo2" name="hallazgo2" maxlength="150" style="width: 84%;" placeholder="Hallazgo" type="text" />
                                    </div>
                                    <div class="btn-group" role="group" >
                                        <span ><b> Acción: </b></span>
                                        <input id="accion2" name="accion2" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                    </div>                                        
                                </div>
                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                    <div class="btn-group" role="group" >
                                        <span ><b> Responsable: </b></span> 
                                        <input id="responsable2" name="responsable2" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                    </div>
                                    <div class="btn-group" role="group" >
                                        <span ><b> Soporte: </b></span>
                                        <input id="soporte2" name="soporte2" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                    </div>                                        
                                    <div class="btn-group" role="group" >
                                        <span ><b> Fecha Inicio: </b></span>    
                                        <input id="fInicio2" name="fInicio2" maxlength="172" style=" width: 45%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                    </div>
                                    <div class="btn-group" role="group">
                                        <span ><b> Fecha Compromiso: </b></span>
                                        <input id="fCompromiso2" name="fCompromiso2" maxlength="150" style=" width: 45%;" value="<?php echo $fProp?>" onKeyUp = "this.value=formateafecha(this.value);" />
                                    </div>                                        
                                </div>                                    
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>


