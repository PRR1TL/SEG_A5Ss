<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

?>

<style>
    .no {
        display:none;
    }
    .si {
        display:block;
    }
</style>

<script>    
    function fPrivilegio() {        
        var x = document.getElementById("privilegio").value;        
        switch(x){
            case '1': //ADMINISTRADOR
                pnlAdmin.className = 'si';
                pnlSup.className = 'no';
                pnlChamp.className = 'no';                
                break;                
            case '2'://SUP / GERENTES
                pnlAdmin.className = 'no';
                pnlSup.className = 'si';
                pnlChamp.className = 'no';                
                break;
            case '3': //CHAMPION
                pnlAdmin.className = 'no';
                pnlSup.className = 'no';
                pnlChamp.className = 'si';
                break;
        }        
    }
    
    function fCValorS(){
        var id = document.getElementById('areaS').value;
        var dataString = 'action='+ id;
        pnlDepto.className = 'no';

        $.ajax({
            url: './db/admin/cadValor.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#cadValor").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
    }
        
    function fDeptos(){
        var id = document.getElementById('cadValor').value;
        var area = document.getElementById('areaS').value;
        var dataString = 'cValor='+ id+', '+area;
        
        if (id === 'N/A'){
            pnlDepto.className = 'si';
            $("#deptoS").empty();
            $.ajax({
                url: './db/admin/deptos.php',
                data: dataString,
                cache: false,
                success: function(r){
                    $("#deptoS").html(r);
                } 
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });
        } else {
            pnlDepto.className = 'no';            
        }
    }
    
    function fDeptosCh(){        
        var area = document.getElementById('areaCh').value;
        var dataString = 'cValor='+area;        
        $("#deptoCh").empty();
        $.ajax({
            url: './db/admin/deptos.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#deptoCh").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });        
    }  
</script>

<form id="fIUsuario">
    <div class="modal fade" id="mIUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel" align="center">NUEVO USUARIO</h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto; ">
                    <div id="datos_ajaxIU"></div>
                    <div class="form-group">
                        <label class="control-label">Privilegio: </label>
                        <select id="privilegio" name="privilegio" onchange="fPrivilegio()" type ="text" class="form-control" style="width: 30vh">
                            <option value=" - " selected disabled > - </option>
                            <option value="1">Administrador</option>
                            <option value="2">Supervisor / Gerente</option>
                            <option value="3">Champion</option>
                        </select>             
                    </div>
                    <hr>                        
                    <div id="pnlSup" class="no">
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="areaS" name="areaS" onchange="fCValorS()" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="0" selected disabled> - </option>
                                <option value="1">Piso</option>
                                <option value="2">Oficina</option>
                            </select>

                            <label class="control-label" style="margin-top: -9vh; margin-left: 38vh" >Cadena de valor: </label>
                            <select id="cadValor" name="cadValor" onchange="fDeptos()" type ="text" class="form-control" style="width: 30vh; margin-top: -4.5vh; margin-left: 38vh" >
                            </select> 
                            
                            <div id="pnlDepto" name="pnlDepto" class="no">
                                <label class="control-label" style="margin-top: -8.5vh; margin-left: 77vh">Departamento: </label>
                                </select>                                
                                    <select id="deptoS" name="deptoS" type ="text" class="form-control" style="width: 30vh; margin-top: -4vh; margin-left: 77vh">
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nombre0" class="control-label">Usuario:</label>
                            <label for="nombre0" class="control-label" style="width: 45%; margin-top: -25px; margin-left: 32vh">Nombre:</label>
                            <input type="text" class="form-control" id="usuario" name="usuarioS" onkeypress="return permite(event,'car')" minlength="5" maxlength="6" style="width: 30vh" >
                            <input type="text" class="form-control" id="nombre" name="nombreS" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 68vh; margin-top: -32px; margin-left: 38vh">
                        </div>
                        <div class="form-group">
                            <label for="moneda0" class="control-label">Correo: </label>
                            <input type="text" class="form-control" id="correoS" name="correoS" onkeypress="return permite(event,'num')" maxlength="100" style="width: 68vh" >
                            <label for="moneda0" class="control-label " style="margin-top: -9vh; margin-left: 72vh;">Contraseña: </label>
                            <input type="password" class="form-control" id="passS" name="passS" minlength="5" onkeypress="return permite(event,'num')" maxlength="100" style="width: 34vh; margin-left: 72vh; margin-top: -32px" >
                        </div>                        
                    </div>
                    
                    <div id="pnlChamp" class="no">
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="areaCh" name="areaCh" onchange="fDeptosCh()" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="0" selected disabled> - </option>
                                <option value="1">Piso</option>
                                <option value="2">Oficina</option>
                            </select> 
                            <label class="control-label" style="margin-top: -8.5vh; margin-left: 77vh">Departamento: </label>
                            </select>                                
                                <select id="deptoCh" name="deptoCh" type ="text" class="form-control" style="width: 30vh; margin-top: -4vh; margin-left: 77vh">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nombre0" class="control-label">Usuario:</label>
                            <label for="nombre0" class="control-label" style="width: 45%; margin-top: -25px; margin-left: 32vh">Nombre:</label>
                            <input type="text" class="form-control" id="usuarioCh" name="usuarioCh" onkeypress="return permite(event,'car')" minlength="5" maxlength="6" style="width: 30vh" >
                            <input type="text" class="form-control" id="nombreCh" name="nombreCh" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 68vh; margin-top: -32px; margin-left: 38vh">
                        </div>
                        <div class="form-group">
                            <label for="moneda0" class="control-label">Correo: </label>
                            <input type="text" class="form-control" id="correoCh" name="correoCh" onkeypress="return permite(event,'num')" maxlength="100" style="width: 68vh" >
                            <label for="moneda0" class="control-label " style="margin-top: -9vh; margin-left: 72vh;">Contraseña: </label>
                            <input type="password" class="form-control" id="passCh" name="passCh" minlength="5" onkeypress="return permite(event,'num')" maxlength="100" style="width: 34vh; margin-left: 72vh; margin-top: -32px" >
                        </div>
                    </div>    
                    
                    <div id="pnlAdmin" class="no">
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="tipoA" name="tipoA" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="1">Administrador total </option>
                                <option value="2">Champion Área</option>
                            </select>                           
                        </div>
                        <div class="form-group">
                            <label for="nombre0" class="control-label">Usuario:</label>
                            <label for="nombre0" class="control-label" style="width: 45%; margin-top: -25px; margin-left: 32vh">Nombre:</label>
                            <input type="text" class="form-control" id="usuarioA" name="usuarioA" onkeypress="return permite(event,'car')" minlength="5" maxlength="6" style="width: 30vh" >
                            <input type="text" class="form-control" id="nombreA" name="nombreA" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 68vh; margin-top: -32px; margin-left: 38vh">
                        </div>
                        <div class="form-group">
                            <label for="moneda0" class="control-label">Correo: </label>
                            <input type="text" class="form-control" id="correoA" name="correoA" onkeypress="return permite(event,'num')" maxlength="100" style="width: 68vh" >
                            <label for="moneda0" class="control-label " style="margin-top: -9vh; margin-left: 72vh;">Contraseña: </label>
                            <input type="password" class="form-control" id="passA" name="passA" minlength="5" onkeypress="return permite(event,'num')" maxlength="100" style="width: 34vh; margin-left: 72vh; margin-top: -32px" >
                        </div>
                    </div>                     
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>



