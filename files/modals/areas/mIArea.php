<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

?>

<style>
    .no {
        display:none;
    }
    .si {
        display:block;
    }
</style>

<script>    
    function fPrivilegio() {        
        var x = document.getElementById("privilegio").value;        
        switch(x){
            case '1': //ADMINISTRADOR
                pnlAdmin.className = 'si';
                pnlSup.className = 'no';
                pnlChamp.className = 'no';                
                break;                
            case '2'://SUP / GERENTES
                pnlAdmin.className = 'no';
                pnlSup.className = 'si';
                pnlChamp.className = 'no';                
                break;
            case '3': //CHAMPION
                pnlAdmin.className = 'no';
                pnlSup.className = 'no';
                pnlChamp.className = 'si';
                break;
        }        
    }
    
    function fCValorS(){
        var id = document.getElementById('area').value;
        var dataString = 'action='+ id;
        pnlDepto.className = 'no';

        $.ajax({
            url: './db/admin/cadValor.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#cadValor").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
    }
        
    function fDeptos(){
        console.log("fDepto");
        var id = document.getElementById('cadValor').value;
        var area = document.getElementById('area').value;
        var dataString = 'cValor='+ id+', '+area;
        $("#depto").val(0);
        pnlDepto.className = 'no';
        
        switch(area){
            case "1":
                if (id === 'N/A'){
                    pnlDepto.className = 'si';                    
                } 
                break;            
        }        
    }    
    
</script>

<form id="fIArea">
    <div class="modal fade" id="mIArea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel" align="center">NUEVA AREA / LINEA</h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto; ">
                    <div id="datos_ajaxIU"></div>                                          
                    <div class="form-group">
                        <label class="control-label" >Área: </label>
                        <select id="area" name="area" onchange="fCValorS()" type = "text" class="form-control" style="width: 30vh; ">
                            <option value="0" selected disabled> - </option>
                            <option value="1">Piso</option>
                            <option value="2">Oficina</option>
                        </select>

                        <label class="control-label" style="margin-top: -8%; margin-left: 38vh" >Cadena de valor: </label>
                        <select id="cadValor" name="cadValor" onchange="fDeptos()" type ="text" class="form-control" style="width: 30vh; margin-top: -3.5%; margin-left: 38vh" >
                                                            
                        </select> 

                        <div id="pnlDepto" name="pnlDepto" class="no">
                            <label class="control-label" style="margin-top: -8%; margin-left: 77vh">Tipo de área: </label>                                                           
                            <select id="depto" name="depto" type ="text" class="form-control" style="width: 30vh; margin-top: -3.5%; margin-left: 77vh">
                                <option value="0" selected disabled> - </option>
                                <option value="1"> ALMACEN</option>
                                <option value="2"> LABORATORIO</option>
                                <option value="3"> LINEA</option>
                                <option value="4"> MANTENIMIENTO</option>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nombre0" class="control-label">Acronimo:</label>
                        <label for="nombre0" class="control-label" style="width: 45%; margin-top: -25px; margin-left: 32vh">Descripcion:</label>
                        <input type="text" class="form-control" id="acronimo" name="acronimo" onkeypress="return permite(event,'car')" minlength="5" maxlength="15" style="width: 30vh" >
                        <input type="text" class="form-control" id="descripcion" name="descripcion" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 68vh; margin-top: -32px; margin-left: 38vh">
                    </div>
                              
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>



