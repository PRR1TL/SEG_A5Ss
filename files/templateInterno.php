<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<head>
    <!--LIBRERIAS DE BOOSTRAP--> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    
    <!--ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS-->
    <link rel="stylesheet" href="css/index.css">    
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align = center id="headerFixed" class="contenedor"> 
        <div class='fila0'>
            <img src="imagenes/logo.jpg">
        </div>             
        <h5 class="tituloPareto">
            NEW PORTAL
        </h5>
        <div class="fila1">            
        </div>   
    </a>    
</head>

<body>
    <div id="main" style="border: 1px solid #FFF;" >
        <ul id="nav">
            <li class="current"><a href="#">Inicio</a></li>            
            <li><a href="#">Administracion</a>
                <ul>
                    <li><a href="#">Departamentos</a></li>
                    <li><a href="#">Usuarios</a></li>                    
                    <li><a href="#">Auditorias</a></li>
                    <li><a href="#">OPL</a></li>
                </ul>
            </li>
            <li><a href="#">Auditoria</a></li>
            <li><a href="#">Reportes y estadistica</a>
                <ul>
                    <li><a href="#">General</a>
                    <li><a href="#">Area</a>
                        <ul>
                            <li><a href="#">Piso</a></li>
                            <li><a href="#">Oficinia</a></li>
                        </ul>
                    </li>                    
                </ul>
            </li>
            <li><a href="#">Calendario</a>
                <ul>
                    <li><a href="#">Mi Calendario</a></li>
                    <li><a href="#">Calendario General</a></li>                    
                </ul>
            </li>
            <li><a href="#">Informacion general</a></li>
            <li><a href="#">OPL</a></li>
        </ul>
    </div>   
</body>
