<?php 
    session_start();

    date_default_timezone_set("America/Mexico_City");
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
    } else {
        $userName = '';
        $name = '';
        $typeUser = 0;
    }    
?>
<html lang="es">
<head>
     <title>Reporte</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="assets/icons/book.ico" />
    <script src="../../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../../css/sweet-alert.css">
    <link rel="stylesheet" href="../../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../../css/normalize.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../../js/modernizr.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../../js/main.js"></script>
    <!--<script src="../js/index.js"></script>-->
<?php 
     include '../modals/informativos/help.php';   
    //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MODAL INFORMATIVOS
    include '../modals/accesos/login.php';
    include '../modals/accesos/cerrarSesion.php';
    include '../modals/informativos/rules.php';
    include '../modals/informativos/NoneAuditoria.php';
    include '../modals/informativos/InfoAuditoria.php';
    include '../modals/informativos/TipoAuditoria.php';

    echo $typeEv;
    //DEACUERDO AL USUARIO VAMOS A PODER HACER LA AUDITORIA Y SE VAN ABRIR LOS MODAL
    switch ($typeEv){
        case 1: //ALMACEN
        case 2: //LABORATORIO
        case 3: //LINEAS
            ?>
                <script src="../../js/iMLinea.js"></script>
            <?php
            echo 'LINEAS' ;
            include '../modals/auditoria/line/mSelect.php';
            include '../modals/auditoria/line/mOrder.php';
            include '../modals/auditoria/line/mOrder2.php';
            include '../modals/auditoria/line/mOrder3.php';
            include '../modals/auditoria/line/mOrder4.php';
            include '../modals/auditoria/line/mClean.php';
            include '../modals/auditoria/line/mClean2.php';
            include '../modals/auditoria/line/mStandar.php';
            include '../modals/auditoria/line/mStandar2.php';   
            include '../modals/auditoria/line/mDisc.php';
            break;
        case 4: //MANTENIMIENTO
            ?>
                <script src="../../js/iMMaintenance.js"></script>
            <?php
            echo 'MANTENIMIENTO' ;
            include '../modals/auditoria/maintenance/mSelect.php';
            include '../modals/auditoria/maintenance/mOrder.php';
            include '../modals/auditoria/maintenance/mOrder2.php';
            include '../modals/auditoria/maintenance/mOrder3.php';
            include '../modals/auditoria/maintenance/mClean.php';
            include '../modals/auditoria/maintenance/mClean2.php';
            include '../modals/auditoria/maintenance/mStandar.php';
            include '../modals/auditoria/maintenance/mStandar2.php';   
            include '../modals/auditoria/maintenance/mDisc.php';
            break;
        case 5: //OFICINA
            ?>
                <script src="../js/iOffice.js"></script>
            <?php
            echo 'OFICINA' ;
            include '../modals/auditoria/office/mSelect.php';
            include '../modals/auditoria/office/mOrder.php';                
            include '../modals/auditoria/office/mClean.php';                
            include '../modals/auditoria/office/mStandar.php';   
            include '../modals/auditoria/office/mDisc.php';
            break;
        default :
            ?>
                <script src="../../js/iMIndex.js"></script>
            <?php
            break;
    } 
?>
    
</head>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">                
                <img src="../../assets/img/icono.png" style="width: 100%">
            </div>
            
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">                    
                    <li>
                        <a href="../index.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a>
                    </li>                    
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-case zmdi-hc-fw"></i>&nbsp;&nbsp; Administración <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="../aUAdmin.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp; Usuarios</a></li>
                            <li><a href="../admin/aAAdmin.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Línea / Área</a></li>
                            <li><a href="../admin/aAuditoria.php"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de Auditoría </a></li>
                            <li><a href="../admin/aPreguntas.php"><i class="zmdi zmdi-format-list-numbered zmdi-hc-fw"></i>&nbsp;&nbsp; Preguntas</a></li>
                        </ul>
                    </li>
                    <?php if ($typeUser != 0) { ?>
                    <li class="btn-new-audit" data-placement="bottom">
                        <a> <i class="zmdi zmdi-assignment-o zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Auditoria </a>
                    </li>
                    <?php } ?>
                    <li>
                        <!--<a href="report.html"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>-->   
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-trending-up  zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="institution.html"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; General</a></li>
                            <li><a href="admin/aAAdmin.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Por área</a></li>
                        </ul>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-calendar zmdi-hc-fw"></i>&nbsp;&nbsp; Calendario <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li>
                                <a href="loanpending.html"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i>&nbsp;&nbsp; Vista general </a>
                            </li>
                            <li>
                                <a href="loanreservation.html"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i>&nbsp;&nbsp; Programación de auditoría </a>
                            </li>
                        </ul>
                    </li> 
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-info zmdi-hc-fw"></i>&nbsp;&nbsp; Información general <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="loan.html"><i class="zmdi zmdi-folder zmdi-hc-fw"></i>&nbsp;&nbsp; Información general </a></li>
                            <li>
                                <a href="loanpending.html"><i class="zmdi zmdi-grid zmdi-hc-fw"></i>&nbsp;&nbsp; Layouts </a>
                            </li>
                        </ul>
                    </li>
                    <?php if ($typeUser != 0) { ?>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-assignment-check zmdi-hc-fw"></i>&nbsp;&nbsp; OPL <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="opl/pCerrados.php"><i class="zmdi zmdi-alert-polygon zmdi-hc-fw"></i>&nbsp;&nbsp; Puntos abiertos </a></li>
                            <li><a href="opl/pAbiertos.php"><i class="zmdi zmdi-check-all zmdi-hc-fw"></i>&nbsp;&nbsp; Puntos Cerrados </a></li>
                            <li><a href="opl/pListado.php"><i class="zmdi zmdi-view-list zmdi-hc-fw"></i>&nbsp;&nbsp; Todos </a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <?php if ($typeUser != 0){ ?>
                    <li style="color:#fff; cursor:default;">
                        <span class="all-tittles"> <?php echo $name ?></span>
                    </li>
                <?php } else { ?>
                    <li  class="tooltips-general btn-login" data-placement="bottom" >
                        <span class="all-tittles">Inicar Sesion</span>
                    </li>  
                <?php } ?>
                    
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>               
                
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Auditorias 5Ss <small>Administración Usuarios</small></h1>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">
                <li role="presentation"  class="active"><a href="admin.html">Administradores</a></li>
                <li role="presentation"><a href="teacher.html">Docentes</a></li>
                <li role="presentation"><a href="student.html">Estudiantes</a></li>
                <li role="presentation"><a href="personal.html">Personal administrativo</a></li>
            </ul>
        </div>
        <div class="container-fluid"  style="margin: 50px 0;">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <img src="assets/img/user01.png" alt="user" class="img-responsive center-box" style="max-width: 110px;">
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Bienvenido a la sección para registrar nuevos administradores del sistema, debes de llenar todos los campos del siguiente formulario para registrar un administrador
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 lead">
                    <ol class="breadcrumb">
                      <li class="active">Nuevo administrador</li>
                      <li><a href="listadmin.html">Listado de administradores</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="container-flat-form">
                <div class="title-flat-form title-flat-blue">Registrar un nuevo administrador</div>
                <form autocomplete="off">
                    <div class="row">
                       <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                            <div class="group-material">
                                <input type="text" class="material-control tooltips-general" placeholder="Nombre completo" required="" maxlength="70" pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{1,70}" data-toggle="tooltip" data-placement="top" title="Escribe el nombre del administrador">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Nombre completo</label>
                            </div>
                           <div class="group-material">
                                <input type="text" class="material-control tooltips-general input-check-user" placeholder="Nombre de usuario" required="" maxlength="20" pattern="[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]{1,20}" data-toggle="tooltip" data-placement="top" title="Escribe un nombre de usuario sin espacios, que servira para iniciar sesión">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Nombre de usuario</label>
                           </div>
                            <div class="group-material">
                                <input type="email" class="material-control tooltips-general" placeholder="E-mail"  maxlength="50" data-toggle="tooltip" data-placement="top" title="Escribe el Email del administrador">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Email</label>
                            </div>
                            <div class="group-material">
                                <input type="password" class="material-control tooltips-general" placeholder="Contraseña" required="" maxlength="200" data-toggle="tooltip" data-placement="top" title="Escribe una contraseña">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Contraseña</label>
                            </div>
                           <div class="group-material">
                                <input type="password" class="material-control tooltips-general" placeholder="Repite la contraseña" required="" maxlength="200" data-toggle="tooltip" data-placement="top" title="Repite la contraseña">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Repetir contraseña</label>
                           </div>
                            <p class="text-center">
                                <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                                <button type="submit" class="btn btn-primary"><i class="zmdi zmdi-floppy"></i> &nbsp;&nbsp; Guardar</button>
                            </p> 
                       </div>
                   </div>
                </form>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore dignissimos qui molestias ipsum officiis unde aliquid consequatur, accusamus delectus asperiores sunt. Quibusdam veniam ipsa accusamus error. Animi mollitia corporis iusto.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Areli Pérez Calixto | Auditoría 5Ss | 2018 </p>
                        <p>© SEG Automotive México Service, S. de R. L. de C.V. 2018</p>           
                    </div>
                </div>
            </div>            
        </footer>
    </div>
</body>
</html>