/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//(function($){
//    $(document).ready(function(){
//        $("button").click(function(){
//          $("p").slideToggle();
//        });
//    });
//});
//
//    function admin() {
//        alert("Admin");
//    }
//    
//    function audit() {
//        alert("AUDIT");
//    }
//    
//    function report() {
//        alert("REPORT");
//    }
//    
//    function calendar() {
//        alert("CALENDAR");
//    }
//    
//    function info() {
//        alert("INFO");
//    }
//    
//    function opl() {
//        alert("OPL");
//    }


(function($){
    $(document).ready(function(){
    
    
    function admin() {
        alert("Admin");
    }
    
    function audit() {
        alert("AUDIT");
    }
    
    function report() {
        alert("REPORT");
    }
    
    function calendar() {
        alert("CALENDAR");
    }
    
    function info() {
        alert("INFO");
    }
    
    function opl() {
        alert("OPL");
    }
    
    var tipoEv = 0;
    
    $('.tooltips-general').tooltip('hide');
    $('.mobile-menu-button').on('click', function(){
        var mobileMenu=$('.navbar-lateral');	
        if(mobileMenu.css('display')=='none'){
            mobileMenu.fadeIn(300);
        }else{
            mobileMenu.fadeOut(300);
        }
    });
    
    $('.dropdown-menu-button').on('click', function(){
        var dropMenu=$(this).next('ul');
        dropMenu.slideToggle('slow');
    });
    
    $('.exit-system-button').on('click', function(e){
        $('#ModalLogOut').modal({
            show: true,
            backdrop: false
        });
    });
    
    $('.btn-login').on('click', function(){
        alert("login");
        console.log("login");
//        $('#ModalLogin').modal({
//            show: true,
//            backdrop: false, 
//            keyboard: false
//        });
    });

    //BOTONES DE LOS MODAL DE AUDITORIA    
    $('.btn-new-audit').on('click', function(){
        console.log("boton auditoria");
        var datA = [];
        var tipo = "";
        //BOTONES DE LOS MODAL DE AUDITORIA    
        //AQUI HACEMOS LA CONSULTA PARA VER LAS AUDITORIAS 
        $.ajax({
            type: "POST",
            url: "../files/db/progAuditoria/cUserAuditoria.php",
            //data: parametros,                
            success: function(datos){
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                
                switch(res1){
                    case "0":
                        $('#NoneAuditoria').modal({
                            show: true
                        });
                        break;
                    case "1":
                        var a1 = respuesta.split(",");
                        for (var i in a1) {
                            datA[i] = a1[i];
                        }                        
                        //TRANSFROMAMOS EL VALOR DE TIPO
                        switch (datA[4]){
                            case "1":
                                tipo = "SEMANAL";
                                break;
                            case "2":
                                tipo = "MENSUAL";
                                break;
                            case "3":
                                tipo = "CONFIRMACION";
                                break;
                        }                        
                        //MANDAMOS LOS VALORES DE LA AUDITORIA 
                        $('#idAuditoria').val(datA[1]);//ID 
                        $('#areaAuditoria').val(datA[2]); //LINEA
                        $('#auditado').val(datA[3]); //AUDITADO
                        $('#tipo').val(tipo); //TIPO AUDITORIA
                        $('#tipoEv').val(datA[5]); //TIPO EVALUACION
                        
                        $('#mInfoAuditoria').modal({
                            show: true
                        });
                        break;
                    case "2":
                        $('#mTipoAuditoria').modal({
                            show: true
                        });
                        break;
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(iLine: Line 89)");
                        break;                        
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();  
    });

    //EVENTOS PARA LOS BOTONES DE MODALS INFORMATIVOS DE LAS AUDITORIAS   
    //BOTON DE MODAL PARA SELECCION DE AUDITORIA
    $('#fTipoAuditoria').submit(function( event ) { 
        var datA = [];
        var tipo = "";
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/auditoria/selecAuditoria.php",
                data: parametros,                
                success: function(datos){
                    var respuesta = datos.trim();
                    var res1 = respuesta.substr(0,1);
                    switch(res1){
                        case "0":
                            $('#NoneAuditoria').modal({
                                show: true
                            });
                            break;
                        case "1":   
                            $('#mTipoAuditoria').modal('hide');
                            var a1 = respuesta.split(",");
                            for (var i in a1) {
                                datA[i] = a1[i];
                            }

                            //TRANSFROMAMOS EL VALOR DE TIPO
                            switch (datA[4]){
                                case "1":
                                    tipo = "SEMANAL";
                                    break;
                                case "2":
                                    tipo = "MENSUAL";
                                    break;
                                case "3":
                                    tipo = "CONFIRMACION";
                                    break;
                            }
                            //MANDAMOS LOS VALORES DE LA AUDITORIA 
                            $('#idAuditoria').val(datA[1]);//ID 
                            $('#areaAuditoria').val(datA[2]); //LINEA
                            $('#auditado').val(datA[3]); //AUDITADO
                            $('#tipo').val(tipo); //TIPO AUDITORIA
                            $('#tipoEv').val(datA[5]); //TIPO EVALUACION

                            $('#mInfoAuditoria').modal({
                                show: true
                            });
                            break;                        
                        default :
                            alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(iLine: Line 161)");
                            break;                        
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    }); 
    
    //BOTON DE MODAL INFORMATIVO DE AUDITORIA
    $('#fInfoAuditoria').submit(function( event ) { 
        var datA = [];
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/auditoria/sessionID.php",
                data: parametros,                
                success: function(datos){
                    var respuesta = datos.trim();
                    var res1 = respuesta.substr(0,1);
                    var a1 = respuesta.split(",");
                    for (var i in a1) {
                        datA[i] = a1[i].trim();
                    }                    
                    tipoEv = datA[4];
                    
                    switch(res1){
                        case "0":
                            alert("NO SE TIENE ID \n Error(iOffice: Line 205)");
                            break;
                        case "1":  
                            $('#mInfoAuditoria').modal('hide');
                            $('#ModalRules').modal({
                                show: true,
                                backdrop: 'static', 
                                keyboard: false
                            });
                            break;                        
                        default :
                            alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(iLine: Line 216)");
                            break;                        
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    $('.btn-rules').on('click', function(){   
        $('#ModalRules').modal('hide');        
        switch (tipoEv){
            case '1':
            case '2':
            case '3':
                $('#mLSelectL').modal({
                    show: true,
                    backdrop: 'static', 
                    keyboard: false
                });
                break;
            case '4':
                $('#mLSelectM').modal({
                    show: true,
                    backdrop: 'static', 
                    keyboard: false
                });
                break;
            case '5':
                $('#mLSelectO').modal({
                    show: true,
                    backdrop: 'static', 
                    keyboard: false
                });
                break;
        }        
    });
    
    //FUNCIONES DE MODALS PAR LINEAS
//        AJAX PARA MANDAR A BD
    $('#fLSelectO').submit(function( event ) { 
        var parametros = $(this).serialize();
        $("#datos_ajaxSEO").html("LOADING... ");
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iSelect.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){                        
                        $('#mLSelectO').modal('hide');
                        $('#mLOrderO').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });                        
                    } else {
                        $("#datos_ajaxSEO").html(datos);
                    }                    
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    //        AJAX PARA MANDAR A BD
    $('#fLOrdenO').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLOrderO').modal('hide');
                        $('#mLCleanO').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxORF").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    //        AJAX PARA MANDAR A BD
    $('#fLCleanO').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden2.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLCleanO').modal('hide');
                        $('#mLStandarO').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxCLO").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });      
    
    //        AJAX PARA MANDAR A BD
    $('#fLStandarO').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden3.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLStandarO').modal('hide');
                        $('#mLDiscO').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxSTO").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
        });
    
    //        AJAX PARA MANDAR A BD
    $('#fLDiscO').submit(function( event ) { 
        var datA = [];
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iClean.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    var a1 = res.split(",");
                    for (var i in a1) {
                        datA[i] = a1[i];
                    }
                    console.log(datA[0]);
                    if (datA[0] === "Bien"){
                        $('#mLDiscO').modal('hide');
                        if (datA[1] < 80){
                            $('#scooreR').val(datA[1]);
                            $('#mPuntosAuditoriaR').modal({
                                show: true,
                                backdrop: 'static', 
                                keyboard: false
                            }); 
                        }else
                        if (datA[1] < 60){
                            $('#scooreM').val(datA[1]);
                            $('#mPuntosAuditoriaM').modal({
                                show: true,
                                backdrop: 'static', 
                                keyboard: false
                            }); 
                        } else {
                            $('#scooreB').val(datA[1]);
                            $('#mPuntosAuditoriaB').modal({
                                show: true,
                                backdrop: 'static', 
                                keyboard: false
                            }); 
                        }   
                    } else {
                        $("#datos_ajaxDIO").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    /**************************** MANTENIMIENTO *******************************/
    $('#fLSelectM').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iSelect.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLSelectM').modal('hide');
                        $('#mLOrderM').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxSEM").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    //        AJAX PARA MANDAR A BD
    $('#fLOrdenM').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){    
                        $('#mLOrderM').modal('hide');
                        $('#mLOrder2M').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxORM1").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    //        AJAX PARA MANDAR A BD
    $('#fLOrden2M').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden2.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLOrder2M').modal('hide');
                        $('#mLOrder3M').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxORM2").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    //        AJAX PARA MANDAR A BD
    $('#fLOrden3M').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden3.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLOrder3M').modal('hide');
                        $('#mLCleanM').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxORM3").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    //        AJAX PARA MANDAR A BD
    $('#fLCleanM').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iClean.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLCleanM').modal('hide');
                        $('#mLClean2M').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxCLM1").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    }); 

    //        AJAX PARA MANDAR A BD
    $('#fLClean2M').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iClean2.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLClean2M').modal('hide');
                        $('#mLStandarM').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxCLM2").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    }); 
    
    //        AJAX PARA MANDAR A BD
        $('#fLStandarM').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iStandar.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLStandarM').modal('hide');
                        $('#mLStandar2M').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxSTM1").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
        });
     
    //        AJAX PARA MANDAR A BD
    $('#fLStandar2M').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iStandar2.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLStandar2M').modal('hide');
                        $('#mLDiscM').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxSTM2").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
        });

//        AJAX PARA MANDAR A BD
    $('#fLDiscM').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iDisc.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLDiscM').modal('hide');  
                        $('#mPuntosAuditoria').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxDIM").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    /********************************* LINEAS *********************************/
    $('#fLSelectL').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iSelect.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLSelectL').modal('hide');
                        $('#mLOrderL').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxSEL").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    //        AJAX PARA MANDAR A BD
    $('#fLOrdenL').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){    
                        $('#mLOrderL').modal('hide');
                        $('#mLOrder2L').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxORL1").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    //        AJAX PARA MANDAR A BD
    $('#fLOrden2L').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden2.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLOrder2L').modal('hide');
                        $('#mLOrder3L').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxORL2").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    //        AJAX PARA MANDAR A BD
    $('#fLOrden3L').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iOrden3.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLOrder3L').modal('hide');
                        $('#mLCleanL').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxORL3").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    //        AJAX PARA MANDAR A BD
    $('#fLCleanL').submit(function( event ) { 
        var datA = [];
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iClean.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    var a1 = res.split(",");
                    for (var i in a1) {
                        datA[i] = a1[i];
                    }
                    console.log(datA[0]);
                    if (datA[0] === "Bien"){
                        $('#mLCleanL').modal('hide');
                        $('#mLClean2L').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxCLL1").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    }); 

    //        AJAX PARA MANDAR A BD
    $('#fLClean2L').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iClean2.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLClean2L').modal('hide');
                        $('#mLStandarL').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxCLL2").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    }); 
    
    //        AJAX PARA MANDAR A BD
    $('#fLStandarL').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iStandar.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLStandarL').modal('hide');
                        $('#mLStandar2L').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxSTL1").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
        });
     
    //        AJAX PARA MANDAR A BD
    $('#fLStandar2L').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iStandar2.php",
                data: parametros,                
                success: function(datos){
                    var res = datos.trim();
                    if (res === "Bien"){
                        $('#mLStandar2L').modal('hide');
                        $('#mLDiscL').modal({
                            show: true,
                            backdrop: 'static', 
                            keyboard: false
                        });
                    } else {
                        $("#datos_ajaxSTL2").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
        });

//        AJAX PARA MANDAR A BD
    $('#fLDiscL').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/iRespuestas/iDisc.php",
                data: parametros,                
                success: function(datos){
                    var datA = [];
                    var res = datos.trim();
                    var a1 = res.split(",");
                    for (var i in a1) {
                        datA[i] = a1[i];
                    }
                    if (datA[0] === "Bien"){
                        $('#mLDiscL').modal('hide');
                        if (datA[1] < 80){
                            $('#scooreR').val(datA[1]);
                            $('#mPuntosAuditoriaR').modal({
                                show: true,
                                backdrop: 'static', 
                                keyboard: false
                            }); 
                        }else
                        if (datA[1] < 60){
                            $('#scooreM').val(datA[1]);
                            $('#mPuntosAuditoriaM').modal({
                                show: true,
                                backdrop: 'static', 
                                keyboard: false
                            }); 
                        } else {
                            $('#scooreB').val(datA[1]);
                            $('#mPuntosAuditoriaB').modal({
                                show: true,
                                backdrop: 'static', 
                                keyboard: false
                            }); 
                        } 
                    } else {
                        $("#datos_ajaxDSL").html(datos);
                    }
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    //FUNCIONES PARA LA CABECERA LOS TOOLTIP    
    $('.btn-help').on('click', function(){
        $('#ModalHelp').modal({
            show: true,
            backdrop: "static"
        });
    });
    
    //FUNCION PARA EL LOGUIN
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/db/cLogin.php",
                data: parametros,
                success: function(datos){
                    $("#datos_ajax").html(datos);
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });       
    
    $( "#logOut" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../files/modals/accesos/exit.php",
                data: parametros,
                success: function(datos){
                    $("#datos_ajax").html(datos);
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });   
    
    //OPL
    $('#mUOpl').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var idOpl = button.data('hallazgo1'); // Extraer la información de atributos de datos     
        var fInicio = button.data('hallazgo2'); // Extraer la información de atributos de datos
        var fCompromiso = button.data('hallazgo3'); // Extraer la información de atributos de datos
        var hallazgo = button.data('hallazgo'); // Extraer la información de atributos de datos
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var responsable = button.data('responsable'); // Extraer la información de atributos de datos
        var soporte = button.data('soporte'); // Extraer la información de atributos de datos
        var estado = button.data('estado'); // Extraer la información de atributos de datos

        console.log("idOpl: "+idOpl+", fInicio: "+fInicio+", fCompromiso: "+fCompromiso+", hallazgo: "+hallazgo+", accion: "+accion+", responsable: "+responsable+", soporte: "+soporte+", estado: "+estado);

        var modal = $(this);
        modal.find('.modal-body #idOpl').val(idOpl);
        modal.find('.modal-body #fInicio').val(fInicio);
        modal.find('.modal-body #fCompromiso').val(fCompromiso);
        modal.find('.modal-body #hallazgo').val(hallazgo);
        modal.find('.modal-body #accion').val(accion);
        modal.find('.modal-body #responsable').val(responsable);
        modal.find('.modal-body #soporte').val(soporte);
        modal.find('.modal-body #estado').val(estado);

        $('.alert').hide();//Oculto alert
    });
    
    function loadOplA(){
        location.href="../files/pAbiertos.php";
        location.reload(true);
    }    
    
    $( "#fUOpl" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "db/opl/uOpl.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax").html("Mensaje: Cargando...");
                },
                success: function(datos){
                    $("#datos_ajax").html(datos);
                    loadOplA();
                }
        });
        event.preventDefault();
    });
    
    
});
});

(function($){
    $(window).load(function(){
        $(".custom-scroll-containers").mCustomScrollbar({
            theme:"dark-thin",
            scrollbarPosition: "inside",
            autoHideScrollbar: true,
            scrollButtons:{ enable: true }
        });
    });
})(jQuery);