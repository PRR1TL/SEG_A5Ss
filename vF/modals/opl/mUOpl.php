<?php 
    $f = date('Y-m-d');
?>

<form id="fUOpl">
    <div class="modal" id="mUOpl" tabindex="-1" role="dialog" data-backdrop="false" >
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">                    
                    <h4 class="modal-title text-center all-tittles col-lg-12" style="color: #ffffff" >Actualizar actividad:</h4>
                    <button type="button" data-dismiss="modal" style="background-color: transparent; border: 0; margin-top: -3.9vh;" > <img src="./imagenes/cl.png" > </button>  
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto; overflow-x: hidden; " >
                    <div id="datos_ajax"></div>
                    <input type="hidden" class="form-control" id="idOpl" name="idOpl" maxlength="6" required>
                    <!--</div>-->
                    <div class="form-group">
                        <label for="nombre0" class="control-label">Fecha Inicio:</label>
                        <label for="nombre0" class="control-label" style="width: 45%; margin-top: -25px; margin-left: 27%">Fecha Compromiso:</label>
                        <label for="nombre0" class="control-label" style="width: 45%; margin-top: -33px; margin-left: 70%">Fecha Actual:</label>
                        <input type="text" class="form-control" id="fInicio" name="fInicio" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 25%" readonly>
                        <input type="text" class="form-control" id="fCompromiso" name="fCompromiso" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 25%; margin-top: -32px; margin-left: 35%" readonly>
                        <input type="text" class="form-control" id="fActual" name="fActual" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 25%; margin-top: -32px; margin-left: 70%" value="<?php echo $f?>" readonly>
                    </div>
                    
                    <div class="form-group">
                        <label for="moneda0" class="control-label">Hallazgo</label>
                        <label for="moneda0" class="control-label" style="width: 45%; margin-top: -33px; margin-left: 51%">Accion</label>
                        <input type="text" class="form-control" id="hallazgo" name="hallazgo" onkeypress="return permite(event,'num')" maxlength="10" style="width: 45%" readonly>
                        <input type="text" class="form-control" id="accion" name="accion" onkeypress="return permite(event,'num')" maxlength="10" style="width: 45%; margin-top: -32px; margin-left: 50%" readonly>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label">Responsable</label>
                        <label  class="control-label" style="width: 45%; margin-top: -33px; margin-left: 51%">Soporte</label>
                        <input type="text" class="form-control" id="responsable" name="responsable" onkeypress="return permite(event,'num')" maxlength="10" style="width: 45%" readonly>
                        <input type="text" class="form-control" id="soporte" name="soporte" onkeypress="return permite(event,'num')" maxlength="10" style="width: 45%; margin-top: -32px; margin-left: 50%" readonly>
                    </div>
                    
                    <div class="form-group">
                        <label for="tipo0" class="control-label">Estado:</label> 
                        <select type ="text" class="form-control" id="estado" name="estado" style="width: 95%;" required >                    
                            <option value="" selected disabled> Selecciona Estado </option>
                            <option value = "1"> Accion definida</option>
                            <option value = "2"> Accion en proceso</option>
                            <option value = "3"> Accion ejecutada </option>
                            <option value = "4"> Accion cerrada </option>  
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>
