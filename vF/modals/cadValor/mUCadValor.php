<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

?>

<style>
    .no {
        display:none;
    }
    .si {
        display:block;
    }
</style>

<form id="fUCadValor">
    <div class="modal fade" id="mUCadValor" tabindex="-1" role="dialog" data-backdrop="false" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel" align="center">ACTUALIZAR CADENA DE VALOR</h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto; ">
                    <div id="alertUCadValor"></div>                                          
                    <div class="form-group">                 
                        <label class="control-label" >Tipo de área: </label>                                                           
                        <select id="deptoU" name="deptoU" type ="text" class="form-control" style=" width: 30%" disabled="true">
                            <option value="0" selected disabled> - </option>
                            <option value="1"> ALMACEN</option>
                            <option value="3"> LINEA</option>
                            <option value="2"> LABORATORIO</option>
                            <option value="4"> MANTENIMIENTO</option>
                            <option value="5"> OFICINA</option>
                        </select> 

                        <label for="nombre0" class="control-label" style="margin-left: 39%; margin-top: -10.5%">Acronimo:</label>
                        <input type="text" class="form-control" id="acronimoU" name="acronimoU" minlength="2" maxlength="15" style=" width: 55%; margin-left: 38%; margin-top: -4.5%" >
                        <input type="text" class="form-control no" id="acronimoU2" name="acronimoU2" minlength="2" maxlength="15" style=" width: 55%; margin-left: 38%; margin-top: -4.5%" >
                    </div>                              
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>



