<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

    <div class="modal" tabindex="-1" role="dialog"  id="ModalRules" >
        <div class="modal-dialog modal-xsm">
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; ">
                    <h4 class="modal-title text-center all-tittles col-lg-12" style="color: #ffffff" >INTRUCCIONES</h4>
                    <button type="button" data-dismiss="modal" style="background-color: transparent; border: 0; margin-top: -3.9vh;" > <img src="./imagenes/cl.png" > </button> 
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <h3>
                        <b style="margin-left: auto; margin-right: auto;">ANTES DE INICIAR LA AUDITORÍA:</b>
                    <br><br>
                    <p>Lee atentamente los ítems y selecciona la puntuaci&oacute;n, de acuerdo a los siguientes criterios:  
                        <br><br><br>
                        <b>&nbsp;SI </b>: La carater&iacute;stica se cumple completamente.
                        <br><br>
                        <b> NO </b>: La caracter&iacute;stica no se cumple. </p>
                    <br>
                    </h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-rules btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"></i></button>
                </div>
            </div>
        </div>
    </div>
