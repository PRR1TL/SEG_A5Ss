<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<form id="fTipoAuditoriaCh" method="post"> 
    <div class="modal" tabindex="-1" role="dialog" id="mTipoAuditoriaCh">
        <div class="modal-dialog modal-xsm">
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="col-lg-12" style="color: #ffffff" >TIPO DE AUDITORIA</h4>
                    <button type="button" data-dismiss="modal" style="background-color: transparent; border: 0; margin-top: -3.5vh; margin-left: 1%;" > <img src="./imagenes/cl.png" > </button>
                </div>
                <div class="modal-body" >
                    <div class="row " >
                        <div class="col-sm-12"  >
                            <span ><b> Tienes asignada y/o pendiente más de una Auditoria.  </b></span> 
                            <span ><b><br> Selecciona la que desees realizar.  <br></b></span> 
                        </div>
                        <br><br><br>
                        <div id="a12" name="a12" class="col-sm-12" align="center">
                            <span ><b> Opcion: </b></span>
                            <select id="cmbTipoAuditoriaCh" name="cmbTipoAuditoria" >
                            </select>                        
                        </div>                                       
                        <br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-tipoAuditoria btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"></i></button>
                </div>
            </div>
        </div>
    </div>
</form>
