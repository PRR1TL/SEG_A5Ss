<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('d/m/Y');
$fNow = date('Y-m-d');
$fProp = date('d/m/Y' ,strtotime('+1 day', strtotime($fNow)));

?>

<style>
    td, th {
        border: 1px solid gray;
    }

    .bordesCompletos {
        border: 1px solid gray;
    }
    
    .cabecera {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
    }
    
    .filaPregunta {
        height: 44px;
        font-size: 21px;
    }
    
    .centrado {
        align: center;
        text-align: center;
        align-content: center;
    }
    
    /*TAMAÑO DE LOS RADIO BUTOM*/
    .rdBtn {
        width: 25px;
        height: 25px;
    }
    
</style>

<form id="fADaily" method="post">
    <div class="modal" tabindex="-1" role="dialog" id="mADaily">
        <div class="modal-dialog modal-lg" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center all-tittles"> CHECK LIST SEMANAL DE 5S's (ALMACEN) </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertADaily"> </div>
                    
                    <table style="width: 100%;" >
                        <thead class="cabecera" >
                            <tr>
                                <td rowspan="2" style="width: 4%" > INFO </td>
                                <td rowspan="2" > No. </td>
                                <td rowspan="2" > PUNTO A REVISAR </td>
                                <td colspan="2" style="width: 6%" > CUMPLE </td>                                
                            </tr>
                            <tr>
                                <td> SI &nbsp;</td>
                                <td> NO </td>
                            </tr>
                        </thead>
                        <tbody >
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA1();' > </td>
                                <td>1</td>
                                <td > No se encuentran condiciones inseguras.
                                    (Ej. Cables en mal estado, derrames de líquidos, obstrucción de equipos de emergencia, rejas/racks/mobiliario en mal estado, etc.) 
                                    
                                    <div id="descAcciones1A" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckA1" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo1A" class="form-control" name="hallazgo1" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion1A" class="form-control" name="accion1" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable1A" class="form-control" name="responsable1" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte1A" class="form-control" name="soporte1" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin1A" name="fFin1" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave1A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA1()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar1A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC1()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </td>                                
                                <td class="centrado" colspan="2">
                                    <input type="radio" id="checkAP1" name="checkAP1" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP1" name="checkAP1" value="0" class="rdBtn"> 
                                </td>                      
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA2();' > </td>
                                <td>2</td>
                                <td >
                                    La línea/área se encuentra libre de objetos innecesarios tales como objetos personales, herramientas, objetos bloqueando el paso, etc.
                                    
                                    <div id="descAcciones2A" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckA2" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo2A" class="form-control" name="hallazgo2" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion2A" class="form-control" name="accion2" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable2A" class="form-control" name="responsable2" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte2A" class="form-control" name="soporte2" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin2A" name="fFin2" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave2A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA2()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar2A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC2()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP2" name="checkAP2" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP2" name="checkAP2" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA3();' > </td>
                                <td>3</td>
                                <td>
                                    Todos los objetos necesarios tienen un lugar asignado y se encuentran ordenados.
                                    
                                    <div id="descAcciones3A" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckA3" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo3A" class="form-control" name="hallazgo3" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion3A" class="form-control" name="accion3" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable3A" class="form-control" name="responsable3" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte3A" class="form-control" name="soporte3" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin3A" name="fFin3" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                    
                                                <div class="form-inline">
                                                    <div id="btnSave3A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA3()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar3A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC3()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP3" name="checkAP3" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP3" name="checkAP3" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA4();' > </td>
                                <td>4</td>
                                <td>
                                    Maquinarias, mobiliario, materiales, químicos, etc., se encuentran correctamente identificados y de acuerdo al estándar.                                    
                                    <div id="descAcciones4A" style="color:#337ab7;" > </div>
                                    <div class="panel no" id="pnlOplCheckA4" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo4A" class="form-control" name="hallazgo4" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion4A" class="form-control" name="accion4" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable4A" class="form-control" name="responsable4" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte4A" class="form-control" name="soporte4" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin4A" name="fFin4" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave4A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA4()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar4A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC4()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP4" name="checkAP4" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP4" name="checkAP4" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA5();' > </td>
                                <td>5</td>
                                <td>
                                    El interior y exterior del mobiliario/maquinaria se encuentra libre de basura/polvo/aceite/rebaba.
                                    
                                    <div id="descAcciones5A" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckA5" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo5A" class="form-control" name="hallazgo5" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion5A" class="form-control" name="accion5" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable5A" class="form-control" name="responsable5" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte5A" class="form-control" name="soporte5" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin5A" name="fFin5" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave5A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA5()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar5A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC5()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP5" name="checkAP5" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP5" name="checkAP5" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA6();' > </td>
                                <td>6</td>
                                <td>
                                    La basura es tirada en el bote acorde a su código de color.
                                    
                                    <div id="descAcciones6A" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckA6" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo6A" class="form-control" name="hallazgo6" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion6A" class="form-control" name="accion6" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable6A" class="form-control" name="responsable6" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte6A" class="form-control" name="soporte6" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin6A" name="fFin6" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave6A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA6()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar6A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC6()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP6" name="checkAP6" value="1"class="rdBtn" >                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP6" name="checkAP6" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA7();' > </td>
                                <td>7</td>
                                <td>
                                    Los colaboradores conocen qué son las 5S's (preguntar a un colaborador qué son las 5S's y cuál es su beneficio en el lugar de trabajo).
                                    
                                    <div id="descAcciones7A" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckA7" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo7A" class="form-control" name="hallazgo7" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion7A" class="form-control" name="accion7" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable7A" class="form-control" name="responsable7" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte7A" class="form-control" name="soporte7" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicio7A" class="form-control" name="fInicio7" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin7A" name="fFin7" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave7A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA7()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar7A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC7()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP7" name="checkAP7" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP7" name="checkAP7" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA8();' > </td>
                                <td>8</td>
                                <td>
                                    El estándar físico y las mamparas de la línea/área se encuentran actualizadas y desplegadas en el lugar correspondiente.
                                    
                                    <div id="descAcciones8A" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckA8" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo8A" class="form-control" name="hallazgo8" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion8A" class="form-control" name="accion8" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable8A" class="form-control" name="responsable8" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte8A" class="form-control" name="soporte8" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin8A" name="fFin8" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave8A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA8()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar8A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC8()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP8" name="checkAP8" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP8" name="checkAP8" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA9();' > </td>
                                <td>9</td>
                                <td>
                                    El check list del día anterior se llevó a cabo así como las actividades de 5S's al finalizar el turno (o en paros de línea).
                                    
                                    <div id="descAcciones9A" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckA9" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo9A" class="form-control" name="hallazgo9" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion9A" class="form-control" name="accion9" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable9A" class="form-control" name="responsable9" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte9A" class="form-control" name="soporte9" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin9A" name="fFin9" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave9A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA9()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar9A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC9()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP9" name="checkAP9" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP9" name="checkAP9" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dA10();' > </td>
                                <td>10</td>
                                <td>
                                    Se cerró al menos un punto abierto del día anterior.
                                    
                                    <div id="descAcciones10A" style="color:#337ab7;" > </div>
                                    
                                    <div class="panel no" id="pnlOplCheckA10" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo10A" class="form-control" name="hallazgo10" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion10A" class="form-control" name="accion10" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable10A" class="form-control" name="responsable10" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte10A" class="form-control" name="soporte10" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioA" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin10A" name="fFin10" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave10A" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckA10()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar10A" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckAC10()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkAP10" name="checkAP10" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkAP10" name="checkAP10" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                        </tbody>
                    </table>
                    <!--Descripción rápida-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>

