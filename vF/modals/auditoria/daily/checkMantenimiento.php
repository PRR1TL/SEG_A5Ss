<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('d/m/Y');
$fNow = date('Y-m-d');
$fProp = date('d/m/Y' ,strtotime('+1 day', strtotime($fNow)));

?>

<style>
    td, th {
        border: 1px solid gray;
    }

    .bordesCompletos {
        border: 1px solid gray;
    }
    
    .cabecera {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
    }
    
    .filaPregunta {
        height: 44px;
        font-size: 21px;
    }
    
    .centrado {
        align: center;
        text-align: center;
        align-content: center;
    }
    
    /*TAMAÑO DE LOS RADIO BUTOM*/
    .rdBtn {
        width: 25px;
        height: 25px;
    }
    
</style>

<form id="fMDaily" method="post">
    <div class="modal" tabindex="-1" role="dialog" id="mMDaily">
        <div class="modal-dialog modal-lg" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center all-tittles"> CHECK LIST SEMANAL DE 5S's (MANTENIMIENTO) </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertMDaily"> </div>                    
                    <table style="width: 100%;" >
                        <thead class="cabecera" >
                            <tr>
                                <td rowspan="2" style="width: 4%" > INFO </td>
                                <td rowspan="2" > No. </td>
                                <td rowspan="2" > PUNTO A REVISAR </td>
                                <td colspan="2" style="width: 6%" > CUMPLE </td>                                
                            </tr>
                            <tr>
                                <td> SI &nbsp;</td>
                                <td> NO </td>
                            </tr>
                        </thead>
                        <tbody >
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM1();' > </td>
                                <td>1</td>
                                <td > No se encuentran condiciones inseguras.
                                    (Ej. Cables en mal estado, derrames de líquidos, obstrucción de equipos de emergencia, rejas/racks/mobiliario en mal estado, etc.) 
                                    
                                    <div id="descAcciones1M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM1" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo1M" class="form-control" name="hallazgo1" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion1M" class="form-control" name="accion1" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable1M" class="form-control" name="responsable1" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte1M" class="form-control" name="soporte1" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin1M" name="fFin1" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave1M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM1()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar1M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC1()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </td>                                
                                <td class="centrado" colspan="2">
                                    <input type="radio" id="checkMP1" name="checkMP1" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP1" name="checkMP1" value="0" class="rdBtn"> 
                                </td>                      
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM2();' > </td>
                                <td>2</td>
                                <td >
                                    La línea/área se encuentra libre de objetos innecesarios tales como objetos personales, herramientas, objetos bloqueando el paso, etc.
                                    
                                    <div id="descAcciones2M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM2" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo2M" class="form-control" name="hallazgo2" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion2M" class="form-control" name="accion2" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable2M" class="form-control" name="responsable2" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte2M" class="form-control" name="soporte2" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin2M" name="fFin2" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave2M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM2()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar2M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC2()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP2" name="checkMP2" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP2" name="checkMP2" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM3();' > </td>
                                <td>3</td>
                                <td>
                                    Todos los objetos necesarios tienen un lugar asignado y se encuentran ordenados.
                                    
                                    <div id="descAcciones3M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM3" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo3M" class="form-control" name="hallazgo3" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion3M" class="form-control" name="accion3" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable3M" class="form-control" name="responsable3" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte3M" class="form-control" name="soporte3" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin3M" name="fFin3" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                    
                                                <div class="form-inline">
                                                    <div id="btnSave3M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM3()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar3M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC3()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP3" name="checkMP3" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP3" name="checkMP3" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM4();' > </td>
                                <td>4</td>
                                <td>
                                    Maquinarias, mobiliario, materiales, químicos, etc., se encuentran correctamente identificados y de acuerdo al estándar.                                    
                                    
                                    <div id="descAcciones4M" style="color:#337ab7;" > </div>
                                    <div class="panel no" id="pnlOplCheckM4" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo4M" class="form-control" name="hallazgo4" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion4M" class="form-control" name="accion4" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable4M" class="form-control" name="responsable4" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte4M" class="form-control" name="soporte4" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin4M" name="fFin4" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave4M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM4()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar4M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC4()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP4" name="checkMP4" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP4" name="checkMP4" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM5();' > </td>
                                <td>5</td>
                                <td>
                                    El interior y exterior del mobiliario/maquinaria se encuentra libre de basura/polvo/aceite/rebaba.
                                    
                                    <div id="descAcciones5M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM5" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo5M" class="form-control" name="hallazgo5" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion5M" class="form-control" name="accion5" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable5M" class="form-control" name="responsable5" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte5M" class="form-control" name="soporte5" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin5M" name="fFin5" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave5M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM5()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar5M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC5()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP5" name="checkMP5" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP5" name="checkMP5" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM6();' > </td>
                                <td>6</td>
                                <td>
                                    La basura es tirada en el bote acorde a su código de color.
                                    
                                    <div id="descAcciones6M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM6" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo6M" class="form-control" name="hallazgo6" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion6M" class="form-control" name="accion6" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable6M" class="form-control" name="responsable6" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte6M" class="form-control" name="soporte6" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin6M" name="fFin6" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave6M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM6()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar6M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC6()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP6" name="checkMP6" value="1"class="rdBtn" >                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP6" name="checkMP6" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM7();' > </td>
                                <td>7</td>
                                <td>
                                    Los colaboradores conocen qué son las 5S's (preguntar a un colaborador qué son las 5S's y cuál es su beneficio en el lugar de trabajo).
                                    
                                    <div id="descAcciones7M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM7" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo7M" class="form-control" name="hallazgo7" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion7M" class="form-control" name="accion7" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable7M" class="form-control" name="responsable7" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte7M" class="form-control" name="soporte7" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicio7M" class="form-control" name="fInicio7" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin7M" name="fFin7" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave7M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM7()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar7M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC7()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP7" name="checkMP7" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP7" name="checkMP7" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM8();' > </td>
                                <td>8</td>
                                <td>
                                    El estándar físico y las mamparas de la línea/área se encuentran actualizadas y desplegadas en el lugar correspondiente.
                                    
                                    <div id="descAcciones8M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM8" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo8M" class="form-control" name="hallazgo8" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion8M" class="form-control" name="accion8" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable8M" class="form-control" name="responsable8" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte8M" class="form-control" name="soporte8" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin8M" name="fFin8" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave8M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM8()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar8M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC8()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP8" name="checkMP8" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP8" name="checkMP8" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM9();' > </td>
                                <td>9</td>
                                <td>
                                    El check list del día anterior se llevó a cabo así como las actividades de 5S's al finalizar el turno (o en paros de línea).
                                    
                                    <div id="descAcciones9M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM9" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo9M" class="form-control" name="hallazgo9" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion9M" class="form-control" name="accion9" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable9M" class="form-control" name="responsable9" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte9M" class="form-control" name="soporte9" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin9M" name="fFin9" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave9M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM9()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar9M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC9()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP9" name="checkMP9" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP9" name="checkMP9" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dM10();' > </td>
                                <td>10</td>
                                <td>
                                    Se cerró al menos un punto abierto del día anterior.
                                    
                                    <div id="descAcciones10M" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckM10" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo10M" class="form-control" name="hallazgo10" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion10M" class="form-control" name="accion10" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable10M" class="form-control" name="responsable10" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte10M" class="form-control" name="soporte10" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin10M" name="fFin10" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave10M" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckM10()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar10M" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckMC10()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkMP10" name="checkMP10" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkMP10" name="checkMP10" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                        </tbody>
                    </table>
                    <!--Descripción rápida-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>

