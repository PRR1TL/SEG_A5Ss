<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('d/m/Y');
$fNow = date('Y-m-d');
$fProp = date('d/m/Y' ,strtotime('+1 day', strtotime($fNow)));

?>

<style>
    td, th {
        border: 1px solid gray;
    }

    .bordesCompletos {
        border: 1px solid gray;
    }
    
    .cabecera {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
    }
    
    .filaPregunta {
        height: 44px;
        font-size: 21px;
    }
    
    .centrado {
        align: center;
        text-align: center;
        align-content: center;
    }
    
    /*TAMAÑO DE LOS RADIO BUTOM*/
    .rdBtn {
        width: 25px;
        height: 25px;
    }
    
</style>

<form id="fLMonthly1" method="post">
    <div class="modal" tabindex="-1" role="dialog" id="mLMonthly1">
         <div class="modal-dialog modal-lg" style="width: 100%" >
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title text-center all-tittles"> MENSUAL DE 5S's (LINEA) </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertLMonthly1"> </div>
                    
                    <table style="width: 100%;" >
                        <thead class="cabecera" >
                            <tr>
                                <td rowspan="2" style="width: 4%" > INFO </td>
                                <td rowspan="2" > No. </td>
                                <td rowspan="2" > PUNTO A REVISAR </td>
                                <td colspan="2" style="width: 8%" > CUMPLE </td>                                
                            </tr>
                            <tr>
                                <td> SI &nbsp;</td>
                                <td> NO </td>
                            </tr>
                        </thead>
                        <tbody >
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM1();' > </td>
                                <td>1</td>
                                <td > Se realizaron las acciones definidas tras la última auditoría 5S en el plazo establecido.
                                    
                                    <div id="descAccionesM1" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM1" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo1LM" class="form-control" name="hallazgo1" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion1LM" class="form-control" name="accion1" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable1LM" class="form-control" name="responsable1" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte1LM" class="form-control" name="soporte1" maxlength="150" style="width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio1" name="fInicio1" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input id="fFinLM1" class="form-control" type="text" name="fFinLM1" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM1" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM1()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM1" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC1()" data-zdp_readonly_element="false" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>                                
                                <td class="centrado"  colspan="2">                                    
                                    <input type="radio" id="checkLMP1" name="checkLMP1" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP1" name="checkLMP1" value="0" class="rdBtn"> 
                                </td>                      
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM2();' > </td>
                                <td>2</td>
                                <td >
                                    No existen desviaciones significativas de seguridad
                                    
                                    <div id="descAccionesM2" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM2" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo2LM" class="form-control" name="hallazgo2" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion2LM" class="form-control" name="accion2" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable2LM" class="form-control" name="responsable2" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte2LM" class="form-control" name="soporte2" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio2" name="fInicio2" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM2" name="fFinLM2" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM2" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM2()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM2" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC2()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP2" name="checkLMP2" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP2" name="checkLMP2" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM3();' > </td>
                                <td>3</td>
                                <td>
                                    El asociado conoce las 5S´s y cuáles son los beneficios de su aplicación en el área de trabajo.
                                    
                                    <div id="descAccionesM3" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM3" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo3LM" class="form-control" name="hallazgo3" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion3LM" class="form-control" name="accion3" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable3LM" class="form-control" name="responsable3" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte3LM" class="form-control" name="soporte3" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio3" name="fInicio3" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM3" name="fFinLM3" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                   
                                                <div class="form-inline">
                                                    <div id="btnSaveM3" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM3()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM3" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC3()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP3" name="checkLMP3" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP3" name="checkLMP3" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM4();' > </td>
                                <td>4</td>
                                <td>
                                    La línea/área está libre de piezas, máquinas, objetos innecesarios y/o defectuosos, incluyendo artículos personales no necesarios.                                   
                                    <div id="descAccionesM4" style="color:#337ab7;" > </div>
                                    <div class="panel no" id="pnlOplCheckLM4" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo4LM" class="form-control" name="hallazgo4" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion4LM" class="form-control" name="accion4" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable4LM" class="form-control" name="responsable4" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte4LM" class="form-control" name="soporte4" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control" id="fInicio4" name="fInicio4" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM4" name="fFinLM4" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM4" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM4()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM4" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC4()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP4" name="checkLMP4" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP4" name="checkLMP4" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM5();' > </td>
                                <td>5</td>
                                <td>
                                    Todos los objetos, materiales, mobiliario y áreas de trabajo están claramente identificados y tienen definido un lugar.
                                    
                                    <div id="descAccionesM5" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM5" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo5LM" class="form-control" name="hallazgo5" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion5LM" class="form-control" name="accion5" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable5LM" class="form-control" name="responsable5" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte5LM" class="form-control" name="soporte5" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control" id="fInicio5" name="fInicio5" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM5" name="fFinLM5" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM5" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM5()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM5" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC5()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP5" name="checkLMP5" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP5" name="checkLMP5" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM6();' > </td>
                                <td>6</td>
                                <td>
                                    Están determinadas las cantidades max/min y las reglas FIFO se llevan a cabo.
                                    
                                    <div id="descAccionesM6" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM6" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo6LM" class="form-control" name="hallazgo6" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion6LM" class="form-control" name="accion6" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable6LM" class="form-control" name="responsable6" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte6LM" class="form-control" name="soporte6" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control" id="fInicio6" name="fInicio6" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM6" name="fFinLM6" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM6" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM6()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM6" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC6()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP6" name="checkLMP6" value="1"class="rdBtn" >                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP6" name="checkLMP6" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM7();' > </td>
                                <td>7</td>
                                <td>
                                    Los lugares de trabajo de los team leaders se encuentran organizados e identificados de acuerdo al estándar.
                                    
                                    <div id="descAccionesM7" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM7" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo7LM" class="form-control" name="hallazgo7" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion7LM" class="form-control" name="accion7" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable7LM" class="form-control" name="responsable7" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte7LM" class="form-control" name="soporte7" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control" id="fInicio7" name="fInicio7" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM7" name="fFinLM7" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM7" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM7()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM7" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC7()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP7" name="checkLMP7" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP7" name="checkLMP7" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM8();' > </td>
                                <td>8</td>
                                <td>
                                    El interior y exterior de los equipos, maquinaria, y mobiliario se encuentra limpio, libre de polvo, aceite, grasa, residuos, etc.
                                    
                                    <div id="descAccionesM8" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM8" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo8LM" class="form-control" name="hallazgo8" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion8LM" class="form-control" name="accion8" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable8LM" class="form-control" name="responsable8" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte8LM" class="form-control" name="soporte8" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio8" name="fInicio8" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM8" name="fFinLM8" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM8" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM8()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM8" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC8()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP8" name="checkLMP8" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP8" name="checkLMP8" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM9();' > </td>
                                <td>9</td>
                                <td>
                                    La identificación de materiales (piso, etiquetas, marbetes) están limpios y en buenas condiciones.
                                    
                                    <div id="descAccionesM9" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM9" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo9LM" class="form-control" name="hallazgo9" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion9LM" class="form-control" name="accion9" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable9LM" class="form-control" name="responsable9" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte9LM" class="form-control" name="soporte9" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio9" name="fInicio9" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM9" name="fFinLM9" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM9" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM9()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM9" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC9()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP9" name="checkLMP9" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP9" name="checkLMP9" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM10();' > </td>
                                <td>10</td>
                                <td>
                                    Los productos y materiales de limpieza tienen un espacio asignado y se encuentran en buenas condiciones.
                                    
                                    <div id="descAccionesM10" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM10" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo10LM" class="form-control" name="hallazgo10" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion10LM" class="form-control" name="accion10" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable10LM" class="form-control" name="responsable10" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte10LM" class="form-control" name="soporte10" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio10" name="fInicio10" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM10" name="fFinLM10" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM10" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM10()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM10" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC10()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP10" name="checkLMP10" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP10" name="checkLMP10" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                        </tbody>
                    </table>
                    <!--Descripción rápida-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>

