<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('d/m/Y');
$fNow = date('Y-m-d');
$fProp = date('d/m/Y' ,strtotime('+1 day', strtotime($fNow)));

?>

<style>
    td, th {
        border: 1px solid gray;
    }

    .bordesCompletos {
        border: 1px solid gray;
    }
    
    .cabecera {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
    }
    
    .filaPregunta {
        height: 44px;
        font-size: 21px;
    }
    
    .centrado {
        align: center;
        text-align: center;
        align-content: center;
    }
    
    /*TAMAÑO DE LOS RADIO BUTOM*/
    .rdBtn {
        width: 25px;
        height: 25px;
    }
    
</style>

<form id="fLAMonthly1" method="post">
    <div class="modal" tabindex="-1" role="dialog" id="mLabMonthly1">
        <div class="modal-dialog modal-lg" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title text-center all-tittles"> MENSUAL DE 5S's (LABORATORIO) </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertLAMonthly1"> </div>                    
                    <table style="width: 100%;" >
                        <thead class="cabecera" >
                            <tr>
                                <td rowspan="2" style="width: 4%" > INFO </td>
                                <td rowspan="2" > No. </td>
                                <td rowspan="2" > PUNTO A REVISAR </td>
                                <td colspan="2" style="width: 6%" > CUMPLE </td>                                
                            </tr>
                            <tr>
                                <td> SI &nbsp;</td>
                                <td> NO </td>
                            </tr>
                        </thead>
                        <tbody >
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM1();' > </td>
                                <td>1</td>
                                <td > No se encuentran condiciones inseguras.
                                    (Ej. Cables en mal estado, derrames de líquidos, obstrucción de equipos de emergencia, rejas/racks/mobiliario en mal estado, etc.) 
                                    
                                    <div id="descAcciones1LAM" style="color:#337ab7;" > </div>
                                    
                                    <div class="panel no" id="pnlOplCheckLAM1" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo1LAM" class="form-control" name="hallazgo1" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion1LAM" class="form-control" name="accion1" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable1LAM" class="form-control" name="responsable1" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte1LAM" class="form-control" name="soporte1" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin1LAM" name="fFin1" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave1LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM1()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar1LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC1()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </td>                                
                                <td class="centrado" colspan="2">
                                    <input type="radio" id="checkLAMP1" name="checkLAMP1" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP1" name="checkLAMP1" value="0" class="rdBtn"> 
                                </td>                      
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM2();' > </td>
                                <td>2</td>
                                <td >
                                    La línea/área se encuentra libre de objetos innecesarios tales como objetos personales, herramientas, objetos bloqueando el paso, etc.
                                    
                                    <div id="descAcciones2LAM" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLAM2" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo2LAM" class="form-control" name="hallazgo2" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion2LAM" class="form-control" name="accion2" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable2LAM" class="form-control" name="responsable2" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte2LAM" class="form-control" name="soporte2" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin2LAM" name="fFin2" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave2LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM2()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar2LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC2()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP2" name="checkLAMP2" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP2" name="checkLAMP2" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM3();' > </td>
                                <td>3</td>
                                <td>
                                    Todos los objetos necesarios tienen un lugar asignado y se encuentran ordenados.
                                    
                                    <div id="descAcciones3LAM" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLAM3" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo3LAM" class="form-control" name="hallazgo3" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion3LAM" class="form-control" name="accion3" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable3LAM" class="form-control" name="responsable3" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte3LAM" class="form-control" name="soporte3" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin3LAM" name="fFin3" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                    
                                                <div class="form-inline">
                                                    <div id="btnSave3LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM3()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar3LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC3()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP3" name="checkLAMP3" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP3" name="checkLAMP3" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM4();' > </td>
                                <td>4</td>
                                <td>
                                    Maquinarias, mobiliario, materiales, químicos, etc., se encuentran correctamente identificados y de acuerdo al estándar.                                    
                                    <div id="descAcciones4LAM" style="color:#337ab7;" > </div>

                                    <div class="panel no" id="pnlOplCheckLAM4" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo4LAM" class="form-control" name="hallazgo4" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion4LAM" class="form-control" name="accion4" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable4LAM" class="form-control" name="responsable4" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte4LAM" class="form-control" name="soporte4" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin4LAM" name="fFin4" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave4LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM4()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar4LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC4()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP4" name="checkLAMP4" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP4" name="checkLAMP4" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM5();' > </td>
                                <td>5</td>
                                <td>
                                    El interior y exterior del mobiliario/maquinaria se encuentra libre de basura/polvo/aceite/rebaba.
                                    
                                    <div id="descAcciones5LAM" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLAM5" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo5LAM" class="form-control" name="hallazgo5" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion5LAM" class="form-control" name="accion5" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable5LAM" class="form-control" name="responsable5" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte5LAM" class="form-control" name="soporte5" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin5LAM" name="fFin5" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave5LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM5()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar5LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC5()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP5" name="checkLAMP5" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP5" name="checkLAMP5" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM6();' > </td>
                                <td>6</td>
                                <td>
                                    La basura es tirada en el bote acorde a su código de color.
                                    
                                    <div id="descAcciones6LAM" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLAM6" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo6LAM" class="form-control" name="hallazgo6" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion6LAM" class="form-control" name="accion6" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable6LAM" class="form-control" name="responsable6" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte6LAM" class="form-control" name="soporte6" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin6LAM" name="fFin6" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave6LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM6()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar6LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC6()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP6" name="checkLAMP6" value="1"class="rdBtn" >                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP6" name="checkLAMP6" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM7();' > </td>
                                <td>7</td>
                                <td>
                                    Los colaboradores conocen qué son las 5S's (preguntar a un colaborador qué son las 5S's y cuál es su beneficio en el lugar de trabajo).
                                    
                                    <div id="descAcciones7LAM" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLAM7" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo7LAM" class="form-control" name="hallazgo7" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion7LAM" class="form-control" name="accion7" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable7LAM" class="form-control" name="responsable7" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte7LAM" class="form-control" name="soporte7" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicio7LAM" class="form-control" name="fInicio7" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin7LAM" name="fFin7" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave7LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM7()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar7LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC7()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP7" name="checkLAMP7" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP7" name="checkLAMP7" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM8();' > </td>
                                <td>8</td>
                                <td>
                                    El estándar físico y las mamparas de la línea/área se encuentran actualizadas y desplegadas en el lugar correspondiente.
                                    
                                    <div id="descAcciones8LAM" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLAM8" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo8LAM" class="form-control" name="hallazgo8" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion8LAM" class="form-control" name="accion8" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable8LAM" class="form-control" name="responsable8" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte8LAM" class="form-control" name="soporte8" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin8LAM" name="fFin8" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave8LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM8()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar8LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC8()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP8" name="checkLAMP8" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP8" name="checkLAMP8" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM9();' > </td>
                                <td>9</td>
                                <td>
                                    El check list del día anterior se llevó a cabo así como las actividades de 5S's al finalizar el turno (o en paros de línea).
                                    
                                    <div id="descAcciones9LAM" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLAM9" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo9LAM" class="form-control" name="hallazgo9" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion9LAM" class="form-control" name="accion9" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable9LAM" class="form-control" name="responsable9" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte9LAM" class="form-control" name="soporte9" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin9LAM" name="fFin9" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave9LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM9()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar9LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC9()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP9" name="checkLAMP9" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP9" name="checkLAMP9" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLAM10();' > </td>
                                <td>10</td>
                                <td>
                                    Se cerró al menos un punto abierto del día anterior.
                                    
                                    <div id="descAcciones10LAM" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLAM10" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo10LAM" class="form-control" name="hallazgo10" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion10LAM" class="form-control" name="accion10" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable10LAM" class="form-control" name="responsable10" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte10LAM" class="form-control" name="soporte10" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input id="fInicioLAM" class="form-control" name="fInicio" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span ><b> Fecha Compromiso: </b></span>
                                                            <input id="fFin10LAM" name="fFin10" type="text" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px" >                                                     
                                                <div class="form-inline">
                                                    <div id="btnSave10LAM" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLAM10()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelar10LAM" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLAMC10()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLAMP10" name="checkLAMP10" value="1" class="rdBtn">
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLAMP10" name="checkLAMP10" value="0" class="rdBtn">
                                </td>                                
                            </tr>
                        </tbody>
                    </table>
                    <!--Descripción rápida-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>

