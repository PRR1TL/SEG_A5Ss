<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('d/m/Y');
$fNow = date('Y-m-d');
$fProp = date('d/m/Y' ,strtotime('+1 day', strtotime($fNow)));

?>

<style>
    td, th {
        border: 1px solid gray;
    }

    .bordesCompletos {
        border: 1px solid gray;
    }
    
    .cabecera {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
    }
    
    .filaPregunta {
        height: 44px;
        font-size: 21px;
    }
    
    .centrado {
        align: center;
        text-align: center;
        align-content: center;
    }
    
    /*TAMAÑO DE LOS RADIO BUTOM*/
    .rdBtn {
        width: 25px;
        height: 25px;
    }
    
</style>

<form id="fLMonthly2" method="post">
    <div class="modal" tabindex="-1" role="dialog" id="mLMonthly2">
         <div class="modal-dialog modal-lg" style="width: 100%" >
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center all-tittles"> MENSUAL DE 5S's (LINEA) </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertLMonthly2"> </div>
                    
                    <table style="width: 100%;" >
                        <thead class="cabecera" >
                            <tr>
                                <td rowspan="2" style="width: 4%" > INFO </td>
                                <td rowspan="2" > No. </td>
                                <td rowspan="2" > PUNTO A REVISAR </td>
                                <td colspan="2" style="width: 8%" > CUMPLE </td>                                
                            </tr>
                            <tr>
                                <td> SI &nbsp;</td>
                                <td> NO </td>
                            </tr>
                        </thead>
                        <tbody >
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM11();' > </td>
                                <td>11</td>
                                <td > Todos los asociados estan involucrados en el rol de limpieza.                                    
                                    <div id="descAccionesM11" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM11" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo11LM" class="form-control" name="hallazgo1" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion11LM" class="form-control" name="accion1" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable11LM" class="form-control" name="responsable1" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte11LM" class="form-control" name="soporte1" maxlength="150" style="width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio1" name="fInicio1" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input id="fFinLM1" class="form-control" type="text" name="fFinLM1" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM11" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM11()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM11" class="no" style="width: 10%; margin-left: 95%; margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC11()" data-zdp_readonly_element="false" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>                                
                                <td class="centrado"  colspan="2">                                    
                                    <input type="radio" id="checkLMP11" name="checkLMP11" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP11" name="checkLMP11" value="0" class="rdBtn"> 
                                </td>                      
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM12();' > </td>
                                <td>12</td>
                                <td >
                                    Existen HTE´S para el uso de instalaciones, procedimientos/ procesos en los que se requiere.                                    
                                    <div id="descAccionesM12" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM12" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo12LM" class="form-control" name="hallazgo2" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion12LM" class="form-control" name="accion2" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable12LM" class="form-control" name="responsable2" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte12LM" class="form-control" name="soporte2" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio2" name="fInicio2" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM2" name="fFinLM2" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM12" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM12()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM12" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC12()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP12" name="checkLMP12" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP12" name="checkLMP12" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM13();' > </td>
                                <td>13</td>
                                <td>
                                    La información desplegada en el área de trabajo (mamparas, tableros, pantallas) está actualizada y en buenas condiciones                                    
                                    <div id="descAccionesM13" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM13" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo13LM" class="form-control" name="hallazgo3" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion13LM" class="form-control" name="accion3" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable13LM" class="form-control" name="responsable3" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte13LM" class="form-control" name="soporte3" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control"  id="fInicio3" name="fInicio3" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM3" name="fFinLM3" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                   
                                                <div class="form-inline">
                                                    <div id="btnSaveM13" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM13()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM13" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC13()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP13" name="checkLMP13" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP13" name="checkLMP13" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta" >
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM14();' > </td>
                                <td>14</td>
                                <td>
                                    El diagrama de escalamiento está disponible en el área de trabajo.                                 
                                    <div id="descAccionesM14" style="color:#337ab7;" > </div>
                                    <div class="panel no" id="pnlOplCheckLM14" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo14LM" class="form-control" name="hallazgo4" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion14LM" class="form-control" name="accion4" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable14LM" class="form-control" name="responsable4" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte14LM" class="form-control" name="soporte4" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control" id="fInicio4" name="fInicio4" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM4" name="fFinLM4" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM14" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM14()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM14" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC14()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP14" name="checkLMP14" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP14" name="checkLMP14" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                            <tr class="filaPregunta">
                                <td class="centrado" > <img src="./imagenes/ico/info.png" OnmouseOver='return dLM15();' > </td>
                                <td>15</td>
                                <td>
                                    Los resultados del check list semanal de 5S's están publicados en el indicador y se encuentra actualizado.                                    
                                    <div id="descAccionesM15" style="color:#337ab7;" > </div>                                    
                                    <div class="panel no" id="pnlOplCheckLM15" >
                                        <div class="panel panel-info" style="width: 98%; margin-left: 1%" >
                                            <div class="panel-heading" >Opl </div>
                                            <div class="panel-body" >
                                                <div class="btn-group btn-group-justified" role="group" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b>Hallazgo:</b></span>
                                                            <input id="hallazgo15LM" class="form-control" name="hallazgo5" maxlength="150" style="width: 83%;" placeholder="Hallazgo" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Acción: </b></span>
                                                            <input id="accion15LM" class="form-control" name="accion5" maxlength="150" style="width: 89%;" placeholder="Tarea / Acción / Info" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-group btn-group-justified" role="group" style="margin-top: 10px" >
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Responsable: </b></span>
                                                            <input id="responsable15LM" class="form-control" name="responsable5" maxlength="150" style="width: 58%;" placeholder="Responsable" type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Soporte: </b></span>
                                                            <input id="soporte15LM" class="form-control" name="soporte5" maxlength="150" style=" width: 67%;" placeholder="Soporte" />
                                                        </div>
                                                    </div>                                        
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" >
                                                            <span ><b> Fecha Inicio: </b></span>
                                                            <input class="form-control" id="fInicio5" name="fInicio5" maxlength="172" style="width: 30%;" value="<?php echo $fInicio?>" type="text" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="btn-group" role="group" >
                                                        <div class="form-inline" style="margin-right: -44px" >
                                                            <span><b> Compromiso:&nbsp; </b></span>
                                                            <input type="text" id="fFinLM5" name="fFinLM5" class="form-control" style="width: 15vh;" value="<?php echo $fProp ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="height: 60px">                                                    
                                                <div class="form-inline">
                                                    <div id="btnSaveM15" style="width: 10%; margin-left: 89%" >
                                                        <button type="button" class="btn btn-info" onclick="oplCheckLM15()" > Guardar </button>
                                                    </div>
                                                    <div id="btnCancelarM15" class="no" style="width: 10%; margin-left: 95%;  margin-top: -4.3vh;" >
                                                        <button type="button" class="btn btn-warning" onclick="oplCheckLMC15()" > Cerrar </button> &nbsp;&nbsp;&nbsp;                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="centrado" colspan="2" >
                                    <input type="radio" id="checkLMP15" name="checkLMP15" value="1" class="rdBtn">                                     
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="checkLMP15" name="checkLMP15" value="0" class="rdBtn"> 
                                </td>                                
                            </tr>
                        </tbody>
                    </table>
                    <!--Descripción rápida-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" > Siguiente &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                </div>
            </div>
        </div>
    </div>
</form>

