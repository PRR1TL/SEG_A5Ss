<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   
?>

<form id="fUSChamp">
    <div class="modal " id="mUSChamp" tabindex="-1" role="dialog" data-backdrop="false" >
        <div class="modal-dialog modal-lg" style="width: 70%" >
            <div class="modal-content">  
                <div class="modal-header">
                    <h4 class="modal-title col-lg-12" id="exampleModalLabelU" align="center">MODIFICACION</h4>
                    <button type="button" data-dismiss="modal" style="background-color: transparent; border: 0; margin-top: -3.5vh; margin-left: 1%;" > <img src="./imagenes/cl.png" > </button>
                </div>
                <div class="modal-body col-xs-12 col-sh-4 col-md-6 col-lg-12" style="max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertUUsuario"> </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-4" >
                            <label class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" > Privilegio: </label>
                            <select id="privilegioSChamp" name="privilegioSChamp" type ="text" class="form-control control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12">
                                <option value=" - " selected disabled > - </option>
                                <option value="6"> Champion </option>
                                <option value="7"> Coworker </option>
                            </select>  
                        </div> 
                        <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-4" >
                            <label class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" > Estado: </label>
                            <select id="estadoSChamp" name="estadoSChamp" type ="text" class="form-control control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12">
                                <option value="1"> Activo </option>
                                <option value="0"> Desactivo </option>
                            </select> 
                        </div> 
                    </div> 
                    <hr> 
                    <div class="row">
                        <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-4" >
                            <label for="nombre0" class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12">Usuario:</label> 
                            <input type="text" class="form-control" id="usuarioSChampU" name="usuarioSChampU" minlength="5" maxlength="6" required >                            
                        </div> 
                        <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-8" >    
                            <label for="nombre0" class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" >Nombre:</label> 
                            <input type="text" class="form-control control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" id="nombreSChampU" name="nombreSChampU" minlength="10" maxlength="48" required >
                        </div> 
                        <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-8" > 
                            <label for="moneda0" class="control-label">Correo: </label> 
                            <input type="text" class="form-control control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" id="correoSChampU" name="correoSChampU" axlength="100"  >
                        </div> 
                        <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-4" >
                            <label for="moneda0" class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" >Contraseña: </label> 
                            <input type="password" class="form-control control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" id="passSChampU" name="passSChampU" minlength="5" maxlength="20" placeholder="SOLO SI DESEA CAMBIAR CONTRASEÑA" >
                        </div> 
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>