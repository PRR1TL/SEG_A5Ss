<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

?>

<style>
    .no {
        display:none;
    }
    .si {
        display:block;
    }
</style>

<script>    
    
</script>

<form id="fIUsuario" >
    <div class="modal fade" id="mIUsuario" tabindex="-1" role="dialog" data-backdrop="false" >
        <div class="modal-dialog modal-lg" style="width: 70%" >
            <div class="modal-content">  
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel" align="center">NUEVO USUARIO</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto; ">
                    <div id="alertUser"></div>
                    <div class="form-group">
                        <label class="control-label"> Privilegio: </label>
                        <select id="privilegio" name="privilegio" onchange="fPrivilegio()" type ="text" class="form-control" style="width: 30vh">
                            <option value=" - " selected disabled > - </option>
                            <option value="1"> Administrador </option>
                            <option value="2"> Presidente </option>
                            <option value="3"> Supervisor / Gerente </option>
                            <option value="4"> Supervisor / Gerente </option>
                            <option value="4"> Ing de proceso </option>
                            <option value="6"> Champion </option>
                        </select>             
                    </div>
                    <hr>                        
                    <div id="pnlSup" class="no">
                        <div id="alertUserS"></div>
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="areaSIU" name="areaSIU" onchange="fCValorSIU()" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="0" selected disabled> - </option>
                                <option value="1"> Piso </option>
                                <option value="2"> Oficina </option>
                            </select>

                            <label class="control-label" style="margin-top: -10vh; margin-left: 33vh" >Cadena de valor: </label>
                            <select id="cadValorIU" name="cadValorIU" onchange="fDeptosIU()" type ="text" class="form-control" style="width: 25vh; margin-top: -4vh; margin-left: 32vh" >
                            </select> 
                            
                            <div id="pnlDeptoIU" name="pnlDeptoIU" class="no">
                                <label class="control-label" style="margin-top: -9.5vh; margin-left: 63vh">Departamento: </label>
                                </select>                                
                                    <select id="deptoSIU" name="deptoSIU" type ="text" class="form-control" style=" margin-top: -4vh; margin-left: 62vh; width: 25vh">
                                </select> 
                            </div>
                        </div>
                                               
                    </div>
                    
                    <div id="pnlChamp" class="no">
                        <div id="alertUserCh"></div>
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="areaCh" name="areaCh" onchange="fDeptosCh()" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="0" selected disabled> - </option> 
                                <option value="1">Piso</option> 
                                <option value="2">Oficina</option> 
                            </select> 
                            <label class="control-label" style="margin-top: -10vh; margin-left: 33vh">Departamento: </label> 
                            </select> 
                                <select id="deptoCh" name="deptoCh" type ="text" class="form-control" style="width: 30vh; margin-top: -4vh; margin-left: 32vh">
                            </select>
                        </div>                        
                    </div> 
                    
                    <div id="pnlAdmin" class="no">
                        <div id="alertUserA"></div>
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="tipoA" name="tipoA" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="1"> Administrador total </option>
                                <option value="6"> Champion Área </option>
                            </select> 
                        </div>
                    </div> 
                    
                    <div id="datosGeneralesI" class="no">
                        <div class="form-group">
                            <label for="nombre0" class="control-label">Usuario:</label>
                            <label for="nombre0" class="control-label" style="width: 45%; margin-top: -25px; margin-left: 25vh">Nombre:</label>
                            <input type="text" class="form-control" id="usuarioI" name="usuarioI" onkeypress="return permite(event,'car')" minlength="5" maxlength="6" style="width: 30vh" >
                            <input type="text" class="form-control" id="nombreI" name="nombreI" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 60vh; margin-top: -35px; margin-left: 32vh" >
                        </div>
                        <div class="form-group">
                            <label for="moneda0" class="control-label">Correo: </label>
                            <input type="text" class="form-control" id="correoI" name="correoI" onkeypress="return permite(event,'num')" maxlength="100" style="width: 60vh" >
                            <label for="moneda0" class="control-label " style="margin-top: -10vh; margin-left: 62vh;">Contraseña: </label>
                            <input type="password" class="form-control" id="passI" name="passI" minlength="5" onkeypress="return permite(event,'num')" maxlength="20" style="width: 30vh; margin-left: 62vh; margin-top: -33px" >
                        </div> 
                    </div>    
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>
