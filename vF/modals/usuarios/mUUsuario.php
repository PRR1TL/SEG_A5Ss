<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   
?>

<style>
    .no {
        display:none;
    }
    .si {
        display:block;
    }
</style>

<form id="fUUsuario">
    <div class="modal fade" id="mUUsuario" tabindex="-1" role="dialog" data-backdrop="false" >
        <div class="modal-dialog modal-lg" style="width: 70%" >
            <div class="modal-content">  
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabelU" align="center">MODIFICACION</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                </div>
                <div class="modal-body" style="max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertUUsuario"> </div>
                    <div class="form-group">
                        <label class="control-label"> Privilegio: </label>
                        <select id="privilegioU" name="privilegioU" onchange="fPrivilegioU()" type ="text" class="form-control" style="width: 30vh" >
                            <option value=" - " selected disabled > - </option>
                            <option value="1"> Administrador </option>
                            <option value="2"> Supervisor / Gerente </option>
                            <option value="3"> Champion </option>
                        </select>  
                        
                        <label class="control-label" style="margin-top: -9.8vh; margin-left: 32.8vh; " > Estado: </label>
                        <select id="estadoU" name="estadoU" type ="text" class="form-control" style="width: 20vh; margin-top: -4.8vh; margin-left: 32vh; " >
                            <option value="1"> Activo </option>
                            <option value="0"> Desactivo </option>
                        </select>
                        
                    </div> 
                    <hr> 
                    <div id="pnlSupU" class="no"> 
                        <div id="alertUserSU"></div> 
                        <div class="form-group"> 
                            <label class="control-label" >Área: </label> 
                            <select id="areaSU" name="areaSU" onchange="fCValorSU()" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="0" selected disabled> - </option>
                                <option value="1"> Piso </option>
                                <option value="2"> Oficina </option>
                            </select>

                            <label class="control-label" style="margin-top: -10.8vh; margin-left: 33vh" >Cadena de valor: </label>
                            <select id="cadValorSU" name="cadValorSU" onchange="fDeptosUU()" type ="text" class="form-control" style="width: 30vh; margin-top: -4.5vh; margin-left: 32vh" >
                            </select>
                            <input id="deptoSUO" name="deptoSUO" class="no" style="width: 25vh; margin-top: -10.1vh; margin-left: 50vh; border:none;" readonly >
                            
                            <div id="pnlDeptoUU" name="pnlDeptoUU" class="no">
                                <label class="control-label" style="margin-top: -10.8vh; margin-left: 64.5vh">Departamento: </label>
                                </select>                                
                                    <select id="deptoSUU" name="deptoSUU" type ="text" class="form-control" style=" margin-top: -4.5vh; margin-left: 64.5vh; width: 25vh">
                                </select> 
                                
                            </div>                            
                        </div> 
                    </div>
                    
                    <div id="pnlChampU" class="no">
                        <div id="alertUserChU"></div>
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="areaChU" name="areaChU" onchange="fDeptosChU()" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="0" selected disabled> - </option> 
                                <option value="1">Piso</option> 
                                <option value="2">Oficina</option> 
                            </select> 
                            <label class="control-label" style="margin-top: -10vh; margin-left: 33vh">Departamento: </label> 
                            </select> 
                                <select id="deptoChU" name="deptoChU" type ="text" class="form-control" style="width: 30vh; margin-top: -4vh; margin-left: 32vh">
                            </select>
                            <input id="deptoChO" name="deptoChO" style="width: 25vh; margin-top: -5.5vh; margin-left: 64vh; border:none;" readonly >
                        </div>
                    </div> 
                    
                    <div id="pnlAdminU" class="no">
                        <div id="alertUserAU"></div>
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="tipoAU" name="tipoAU" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="1"> Administrador total </option>
                                <option value="2"> Champion Área </option>
                            </select> 
                        </div> 
                    </div>
                    
                    <div id="pnlDatosG" class="no" >
                        <div class="form-group">
                            <label for="nombre0" class="control-label">Usuario:</label>
                            <label for="nombre0" class="control-label" style="width: 45%; margin-top: -25px; margin-left: 25vh">Nombre:</label>
                            <input type="text" class="form-control" id="usuarioU" name="usuarioU" onkeypress="return permite(event,'car')" minlength="5" maxlength="6" style="width: 30vh;" readonly >
                            <input type="text" class="form-control" id="nombreU" name="nombreU" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 60vh; margin-top: -32px; margin-left: 32vh">
                        </div> 
                        <div class="form-group">
                            <label for="moneda0" class="control-label">Correo: </label>
                            <input type="text" class="form-control" id="correoU" name="correoU" onkeypress="return permite(event,'num')" maxlength="100" style="width: 60vh; " >
                            <label for="moneda0" class="control-label " style="margin-top: -10vh; margin-left: 62vh;">Contraseña: </label>
                            <input type="password" class="form-control" id="passU" name="passU" minlength="5" onkeypress="return permite(event,'num')" maxlength="25" style="width: 30vh; margin-left: 62vh; margin-top: -34px" placeholder="SOLO SI DESEA CAMBIAR CONTRASEÑA" >
                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>