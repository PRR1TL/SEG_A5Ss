<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

?>
<form id="fISChamp" method="post" >
    <div class="modal fade" id="mISChamp" tabindex="-1" role="dialog" data-backdrop="false" > 
        <div class="modal-dialog modal-lg" style="width: 100vh" > 
            <div class="modal-content"> 
                <div class="modal-header"> 
                    <h4 class="modal-title text-center all-tittles col-lg-12" style="color: #ffffff" align="center">NUEVO USUARIO</h4> 
                    <button type="button" data-dismiss="modal" style="background-color: transparent; border: 0; margin-top: -3.9vh;" > <img src="./imagenes/cl.png" > </button>                 
                </div> 
                <div class="modal-body col-xs-12 col-sh-4 col-md-6 col-lg-12" style=" max-height: calc(100vh - 190px); overflow-y: auto;"> 
                    <div id="alertUsersChamp"></div> 
                    <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-4" > 
                        <label for="nombre0" class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" >Usuario:</label> 
                        <input type="text" class="form-control control-label col-xs-12 col-sh-2 col-md-3 col-lg-12" id="usuarioSChamp" name="usuarioSChamp" minlength="5" maxlength="6" required >
                    </div> 
                    <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-8" > 
                        <label for="nombre0" class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" >Nombre:</label>                        
                        <input type="text" class="form-control control-label col-xs-12 col-sh-2 col-md-3 col-lg-12" id="nombreSChamp" name="nombreSChamp" minlength="10" maxlength="48" required >
                    </div>                     
                    <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-8"> 
                        <label for="moneda0" class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12">Correo: </label> 
                        <input type="text" class="form-control control-label col-xs-12 col-sh-2 col-md-3 col-lg-12" id="correoSChamp" name="correoSChamp" axlength="100" >
                    </div> 
                    <div class="form-group col-xs-12 col-sh-4 col-md-4 col-lg-4"> 
                        <label for="moneda0" class="control-label form-group col-xs-12 col-sh-2 col-md-3 col-lg-12" >Contraseña: </label> 
                        <input type="password" class="form-control control-label col-xs-12 col-sh-2 col-md-3 col-lg-12" id="passSChamp" name="passSChamp" minlength="5" maxlength="20" required>
                    </div> 
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary">Actualizar datos</button> 
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> 
                </div>
            </div>
        </div>
    </div>
</form>
