<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<form id="fDUsuario">
    <div class="modal fade" id="mDUsuario" tabindex="-1" role="dialog" data-backdrop="false" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title col-lg-12" id="exampleModalLabel">Eliminar:</h4>
                    <button type="button" data-dismiss="modal" style="background-color: transparent; border: 0; margin-top: -3.5vh; margin-left: 1%;" > <img src="./imagenes/cl.png" > </button>
                </div>
                <div class="modal-body" style="display: block;margin:8px">
                    <div id="alertDUsuario" > </div>
                    <p> <h8 class="lead text-muted text-center" style="display: block;margin:8px">Esta acción eliminará de forma permanente el registro.</h8></p>
                    <p> <h5 class="lead text-muted text-center" style="display: block;margin:8px">Estas seguro que deseas eliminar </h5></p>
                    <p> <h5 class="lead text-muted text-center" style="display: block;margin:8px"><br> Usuario:  
                        <b> <input id="usuarioD" name="usuarioD" style="width: 15%; border:none" > </b> perteneciente a : <br>
                        <b> <input type="text" id="nombreD" name="nombreD" style="border:none; width: 100%; text-align: center;"></b> 
                    </h5></p>                            
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-lg btn-primary">Aceptar</button>
                    <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Cancelar</button>                    
                </div>
            </div>
        </div>
    </div>    
</form>
