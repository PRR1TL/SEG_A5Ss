<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

?>

<style>
    .no {
        display:none;
    }
    .si {
        display:block;
    }
</style>

<form id="fUDepto">
    <div class="modal fade" id="mUDepto" tabindex="-1" role="dialog" data-backdrop="false" >
        <div class="modal-dialog modal-lg" style="width: 70%" >
            <div class="modal-content">  
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabelU" align="center">MODIFICACION</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                </div>
                <div class="modal-body" style="max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertUDepto"> </div>  
                    <div id="pnlSupU" >
                        <div id="alertUDepto"></div>
                        <input type="text" class="form-control no" id="idU" name="idU" >
                        <input type="text" class="form-control no" id="acronimo" name="acronimo" >
                        <div class="form-group">
                            <label class="control-label" >Área: </label>
                            <select id="areaDepU" name="areaDepU" onchange="fCValorDeptoU()" type = "text" class="form-control" style="width: 30vh; ">
                                <option value="0" selected disabled> - </option> 
                                <option value="1"> ALMACEN </option> 
                                <option value="2"> LABORATORIO </option> 
                                <option value="3"> LINEA </option> 
                                <option value="4"> MANTENIMIENTO </option> 
                                <option value="5"> OFICINA </option> 
                            </select>

                            <label class="control-label" style="margin-top: -10vh; margin-left: 33vh" >Cadena de valor: </label>
                            <select id="cadValorDepU" name="cadValorDepU" type ="text" class="form-control" style="width: 30vh; margin-top: -4vh; margin-left: 32vh" >
                            </select>
                        </div> 
                    </div> 
                    
                    <div id="pnlDatosG" > 
                        <div class="form-group"> 
                            <label for="nombre0" class="control-label">Acronimo:</label> 
                            <label for="nombre0" class="control-label" style="width: 45%; margin-top: -25px; margin-left: 25vh">Descripcion:</label>
                            <input type="text" class="form-control" id="deptoU" name="deptoU" onkeypress="return permite(event,'car')" minlength="5" maxlength="6" style="width: 30vh;" >
                            <input type="text" class="form-control" id="descU" name="descU" onkeypress="return permite(event,'car')" minlength="5" maxlength="48" style="width: 60vh; margin-top: -32px; margin-left: 32vh">
                        </div>                         
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>



