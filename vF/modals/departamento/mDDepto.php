<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<form id="fDDepto" >
    <div class="modal fade" id="mDDepto" tabindex="-1" role="dialog" data-backdrop="false" >
        <div class="modal-dialog" role="document" >
            <div class="modal-content" >
                <div class="modal-header" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel" >Eliminar:</h4>
                </div>
                <div class="modal-body" style="display: block;margin:8px" >
                    <input type="text" class="form-control no" id="idD" name="idD" >
                    <div id="alertDDepto" > </div>
                    <p> <h8 class="lead text-muted text-center" style="display: block;margin:8px">Esta acción eliminará de forma permanente el registro.</h8></p>
                    <p> <h5 class="lead text-muted text-center" style="display: block;margin:8px">Estas seguro que deseas eliminar </h5></p>
                    <p> <h5 class="lead text-muted text-center" style="display: block;margin:8px"><br> Departamento:  
                    <b> <input id="deptoD" name="deptoD" style="width: 15%; border:none" onlyread > 
                    </h5></p>                            
                </div>
                <div class="modal-footer" >
                    <button type="submit" class="btn btn-lg btn-primary">Aceptar</button>
                    <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Cancelar</button>                    
                </div>
            </div>
        </div>
    </div>    
</form>
