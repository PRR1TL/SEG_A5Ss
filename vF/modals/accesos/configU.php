<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //session_start();

?>
<form id="fuMU">
    <div class="modal" id="mUMIU" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog modal-lg" role="document" style="width: 65vh"  >
            <div class="modal-content"> 
                <div class="modal-header" style="background: #02538B;" >                    
                    <h4 class="col-lg-12" style="color: #ffffff" >MODIFICAR MI USUARIO</h4>
                    <button type="button" data-dismiss="modal" style="background-color: transparent; border: 0; margin-top: -3.5vh; margin-left: 1%;" > <img src="./imagenes/cl.png" > </button>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;"> 
                    <div id="alert_uMIU"> </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="nombre0" class="control-label">Depto/Area:</label>
                            <input type="text" class="form-control" id="miDep" name="miDep" value="<?php echo $_SESSION["depto"] ?>" readonly >
                        </div>
                        <div class="col-lg-6">
                            <label for="nombre0" class="control-label">Usuario:</label>
                            <input type="text" class="form-control" id="miUsuario" name="miUsuario" value="<?php echo $_SESSION["userName"] ?>" readonly >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="nombre0" class="control-label">Nombre:</label>
                            <input type="text" class="form-control" id="miNombre" name="miNombre" value="<?php echo $_SESSION["name"] ?>" readonly >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="pass1" class="control-label">Correo:</label>
                            <input type="text" class="form-control" id="miCorreo" name="miCorreo" value="<?php echo $_SESSION["correo"] ?>" style="text-transform:lowercase;" minlength="12" maxlength="30" required> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <label for="pass1" class="control-label">Contraseña</label>
                            <input type="password" class="form-control" id="miPass" name="miPass" style="text-transform:uppercase;" minlength="6" maxlength="30" required> 
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar datos</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
                </div>
            </div>
        </div>
    </div>
</form>

