<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
    session_start();
    date_default_timezone_set("America/Mexico_City");
    $_SESSION['path'] = $_SERVER['PHP_SELF'];
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
        $typeEv = $_SESSION['ev'];    
    } else {
        $userName = '';
        $name = 'INICIAR SESION';
        $typeUser = 0;
        $typeEv = 0;
    }

?>

<head>
    <!--LIBRERIAS DE BOOSTRAP--> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
     <!-- LIBRERIAS PARA HIGHCHART -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>    
    
    <!--LINK PARA ICONOS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    
    <!--ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS-->
    <link rel="stylesheet" href="css/modulo.css">    
    <script src="js/index.js"> </script>
    
    <script type="text/javascript">
//        jQuery().ready(            
//            function() {
//                getResult();
//                setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
//            }
//        );
//    
//        function getResult() {
//            //CUANDO SE ACTIVA LA HOJA TENDREMOS QUE MANDAR SOLO EL TEMA, PARA PODER HACER LA CONSULTA ADECUADA
//            //PARA ELLO SE UTILIZA AJAX
//        }

    
    </script>
    
    
    <?php 
        include './modals/informativos/help.php';
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MOD AL INFORMATIVOS
        include './modals/accesos/login.php';
        include './modals/accesos/cerrarSesion.php';
        include './modals/informativos/rules.php';
        
        //MODALS PARA LA SELECCION DE AUDITORIAS
        //DE ACUERDO A LOS PRIVILEGIOS DE USUARIOS
//        switch ($typeUser) {
//            case 1:
                include './modals/vse/AreaAudit.php';
                include './modals/vse/DeptoAudit.php';                
//                break;
//            case 2: 
                include './modals/supervisor/DeptoAudit.php';
//                include './modals/informativos/InfoAuditoria.php';
//                break;
//            case 3:
                include './modals/informativos/NoneAuditoria.php';
                include './modals/informativos/InfoAuditoria.php';
                include './modals/informativos/TipoAuditoriaCh.php';
//                break;
//        }
        
        include './modals/auditoria/daily/daily.php';
        include './modals/informativos/PuntosAuditoriaB.php';
        include './modals/informativos/PuntosAuditoriaR.php';
        include './modals/informativos/PuntosAuditoriaM.php';
        
        include './modals/auditoria/maintenance/mSelect.php';
        include './modals/auditoria/maintenance/mOrder.php';
        include './modals/auditoria/maintenance/mOrder2.php';
        include './modals/auditoria/maintenance/mOrder3.php';
        include './modals/auditoria/maintenance/mClean.php';
        include './modals/auditoria/maintenance/mClean2.php';
        include './modals/auditoria/maintenance/mStandar.php';
        include './modals/auditoria/maintenance/mStandar2.php';   
        include './modals/auditoria/maintenance/mDisc.php';
        
        include './modals/auditoria/office/mSelect.php';
        include './modals/auditoria/office/mOrder.php';                
        include './modals/auditoria/office/mClean.php';                
        include './modals/auditoria/office/mStandar.php';   
        include './modals/auditoria/office/mDisc.php';
        
        //DEACUERDO AL USUARIO VAMOS A PODER HACER LA AUDITORIA Y SE VAN ABRIR LOS MODAL        
        include './modals/auditoria/line/mSelect.php';
        include './modals/auditoria/line/mOrder.php';
        include './modals/auditoria/line/mOrder2.php';
        include './modals/auditoria/line/mOrder3.php';
        include './modals/auditoria/line/mClean.php';
        include './modals/auditoria/line/mClean2.php';
        include './modals/auditoria/line/mStandar.php';
        include './modals/auditoria/line/mStandar2.php';   
        include './modals/auditoria/line/mDisc.php'; 
        
        //AYUDAS VISUALES DE PREGUNTAS DE DAILY
        include './modals/informativos/daily/mDP1.php';        
        include './modals/informativos/daily/mDP2.php';
        include './modals/informativos/daily/mDP3.php';
        include './modals/informativos/daily/mDP4.php';
        include './modals/informativos/daily/mDP5.php';
        include './modals/informativos/daily/mDP6.php';
        include './modals/informativos/daily/mDP7.php';
        include './modals/informativos/daily/mDP8.php';
        include './modals/informativos/daily/mDP9.php';
        include './modals/informativos/daily/mDP10.php';
        
        //OPL
        include './modals/opl/mIOPL.php';
        
        
        $anio = date("Y");         
        $month[1]= "JAN";
        $month[2]= "FEB";
        $month[3]= "MAR";
        $month[4]= "APR";
        $month[5]= "MAY";
        $month[6]= "JUN";
        $month[7]= "JUL";
        $month[8]= "AUG";
        $month[9]= "SEP";
        $month[10]= "OCT";
        $month[11]= "NOV";
        $month[12]= "DEC"; 
        
    
    ?>
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align = center id="headerFixed" class="contenedor"> 
        <div class='fila0 col-lg-6'>
            <img src="imagenes/log.jpg" style="height: 5.2vh; margin-top: 2.7vh" >
        </div>        
        <div class="fila1 col-lg-6" >
            <h3 class="tituloPortal" >
                PORTAL 5S's: Auditorias
            </h3 >
            <?php if ($name != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" onclick="logiin()" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 
</head>

<body>
    <div id="main" style="border: 1px solid #FFF;" >
        <ul <?php if ($typeUser == 1) { ?> id="navA" <?php } else { ?> id="navC" <?php } ?> >
            <li ><a href="./index.php">Inicio</a></li>
            <?php if ($typeUser == 1) { ?>
            <li><a href="admin.php">Administracion</a> </li>
            <?php } ?>
            <li><a class="btn-new-audit" >Auditoria</a></li>
            <li><a href="calendar.php">Calendario</a></li>
            <li><a href="info.php">Informacion</a></li>
            <li><a href="opl.php">OPL</a></li>
        </ul>
        
        <div style="margin-left: 82%; margin-top: 3%;">
            <form  id = "fCalcular" method = "POST"  >
                <ul class="nav">
                    <li>
                        ANIO:
                        <select id="anio"> 
                            <option value="<?PHP echo $anio ?>" ><?php echo $anio ?></option>
                            <option value="<?PHP echo $anio-1 ?>" ><?php echo $anio-1 ?></option>
                        </select>
                    </li>                            
                    <li>
                        MES: 
                        <select id="mes" >
                            <option value="0">ALL</option>
                            <?php for ($i = 1; $i < 13; $i++){ ?>
                            <option value="<?PHP echo $i ?>" ><?php echo $month[$i] ?></option>
                            <?php } ?>
                        </select>
                    </li>
                </ul>
            </form>
        </div>
        
    </div>   
    <div id="menuPrincipalReportes" >
        <ul class="nav nav-tabs  panel-default" role="tablist">
            <li class="nav-item">
              <a class="nav-link" oncli>General</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#r_area">Área</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content" id = "panel">
            <div id="r_general" class="container tab-pane "><br>
                
                <div id="contGeneral" style="margin-left: -10%; margin-top: -3%; width: 120%" >
                    
                </div>
            </div>
            <div id="r_area" class="container tab-pane "><br>
                <div>
                    
                </div>
                <div id="contArea">
                    
                </div>
            </div>            
        </div>
    </div>    
</body>
