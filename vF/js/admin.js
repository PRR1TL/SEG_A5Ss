/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * CREATED  : 29-01-2019
 * 
 */

$(document).ready(function() {
    var tipoEv = 0;
    var tipoAudit = 0;
    
    $('.btn-logOut').on('click', function(e){
        $('#ModalLogOut').modal({
            show: true,
            backdrop: false
        });
    });
    
    /* MODULO DE USUARIOS */ 
    $('#mUUsuario').on('show.bs.modal', function (event) {
        var depto;
        var button = $(event.relatedTarget); // Botón que activó el modal
        var dep = button.data('depto'); // Extraer la información de atributos de datos
        var privilegio = button.data('priv'); // Extraer la información de atributos de datos
        var area = button.data('area'); // Extraer la información de atributos de datos
        var usuario = button.data('usuario'); // Extraer la información de atributos de datos
        var nombre = button.data('nombre'); // Extraer la información de atributos de datos
        var correo = button.data('correo'); // Extraer la información de atributos de datos
        var estado = button.data('estado');
       
        depto = dep.trim();
        console.log("depto:"+depto+" priv: "+privilegio+" a: "+area+" u: "+usuario+" n: "+nombre+" c: "+correo);
        
        var modal = $(this);
        modal.find('.modal-body #privilegioU').val(privilegio);
        switch(privilegio){
            case 1: //ADMINISTRADOR
                pnlAdminU.className = 'si'; 
                pnlSupU.className = 'no'; 
                pnlChampU.className = 'no';
                pnlDatosG.className = 'si';
                
                //MANDAMOS LOS VALORES A COMPONENTES DE ADMININISTRADsOR
                modal.find('.modal-body #tipoAU').val(area); 
                
                break;  
                
            case 2://SUP / GERENTES 
                if (area < 5){
                    modal.find('.modal-body #areaSU').val('1');
                } else {
                    modal.find('.modal-body #areaSU').val('2');
                }                
                
                fCValorSU();
                pnlAdminU.className = 'no'; 
                pnlSupU.className = 'si';
                pnlChampU.className = 'no'; 
                pnlDatosG.className = 'si'; 
                
                //MANDAMOS LOS VALORES A COMPONENTES DE ADMININISTRADOR                
                modal.find('.modal-body #cadValorSU').val(depto); 
                modal.find('.modal-body #deptoSUO').val(depto);
                break;
                
            case 3: //CHAMPION
                modal.find('.modal-body #areaChU').val(area); 
                fDeptosChU();
                pnlAdminU.className = 'no'; 
                pnlSupU.className = 'no'; 
                pnlChampU.className = 'si'; 
                pnlDatosG.className = 'si'; 
                
                modal.find('.modal-body #deptoChU').val(depto);
                modal.find('.modal-body #deptoChO').val(depto); 
                //MANDAMOS LOS VALORES A COMPONENTES DE ADMININISTRADOR
                break;
        }
        
        modal.find('.modal-title').text('Modificar usuario: ('+usuario+'/'+dep+') '+ nombre);        
        modal.find('.modal-body #estadoU').val(estado); 
        modal.find('.modal-body #usuarioU').val(usuario); 
        modal.find('.modal-body #nombreU').val(nombre); 
        modal.find('.modal-body #correoU').val(correo); 
    });
    
    $('#mDUsuario').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var depto = button.data('depto'); // Extraer la información de atributos de datos
        var usuario = button.data('usuario'); // Extraer la información de atributos de datos
        var nombre = button.data('nombre'); // Extraer la información de atributos de datos

        var modal = $(this);
        modal.find('.modal-body #usuarioD').val(usuario);
        modal.find('.modal-body #nombreD').val(nombre);
    });
    
    $('#fIUsuario').submit(function( event ) {     
        var datA = [];
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/usuarios/iUsuario.php",
            data: parametros,                
            success: function(datos){
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();
                    //console.log(i+": "+datA[i]); ;
                }
                
                if (datA[0] == 'Bien') {
                    $("#alertUser").html(datA[1]);
                    location.href = datA[2];
                } else {
                    $("#alertUser").html(datA[0]);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $("#fDUsuario").submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/dUsuario.php",
            data: parametros,
            success: function(datos) {
                var datA = [];
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();                    
                }
                
                if (datA[0] == 'Bien') {
                    $("#alertDUsuario").html("<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ACTUALIZACION EXITOSA<BR> </strong></div>");
                }   
                location.href=datA[1];
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $("#fUUsuario").submit(function( event ) {
        //alert("Bien Areli");
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/usuarios/uUsuario.php",
            data: parametros,
            success: function(datos) {
                var datA = [];
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();                    
                }
                
                if (datA[0] == 'Bien') {
                    $("#alertDUsuario").html("<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ACTUALIZACION EXITOSA<BR> </strong></div>");
//                    location.href="./champions.php";
                    location.href=datA[1];
                }   
                
                //$("#alertDUsuario").html("<div class='alert alert-danger' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ALGO OCURRIO (admin.js: Linea 260)<BR> </strong></div>");
                //$("#alertUUsuario").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    /** CADENA DE VALOR **/
    $('#mUCadValor').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var area = button.data('area'); // Extraer la información de atributos de datos
        var cadvalor = button.data('cadvalor'); // Extraer la información de atributos de datos
        
        var modal = $(this);
        modal.find('.modal-body #deptoU').val(area);
        modal.find('.modal-body #acronimoU').val(cadvalor);
        modal.find('.modal-body #acronimoU2').val(cadvalor);
    }); 
    
    $('#mDCadValor').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var cadvalor = button.data('cadvalor'); // Extraer la información de atributos de datos
        
        var modal = $(this);
        modal.find('.modal-body #cadValorD').val(cadvalor);
    });    
    
    $("#fICadValor" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/cadValor/iCadValor.php",
            data: parametros,
            success: function(datos) {
                var respuesta = datos.trim();                
                if(respuesta == 'Bien') {
                    $("#mICadValor").hide();
                    //Refrescamos la ventana
                    location.href="./cadValor.php";
                } else {
                    $("#alertICadValor").html(datos);
                } 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $("#fUCadValor" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/cadValor/uCadValor.php",
            data: parametros,
            success: function(datos) {
                var respuesta = datos.trim(); 
                if(respuesta == 'Bien') {
                    $("#mUCadValor").hide();
                    //Refrescamos la ventana
                    location.href="./cadValor.php";
                } else {
                    $("#alertUCadValor").html(datos);
                } 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $("#fDCadValor" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/cadValor/dCadValor.php",
            data: parametros,
            success: function(datos) {
                var respuesta = datos.trim(); 
                if(respuesta == 'Bien') {
                    $("#mUCadValor").hide();
                    //Refrescamos la ventana
                    location.href="./cadValor.php";
                } else {
                    $("#alertUCadValor").html(datos);
                } 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });    
    
    /* DEPARTAMENTOS */
    $('#mUDepto').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var id = button.data('id'); // Extraer la información de atributos de datos
        var tipo = button.data('tipo'); // Extraer la información de atributos de datos
        var depto = button.data('depto'); // Extraer la información de atributos de datos
        var desc = button.data('desc'); // Extraer la información de atributos de datos
        var cvalor = button.data('cvalor'); // Extraer la información de atributos de datos

        //console.log("id: "+id+" tipo: "+tipo+" dep: "+depto+" desc: "+desc+" cval: "+cvalor);
        var modal = $(this);
        modal.find('.modal-body #areaDepU').val(tipo);
        fCValorDeptoU();
        modal.find('.modal-body #idU').val(id);
        modal.find('.modal-body #acronimo').val(depto);
        modal.find('.modal-body #cadValorDepU').val(cvalor);
        modal.find('.modal-body #deptoU').val(depto);
        modal.find('.modal-body #descU').val(desc);
    }); 
    
    $('#mDDepto').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var id = button.data('id'); // Extraer la información de atributos de datos
        var depto = button.data('depto'); // Extraer la información de atributos de datos

        //console.log("id: "+id+" dep: "+depto);        
        var modal = $(this);
        modal.find('.modal-body #idD').val(id);
        modal.find('.modal-body #deptoD').val(depto);
    });    
    
    $("#fIDepto" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/depto/iDepto.php",
            data: parametros,
            success: function(datos) {
                var respuesta = datos.trim(); 
                if(respuesta == 'Bien') {
                    location.href="./deptos.php";
                } else {
                    $("#alertIDepto").html(datos);
                } 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $("#fUDepto" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/depto/uDepto.php",
            data: parametros,
            success: function(datos) {
                var respuesta = datos.trim(); 
                if(respuesta == 'Bien') {
                    location.href="./deptos.php";
                } else {
                    $("#alertUDepto").html(datos);
                } 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $("#fDDepto" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/depto/dDepto.php",
            data: parametros,
            success: function(datos) {
                var respuesta = datos.trim(); 
                if(respuesta == 'Bien') {
                    location.href="./deptos.php";
                } else {
                    $("#alertDDepto").html(datos);
                } 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $('.btn-rules').on('click', function() {
        //alert("algo esta pasando aca ");
        console.log("tipoEv: "+tipoEv);
        var datA = [];        
        //EVALUAMOS EL TIPO DE EVALUACION  
        var parametros = $(this).serialize();
        
        $.ajax({
            type: "POST",        
            url: './db/auditoria/cLAudit_OPL.php',
            data: parametros, 
            success: function(respuesta) {
                var dat = respuesta.trim();
                var d1 = dat.substr(0,1);
                var a1 = dat.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();
                }
                
                lAOPL = datA[0]; 
                lAAudit = datA[1]; 
                console.log("0. datO: "+lAOPL+" datA: "+lAAudit+" tAudit: "+tipoAudit);
               
                switch (tipoEv) { 
                    case '1': //ALMACEN 
                        switch (tipoAudit) { 
                            case '1': //CHECKLIST
                                $('#mADaily').modal({ 
                                    show: true, 
                                    backdrop: false, 
                                    keyboard: false 
                                });
                                break;
                            case '2': //SEMANAL
                                $('#mAWeekly').modal({ 
                                    show: true, 
                                    backdrop: false, 
                                    keyboard: false
                                }); 
                                break;
                            case '3': //MENSUAL
                                $('#mAMonthly1').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                        }
                        break;

                    case '2': //LABORATORIO
                        switch (tipoAudit){
                            case '1': //CHECKLIST
                                $('#mLabDaily').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '2': //SEMANAL
                                $('#mLabWeekly').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '3': //MENSUAL
                                $('#mLabMonthly1').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                        }
                        break;

                    case '3': //LINEA
                        switch (tipoAudit){
                            case '1': //CHECKLIST
                                $('#mLDaily').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '2': //SEMANAL
                                $('#mLWeekly').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '3': //MENSUAL
                                $('#mLMonthly1').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                        }                
                        break;

                    case '4': //MANTENIMIENTO
                        switch (tipoAudit){
                            case '1': //CHECKLIST
                                $('#mMDaily').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '2': //SEMANAL
                                $('#mMWeekly').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '3': //MENSUAL
                                $('#mMMonthly1').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                        }
                        break;

                    case '5': //OFICINA
                        switch (tipoAudit){
                            case '1': //CHECKLIST 
                                $('#mODaily').modal({ 
                                    show: true,
                                    backdrop: false, 
                                    keyboard: false
                                }); 
                                break;
                                
                            case '2': //SEMANAL                                
                                $("#bnOWP1").val(lAOPL);
                                if (lAOPL == '1' || lAOPL == 1) { 
                                    
                                    $('input:radio[name=checkOWP1]:nth(0)').attr('checked',true);
                                    pnlOplCheckOW1.className = 'no';
                                } else {
                                    $('input:radio[name=checkOWP1]:nth(1)').attr('checked',true);
                                    pnlOplCheckOW1.className = 'si';
                                }
                                
                                $("#bnOWP10").val(lAAudit);
                                if (lAAudit == '1' || lAAudit == 1) {
                                    $('input:radio[name=checkOWP10]:nth(0)').attr('checked',true);
                                    pnlOplCheckOW10.className = 'no';
                                } else { 
                                    $('input:radio[name=checkOWP10]:nth(1)').attr('checked',true);
                                    pnlOplCheckOW10.className = 'si';
                                } 

                                $('#mOWeekly').modal({ 
                                    show: true, 
                                    backdrop: false, 
                                    keyboard: false 
                                });
                                break;
                                
                            case '3': //MENSUAL                                
                                $("#bnOMP1").val(lAOPL);
                                if (lAOPL == '1' || lAOPL == 1) {                                     
                                    $('input:radio[name=checkOMP1]:nth(0)').attr('checked',true);
                                    pnlOplCheckOM10.className = 'no';
                                } else {
                                    $('input:radio[name=checkOMP1]:nth(1)').attr('checked',true);
                                    pnlOplCheckOM1.className = 'si';
                                }
                                
                                $("#bnOMP10").val(lAAudit);
                                if (lAAudit == '1' || lAAudit == 1) {
                                    $('input:radio[name=checkOMP10]:nth(0)').attr('checked',true);
                                    pnlOplCheckOM10.className = 'no';
                                } else { 
                                    $('input:radio[name=checkOMP10]:nth(1)').attr('checked',true);
                                    pnlOplCheckOM10.className = 'si';
                                } 
                                
                                $('#mOMonthly').modal({ 
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                }); 
                                break;
                        } 
                        break; 
                } 
                
                $('#ModalRules').modal('hide'); 
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        });
    });
    
    $("#fTipoAuditVSE" ).submit(function( event ) {
        var parametros = $(this).serialize();
        //console.log(parametros);
        $.ajax({
            type: "POST",
            url: "./db/admin/tipoAudit.php",
            data: parametros,
            success: function(datos) {
                var datA = [];
                var tipo = "";
                
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    //console.log(i+": "+datA[i]);
                }
                
                switch(res1) {
                    case "1": case 1:
                            //ABRE PANEL INFORMATIVO DE LA AUDITORIA
                            //TRANSFROMAMOS EL VALOR DE TIPO
                            switch (datA[4]){
                                case "1":
                                    tipo = "CHECKLIST";
                                    break;
                                case "2":
                                    tipo = "SEMANAL";
                                    break;
                                case "3":
                                    tipo = "MENSUAL";
                                    break;
                                case "4":
                                    tipo = "CONFIRMACION";
                                    break;
                            }
                            
                            tipoAudit = datA[4];
                            //MANDAMOS LOS VALORES DE LA AUDITORIA 
                            $('#idAuditoria').val(datA[1]);//ID 
                            $('#areaAuditoria').val(datA[2]); //LINEA
                            $('#auditado').val(datA[3]); //AUDITADO
                            $('#tipoAudit').val(tipo); //TIPO AUDITORIA
                            $('#tipoEv').val(datA[5]); //TIPO EVALUACION

                            $('#mTipoAuditVSE').modal('hide');                            
                            $('#mInfoAuditoria').modal({
                                show: true,
                                backdrop: false, 
                                keyboard: false
                            });
                            break;
                            
                    case "2": case 2:
                            //ABRE PANEL DE SELECCION DEPARTAMENTO
                            $('#mTipoAuditVSE').modal('hide');
                            $('#mAreaAuditAdmin').modal({
                                show: true,
                                backdrop: false, 
                                keyboard: false
                            });
                            break;
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(admin.js: Line 5013)");
                        break;
                                
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    /* FUNCIONES GENERALES */
    $('.btn-login').on('click', function(){
        $('#ModalLogin').modal({
            show: true,
            backdrop: false, 
            keyboard: false
        });
    });

    //BOTONES DE LOS MODAL DE AUDITORIA    
    $('.btn-new-audit').on('click', function() {
        var datA = [];
        var tipo = "";
        //BOTONES DE LOS MODAL DE AUDITORIA    
        //AQUI HACEMOS LA CONSULTA PARA VER LAS AUDITORIAS 
        $.ajax({
            type: "POST",
            url: "./db/progAuditoria/cUserAuditoria.php",          
            success: function(datos){
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                $("#cmbTipoAuditoriaVSE").empty();
                //console.log("res: "+res1);
                switch(res1){
                    case "0": case 0:
                        $("#cmbTipoAuditoriaVSE").append('<option value = "4" >CONFIRMACIONAL</option>');
                        $('#mTipoAuditVSE').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false
                        });
                        break;
                    case "1": case 1:
                        var a1 = respuesta.split(",");
                        for (var i in a1) {
                            datA[i] = a1[i];
                            console.log(i+": "+datA[i]);
                        }                        
                        //TRANSFROMAMOS EL VALOR DE TIPO
                        switch (datA[4]){
                            case 1: case "1":
                                tipo = "CHECKLIST";
                                break;
                            case 2: case "2": 
                                tipo = "SEMANAL";
                                break;
                            case 3: case "3":
                                tipo = "MENSUAL";
                                break;
                            case 4: case "4": 
                                tipo = "CONFIRMACION";
                                break;
                        }                        
                        //MANDAMOS LOS VALORES DE LA AUDITORIA 
                        $('#idAuditoria').val(datA[1]);//ID 
                        $('#areaAuditoria').val(datA[2]); //LINEA
                        $('#auditado').val(datA[3]); //AUDITADO
                        $('#tipoAudit').val(tipo); //TIPO AUDITORIA
                        $('#tipoEv').val(datA[5]); //TIPO EVALUACION
                        $("#cmbTipoAuditoriaVSE").empty();
                        $("#cmbTipoAuditoriaVSE").append('<option value='+datA[4]+'>'+tipo+'</option>');
                        $("#cmbTipoAuditoriaVSE").append('<option value = "4" >CONFIRMACIONAL</option>');

                        $('#mTipoAuditVSE').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false
                        });

                        $('#mTipoAuditVSE').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false
                        });
                        break;
                    case "2":  case 2: 
                        $("#cmbTipoAuditoriaVSE").empty();
                        var a1 = respuesta.split(",");
                        for (var i in a1) {
                            datA[i] = a1[i];
                        }                   

                        switch (datA[1]) {
                            case '1':
                                var dTipo1 = "CHECKLIST";
                                break;
                            case '2':
                                var dTipo1 = "SEMANAL";
                                break;
                            case '3':
                                var dTipo1 = "MENSUAL";
                                break;
                        }

                        switch (datA[2]) {
                            case '1':
                                var dTipo2 = "CHECKLIST";
                                break;
                            case '2':
                                var dTipo2 = "SEMANAL";
                                break;
                            case '3':
                                var dTipo2 = "MENSUAL";
                                break;
                        }
                        $("#cmbTipoAuditoriaVSE").empty();
                        $("#cmbTipoAuditoriaVSE").append('<option value='+datA[3]+'>'+dTipo1+'</option>');
                        $("#cmbTipoAuditoriaVSE").append('<option value='+datA[4]+'>'+dTipo2+'</option>');
                        $("#cmbTipoAuditoriaVSE").append('<option value = "4" >CONFIRMACIONAL</option>');

                        $('#mTipoAuditVSE').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false
                        });
                        break;
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(champion.js: Line 1100)");
                        break;                        
                }
            }
        });
    });

    //EVENTOS PARA LOS BOTONES DE MODALS INFORMATIVOS DE LAS AUDITORIAS   
    //BOTON DE MODAL PARA SELECCION DE AUDITORIA
    $('#fTipoAuditoria').submit(function( event ) { 
        var datA = [];
        var tipo = "";
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/auditoria/selecAuditoria.php",
            data: parametros,                
            success: function(datos){
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                switch(res1){
                    case "0":
                        $('#NoneAuditoria').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false                                
                        });
                        break;
                    case "1":   
                        $('#mTipoAuditoria').modal('hide');
                        var a1 = respuesta.split(",");
                        for (var i in a1) {
                            datA[i] = a1[i];
                        }

                        //TRANSFROMAMOS EL VALOR DE TIPO
                        switch (datA[4]){
                            case "1":
                                tipo = "CHECKLIST";
                                break;
                            case "2":
                                tipo = "SEMANAL";
                                break;
                            case "3":
                                tipo = "MENSUAL";
                                break;
                            case "4":
                                tipo = "CONFIRMACION";
                                break;
                        }
                        //MANDAMOS LOS VALORES DE LA AUDITORIA 
                        $('#idAuditoria').val(datA[1]);//ID 
                        $('#areaAuditoria').val(datA[2]); //LINEA
                        $('#auditado').val(datA[3]); //AUDITADO
                        $('#tipo').val(tipo); //TIPO AUDITORIA
                        $('#tipoEv').val(datA[5]); //TIPO EVALUACION

                        $('#mInfoAuditoria').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false
                        });
                        break;                        
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(iLine: Line 161)");
                        break;                        
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    }); 
    
    //BOTON DE MODAL INFORMATIVO DE AUDITORIA
    $('#fInfoAuditoria').submit(function( event ) { 
        var datA = [];
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/auditoria/sessionID.php",
            data: parametros,                
            success: function(datos){
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();
                }                    
                tipoEv = datA[4];

                switch(res1){
                    case "0":
                        alert("NO SE TIENE ID \n Error(admin.js: Line 1004)");
                        break;
                    case "1":  
                        $('#mInfoAuditoria').modal('hide');
                        $('#ModalRules').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false
                        });
                        break;                        
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(iLine: Line 216)");
                        break;                        
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    //        AJAX PARA MANDAR A BD
    $('#fAreaAuditAdmin').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/auditoriaVSE.php",
            data: parametros,          
            success: function(datos){
                var res = datos.trim();
                if (res === "Bien"){
                    //$('#mAreaAuditAdmin').modal('hide');
                    $('#mConfirmacion').modal({
                        show: true,
                        backdrop: false, 
                        keyboard: false
                    });
                } else {
                    $("#msg_AuditVSE").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });    
    
    //OFICINAS
    $('#fOWeekly').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosOW.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mOWeekly').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertOWeekly").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });    
    
    $('#fOMonthly').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosOM.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }
                
                if (datA[0] === "Bien"){
                    $('#mOMonthly').modal('hide');
                    if (datA[1] < 85){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 75 && datA[1] > 84){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertOMonthly").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    //FUNCIONES PARA LA CABECERA LOS TOOLTIP    
    $('.btn-help').on('click', function(){
        $('#ModalHelp').modal({
            show: true,
            backdrop: false, 
            keyboard: false
        });
    });
    
    //FUNCION PARA EL LOGIN
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/cLogin.php",
            data: parametros,
            success: function(datos){
                //alert ("echo");
                $("#datos_ajax").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });       
    
    $( "#logOut" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./modals/accesos/exit.php",
            data: parametros,
            success: function(datos){
                $("#datos_ajax").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });   
    
    //OPL
    $('#mUOpl').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var idOpl = button.data('id'); // Extraer la información de atributos de datos     
        var fInicio = button.data('finicio'); // Extraer la información de atributos de datos
        var fCompromiso = button.data('fcompromiso'); // Extraer la información de atributos de datos
        var hallazgo = button.data('hallazgo'); // Extraer la información de atributos de datos
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var responsable = button.data('resp'); // Extraer la información de atributos de datos
        var soporte = button.data('sop'); // Extraer la información de atributos de datos
        var estado = button.data('estado'); // Extraer la información de atributos de datos

       //console.log("idOpl: "+idOpl+", fInicio: "+fInicio+", fCompromiso: "+fCompromiso+", hallazgo: "+hallazgo+", accion: "+accion+", responsable: "+responsable+", soporte: "+soporte+", estado: "+estado);

        var modal = $(this);
        modal.find('.modal-body #idOpl').val(idOpl);
        modal.find('.modal-body #fInicio').val(fInicio);
        modal.find('.modal-body #fCompromiso').val(fCompromiso);
        modal.find('.modal-body #hallazgo').val(hallazgo);
        modal.find('.modal-body #accion').val(accion);
        modal.find('.modal-body #responsable').val(responsable);
        modal.find('.modal-body #soporte').val(soporte);
        modal.find('.modal-body #estado').val(estado);

       // $('.alert').hide();//Oculto alert
    }); 
    
    $('#mDOpl').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var idOpl = button.data('id'); // Extraer la información de atributos de datos
        var hallazgo = button.data('hallazgo'); // Extraer la información de atributos de datos
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var responsable = button.data('resp'); // Extraer la información de atributos de datos

        //console.log("idOpl: "+idOpl+", hallazgo: "+hallazgo+", accion: "+accion+", responsable: "+responsable);

        var modal = $(this); 
        modal.find('.modal-body #idD').val(idOpl); 
        modal.find('.modal-body #hallazgoD').val('<b>HALLAZGO: </b>'+hallazgo); 
        modal.find('.modal-body #accionD').val('<b>ACCION: </b>'+accion); 
        modal.find('.modal-body #respD').val('<b>RESPONSABLE: </b>'+responsable); 
    }); 
    
    $( "#fUOpl" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "db/opl/uOpl.php",
            data: parametros,
            beforeSend: function(objeto){
                $("#datos_ajax").html("Mensaje: Cargando...");
            },
            success: function(datos){
                $("#datos_ajax").html(datos);
                loadOplA();
            }
        });
        event.preventDefault();
    });
    
    //ROLES
    $( "#fIRol" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/progAuditoria/SChampRol.php",
            data: parametros,
            success: function(datos) { 
                if (datos = 'Bien') { 
                    $("#alertRolChamp").html("<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ROL ACTUALIZADO</strong></div>");
                    location.href="./rolVSE.php";
                } 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    function loadOplA(){
        location.href="./pAbiertos.php";
        location.reload(true);
    } 
});

function index() {
    location.href = "./index.php";
}

function areaVSE(){
    var area = document.getElementById("areaSVSE").value;
    $.ajax({
        type: "POST",
        url: "./db/admin/deptos.php",
        data: {area: area},          
        success: function(datos){
            $("#pnlDepto").html(datos);            
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
    event.preventDefault();
}

function deptoVSE(){
    var depto = document.getElementById("deptoSVSE").value;
    $.ajax({
        type: "POST",
        url: "./db/admin/usuariosDepto.php",
        data: {depto: depto},          
        success: function(datos){
            $("#pnlUsuario").html(datos);            
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
    event.preventDefault();
}

function audit() {  
    var datA = [];
    var tipo = "";
    //BOTONES DE LOS MODAL DE AUDITORIA    
    //AQUI HACEMOS LA CONSULTA PARA VER LAS AUDITORIAS 
    $.ajax({
        type: "POST",
        url: "./db/progAuditoria/cUserAuditoria.php",          
        success: function(datos){
            var respuesta = datos.trim();
            var res1 = respuesta.substr(0,1);
            $("#cmbTipoAuditoriaVSE").empty();
            //console.log("res: "+res1);
            switch(res1){
                case "0": case 0:
                    $("#cmbTipoAuditoriaVSE").append('<option value = "4" >CONFIRMACIONAL</option>');
                    $('#mTipoAuditVSE').modal({
                        show: true,
                        backdrop: false, 
                        keyboard: false
                    });
                    break;
                case "1": case 1:
                    var a1 = respuesta.split(",");
                    for (var i in a1) {
                        datA[i] = a1[i];
                        console.log(i+": "+datA[i]);
                    }                        
                    //TRANSFROMAMOS EL VALOR DE TIPO
                    switch (datA[4]){
                        case 1: case "1":
                            tipo = "CHECKLIST";
                            break;
                        case 2: case "2": 
                            tipo = "SEMANAL";
                            break;
                        case 3: case "3":
                            tipo = "MENSUAL";
                            break;
                        case 4: case "4": 
                            tipo = "CONFIRMACION";
                            break;
                    } 
                    
                    //MANDAMOS LOS VALORES DE LA AUDITORIA 
                    $('#idAuditoria').val(datA[1]);//ID 
                    $('#areaAuditoria').val(datA[2]); //LINEA
                    $('#auditado').val(datA[3]); //AUDITADO
                    $('#tipoAudit').val(tipo); //TIPO AUDITORIA
                    $('#tipoEv').val(datA[5]); //TIPO EVALUACION
                    $("#cmbTipoAuditoriaVSE").empty();
                    $("#cmbTipoAuditoriaVSE").append('<option value='+datA[4]+'>'+tipo+'</option>');
                    $("#cmbTipoAuditoriaVSE").append('<option value = "4" >CONFIRMACIONAL</option>');
                    
                    $('#mTipoAuditVSE').modal({
                        show: true,
                        backdrop: false, 
                        keyboard: false
                    });
                    
                    $('#mTipoAuditVSE').modal({
                        show: true,
                        backdrop: false, 
                        keyboard: false
                    });
                    break;
                case "2":  case 2: 
                    $("#cmbTipoAuditoriaVSE").empty();
                    var a1 = respuesta.split(",");
                    for (var i in a1) {
                        datA[i] = a1[i];
                    }                   
                    
                    switch (datA[1]) {
                        case '1':
                            var dTipo1 = "CHECKLIST";
                            break;
                        case '2':
                            var dTipo1 = "SEMANAL";
                            break;
                        case '3':
                            var dTipo1 = "MENSUAL";
                            break;
                    }
                    
                    switch (datA[2]) {
                        case '1':
                            var dTipo2 = "CHECKLIST";
                            break;
                        case '2':
                            var dTipo2 = "SEMANAL";
                            break;
                        case '3':
                            var dTipo2 = "MENSUAL";
                            break;
                    }
                    $("#cmbTipoAuditoriaVSE").empty();
                    $("#cmbTipoAuditoriaVSE").append('<option value='+datA[3]+'>'+dTipo1+'</option>');
                    $("#cmbTipoAuditoriaVSE").append('<option value='+datA[4]+'>'+dTipo2+'</option>');
                    $("#cmbTipoAuditoriaVSE").append('<option value = "4" >CONFIRMACIONAL</option>');
                    
                    $('#mTipoAuditVSE').modal({
                        show: true,
                        backdrop: false, 
                        keyboard: false
                    });
                    break;
                default :
                    alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(champion.js: Line 1100)");
                    break;                        
            }
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        } 
    }); 
}

function report() {
    location.href = "report.php";
}

function calendar() {
    location.href = "calendar.php";
}

function info() {
    location.href = "info.php";
}

function opl() {
    location.href = "opl.php";
}

function fPrivilegio() {        
    var x = document.getElementById("privilegio").value;        
    switch(x){
        case '1': //ADMINISTRADOR
            pnlAdmin.className = 'si';
            pnlSup.className = 'no';
            pnlChamp.className = 'no'; 
            datosGeneralesI.className = 'si';
            break;                
        case '2'://SUP / GERENTES
            pnlAdmin.className = 'no';
            pnlSup.className = 'si';
            pnlChamp.className = 'no'; 
            datosGeneralesI.className = 'si';
            break;
        case '3': //CHAMPION
            pnlAdmin.className = 'no';
            pnlSup.className = 'no';
            pnlChamp.className = 'si';
            datosGeneralesI.className = 'si';
            break;
    }        
}
    
function fCValorSIU(){
    var id = document.getElementById('areaSIU').value;
    var dataString = 'action='+ id;
    pnlDeptoIU.className = 'no';

    $.ajax({
        type: "post",
        url: './db/admin/cadValor.php',
        data: dataString,
        cache: false,
        success: function(r){
            $("#cadValorIU").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}    

function fCValorS() { 
    var id = document.getElementById('areaS').value;
    var dataString = 'action='+ id;
    pnlDepto.className = 'no';

    $.ajax({
        type: "post",
        url: './db/admin/cadValor.php',
        data: dataString,
        cache: false,
        success: function(r){
            $("#cadValor").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function fDeptosIU(){
    var id = document.getElementById('cadValorIU').value;
    var area = document.getElementById('areaSIU').value;
    var dataString = 'area='+area;
    
    if (id === 'N/A' || id === '-' ) {
        pnlDeptoIU.className = 'si';
        $("#deptoSIU").empty();
        $.ajax({
            type: "post",
            url: './db/admin/depSinCValor.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#deptoSIU").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
    } else {
        pnlDeptoIU.className = 'no';            
    }
}

function fDeptosUU() {
    var id = document.getElementById('cadValorSU').value;
    var area = document.getElementById('areaSU').value;
    var dataString = 'area='+area;
    
    //alert("Areliuts id:"+id+" area: "+area);
    if (id === 'N/A' || id === '-' ) {
        pnlDeptoUU.className = 'si';
        $("#deptoSUU").empty();
        $.ajax({
            type: "post",
            url: './db/admin/depSinCValor.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#deptoSUU").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
    } else {
        pnlDeptoUU.className = 'no';            
    }
}

function fDeptos(){
    var id = document.getElementById('cadValor').value;
    var area = document.getElementById('areaS').value;
    var dataString = 'cValor='+ id+', '+area;
    //alert("Vamos bien :)");
    if (id === 'N/A'){
        pnlDepto.className = 'si';
        alert("Vamos bien :)");
        $("#deptoS").empty();
        $.ajax({
            url: './db/admin/deptos.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#deptoS").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
    } else {
        pnlDepto.className = 'no';            
    }
}

function fDeptosCh(){        
    var area = document.getElementById('areaCh').value;
    var dataString = 'cValor='+area;        
    $("#deptoCh").empty();
    $.ajax({
        url: './db/admin/deptos.php',
        data: dataString,
        cache: false,
        success: function(r){
            $("#deptoCh").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function r_areaU (){
    var anio = document.getElementById("anioAU").value;
    var mes = document.getElementById("mesAU").value;
    var area = document.getElementById("areaU").value;
    $.ajax({
        url: "contenedores/rArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contGeneralU").html("");
            $("#hGeneralU").hide();
            $("#hAreaU").show();
            jQuery("#contAreaU").html(resultA);
        }
    });
}

function r_generalU(){    
    var anio = document.getElementById("anioGU").value;
    var mes = document.getElementById("mesGU").value;         
    $.ajax({
        url: "contenedores/rGeneral.php",
        type: "post",
        data: { a: anio, m: mes},
        success: function (resultG) {
            jQuery("#contAreaU").html("");
            $("#hAreaU").hide();
            $("#hGeneralU").show();
            jQuery("#contGeneralU").html(resultG);
        }
    });
}

function fPrivilegioU() {        
    var x = document.getElementById("privilegioU").value;        
    switch(x){
        case '1': //ADMINISTRADOR
            pnlAdminU.className = 'si'; 
            pnlSupU.className = 'no'; 
            pnlChampU.className = 'no';
            pnlDatosG.className = 'si';
            break;  
        
        case '2'://SUP / GERENTES
            pnlAdminU.className = 'no';
            pnlSupU.className = 'si';
            pnlChampU.className = 'no';      
            pnlDatosG.className = 'si';
            break;
            
        case '3': //CHAMPION
            pnlAdminU.className = 'no';
            pnlSupU.className = 'no';
            pnlChampU.className = 'si';
            pnlDatosG.className = 'si';
            break;
    }        
}
    
function fCValorSU(){
    var id = document.getElementById('areaSU').value;
    var dataString = 'action='+ id;
    $.ajax({
        url: './db/admin/cadValor.php',
        data: dataString,
        cache: false,
        success: function(r) { 
            $("#cadValorSU").html(r);            
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function fDeptosChU(){        
    var area = document.getElementById('areaChU').value;
    var dataString = 'area='+area;        
    $("#deptoChU").empty();
    $.ajax({
        type: "POST",
        url: './db/admin/deptos.php',
        data: dataString,
        cache: false,
        success: function(r){
            $("#deptoChU").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function fCValorDepto(){
    var id = document.getElementById('areaDep').value;
    var dataString = 'action='+ id;
    $.ajax({
        type: "POST",
        url: './db/admin/cadValorArea.php',
        data: dataString,
        cache: false,
        success: function(r) { 
            $("#cadValorDep").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function fCValorDeptoU(){
    var id = document.getElementById('areaDepU').value;
    var dataString = 'action='+ id;
    $.ajax({
        type: "POST",
        url: './db/admin/cadValorArea.php',
        data: dataString,
        cache: false,
        success: function(r) { 
            $("#cadValorDepU").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function r_area (){
    var anio = document.getElementById("anioA").value;
    var mes = document.getElementById("mesA").value;
    var area = document.getElementById("area").value;
    $.ajax({
        url: "contenedores/rArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contGeneral").html("");
//            jQuery("#hGeneral").html("");
            $("#hGeneral").hide();
            $("#hArea").show();
            jQuery("#contArea").html(resultA);
        }
    });
}

function r_general(){    
    var anio = document.getElementById("anioG").value;
    var mes = document.getElementById("mesG").value;    
        
    $.ajax({
        url: "contenedores/rGeneral.php",
        type: "post",
        data: { a: anio, m: mes},
        success: function (resultG) {
            jQuery("#contArea").html("");
            $("#hArea").hide();
            $("#hGeneral").show();
            jQuery("#contGeneral").html(resultG);
        }
    });
}

function aOpl_area (){
    var anio = document.getElementById("anioA").value;
    var mes = document.getElementById("mesA").value;
    var area = document.getElementById("area").value;
    $.ajax({
        url: "contenedores/opl/aArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contGeneral").html("");
            $("#hGeneral").hide();
            $("#hArea").show();
            jQuery("#contArea").html(resultA);
        }
    });
}

function aOpl_general(){    
    var anio = document.getElementById("anioG").value;
    var mes = document.getElementById("mesG").value;    
        
    $.ajax({
        url: "contenedores/opl/aGeneral.php",
        type: "post",
        data: { a: anio, m: mes},
        success: function (resultG) {
            jQuery("#contArea").html("");
            $("#hArea").hide();
            $("#hGeneral").show();
            jQuery("#contGeneral").html(resultG);
        }
    });
}


