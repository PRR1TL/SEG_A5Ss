/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Areli Pérez Calixto (PRR1TL)
 * 21/01/2019
 *  
 */

    $(window).on('load',function() {
        
        //VALICACIONES PARA OPL
        //1
        $("input[name=checkLMP1]").click(function () {
            var vP1 = $(this).val();
            if (vP1 == '1'){
                pnlOplCheckLM1.className = 'no';
            } else if (cOpl1 !== 3){
                pnlOplCheckLM1.className = 'si';
            }            
        });

        //2
        $("input[name=checkLMP2]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckLM2.className = 'no';
            } else if (cOpl2 !== 3){
                pnlOplCheckLM2.className = 'si';                
            }
        });

        //3
        $("input[name=checkLMP3]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckLM3.className = 'no';
            } else if (cOpl3 !== 3){
                pnlOplCheckLM3.className = 'si';                
            }
        });

        //4
        $("input[name=checkLMP4]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckLM4.className = 'no';
            } else if (cOpl4 !== 3){
                pnlOplCheckLM4.className = 'si';                
            }
        });

        //5
        $("input[name=checkLMP5]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckLM5.className = 'no';
            } else if (cOpl5 !== 3){
                pnlOplCheckLM5.className = 'si';
            }
        });

        //6
        $("input[name=checkLMP6]").click(function () {
            var vP6 = $(this).val(); 
            if (vP6 == '1'){
                pnlOplCheckLM6.className = 'no';
            } else if (cOpl6 !== 3){
                pnlOplCheckLM6.className = 'si';            
            }
        });

        //7
        $("input[name=checkLMP7]").click(function () {
            var vP7 = $(this).val(); 
            if (vP7 == '1'){
                pnlOplCheckLM7.className = 'no';
            } else if (cOpl7 !== 3){
                pnlOplCheckLM7.className = 'si';                
            }
        });

        //8
        $("input[name=checkLMP8]").click(function () {
            var vP8 = $(this).val(); 
            if (vP8 == '1'){
                pnlOplCheckLM8.className = 'no';
            } else if (cOpl8 !== 3){
                pnlOplCheckLM8.className = 'si';                
            }
        });

        //9
        $("input[name=checkLMP9]").click(function () {
            var vP9 = $(this).val(); 
            if (vP9 == '1'){
                pnlOplCheckLM9.className = 'no';
            } else if (cOpl9 !== 3){
                pnlOplCheckLM9.className = 'si';                
            }
        });

        //10
        $("input[name=checkLMP10]").click(function () {
            var vP10 = $(this).val(); 
            if (vP10 == '1'){
                pnlOplCheckLM10.className = 'no';
            } else if (cOpl10 !== 3){
                pnlOplCheckLM10.className = 'si';                
            }
        });
        
        //11
        $("input[name=checkLMP11]").click(function () {
            var vP11 = $(this).val();
            if (vP11 == '1'){
                pnlOplCheckLM11.className = 'no';
            } else if (cOpl11 !== 3){
                pnlOplCheckLM11.className = 'si';
            }            
        });

        //12
        $("input[name=checkLMP12]").click(function () {
            var vP12 = $(this).val(); 
            if (vP12 == '1'){
                pnlOplCheckLM12.className = 'no';
            } else if (cOpl12 !== 3){
                pnlOplCheckLM12.className = 'si';                
            }
        });

        //13
        $("input[name=checkLMP13]").click(function () {
            var vP13 = $(this).val(); 
            if (vP13 == '1'){
                pnlOplCheckLM13.className = 'no';
            } else if (cOpl13 !== 3){
                pnlOplCheckLM13.className = 'si';                
            }
        });

        //14
        $("input[name=checkLMP14]").click(function () {
            var vP14 = $(this).val(); 
            if (vP14 == '1'){
                pnlOplCheckLM14.className = 'no';
            } else if (cOpl14 !== 3){
                pnlOplCheckLM14.className = 'si';                
            }
        });

        //15
        $("input[name=checkLMP15]").click(function () {
            var vP15 = $(this).val(); 
            if (vP15 == '1'){
                pnlOplCheckLM15.className = 'no';
            } else if (cOpl15 !== 3){
                pnlOplCheckLM15.className = 'si';
            }
        });
        
    });    
    
    //FUNCIONES PARA EL PICKER
    $(window).on('load',function() {       
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        
        if (dd < 10) {
            dd = '0' + dd;
        } 
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var today = dd+"/"+mm+"/"+yyyy;
  
        $( "#fFinLM1").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM2").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM3").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM4").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM5").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM6").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM7").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM8").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM9").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM10").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM11").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM12").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM13").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM14").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinLM15").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
    });
    
    //FUNCIONES PARA LAS AYUDAS VISUALES
    function dLM1() {
        $("#mDLIP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM2() {
        $("#mDLIP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM3() {
        $("#mDLIP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM4() {
        $("#mDLIP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM5() {
        $("#mDLIP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM6() {
        $("#mDLIP6").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM7() {
        $("#mDLIP7").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM8() {
        $("#mDLIP8").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM9() {
        $("#mDLIP9").modal({
            show: true,
            backdrop: false 
        });
    }
    
    function dLM10() {
        $("#mDLIP10").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM11() {
        $("#mDLIP11").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM12() {
        $("#mDLIP12").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM13() {
        $("#mDLIP13").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM14() {
        $("#mDLIP14").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLM15() {
        $("#mDLIP15").modal({
            show: true,
            backdrop: false
        });
    }
    
    //CONTADORES PARA LOS INGRESOS DE OPL POR PREGUNTA
    var cOpl1 = 0;
    var cOpl2 = 0;
    var cOpl3 = 0;
    var cOpl4 = 0;
    var cOpl5 = 0;
    var cOpl6 = 0;
    var cOpl7 = 0;
    var cOpl8 = 0;
    var cOpl9 = 0;
    var cOpl10 = 0;
    var cOpl11 = 0;
    var cOpl12 = 0;
    var cOpl13 = 0;
    var cOpl14 = 0;
    var cOpl15 = 0;
    
    //CUANDO SE HAGA CLIC EN OPL, SE JALAN LOS DATOS DE CADA OPL
    //FUNCIONES PARA PASO DE PARAMETTROS DE OPL A BD
    function oplCheckLM1(){
        //alert("algo pasa aca 3");
        var h1 = document.getElementById("hallazgo1LM").value;
        var a1 = document.getElementById("accion1LM").value;
        var r1 = document.getElementById("responsable1LM").value;
        var s1 = document.getElementById("soporte1LM").value;
        var fF1 = document.getElementById("fFinLM1").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl1++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl1 === 3 ){
                        pnlOplCheckLM1.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM1.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM1").append(cOpl1+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo1LM").val("");
                    $("#accion1LM").val("");
                    $("#responsable1LM").val("");
                    $("#soporte1LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("Todos los campos deben estar llenos");
        }        
    }
    
    function oplCheckLM2(){
        var h2 = document.getElementById("hallazgo2LM").value;
        var a2 = document.getElementById("accion2LM").value;
        var r2 = document.getElementById("responsable2LM").value;
        var s2 = document.getElementById("soporte2LM").value;
        var fF2 = document.getElementById("fFinLM2").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl2++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl2 === 3 ){
                        pnlOplCheckLM2.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM2.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM2").append(cOpl2+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo2LM").val("");
                    $("#accion2LM").val("");
                    $("#responsable2LM").val("");
                    $("#soporte2LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLM3(){
        var h3 = document.getElementById("hallazgo3LM").value;
        var a3 = document.getElementById("accion3LM").value;
        var r3 = document.getElementById("responsable3LM").value;
        var s3 = document.getElementById("soporte3LM").value;
        var fF3 = document.getElementById("fFinLM3").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl3++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl3 === 3 ){
                        pnlOplCheckLM3.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM3.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM3").append(cOpl3+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo3LM").val("");
                    $("#accion3LM").val("");
                    $("#responsable3LM").val("");
                    $("#soporte3LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLM4(){
        var h4 = document.getElementById("hallazgo4LM").value;
        var a4 = document.getElementById("accion4LM").value;
        var r4 = document.getElementById("responsable4LM").value;
        var s4 = document.getElementById("soporte4LM").value;
        var fF4 = document.getElementById("fFinLM4").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl4++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl4 === 3 ){
                        pnlOplCheckLM4.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM4.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM4").append(cOpl4+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo4LM").val("");
                    $("#accion4LM").val("");
                    $("#responsable4LM").val("");
                    $("#soporte4LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLM5(){
        var h5 = document.getElementById("hallazgo5LM").value;
        var a5 = document.getElementById("accion5LM").value;
        var r5 = document.getElementById("responsable5LM").value;
        var s5 = document.getElementById("soporte5LM").value;
        var fF5 = document.getElementById("fFinLM5").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl5++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl5 === 3 ){
                        pnlOplCheckLM5.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM5.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM5").append(cOpl5+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo5LM").val("");
                    $("#accion5LM").val("");
                    $("#responsable5LM").val("");
                    $("#soporte5LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLM6(){
        var h6 = document.getElementById("hallazgo6LM").value;
        var a6 = document.getElementById("accion6LM").value;
        var r6 = document.getElementById("responsable6LM").value;
        var s6 = document.getElementById("soporte6LM").value;
        var fF6 = document.getElementById("fFinLM6").value;
        
        var ht6 = h6.trim();
        var at6 = a6.trim();
        var rt6 = r6.trim();
        var st6 = s6.trim();
        var fFt6 = fF6.trim();
        
        if (ht6.length !== 0 && at6.length !== 0 && rt6.length !== 0 && st6.length !== 0 && fFt6.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h6, a: a6, r: r6, s: s6,fF: fF6 },                
                success: function(result){
                    cOpl6++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl6 === 3 ){
                        pnlOplCheckLM6.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM6.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM6").append(cOpl6+". "+a6+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo6LM").val("");
                    $("#accion6LM").val("");
                    $("#responsable6LM").val("");
                    $("#soporte6LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLM7(){
        var h7 = document.getElementById("hallazgo7LM").value;
        var a7 = document.getElementById("accion7LM").value;
        var r7 = document.getElementById("responsable7LM").value;
        var s7 = document.getElementById("soporte7LM").value;
        var fF7 = document.getElementById("fFinLM7").value;
        
        var ht7 = h7.trim();
        var at7 = a7.trim();
        var rt7 = r7.trim();
        var st7 = s7.trim();
        var fFt7 = fF7.trim();
        
        if (ht7.length !== 0 && at7.length !== 0 && rt7.length !== 0 && st7.length !== 0 && fFt7.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h7, a: a7, r: r7, s: s7,fF: fF7 },                
                success: function(result){
                    cOpl7++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl7 === 3 ){
                        pnlOplCheckLM7.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM7.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM7").append(cOpl7+". "+a7+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo7LM").val("");
                    $("#accion7LM").val("");
                    $("#responsable7LM").val("");
                    $("#soporte7LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLM8(){
        var h8 = document.getElementById("hallazgo8LM").value;
        var a8 = document.getElementById("accion8LM").value;
        var r8 = document.getElementById("responsable8LM").value;
        var s8 = document.getElementById("soporte8LM").value;
        var fF8 = document.getElementById("fFinLM8").value;
        
        var ht8 = h8.trim();
        var at8 = a8.trim();
        var rt8 = r8.trim();
        var st8 = s8.trim();
        var fFt8 = fF8.trim();
        
        if (ht8.length !== 0 && at8.length !== 0 && rt8.length !== 0 && st8.length !== 0 && fFt8.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h8, a: a8, r: r8, s: s8,fF: fF8 },                
                success: function(result){
                    cOpl8++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl8 === 3 ){
                        pnlOplCheckLM8.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM8.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM8").append(cOpl8+". "+a8+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo8LM").val("");
                    $("#accion8LM").val("");
                    $("#responsable8LM").val("");
                    $("#soporte8LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLM9(){
        var h9 = document.getElementById("hallazgo9LM").value;
        var a9 = document.getElementById("accion9LM").value;
        var r9 = document.getElementById("responsable9LM").value;
        var s9 = document.getElementById("soporte9LM").value;
        var fF9 = document.getElementById("fFinLM9").value;
        
        var ht9 = h9.trim();
        var at9 = a9.trim();
        var rt9 = r9.trim();
        var st9 = s9.trim();
        var fFt9 = fF9.trim();
        
        if (ht9.length !== 0 && at9.length !== 0 && rt9.length !== 0 && st9.length !== 0 && fFt9.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h9, a: a9, r: r9, s: s9,fF: fF9 },                
                success: function(result){
                    cOpl9++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl9 === 3 ){
                        pnlOplCheckLM9.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM9.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM9").append(cOpl9+". "+a9+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo9LM").val("");
                    $("#accion9LM").val("");
                    $("#responsable9LM").val("");
                    $("#soporte9LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLM10(){
        var h10 = document.getElementById("hallazgo10LM").value;
        var a10 = document.getElementById("accion10LM").value;
        var r10 = document.getElementById("responsable10LM").value;
        var s10 = document.getElementById("soporte10LM").value;
        var fF10 = document.getElementById("fFinLM10").value;
        
        var ht10 = h10.trim();
        var at10 = a10.trim();
        var rt10 = r10.trim();
        var st10 = s10.trim();
        var fFt10 = fF10.trim();
        
        if (ht10.length !== 0 && at10.length !== 0 && rt10.length !== 0 && st10.length !== 0 && fFt10.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h10, a: a10, r: r10, s: s10,fF: fF10 },                
                success: function(result){
                    cOpl10++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl10 === 3 ){
                        pnlOplCheckLM10.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM10.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM10").append(cOpl10+". "+a10+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo10LM").val("");
                    $("#accion10LM").val("");
                    $("#responsable10LM").val("");
                    $("#soporte10LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLM11(){
        //alert("algo pasa aca 13");
        var h11 = document.getElementById("hallazgo11LM").value;
        var a11 = document.getElementById("accion11LM").value;
        var r11 = document.getElementById("responsable11LM").value;
        var s11 = document.getElementById("soporte11LM").value;
        var fF11 = document.getElementById("fFinLM11").value;
        
        var ht11 = h11.trim();
        var at11 = a11.trim();
        var rt11 = r11.trim();
        var st11 = s11.trim();
        
        console.log("h: "+h11+" a: "+a11+" r: "+r11+" s: "+s11+" f: "+fF11);
        
        if (ht11.length !== 0 && at11.length !== 0 && rt11.length !== 0 && st11.length !== 0 && fFt11.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h11, a: a11, r: r11, s: s11,fF: fF11 },                
                success: function(result){
                    cOpl11++;
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl11 === 3 ){
                        pnlOplCheckLM11.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM11.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM11").append(cOpl11+". "+a11+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo11LM").val("");
                    $("#accion11LM").val("");
                    $("#responsable11LM").val("");
                    $("#soporte11LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("Todos los campos deben estar llenos");
        }        
    }
    
    function oplCheckLM12(){
        var h12 = document.getElementById("hallazgo12LM").value;
        var a12 = document.getElementById("accion12LM").value;
        var r12 = document.getElementById("responsable12LM").value;
        var s12 = document.getElementById("soporte12LM").value;
        var fF12 = document.getElementById("fFinLM12").value;
        
        var ht12 = h12.trim();
        var at12 = a12.trim();
        var rt12 = r12.trim();
        var st12 = s12.trim();
        var fFt12 = fF12.trim();
        
        if (ht12.length !== 0 && at12.length !== 0 && rt12.length !== 0 && st12.length !== 0 && fFt12.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h12, a: a12, r: r12, s: s12,fF: fF12 },                
                success: function(result){
                    cOpl12++;
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl12 === 3 ){
                        pnlOplCheckLM12.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM12.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM12").append(cOpl12+". "+a12+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo12LM").val("");
                    $("#accion12LM").val("");
                    $("#responsable12LM").val("");
                    $("#soporte12LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLM13(){
        var h13 = document.getElementById("hallazgo13LM").value;
        var a13 = document.getElementById("accion13LM").value;
        var r13 = document.getElementById("responsable13LM").value;
        var s13 = document.getElementById("soporte13LM").value;
        var fF13 = document.getElementById("fFinLM13").value;
        
        var ht13 = h13.trim();
        var at13 = a13.trim();
        var rt13 = r13.trim();
        var st13 = s13.trim();
        var fFt13 = fF13.trim();
        
        if (ht13.length !== 0 && at13.length !== 0 && rt13.length !== 0 && st13.length !== 0 && fFt13.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h13, a: a13, r: r13, s: s13,fF: fF13 },                
                success: function(result){
                    cOpl13++;
                    
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl13 === 3 ){
                        pnlOplCheckLM13.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM13.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM13").append(cOpl13+". "+a13+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo13LM").val("");
                    $("#accion13LM").val("");
                    $("#responsable13LM").val("");
                    $("#soporte13LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLM14(){
        var h14 = document.getElementById("hallazgo14LM").value;
        var a14 = document.getElementById("accion14LM").value;
        var r14 = document.getElementById("responsable14LM").value;
        var s14 = document.getElementById("soporte14LM").value;
        var fF14 = document.getElementById("fFinLM14").value;
        
        var ht14 = h14.trim();
        var at14 = a14.trim();
        var rt14 = r14.trim();
        var st14 = s14.trim();
        var fFt14 = fF14.trim();
        
        if (ht14.length !== 0 && at14.length !== 0 && rt14.length !== 0 && st14.length !== 0 && fFt14.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h14, a: a14, r: r14, s: s14,fF: fF14 },                
                success: function(result){
                    cOpl14++;
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl14 === 3 ){
                        pnlOplCheckLM14.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM14.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM14").append(cOpl14+". "+a14+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo14LM").val("");
                    $("#accion14LM").val("");
                    $("#responsable14LM").val("");
                    $("#soporte14LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLM15(){
        var h15 = document.getElementById("hallazgo15LM").value;
        var a15 = document.getElementById("accion15LM").value;
        var r15 = document.getElementById("responsable15LM").value;
        var s15 = document.getElementById("soporte15LM").value;
        var fF15 = document.getElementById("fFinLM15").value;
        
        var ht15 = h15.trim();
        var at15 = a15.trim();
        var rt15 = r15.trim();
        var st15 = s15.trim();
        var fFt15 = fF15.trim();
        
        if (ht15.length !== 0 && at15.length !== 0 && rt15.length !== 0 && st15.length !== 0 && fFt15.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h15, a: a15, r: r15, s: s15,fF: fF15 },                
                success: function(result){
                    cOpl15++;
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl15 === 3 ){
                        pnlOplCheckLM15.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarM15.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesM15").append(cOpl15+". "+a15+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo15LM").val("");
                    $("#accion15LM").val("");
                    $("#responsable15LM").val("");
                    $("#soporte15LM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    //FUNCIONES PARA CERRAR OPLS
    function oplCheckLMC1(){
        pnlOplCheckLM1.className = 'no';
    }

    function oplCheckLMC2(){
        pnlOplCheckLM2.className = 'no';
    }
    
    function oplCheckLMC3(){
        pnlOplCheckLM3.className = 'no';
    }
    
    function oplCheckLMC4(){
        pnlOplCheckLM4.className = 'no';
    }
    
    function oplCheckLMC5(){
        pnlOplCheckLM5.className = 'no';
    }
    
    function oplCheckLMC6(){
        pnlOplCheckLM6.className = 'no';
    }
    
    function oplCheckLMC7(){
        pnlOplCheckLM7.className = 'no';
    }
    
    function oplCheckLMC8(){
        pnlOplCheckLM8.className = 'no';
    }
    
    function oplCheckLMC9(){
        pnlOplCheckLM9.className = 'no';
    }
    
    function oplCheckLMC10(){
        pnlOplCheckLM10.className = 'no';
    }

    function oplCheckLMC11(){
        pnlOplCheckLM11.className = 'no';
    }

    function oplCheckLMC12(){
        pnlOplCheckLM12.className = 'no';
    }
    
    function oplCheckLMC13(){
        pnlOplCheckLM13.className = 'no';
    }
    
    function oplCheckLMC14(){
        pnlOplCheckLM14.className = 'no';
    }
    
    function oplCheckLMC15(){
        pnlOplCheckLM15.className = 'no';
    }


