/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Areli Pérez Calixto (PRR1TL)
 * 21/01/2019
 *  
 */

    $(window).on('load',function() { 
        //VALICACIONES PARA OPL
        //1
        $("input[name=checkLAMP1]").click(function () {
            var vP1 = $(this).val(); 
            if (vP1 == '1'){
                pnlOplCheckLAM1.className = 'no';
            } else if (cOpl1 !== 3){
                pnlOplCheckLAM1.className = 'si';                
            }            
        });
        
        //2
        $("input[name=checkLAMP2]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckLAM2.className = 'no';
            } else if (cOpl2 !== 3){
                pnlOplCheckLAM2.className = 'si';                
            }
        });
        
        //3
        $("input[name=checkLAMP3]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckLAM3.className = 'no';
            } else if (cOpl3 !== 3){
                pnlOplCheckLAM3.className = 'si';                
            }
        });
        
        //4
        $("input[name=checkLAMP4]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckLAM4.className = 'no';
            } else if (cOpl4 !== 3){
                pnlOplCheckLAM4.className = 'si';                
            }
        });
        
        //5
        $("input[name=checkLAMP5]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckLAM5.className = 'no';
            } else if (cOpl5 !== 3){
                pnlOplCheckLAM5.className = 'si';                
            }
        });
        
        //6
        $("input[name=checkLAMP6]").click(function () {
            var vP6 = $(this).val(); 
            if (vP6 == '1'){
                pnlOplCheckLAM6.className = 'no';
            } else if (cOpl6 !== 3){
                pnlOplCheckLAM6.className = 'si';                
            }
        });
        
        //7
        $("input[name=checkLAMP7]").click(function () {
            var vP7 = $(this).val(); 
            if (vP7 == '1'){
                pnlOplCheckLAM7.className = 'no';
            } else if (cOpl7 !== 3){
                pnlOplCheckLAM7.className = 'si';                
            }
        });
        
        //8
        $("input[name=checkLAMP8]").click(function () {
            var vP8 = $(this).val(); 
            if (vP8 == '1'){
                pnlOplCheckLAM8.className = 'no';
            } else if (cOpl8 !== 3){
                pnlOplCheckLAM8.className = 'si';                
            }
        });
        
        //9
        $("input[name=checkLAMP9]").click(function () {
            var vP9 = $(this).val(); 
            if (vP9 == '1'){
                pnlOplCheckLAM9.className = 'no';
            } else if (cOpl9 !== 3){
                pnlOplCheckLAM9.className = 'si';                
            }
        });
        
        //10
        $("input[name=checkLAMP10]").click(function () {
            var vP10 = $(this).val(); 
            if (vP10 == '1'){
                pnlOplCheckLAM10.className = 'no';
            } else if (cOpl10 !== 3){
                pnlOplCheckLAM10.className = 'si';                
            }
        });
        
        //11
        $("input[name=checkLAMP11]").click(function () {
            //alert("Vas bien Arelita");
            var vP1 = $(this).val(); 
            if (vP1 == '1'){
                pnlOplCheckLAM11.className = 'no';
            } else if (cOpl11 !== 3){
                pnlOplCheckLAM11.className = 'si';                
            }            
        });
        
        //12
        $("input[name=checkLAMP12]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckLAM12.className = 'no';
            } else if (cOpl12 !== 3){
                pnlOplCheckLAM12.className = 'si';                
            }
        });
        
        //13
        $("input[name=checkLAMP13]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckLAM13.className = 'no';
            } else if (cOpl13 !== 3){
                pnlOplCheckLAM13.className = 'si';                
            }
        });
        
        //14
        $("input[name=checkLAMP14]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckLAM14.className = 'no';
            } else if (cOpl14 !== 3){
                pnlOplCheckLAM14.className = 'si';                
            }
        });
        
        //15
        $("input[name=checkLAMP15]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckLAM15.className = 'no';
            } else if (cOpl15 !== 3){
                pnlOplCheckLAM15.className = 'si';                
            }
        });
        
    });
    
    //FUNCIONES PARA EL PICKER
    $(window).on('load',function() { 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        
        if (dd < 10) {
            dd = '0' + dd;
        } 
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var today = dd+"/"+mm+"/"+yyyy;
        //console.log(today);
  
        $( "#fFin1LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin2LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin3LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin4LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin5LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin6LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin7LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin8LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin9LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin10LAM").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin11LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin12LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin13LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin14LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin15LAM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
    });
    
    function dLAM1() {
        $("#mDAP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM2() {
        $("#mDAP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM3() {
        $("#mDAP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM4() {
        $("#mDAP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM5() {
        $("#mDAP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM6() {
        $("#mDAP6").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM7() {
        $("#mDAP7").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM8() {
        $("#mDAP8").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM9() {
        $("#mDAP9").modal({
            show: true,
            backdrop: false 
        });
    }
    
    function dLAM10() {
        $("#mDAP10").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM11() {
        $("#mDAP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM12() {
        $("#mDAP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM13() {
        $("#mDAP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM14() {
        $("#mDAP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAM15() {
        $("#mDAP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    //CONTADORES PARA LOS INGRESOS DE OPL POR PREGUNTA
    var cOpl1 = 0;
    var cOpl2 = 0;
    var cOpl3 = 0;
    var cOpl4 = 0;
    var cOpl5 = 0;
    var cOpl6 = 0;
    var cOpl7 = 0;
    var cOpl8 = 0;
    var cOpl9 = 0;
    var cOpl10 = 0;
    var cOpl11 = 0;
    var cOpl12 = 0;
    var cOpl13 = 0;
    var cOpl14 = 0;
    var cOpl15 = 0;
    
    //CUANDO SE HAGA CLIC EN OPL, SE JALAN LOS DATOS DE CADA OPL
    //FUNCIONES PARA PASO DE PARAMETTROS DE OPL A BD
    function oplCheckLAM1(){        
        var h1 = document.getElementById("hallazgo1LAM").value;
        var a1 = document.getElementById("accion1LAM").value;
        var r1 = document.getElementById("responsable1LAM").value;
        var s1 = document.getElementById("soporte1LAM").value;
        var fF1 = document.getElementById("fFin1LAM").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        var fFt1 = fF1.trim();
        
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl1++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl1 === 3 ){
                        pnlOplCheckLAM1.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar1LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones1LAM").append(cOpl1+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo1LAM").val("");
                    $("#accion1LAM").val("");
                    $("#responsable1LAM").val("");
                    $("#soporte1LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLAM2(){
        var h2 = document.getElementById("hallazgo2LAM").value;
        var a2 = document.getElementById("accion2LAM").value;
        var r2 = document.getElementById("responsable2LAM").value;
        var s2 = document.getElementById("soporte2LAM").value;
        var fF2 = document.getElementById("fFin2LAM").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl2++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl2 === 3 ){
                        pnlOplCheckLAM2.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar2LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones2LAM").append(cOpl2+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo2LAM").val("");
                    $("#accion2LAM").val("");
                    $("#responsable2LAM").val("");
                    $("#soporte2LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLAM3(){
        var h3 = document.getElementById("hallazgo3LAM").value;
        var a3 = document.getElementById("accion3LAM").value;
        var r3 = document.getElementById("responsable3LAM").value;
        var s3 = document.getElementById("soporte3LAM").value;
        var fF3 = document.getElementById("fFin3LAM").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl3++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl3 === 3 ){
                        pnlOplCheckLAM3.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar3LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones3LAM").append(cOpl3+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo3LAM").val("");
                    $("#accion3LAM").val("");
                    $("#responsable3LAM").val("");
                    $("#soporte3LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLAM4(){
        var h4 = document.getElementById("hallazgo4LAM").value;
        var a4 = document.getElementById("accion4LAM").value;
        var r4 = document.getElementById("responsable4LAM").value;
        var s4 = document.getElementById("soporte4LAM").value;
        var fF4 = document.getElementById("fFin4LAM").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl4++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl4 === 3 ){
                        pnlOplCheckLAM4.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar4LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones4LAM").append(cOpl4+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo4LAM").val("");
                    $("#accion4LAM").val("");
                    $("#responsable4LAM").val("");
                    $("#soporte4LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLAM5(){
        var h5 = document.getElementById("hallazgo5LAM").value;
        var a5 = document.getElementById("accion5LAM").value;
        var r5 = document.getElementById("responsable5LAM").value;
        var s5 = document.getElementById("soporte5LAM").value;
        var fF5 = document.getElementById("fFin5LAM").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl5++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl5 === 3 ){
                        pnlOplCheckLAM5.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar5LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones5LAM").append(cOpl5+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo5LAM").val("");
                    $("#accion5LAM").val("");
                    $("#responsable5LAM").val("");
                    $("#soporte5LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAM6(){
        var h6 = document.getElementById("hallazgo6LAM").value;
        var a6 = document.getElementById("accion6LAM").value;
        var r6 = document.getElementById("responsable6LAM").value;
        var s6 = document.getElementById("soporte6LAM").value;
        var fF6 = document.getElementById("fFin6LAM").value;
        
        var ht6 = h6.trim();
        var at6 = a6.trim();
        var rt6 = r6.trim();
        var st6 = s6.trim();
        var fFt6 = fF6.trim();
        
        if (ht6.length !== 0 && at6.length !== 0 && rt6.length !== 0 && st6.length !== 0 && fFt6.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h6, a: a6, r: r6, s: s6,fF: fF6 },                
                success: function(result){
                    cOpl6++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl6 === 3 ){
                        pnlOplCheckLAM6.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar6LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones6LAM").append(cOpl6+". "+a6+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo6LAM").val("");
                    $("#accion6LAM").val("");
                    $("#responsable6LAM").val("");
                    $("#soporte6LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAM7(){
        var h7 = document.getElementById("hallazgo7LAM").value;
        var a7 = document.getElementById("accion7LAM").value;
        var r7 = document.getElementById("responsable7LAM").value;
        var s7 = document.getElementById("soporte7LAM").value;
        var fF7 = document.getElementById("fFin7LAM").value;
        
        var ht7 = h7.trim();
        var at7 = a7.trim();
        var rt7 = r7.trim();
        var st7 = s7.trim();
        var fFt7 = fF7.trim();
        
        if (ht7.length !== 0 && at7.length !== 0 && rt7.length !== 0 && st7.length !== 0 && fFt7.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h7, a: a7, r: r7, s: s7,fF: fF7 },                
                success: function(result){
                    cOpl7++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl7 === 3 ){
                        pnlOplCheckLAM7.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar7LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones7LAM").append(cOpl7+". "+a7+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo7LAM").val("");
                    $("#accion7LAM").val("");
                    $("#responsable7LAM").val("");
                    $("#soporte7LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAM8(){
        var h8 = document.getElementById("hallazgo8LAM").value;
        var a8 = document.getElementById("accion8LAM").value;
        var r8 = document.getElementById("responsable8LAM").value;
        var s8 = document.getElementById("soporte8LAM").value;
        var fF8 = document.getElementById("fFin8LAM").value;
        
        var ht8 = h8.trim();
        var at8 = a8.trim();
        var rt8 = r8.trim();
        var st8 = s8.trim();
        var fFt8 = fF8.trim();
        
        if (ht8.length !== 0 && at8.length !== 0 && rt8.length !== 0 && st8.length !== 0 && fFt8.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h8, a: a8, r: r8, s: s8,fF: fF8 },                
                success: function(result){
                    cOpl8++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl8 === 3 ){
                        pnlOplCheckLAM8.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar8LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones8LAM").append(cOpl8+". "+a8+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo8LAM").val("");
                    $("#accion8LAM").val("");
                    $("#responsable8LAM").val("");
                    $("#soporte8LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAM9(){
        var h9 = document.getElementById("hallazgo9LAM").value;
        var a9 = document.getElementById("accion9LAM").value;
        var r9 = document.getElementById("responsable9LAM").value;
        var s9 = document.getElementById("soporte9LAM").value;
        var fF9 = document.getElementById("fFin9LAM").value;
        
        var ht9 = h9.trim();
        var at9 = a9.trim();
        var rt9 = r9.trim();
        var st9 = s9.trim();
        var fFt9 = fF9.trim();
        
        if (ht9.length !== 0 && at9.length !== 0 && rt9.length !== 0 && st9.length !== 0 && fFt9.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h9, a: a9, r: r9, s: s9,fF: fF9 },                
                success: function(result){
                    cOpl9++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl9 === 3 ){
                        pnlOplCheckLAM9.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar            
                    btnCancelar9LAM.className = 'si';
                    
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones9LAM").append(cOpl9+". "+a9+"<br>");
                    
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo9LAM").val("");
                    $("#accion9LAM").val("");
                    $("#responsable9LAM").val("");
                    $("#soporte9LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLAM10(){
        var h10 = document.getElementById("hallazgo10LAM").value;
        var a10 = document.getElementById("accion10LAM").value;
        var r10 = document.getElementById("responsable10LAM").value;
        var s10 = document.getElementById("soporte10LAM").value;
        var fF10 = document.getElementById("fFin10LAM").value;
        
        var ht10 = h10.trim();
        var at10 = a10.trim();
        var rt10 = r10.trim();
        var st10 = s10.trim();
        var fFt10 = fF10.trim();
        
        if (ht10.length !== 0 && at10.length !== 0 && rt10.length !== 0 && st10.length !== 0 && fFt10.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h10, a: a10, r: r10, s: s10,fF: fF10 },                
                success: function(result){
                    cOpl10++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl10 === 3 ){
                        pnlOplCheckLAM10.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar10LAM.className = 'si';
                    
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones10LAM").append(cOpl10+". "+a10+"<br>");
                    
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo10LAM").val("");
                    $("#accion10LAM").val("");
                    $("#responsable10LAM").val("");
                    $("#soporte10LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAM11(){        
        var h1 = document.getElementById("hallazgo11LAM").value;
        var a1 = document.getElementById("accion11LAM").value;
        var r1 = document.getElementById("responsable11LAM").value;
        var s1 = document.getElementById("soporte11LAM").value;
        var fF1 = document.getElementById("fFin11LAM").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        var fFt1 = fF1.trim();
        
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl11++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl11 === 3 ){
                        pnlOplCheckLAM11.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar11LAM.className = 'si';

                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones11LAM").append(cOpl11+". "+a1+"<br>"); 

                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo11LAM").val("");
                    $("#accion11LAM").val("");
                    $("#responsable11LAM").val("");
                    $("#soporte11LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLAM12(){
        var h2 = document.getElementById("hallazgo12LAM").value;
        var a2 = document.getElementById("accion12LAM").value;
        var r2 = document.getElementById("responsable12LAM").value;
        var s2 = document.getElementById("soporte12LAM").value;
        var fF2 = document.getElementById("fFin12LAM").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl12++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl12 === 3 ){
                        pnlOplCheckLAM12.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar12LAM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones12LAM").append(cOpl12+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo12LAM").val("");
                    $("#accion12LAM").val("");
                    $("#responsable12LAM").val("");
                    $("#soporte12LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLAM13(){
        var h3 = document.getElementById("hallazgo13LAM").value;
        var a3 = document.getElementById("accion13LAM").value;
        var r3 = document.getElementById("responsable13LAM").value;
        var s3 = document.getElementById("soporte13LAM").value;
        var fF3 = document.getElementById("fFin13LAM").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl13++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl13 === 3 ){
                        pnlOplCheckLAM13.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar13LAM.className = 'si';

                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones13LAM").append(cOpl13+". "+a3+"<br>");

                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo13LAM").val("");
                    $("#accion13LAM").val("");
                    $("#responsable13LAM").val("");
                    $("#soporte13LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLAM14(){
        var h4 = document.getElementById("hallazgo14LAM").value;
        var a4 = document.getElementById("accion14LAM").value;
        var r4 = document.getElementById("responsable14LAM").value;
        var s4 = document.getElementById("soporte14LAM").value;
        var fF4 = document.getElementById("fFin14LAM").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl14++;

                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl14 === 3 ){
                        pnlOplCheckLAM14.className = 'no';
                    }

                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar14LAM.className = 'si';

                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones14LAM").append(cOpl14+". "+a4+"<br>");  

                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo14LAM").val("");
                    $("#accion14LAM").val("");
                    $("#responsable14LAM").val("");
                    $("#soporte14LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLAM15(){
        var h5 = document.getElementById("hallazgo15LAM").value;
        var a5 = document.getElementById("accion15LAM").value;
        var r5 = document.getElementById("responsable15LAM").value;
        var s5 = document.getElementById("soporte15LAM").value;
        var fF5 = document.getElementById("fFin15LAM").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl15++;

                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl15 === 3 ){
                        pnlOplCheckLAM15.className = 'no';
                    }

                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar15LAM.className = 'si';            
                    
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones15LAM").append(cOpl15+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo15LAM").val("");
                    $("#accion15LAM").val("");
                    $("#responsable15LAM").val("");
                    $("#soporte15LAM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    //FUNCIONES PARA CERRAR OPLS
    function oplCheckLAMC1(){
        pnlOplCheckLAM1.className = 'no';
    }

    function oplCheckLAMC2(){
        pnlOplCheckLAM2.className = 'no';
    }
    
    function oplCheckLAMC3(){
        pnlOplCheckLAM3.className = 'no';
    }
    
    function oplCheckLAMC4(){
        pnlOplCheckLAM4.className = 'no';
    }
    
    function oplCheckLAMC5(){
        pnlOplCheckLAM5.className = 'no';
    }
    
    function oplCheckLAMC6(){
        pnlOplCheckLAM6.className = 'no';
    }
    
    function oplCheckLAMC7(){
        pnlOplCheckLAM7.className = 'no';
    }
    
    function oplCheckLAMC8(){
        pnlOplCheckLAM8.className = 'no';
    }
    
    function oplCheckLAMC9(){
        pnlOplCheckLAM9.className = 'no';
    }
    
    function oplCheckLAMC10(){
        pnlOplCheckLAM10.className = 'no';
    }

    function oplCheckLAMC11(){
        pnlOplCheckLAM11.className = 'no';
    }

    function oplCheckLAMC12(){
        pnlOplCheckLAM12.className = 'no';
    }
    
    function oplCheckLAMC13(){
        pnlOplCheckLAM13.className = 'no';
    }
    
    function oplCheckLAMC14(){
        pnlOplCheckLAM14.className = 'no';
    }
    
    function oplCheckLAMC15(){
        pnlOplCheckLAM15.className = 'no';
    }

