/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Areli Pérez Calixto (PRR1TL)
 * 21/01/2019
 *  
 */

    $(window).on('load',function() {
        //VALICACIONES PARA OPL
        //1
        $("input[name=checkMMP1]").click(function () {
            //alert("mueve P1 ajuste");
            var vP1 = $(this).val(); 
            if (vP1 == '1'){
                pnlOplCheckMM1.className = 'no';
            } else if (cOpl1 !== 3){
                pnlOplCheckMM1.className = 'si';                
            }            
        });
        
        //2
        $("input[name=checkMMP2]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckMM2.className = 'no';
            } else if (cOpl2 !== 3){
                pnlOplCheckMM2.className = 'si';                
            }
        });
        
        //3
        $("input[name=checkMMP3]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckMM3.className = 'no';
            } else if (cOpl3 !== 3){
                pnlOplCheckMM3.className = 'si';                
            }
        });
        
        //4
        $("input[name=checkMMP4]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckMM4.className = 'no';
            } else if (cOpl4 !== 3){
                pnlOplCheckMM4.className = 'si';                
            }
        });
        
        //5
        $("input[name=checkMMP5]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckMM5.className = 'no';
            } else if (cOpl5 !== 3){
                pnlOplCheckMM5.className = 'si';                
            }
        });
        
        //6
        $("input[name=checkMMP6]").click(function () {
            var vP6 = $(this).val(); 
            if (vP6 == '1'){
                pnlOplCheckMM6.className = 'no';
            } else if (cOpl6 !== 3){
                pnlOplCheckMM6.className = 'si';                
            }
        });
        
        //7
        $("input[name=checkMMP7]").click(function () {
            var vP7 = $(this).val(); 
            if (vP7 == '1'){
                pnlOplCheckMM7.className = 'no';
            } else if (cOpl7 !== 3){
                pnlOplCheckMM7.className = 'si';                
            }
        });
        
        //8
        $("input[name=checkMMP8]").click(function () {
            var vP8 = $(this).val(); 
            if (vP8 == '1'){
                pnlOplCheckMM8.className = 'no';
            } else if (cOpl8 !== 3){
                pnlOplCheckMM8.className = 'si';                
            }
        });
        
        //9
        $("input[name=checkMMP9]").click(function () {
            var vP9 = $(this).val(); 
            if (vP9 == '1'){
                pnlOplCheckMM9.className = 'no';
            } else if (cOpl9 !== 3){
                pnlOplCheckMM9.className = 'si';                
            }
        });
        
        //10
        $("input[name=checkMMP10]").click(function () {
            var vP10 = $(this).val(); 
            if (vP10 == '1'){
                pnlOplCheckMM10.className = 'no';
            } else if (cOpl10 !== 3){
                pnlOplCheckMM10.className = 'si';                
            }
        });
        
        //11
        $("input[name=checkMMP11]").click(function () {
            var vP11 = $(this).val(); 
            if (vP11 == '1'){
                pnlOplCheckMM11.className = 'no';
            } else if (cOpl11 !== 3){
                pnlOplCheckMM11.className = 'si';                
            }            
        });
        
        //12
        $("input[name=checkMMP12]").click(function () {
            var vP12 = $(this).val(); 
            if (vP12 == '1'){
                pnlOplCheckMM12.className = 'no';
            } else if (cOpl12 !== 3){
                pnlOplCheckMM12.className = 'si';                
            }
        });
        
        //13
        $("input[name=checkMMP13]").click(function () {
            var vP13 = $(this).val(); 
            if (vP13 == '1'){
                pnlOplCheckMM13.className = 'no';
            } else if (cOpl13 !== 3){
                pnlOplCheckMM13.className = 'si';                
            }
        });
        
        //14
        $("input[name=checkMMP14]").click(function () {
            var vP14 = $(this).val(); 
            if (vP14 == '1'){
                pnlOplCheckMM14.className = 'no';
            } else if (cOpl14 !== 3){
                pnlOplCheckMM14.className = 'si';                
            }
        });
        
        //15
        $("input[name=checkMMP15]").click(function () {
            var vP15 = $(this).val(); 
            if (vP15 == '1'){
                pnlOplCheckMM15.className = 'no';
            } else if (cOpl15 !== 13){
                pnlOplCheckMM15.className = 'si';                
            }
        }); 
        
    });
    
    $(window).on('load',function() { 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        
        if (dd < 10) {
            dd = '0' + dd;
        } 
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var today = dd+"/"+mm+"/"+yyyy;
  
        $( "#fFin1MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin2MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin3MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin4MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin5MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin6MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin7MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin8MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin9MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin10MM").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });
         $( "#fFin11MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin12MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin13MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin14MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin15MM").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
    });
    
    function dMM1() {
        $("#mDMP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM2() {
        $("#mDMP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM3() {
        $("#mDMP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM4() {
        $("#mDMP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM5() {
        $("#mDMP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM6() {
        $("#mDMP6").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM7() {
        $("#mDMP7").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM8() {
        $("#mDMP8").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM9() {
        $("#mDMP9").modal({
            show: true,
            backdrop: false 
        });
    }
    
    function dMM10() {
        $("#mDMP10").modal({
            show: true,
            backdrop: false
        });
    }
    
     function dMM11() {
        $("#mDMP11").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM12() {
        $("#mDMP12").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM13() {
        $("#mDMP13").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM14() {
        $("#mDMP14").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dMM15() {
        $("#mDMP15").modal({
            show: true,
            backdrop: false
        });
    }
    
    //CONTADORES PARA LOS INGRESOS DE OPL POR PREGUNTA
    var cOpl1 = 0;
    var cOpl2 = 0;
    var cOpl3 = 0;
    var cOpl4 = 0;
    var cOpl5 = 0;
    var cOpl6 = 0;
    var cOpl7 = 0;
    var cOpl8 = 0;
    var cOpl9 = 0;
    var cOpl10 = 0;
    var cOpl11 = 0;
    var cOpl12 = 0;
    var cOpl13 = 0;
    var cOpl14 = 0;
    var cOpl15 = 0;
    
    //CUANDO SE HAGA CLIC EN OPL, SE JALAN LOS DATOS DE CADA OPL
    //FUNCIONES PARA PASO DE PARAMETTROS DE OPL A BD
    function oplCheckMM1(){        
        var h1 = document.getElementById("hallazgo1MM").value;
        var a1 = document.getElementById("accion1MM").value;
        var r1 = document.getElementById("responsable1MM").value;
        var s1 = document.getElementById("soporte1MM").value;
        var fF1 = document.getElementById("fFin1MM").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        var fFt1 = fF1.trim();
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl1++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl1 === 3 ){
                        pnlOplCheckMM1.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar1MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones1MM").append(cOpl1+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo1MM").val("");
                    $("#accion1MM").val("");
                    $("#responsable1MM").val("");
                    $("#soporte1MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckMM2(){
        var h2 = document.getElementById("hallazgo2MM").value;
        var a2 = document.getElementById("accion2MM").value;
        var r2 = document.getElementById("responsable2MM").value;
        var s2 = document.getElementById("soporte2MM").value;
        var fF2 = document.getElementById("fFin2MM").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl2++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl2 === 3 ){
                        pnlOplCheckMM2.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar2MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones2MM").append(cOpl2+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo2MM").val("");
                    $("#accion2MM").val("");
                    $("#responsable2MM").val("");
                    $("#soporte2MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckMM3(){
        var h3 = document.getElementById("hallazgo3MM").value;
        var a3 = document.getElementById("accion3MM").value;
        var r3 = document.getElementById("responsable3MM").value;
        var s3 = document.getElementById("soporte3MM").value;
        var fF3 = document.getElementById("fFin3MM").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl3++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl3 === 3 ){
                        pnlOplCheckMM3.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar3MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones3MM").append(cOpl3+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo3MM").val("");
                    $("#accion3MM").val("");
                    $("#responsable3MM").val("");
                    $("#soporte3MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckMM4(){
        var h4 = document.getElementById("hallazgo4MM").value;
        var a4 = document.getElementById("accion4MM").value;
        var r4 = document.getElementById("responsable4MM").value;
        var s4 = document.getElementById("soporte4MM").value;
        var fF4 = document.getElementById("fFin4MM").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl4++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl4 === 3 ){
                        pnlOplCheckMM4.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar4MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones4MM").append(cOpl4+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo4MM").val("");
                    $("#accion4MM").val("");
                    $("#responsable4MM").val("");
                    $("#soporte4MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckMM5(){
        var h5 = document.getElementById("hallazgo5MM").value;
        var a5 = document.getElementById("accion5MM").value;
        var r5 = document.getElementById("responsable5MM").value;
        var s5 = document.getElementById("soporte5MM").value;
        var fF5 = document.getElementById("fFin5MM").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl5++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl5 === 3 ){
                        pnlOplCheckMM5.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar5MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones5MM").append(cOpl5+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo5MM").val("");
                    $("#accion5MM").val("");
                    $("#responsable5MM").val("");
                    $("#soporte5MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckMM6(){
        var h6 = document.getElementById("hallazgo6MM").value;
        var a6 = document.getElementById("accion6MM").value;
        var r6 = document.getElementById("responsable6MM").value;
        var s6 = document.getElementById("soporte6MM").value;
        var fF6 = document.getElementById("fFin6MM").value;
        
        var ht6 = h6.trim();
        var at6 = a6.trim();
        var rt6 = r6.trim();
        var st6 = s6.trim();
        var fFt6 = fF6.trim();
        
        if (ht6.length !== 0 && at6.length !== 0 && rt6.length !== 0 && st6.length !== 0 && fFt6.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h6, a: a6, r: r6, s: s6,fF: fF6 },                
                success: function(result){
                    cOpl6++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl6 === 3 ){
                        pnlOplCheckMM6.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar6MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones6MM").append(cOpl6+". "+a6+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo6MM").val("");
                    $("#accion6MM").val("");
                    $("#responsable6MM").val("");
                    $("#soporte6MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckMM7(){
        var h7 = document.getElementById("hallazgo7MM").value;
        var a7 = document.getElementById("accion7MM").value;
        var r7 = document.getElementById("responsable7MM").value;
        var s7 = document.getElementById("soporte7MM").value;
        var fF7 = document.getElementById("fFin7MM").value;
        
        var ht7 = h7.trim();
        var at7 = a7.trim();
        var rt7 = r7.trim();
        var st7 = s7.trim();
        var fFt7 = fF7.trim();
        
        if (ht7.length !== 0 && at7.length !== 0 && rt7.length !== 0 && st7.length !== 0 && fFt7.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h7, a: a7, r: r7, s: s7,fF: fF7 },                
                success: function(result){
                    cOpl7++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl7 === 3 ){
                        pnlOplCheckMM7.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar7MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones7MM").append(cOpl7+". "+a7+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo7MM").val("");
                    $("#accion7MM").val("");
                    $("#responsable7MM").val("");
                    $("#soporte7MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckMM8(){
        var h8 = document.getElementById("hallazgo8MM").value;
        var a8 = document.getElementById("accion8MM").value;
        var r8 = document.getElementById("responsable8MM").value;
        var s8 = document.getElementById("soporte8MM").value;
        var fF8 = document.getElementById("fFin8MM").value;
        
        var ht8 = h8.trim();
        var at8 = a8.trim();
        var rt8 = r8.trim();
        var st8 = s8.trim();
        var fFt8 = fF8.trim();
        
        if (ht8.length !== 0 && at8.length !== 0 && rt8.length !== 0 && st8.length !== 0 && fFt8.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h8, a: a8, r: r8, s: s8,fF: fF8 },                
                success: function(result){
                    cOpl8++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl8 === 3 ){
                        pnlOplCheckMM8.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar8MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones8MM").append(cOpl8+". "+a8+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo8MM").val("");
                    $("#accion8MM").val("");
                    $("#responsable8MM").val("");
                    $("#soporte8MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckMM9(){
        var h9 = document.getElementById("hallazgo9MM").value;
        var a9 = document.getElementById("accion9MM").value;
        var r9 = document.getElementById("responsable9MM").value;
        var s9 = document.getElementById("soporte9MM").value;
        var fF9 = document.getElementById("fFin9MM").value;
        
        var ht9 = h9.trim();
        var at9 = a9.trim();
        var rt9 = r9.trim();
        var st9 = s9.trim();
        var fFt9 = fF9.trim();
        
        if (ht9.length !== 0 && at9.length !== 0 && rt9.length !== 0 && st9.length !== 0 && fFt9.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h9, a: a9, r: r9, s: s9,fF: fF9 },                
                success: function(result){
                    cOpl9++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl9 === 3 ){
                        pnlOplCheckMM9.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar9MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones9MM").append(cOpl9+". "+a9+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo9MM").val("");
                    $("#accion9MM").val("");
                    $("#responsable9MM").val("");
                    $("#soporte9MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckMM10(){
        var h10 = document.getElementById("hallazgo10MM").value;
        var a10 = document.getElementById("accion10MM").value;
        var r10 = document.getElementById("responsable10MM").value;
        var s10 = document.getElementById("soporte10MM").value;
        var fF10 = document.getElementById("fFin10MM").value;
        
        var ht10 = h10.trim();
        var at10 = a10.trim();
        var rt10 = r10.trim();
        var st10 = s10.trim();
        var fFt10 = fF10.trim();
        
        if (ht10.length !== 0 && at10.length !== 0 && rt10.length !== 0 && st10.length !== 0 && fFt10.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h10, a: a10, r: r10, s: s10,fF: fF10 },                
                success: function(result){
                    cOpl10++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl10 === 3 ){
                        pnlOplCheckMM10.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar10MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones10MM").append(cOpl10+". "+a10+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo10MM").val("");
                    $("#accion10MM").val("");
                    $("#responsable10MM").val("");
                    $("#soporte10MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckMM11(){        
        var h11 = document.getElementById("hallazgo11MM").value;
        var a11 = document.getElementById("accion11MM").value;
        var r11 = document.getElementById("responsable11MM").value;
        var s11 = document.getElementById("soporte11MM").value;
        var fF11 = document.getElementById("fFin11MM").value;
        
        var ht11 = h11.trim();
        var at11 = a11.trim();
        var rt11 = r11.trim();
        var st11 = s11.trim();
        var fFt11 = fF11.trim();
        
        if (ht11.length !== 0 && at11.length !== 0 && rt11.length !== 0 && st11.length !== 0 && fFt11.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h11, a: a11, r: r11, s: s11,fF: fF11 },                
                success: function(result){
                    cOpl11++;
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl11 === 3 ){
                        pnlOplCheckMM11.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar11MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones11MM").append(cOpl11+". "+a11+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo11MM").val("");
                    $("#accion11MM").val("");
                    $("#responsable11MM").val("");
                    $("#soporte11MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckMM12(){
        var h12 = document.getElementById("hallazgo12MM").value;
        var a12 = document.getElementById("accion12MM").value;
        var r12 = document.getElementById("responsable12MM").value;
        var s12 = document.getElementById("soporte12MM").value;
        var fF12 = document.getElementById("fFin12MM").value;
        
        var ht12 = h12.trim();
        var at12 = a12.trim();
        var rt12 = r12.trim();
        var st12 = s12.trim();
        var fFt12 = fF12.trim();
        
        if (ht12.length !== 0 && at12.length !== 0 && rt12.length !== 0 && st12.length !== 0 && fFt12.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h12, a: a12, r: r12, s: s12,fF: fF12 },                
                success: function(result){
                    cOpl12++;
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl12 === 3 ){
                        pnlOplCheckMM12.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar12MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones12MM").append(cOpl12+". "+a12+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo12MM").val("");
                    $("#accion12MM").val("");
                    $("#responsable12MM").val("");
                    $("#soporte12MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 14014) {
                    alert('Requested page not found [14014]');
                } else if (jqXHR.status == 1500) {
                    alert('Internal Server Error [1500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckMM13(){
        var h13 = document.getElementById("hallazgo13MM").value;
        var a13 = document.getElementById("accion13MM").value;
        var r13 = document.getElementById("responsable13MM").value;
        var s13 = document.getElementById("soporte13MM").value;
        var fF13 = document.getElementById("fFin13MM").value;
        
        var ht13 = h13.trim();
        var at13 = a13.trim();
        var rt13 = r13.trim();
        var st13 = s13.trim();
        var fFt13 = fF13.trim();
        
        if (ht13.length !== 0 && at13.length !== 0 && rt13.length !== 0 && st13.length !== 0 && fFt13.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h13, a: a13, r: r13, s: s13,fF: fF13 },                
                success: function(result){
                    cOpl13++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl13 === 3 ){
                        pnlOplCheckMM13.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar13MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones13MM").append(cOpl13+". "+a13+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo13MM").val("");
                    $("#accion13MM").val("");
                    $("#responsable13MM").val("");
                    $("#soporte13MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckMM14(){
        var h14 = document.getElementById("hallazgo14MM").value;
        var a14 = document.getElementById("accion14MM").value;
        var r14 = document.getElementById("responsable14MM").value;
        var s14 = document.getElementById("soporte14MM").value;
        var fF14 = document.getElementById("fFin14MM").value;
        
        var ht14 = h14.trim();
        var at14 = a14.trim();
        var rt14 = r14.trim();
        var st14 = s14.trim();
        var fFt14 = fF14.trim();
        
        if (ht14.length !== 0 && at14.length !== 0 && rt14.length !== 0 && st14.length !== 0 && fFt14.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h14, a: a14, r: r14, s: s14,fF: fF14 },                
                success: function(result){
                    cOpl14++;
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl14 === 3 ){
                        pnlOplCheckMM14.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar14MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones14MM").append(cOpl14+". "+a14+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo14MM").val("");
                    $("#accion14MM").val("");
                    $("#responsable14MM").val("");
                    $("#soporte14MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckMM15(){
        var h15 = document.getElementById("hallazgo15MM").value;
        var a15 = document.getElementById("accion15MM").value;
        var r15 = document.getElementById("responsable15MM").value;
        var s15 = document.getElementById("soporte15MM").value;
        var fF15 = document.getElementById("fFin15MM").value;
        
        var ht15 = h15.trim();
        var at15 = a15.trim();
        var rt15 = r15.trim();
        var st15 = s15.trim();
        var fFt15 = fF15.trim();
        
        if (ht15.length !== 0 && at15.length !== 0 && rt15.length !== 0 && st15.length !== 0 && fFt15.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h15, a: a15, r: r15, s: s15,fF: fF15 },                
                success: function(result){
                    cOpl15++;
                    //SOLO PERMITE 13 OPL POR PREGUNTA NO MÁS
                    if (cOpl15 === 3 ){
                        pnlOplCheckMM15.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar15MM.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones15MM").append(cOpl15+". "+a15+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo15MM").val("");
                    $("#accion15MM").val("");
                    $("#responsable15MM").val("");
                    $("#soporte15MM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 1500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    //FUNCIONES PARA CERRAR OPLS
    function oplCheckMMC1(){
        pnlOplCheckMM1.className = 'no';
    }

    function oplCheckMMC2(){
        pnlOplCheckMM2.className = 'no';
    }
    
    function oplCheckMMC3(){
        pnlOplCheckMM3.className = 'no';
    }
    
    function oplCheckMMC4(){
        pnlOplCheckMM4.className = 'no';
    }
    
    function oplCheckMMC5(){
        pnlOplCheckMM5.className = 'no';
    }
    
    function oplCheckMMC6(){
        pnlOplCheckMM6.className = 'no';
    }
    
    function oplCheckMMC7(){
        pnlOplCheckMM7.className = 'no';
    }
    
    function oplCheckMMC8(){
        pnlOplCheckMM8.className = 'no';
    }
    
    function oplCheckMMC9(){
        pnlOplCheckMM9.className = 'no';
    }
    
    function oplCheckMMC10(){
        pnlOplCheckMM10.className = 'no';
    }
    
    function oplCheckMMC11(){
        pnlOplCheckMM11.className = 'no';
    }

    function oplCheckMMC12(){
        pnlOplCheckMM12.className = 'no';
    }
    
    function oplCheckMMC13(){
        pnlOplCheckMM13.className = 'no';
    }
    
    function oplCheckMMC14(){
        pnlOplCheckMM14.className = 'no';
    }
    
    function oplCheckMMC15(){
        pnlOplCheckMM15.className = 'no';
    }


