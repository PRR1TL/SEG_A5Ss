/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Areli Pérez Calixto (PRR1TL)
 * 21/01/2019
 *  
 */

    $(window).on('load',function() { 
        //VALICACIONES PARA OPL
        //1
        $("input[name=checkAMP1]").click(function () {
            var vP1 = $(this).val(); 
            if (vP1 == '1'){
                pnlOplCheckAM1.className = 'no';
            } else if (cOpl1 !== 3){
                pnlOplCheckAM1.className = 'si';                
            }            
        });
        
        //2
        $("input[name=checkAMP2]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckAM2.className = 'no';
            } else if (cOpl2 !== 3){
                pnlOplCheckAM2.className = 'si';                
            }
        });
        
        //3
        $("input[name=checkAMP3]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckAM3.className = 'no';
            } else if (cOpl3 !== 3){
                pnlOplCheckAM3.className = 'si';                
            }
        });
        
        //4
        $("input[name=checkAMP4]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckAM4.className = 'no';
            } else if (cOpl4 !== 3){
                pnlOplCheckAM4.className = 'si';                
            }
        });
        
        //5
        $("input[name=checkAMP5]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckAM5.className = 'no';
            } else if (cOpl5 !== 3){
                pnlOplCheckAM5.className = 'si';                
            }
        });
        
        //6
        $("input[name=checkAMP6]").click(function () {
            var vP6 = $(this).val(); 
            if (vP6 == '1'){
                pnlOplCheckAM6.className = 'no';
            } else if (cOpl6 !== 3){
                pnlOplCheckAM6.className = 'si';                
            }
        });
        
        //7
        $("input[name=checkAMP7]").click(function () {
            var vP7 = $(this).val(); 
            if (vP7 == '1'){
                pnlOplCheckAM7.className = 'no';
            } else if (cOpl7 !== 3){
                pnlOplCheckAM7.className = 'si';                
            }
        });
        
        //8
        $("input[name=checkAMP8]").click(function () {
            var vP8 = $(this).val(); 
            if (vP8 == '1'){
                pnlOplCheckAM8.className = 'no';
            } else if (cOpl8 !== 3){
                pnlOplCheckAM8.className = 'si';                
            }
        });
        
        //9
        $("input[name=checkAMP9]").click(function () {
            var vP9 = $(this).val(); 
            if (vP9 == '1'){
                pnlOplCheckAM9.className = 'no';
            } else if (cOpl9 !== 3){
                pnlOplCheckAM9.className = 'si';                
            }
        });
        
        //10
        $("input[name=checkAMP10]").click(function () {
            var vP10 = $(this).val(); 
            if (vP10 == '1'){
                pnlOplCheckAM10.className = 'no';
            } else if (cOpl10 !== 3){
                pnlOplCheckAM10.className = 'si';                
            }
        });
        
        //11
        $("input[name=checkAMP11]").click(function () {
            var vP1 = $(this).val(); 
            if (vP1 == '1'){
                pnlOplCheckAM11.className = 'no';
            } else if (cOpl11 !== 3){
                pnlOplCheckAM11.className = 'si';                
            }            
        });
        
        //12
        $("input[name=checkAMP12]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckAM12.className = 'no';
            } else if (cOpl12 !== 3){
                pnlOplCheckAM12.className = 'si';                
            }
        });
        
        //13
        $("input[name=checkAMP13]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckAM13.className = 'no';
            } else if (cOpl13 !== 3){
                pnlOplCheckAM13.className = 'si';                
            }
        });
        //14
        $("input[name=checkAMP14]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckAM14.className = 'no';
            } else if (cOpl14 !== 3){
                pnlOplCheckAM14.className = 'si';                
            }
        });
        
        //15
        $("input[name=checkAMP15]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckAM15.className = 'no';
            } else if (cOpl15 !== 3){
                pnlOplCheckAM15.className = 'si';                
            }
        });   
        
    });
    
    //FUNCIONES PARA EL PICKER
    $(window).on('load',function() { 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        
        if (dd < 10) {
            dd = '0' + dd;
        } 
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var today = dd+"/"+mm+"/"+yyyy;
  
        $( "#fFinAM1").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM2").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM3").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM4").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM5").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM6").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM7").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM8").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM9").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM10").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM11").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM12").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM13").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM14").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinAM15").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });    
    });
    
    function dAM1() {
        $("#mDAP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM2() {
        $("#mDAP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM3() {
        $("#mDAP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM4() {
        $("#mDAP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM5() {
        $("#mDAP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM6() {
        $("#mDAP6").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM7() {
        $("#mDAP7").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM8() {
        $("#mDAP8").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM9() {
        $("#mDAP9").modal({
            show: true,
            backdrop: false 
        });
    }
    
    function dAM10() {
        $("#mDAP10").modal({
            show: true,
            backdrop: false
        });
    }
    
    
    function dAM11() {
        $("#mDAP11").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM12() {
        $("#mDAP12").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM13() {
        $("#mDAP13").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM14() {
        $("#mDAP14").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dAM15() {
        $("#mDAP15").modal({
            show: true,
            backdrop: false
        });
    }
    
    
    //CONTADORES PARA LOS INGRESOS DE OPL POR PREGUNTA
    var cOpl1 = 0;
    var cOpl2 = 0;
    var cOpl3 = 0;
    var cOpl4 = 0;
    var cOpl5 = 0;
    var cOpl6 = 0;
    var cOpl7 = 0;
    var cOpl8 = 0;
    var cOpl9 = 0;
    var cOpl10 = 0;
    var cOpl11 = 0;
    var cOpl12 = 0;
    var cOpl13 = 0;
    var cOpl14 = 0;
    var cOpl15 = 0;
    
    //CUANDO SE HAGA CLIC EN OPL, SE JALAN LOS DATOS DE CADA OPL
    //FUNCIONES PARA PASO DE PARAMETTROS DE OPL A BD
    function oplCheckAM1(){        
        var h1 = document.getElementById("hallazgo1AM").value;
        var a1 = document.getElementById("accion1AM").value;
        var r1 = document.getElementById("responsable1AM").value;
        var s1 = document.getElementById("soporte1AM").value;
        var fF1 = document.getElementById("fFinAM1").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        var fFt1 = fF1.trim();
        
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl1++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl1 === 3 ){
                        pnlOplCheckAM1.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM1.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM1").append(cOpl1+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo1AM").val("");
                    $("#accion1AM").val("");
                    $("#responsable1AM").val("");
                    $("#soporte1AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckAM2(){
        var h2 = document.getElementById("hallazgo2AM").value;
        var a2 = document.getElementById("accion2AM").value;
        var r2 = document.getElementById("responsable2AM").value;
        var s2 = document.getElementById("soporte2AM").value;
        var fF2 = document.getElementById("fFinAM2").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl2++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl2 === 3 ){
                        pnlOplCheckAM2.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM2.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM2").append(cOpl2+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo2AM").val("");
                    $("#accion2AM").val("");
                    $("#responsable2AM").val("");
                    $("#soporte2AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckAM3(){
        var h3 = document.getElementById("hallazgo3AM").value;
        var a3 = document.getElementById("accion3AM").value;
        var r3 = document.getElementById("responsable3AM").value;
        var s3 = document.getElementById("soporte3AM").value;
        var fF3 = document.getElementById("fFinAM3").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl3++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl3 === 3 ){
                        pnlOplCheckAM3.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM3.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM3").append(cOpl3+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo3AM").val("");
                    $("#accion3AM").val("");
                    $("#responsable3AM").val("");
                    $("#soporte3AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckAM4(){
        var h4 = document.getElementById("hallazgo4AM").value;
        var a4 = document.getElementById("accion4AM").value;
        var r4 = document.getElementById("responsable4AM").value;
        var s4 = document.getElementById("soporte4AM").value;
        var fF4 = document.getElementById("fFinAM4").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl4++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl4 === 3 ){
                        pnlOplCheckAM4.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM4.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM4").append(cOpl4+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo4AM").val("");
                    $("#accion4AM").val("");
                    $("#responsable4AM").val("");
                    $("#soporte4AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckAM5(){
        var h5 = document.getElementById("hallazgo5AM").value;
        var a5 = document.getElementById("accion5AM").value;
        var r5 = document.getElementById("responsable5AM").value;
        var s5 = document.getElementById("soporte5AM").value;
        var fF5 = document.getElementById("fFinAM5").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl5++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl5 === 3 ){
                        pnlOplCheckAM5.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM5.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM5").append(cOpl5+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo5AM").val("");
                    $("#accion5AM").val("");
                    $("#responsable5AM").val("");
                    $("#soporte5AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckAM6(){
        var h6 = document.getElementById("hallazgo6AM").value;
        var a6 = document.getElementById("accion6AM").value;
        var r6 = document.getElementById("responsable6AM").value;
        var s6 = document.getElementById("soporte6AM").value;
        var fF6 = document.getElementById("fFinAM6").value;
        
        var ht6 = h6.trim();
        var at6 = a6.trim();
        var rt6 = r6.trim();
        var st6 = s6.trim();
        var fFt6 = fF6.trim();
        
        if (ht6.length !== 0 && at6.length !== 0 && rt6.length !== 0 && st6.length !== 0 && fFt6.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h6, a: a6, r: r6, s: s6,fF: fF6 },                
                success: function(result){
                    cOpl6++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl6 === 3 ){
                        pnlOplCheckAM6.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM6.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM6").append(cOpl6+". "+a6+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo6AM").val("");
                    $("#accion6AM").val("");
                    $("#responsable6AM").val("");
                    $("#soporte6AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckAM7(){
        var h7 = document.getElementById("hallazgo7AM").value;
        var a7 = document.getElementById("accion7AM").value;
        var r7 = document.getElementById("responsable7AM").value;
        var s7 = document.getElementById("soporte7AM").value;
        var fF7 = document.getElementById("fFinAM7").value;
        
        var ht7 = h7.trim();
        var at7 = a7.trim();
        var rt7 = r7.trim();
        var st7 = s7.trim();
        var fFt7 = fF7.trim();
        
        if (ht7.length !== 0 && at7.length !== 0 && rt7.length !== 0 && st7.length !== 0 && fFt7.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h7, a: a7, r: r7, s: s7,fF: fF7 },                
                success: function(result){
                    cOpl7++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl7 === 3 ){
                        pnlOplCheckAM7.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM7.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM7").append(cOpl7+". "+a7+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo7AM").val("");
                    $("#accion7AM").val("");
                    $("#responsable7AM").val("");
                    $("#soporte7AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckAM8(){
        var h8 = document.getElementById("hallazgo8AM").value;
        var a8 = document.getElementById("accion8AM").value;
        var r8 = document.getElementById("responsable8AM").value;
        var s8 = document.getElementById("soporte8AM").value;
        var fF8 = document.getElementById("fFinAM8").value;
        
        var ht8 = h8.trim();
        var at8 = a8.trim();
        var rt8 = r8.trim();
        var st8 = s8.trim();
        var fFt8 = fF8.trim();
        
        if (ht8.length !== 0 && at8.length !== 0 && rt8.length !== 0 && st8.length !== 0 && fFt8.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h8, a: a8, r: r8, s: s8,fF: fF8 },                
                success: function(result){
                    cOpl8++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl8 === 3 ){
                        pnlOplCheckAM8.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM8.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM8").append(cOpl8+". "+a8+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo8AM").val("");
                    $("#accion8AM").val("");
                    $("#responsable8AM").val("");
                    $("#soporte8AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckAM9(){
        var h9 = document.getElementById("hallazgo9AM").value;
        var a9 = document.getElementById("accion9AM").value;
        var r9 = document.getElementById("responsable9AM").value;
        var s9 = document.getElementById("soporte9AM").value;
        var fF9 = document.getElementById("fFinAM9").value;
        
        var ht9 = h9.trim();
        var at9 = a9.trim();
        var rt9 = r9.trim();
        var st9 = s9.trim();
        var fFt9 = fF9.trim();
        
        if (ht9.length !== 0 && at9.length !== 0 && rt9.length !== 0 && st9.length !== 0 && fFt9.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h9, a: a9, r: r9, s: s9,fF: fF9 },                
                success: function(result){
                    cOpl9++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl9 === 3 ){
                        pnlOplCheckAM9.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM9.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM9").append(cOpl9+". "+a9+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo9AM").val("");
                    $("#accion9AM").val("");
                    $("#responsable9AM").val("");
                    $("#soporte9AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckAM10(){
        var h10 = document.getElementById("hallazgo10AM").value;
        var a10 = document.getElementById("accion10AM").value;
        var r10 = document.getElementById("responsable10AM").value;
        var s10 = document.getElementById("soporte10AM").value;
        var fF10 = document.getElementById("fFinAM10").value;
        
        var ht10 = h10.trim();
        var at10 = a10.trim();
        var rt10 = r10.trim();
        var st10 = s10.trim();
        var fFt10 = fF10.trim();
        
        if (ht10.length !== 0 && at10.length !== 0 && rt10.length !== 0 && st10.length !== 0 && fFt10.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h10, a: a10, r: r10, s: s10,fF: fF10 },                
                success: function(result){
                    cOpl10++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl10 === 3 ){
                        pnlOplCheckAM10.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM10.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM10").append(cOpl10+". "+a10+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo10AM").val("");
                    $("#accion10AM").val("");
                    $("#responsable10AM").val("");
                    $("#soporte10AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckAM11(){        
        var h1 = document.getElementById("hallazgo11AM").value;
        var a1 = document.getElementById("accion11AM").value;
        var r1 = document.getElementById("responsable11AM").value;
        var s1 = document.getElementById("soporte11AM").value;
        var fF1 = document.getElementById("fFinAM11").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        var fFt1 = fF1.trim();
        
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl11++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl11 === 3 ){
                        pnlOplCheckAM11.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM11.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM11").append(cOpl11+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo11AM").val("");
                    $("#accion11AM").val("");
                    $("#responsable11AM").val("");
                    $("#soporte11AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckAM12(){
        var h2 = document.getElementById("hallazgo12AM").value;
        var a2 = document.getElementById("accion12AM").value;
        var r2 = document.getElementById("responsable12AM").value;
        var s2 = document.getElementById("soporte12AM").value;
        var fF2 = document.getElementById("fFinAM12").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl12++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl12 === 3 ){
                        pnlOplCheckAM12.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM12.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM12").append(cOpl12+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo12AM").val("");
                    $("#accion12AM").val("");
                    $("#responsable12AM").val("");
                    $("#soporte12AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckAM13(){
        var h3 = document.getElementById("hallazgo13AM").value;
        var a3 = document.getElementById("accion13AM").value;
        var r3 = document.getElementById("responsable13AM").value;
        var s3 = document.getElementById("soporte13AM").value;
        var fF3 = document.getElementById("fFinAM13").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl13++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl13 === 3 ){
                        pnlOplCheckAM13.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM13.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM13").append(cOpl13+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo13AM").val("");
                    $("#accion13AM").val("");
                    $("#responsable13AM").val("");
                    $("#soporte13AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckAM14(){
        var h4 = document.getElementById("hallazgo14AM").value;
        var a4 = document.getElementById("accion14AM").value;
        var r4 = document.getElementById("responsable14AM").value;
        var s4 = document.getElementById("soporte14AM").value;
        var fF4 = document.getElementById("fFinAM14").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl14++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl14 === 3 ){
                        pnlOplCheckAM14.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM14.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM14").append(cOpl14+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo14AM").val("");
                    $("#accion14AM").val("");
                    $("#responsable14AM").val("");
                    $("#soporte14AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckAM15(){
        var h5 = document.getElementById("hallazgo15AM").value;
        var a5 = document.getElementById("accion15AM").value;
        var r5 = document.getElementById("responsable15AM").value;
        var s5 = document.getElementById("soporte15AM").value;
        var fF5 = document.getElementById("fFinAM15").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl15++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl15 === 3 ){
                        pnlOplCheckAM15.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarAM15.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesAM15").append(cOpl15+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo15AM").val("");
                    $("#accion15AM").val("");
                    $("#responsable15AM").val("");
                    $("#soporte15AM").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    
    //FUNCIONES PARA CERRAR OPLS
    function oplCheckAMC1(){
        pnlOplCheckAM1.className = 'no';
    }

    function oplCheckAMC2(){
        pnlOplCheckAM2.className = 'no';
    }
    
    function oplCheckAMC3(){
        pnlOplCheckAM3.className = 'no';
    }
    
    function oplCheckAMC4(){
        pnlOplCheckAM4.className = 'no';
    }
    
    function oplCheckAMC5(){
        pnlOplCheckAM5.className = 'no';
    }
    
    function oplCheckAMC6(){
        pnlOplCheckAM6.className = 'no';
    }
    
    function oplCheckAMC7(){
        pnlOplCheckAM7.className = 'no';
    }
    
    function oplCheckAMC8(){
        pnlOplCheckAM8.className = 'no';
    }
    
    function oplCheckAMC9(){
        pnlOplCheckAM9.className = 'no';
    }
    
    function oplCheckAMC10(){
        pnlOplCheckAM10.className = 'no';
    }

    function oplCheckAMC11(){
        pnlOplCheckAM11.className = 'no';
    }

    function oplCheckAMC12(){
        pnlOplCheckAM12.className = 'no';
    }
    
    function oplCheckAMC13(){
        pnlOplCheckAM13.className = 'no';
    }
    
    function oplCheckAMC14(){
        pnlOplCheckAM14.className = 'no';
    }
    
    function oplCheckAMC15(){
        pnlOplCheckAM15.className = 'no';
    }

