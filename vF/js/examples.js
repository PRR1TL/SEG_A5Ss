$(document).ready(function() {

    var fHNow = new Date();
    var day = fHNow.getDate();
    var month = fHNow.getMonth()+1;
    var year = fHNow.getFullYear();

    if(day < 10 )
        day = '0'+day;
    
    if(month < 10 )
        month = '0'+month;

    var fecha = day+'/'+month+'/'+year;    
    
    $('#datepicker').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });

    $('#fFin1').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin2').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin3').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin4').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin5').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin6').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin7').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin8').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin9').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    });
    
    $('#fFin10').Zebra_DatePicker({
        format: 'd/m/Y',
        direction: [fecha, false]
    }); 

    $('#datepicker-future-tomorrow').Zebra_DatePicker({
        format: 'd-m-Y',
        direction: ['16-01-2019', false]
        //direction: -1
    });

    $('#datepicker-dynamic-interval').Zebra_DatePicker({
        direction: [1, 10]
    });

    $('#datepicker-dates-interval').Zebra_DatePicker({
        direction: ['2012-08-01', '2012-08-12']
    });

    $('#datepicker-after-date').Zebra_DatePicker({
        direction: ['2012-08-01', false]
    });

    $('#datepicker-disabled-dates').Zebra_DatePicker({
        direction: true,
        disabled_dates: ['* * * 0,6']
    });

    $('#datepicker-range-start').Zebra_DatePicker({
        direction: true,
        pair: $('#datepicker-range-end')
    });

    $('#datepicker-range-end').Zebra_DatePicker({
        direction: 1
    });

    $('#datepicker-formats').Zebra_DatePicker({
        format: 'M d, Y'
    });

    $('#datepicker-time').Zebra_DatePicker({
        format: 'd-m-Y H:i'
    });

    $('#datepicker-week-number').Zebra_DatePicker({
        show_week_number: 'Wk'
    });

    $('#datepicker-starting-view').Zebra_DatePicker({
        view: 'years'
    });

    $('#datepicker-partial-date-formats').Zebra_DatePicker({
        format: 'm Y'
    });

    $('#datepicker-custom-classes').Zebra_DatePicker({
        disabled_dates: ['* * * 0,6'],
        custom_classes: {
            'myclass':  ['* * * 0,6']
        }
    });

    $('#datepicker-on-change').Zebra_DatePicker({
        onChange: function(view, elements) {
            if (view === 'days') {
                elements.each(function() {
                    if ($(this).data('date').match(/\-24$/))
                        $(this).css({
                            background: '#C40000',
                            color:      '#FFF'
                        });
                });
            }
        }
    });

    $('#datepicker-always-visible').Zebra_DatePicker({
        always_visible: $('#container')
    });

    $('#datepicker-rtl-support').Zebra_DatePicker({
        rtl: true
    });

    $('#datepicker-data-attributes').Zebra_DatePicker();


});