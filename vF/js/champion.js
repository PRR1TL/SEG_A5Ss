/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * CREATED  : 29-01-2019
 * 
 */

$(document).ready(function() {
    var tipoEv = 0;
    var tipoAudit = 0;
    var lAOPL; 
    var lAAudit; 
    
    $('.btn-logOut').on('click', function(e){
        $('#ModalLogOut').modal({
            show: true,
            backdrop: false
        });
        event.preventDefault();
    });
    
    $('.btn-login').on('click', function(){
        $('#ModalLogin').modal({
            show: true,
            backdrop: false, 
            keyboard: false
        });
        event.preventDefault();
    });
    
    $('#mDUsuario').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var depto = button.data('depto'); // Extraer la información de atributos de datos
        var usuario = button.data('usuario'); // Extraer la información de atributos de datos
        var nombre = button.data('nombre'); // Extraer la información de atributos de datos

        var modal = $(this);
        modal.find('.modal-body #usuarioD').val(usuario);
        modal.find('.modal-body #nombreD').val(nombre);
    });     
    
    $('#mUSChamp').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var priv = button.data('privilegio'); // Extraer la información de atributos de datos
        var estado = button.data('estado'); // Extraer la información de atributos de datos
        var usuario = button.data('usuario'); // Extraer la información de atributos de datos
        var nombre = button.data('nombre'); // Extraer la información de atributos de datos
        var correo = button.data('correo'); // Extraer la información de atributos de datos

        console.log("p: "+priv+" e: "+estado+" u: "+usuario+" n: "+nombre+" c: "+correo);

        var modal = $(this);
        modal.find('.modal-body #privilegioSChamp').val(priv);
        modal.find('.modal-body #estadoSChamp').val(estado);
        modal.find('.modal-body #usuarioSChampU').val(usuario);
        modal.find('.modal-body #nombreSChampU').val(nombre);
        modal.find('.modal-body #correoSChampU').val(correo);
    }); 
    
    $("#fUSChamp").submit(function( event ) {
        //alert("Bien Areli"); 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "./db/admin/usuarios/uSChamp.php", 
            data: parametros, 
            success: function(datos) {
                var datA = [];
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();                    
                }
                
                if (datA[0] == 'Bien') {
                    $("#alertUUsuario").html("<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ACTUALIZACION EXITOSA<BR> </strong></div>");
                    location.href="./aChampion.php";
                }   
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $('.btn-miU').on('click', function() {
        $('#mUMIU').modal({
            show: true,
            backdrop: false, 
            keyboard: true
        });
        event.preventDefault();
    });
    
    //MODULO DE USUARIOS (SUBCHAMPIONS)
    $( "#fISChamp" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/usuarios/iSChamp.php",
            data: parametros,
            success: function(datos){
                $("#alertUsersChamp").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $("#fDUsuario").submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/dUsuario.php",
            data: parametros,
            success: function(datos) {
                var datA = [];
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();                    
                }
                
                if (datA[0] == 'Bien') {
                    $("#alertDUsuario").html("<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ACTUALIZACION EXITOSA<BR> </strong></div>");
                }   
                location.href=datA[1];
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') { 
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
        
    //ROLES
    $( "#fIRol" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/progAuditoria/SChampRol.php",
            data: parametros,
            success: function(datos) { 
                if (datos = 'Bien'){
                    location.href="./aChampionRol.php";
                    $("#alertRolChamp").html("<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ROL ACTUALIZADO</strong></div>");
                } 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    //EVENTOS PARA LOS BOTONES DE MODALS INFORMATIVOS DE LAS AUDITORIAS   
    //BOTON DE MODAL PARA SELECCION DE AUDITORIA
    $('#fTipoAuditoria').submit(function( event ) { 
        var datA = [];
        var tipo = "";
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../db/auditoria/sTAuditChamp.php",
            data: parametros,                
            success: function(datos){
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                switch(res1){
                    case "0":
                        $('#NoneAuditoria').modal({
                            show: true,
                            backdrop: 'static',  
                            keyboard: true
                        });
                        break;
                    case "1":   
                        $('#mTipoAuditoria').modal('hide');
                        var a1 = respuesta.split(",");
                        for (var i in a1) {
                            datA[i] = a1[i];
                        }

                        //TRANSFROMAMOS EL VALOR DE TIPO
                        switch (datA[4]){
                            case 1: case "1":
                                tipo = "CHECKLIST";
                                break;
                            case 2: case "2":
                                tipo = "SEMANAL";
                                break;
                            case 3: case "3":
                                tipo = "MENSUAL";
                                break;
                            case 4: case "4":
                                tipo = "CONFIRMACION";
                                break;
                        }
                        //MANDAMOS LOS VALORES DE LA AUDITORIA 
                        $('#idAuditoria').val(datA[1]);//ID 
                        $('#areaAuditoria').val(datA[2]); //LINEA
                        $('#auditado').val(datA[3]); //AUDITADO
                        $('#tipoAudit').val(tipo); //TIPO AUDITORIA
                        $('#tipoEv').val(datA[5]); //TIPO EVALUACION

                        $('#mInfoAuditoria').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        });
                        break;                        
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(champio.js: Line 161)");
                        break;                        
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    //BOTON DE MODAL INFORMATIVO DE AUDITORIA
    $('#fInfoAuditoria').submit(function( event ) { 
        var datA = [];
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/auditoria/sessionID.php",
            data: parametros,                
            success: function(datos) {
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                console.log("sesion");
                for (var i in a1) {
                    datA[i] = a1[i].trim();
                    console.log(i+": "+datA[i]);
                }
                tipoAudit = datA[2];
                tipoEv = datA[4];

                switch(res1){
                    case "0": case 0:
                        alert("NO SE TIENE ID \n Error(champion.js: Line 143)");
                        break;
                    case "1": case 1:
                        $('#mInfoAuditoria').modal('hide');
                        $('#ModalRules').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: true
                        });
                        break;                        
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(champion.js: Line 154)");
                        break;                        
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $('.btn-rules').on('click', function() {
        //alert("algo esta pasando aca ");
        console.log("tipoEv: "+tipoEv);
        var datA = [];
        
        //EVALUAMOS EL TIPO DE EVALUACION  
        var parametros = $(this).serialize();
        
        $.ajax({
            type: "POST",        
            url: './db/auditoria/cLAudit_OPL.php',
            data: parametros, 
            success: function(respuesta) {
                var dat = respuesta.trim();
                var d1 = dat.substr(0,1);
                var a1 = dat.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();
                }
                
                lAOPL = datA[0]; 
                lAAudit = datA[1]; 
                //console.log("0. datO: "+lAOPL+" datA: "+lAAudit);
               
                switch (tipoEv) { 
                    case '1': //ALMACEN 
                        switch (tipoAudit) { 
                            case '1': //CHECKLIST
                                $('#mADaily').modal({ 
                                    show: true, 
                                    backdrop: false, 
                                    keyboard: false 
                                });
                                break;
                            case '2': //SEMANAL
                                $('#mAWeekly').modal({ 
                                    show: true, 
                                    backdrop: false, 
                                    keyboard: false
                                }); 
                                break;
                            case '3': //MENSUAL
                                $('#mAMonthly1').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                        }
                        break;

                    case '2': //LABORATORIO
                        switch (tipoAudit){
                            case '1': //CHECKLIST
                                $('#mLabDaily').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '2': //SEMANAL
                                $('#mLabWeekly').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '3': //MENSUAL
                                $('#mLabMonthly1').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                        }
                        break;

                    case '3': //LINEA
                        switch (tipoAudit){
                            case '1': //CHECKLIST
                                $('#mLDaily').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '2': //SEMANAL
                                $('#mLWeekly').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '3': //MENSUAL
                                $('#mLMonthly1').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                        }                
                        break;

                    case '4': //MANTENIMIENTO
                        switch (tipoAudit){
                            case '1': //CHECKLIST
                                $('#mMDaily').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '2': //SEMANAL
                                $('#mMWeekly').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                            case '3': //MENSUAL
                                $('#mMMonthly1').modal({                            
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                });
                                break;
                        }
                        break;

                    case '5': //OFICINA
                        switch (tipoAudit){
                            case '1': //CHECKLIST 
                                $('#mODaily').modal({ 
                                    show: true,
                                    backdrop: false, 
                                    keyboard: false
                                }); 
                                break;
                                
                            case '2': //SEMANAL                                
                                $("#bnOWP1").val(lAOPL);
                                if (lAOPL == '1' || lAOPL == 1) { 
                                    
                                    $('input:radio[name=checkOWP1]:nth(0)').attr('checked',true);
                                    pnlOplCheckOW1.className = 'no';
                                } else {
                                    $('input:radio[name=checkOWP1]:nth(1)').attr('checked',true);
                                    pnlOplCheckOW1.className = 'si';
                                }
                                
                                $("#bnOWP10").val(lAAudit);
                                if (lAAudit == '1' || lAAudit == 1) {
                                    $('input:radio[name=checkOWP10]:nth(0)').attr('checked',true);
                                    pnlOplCheckOW10.className = 'no';
                                } else { 
                                    $('input:radio[name=checkOWP10]:nth(1)').attr('checked',true);
                                    pnlOplCheckOW10.className = 'si';
                                } 

                                $('#mOWeekly').modal({ 
                                    show: true, 
                                    backdrop: false, 
                                    keyboard: false 
                                });
                                break;
                                
                            case '3': //MENSUAL                                
                                $("#bnOMP1").val(lAOPL);
                                if (lAOPL == '1' || lAOPL == 1) {                                     
                                    $('input:radio[name=checkOMP1]:nth(0)').attr('checked',true);
                                    pnlOplCheckOM10.className = 'no';
                                } else {
                                    $('input:radio[name=checkOMP1]:nth(1)').attr('checked',true);
                                    pnlOplCheckOM1.className = 'si';
                                }
                                
                                $("#bnOMP10").val(lAAudit);
                                if (lAAudit == '1' || lAAudit == 1) {
                                    $('input:radio[name=checkOMP10]:nth(0)').attr('checked',true);
                                    pnlOplCheckOM10.className = 'no';
                                } else { 
                                    $('input:radio[name=checkOMP10]:nth(1)').attr('checked',true);
                                    pnlOplCheckOM10.className = 'si';
                                } 
                                
                                $('#mOMonthly').modal({ 
                                    show: true,
                                    backdrop: false,  
                                    keyboard: false
                                }); 
                                break;
                        } 
                        break; 
                } 
                
                $('#ModalRules').modal('hide'); 
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        });
    });
    
    //FUNCIONES DE MODALS PARA CHECKLIST
    /**************************** ALMACE **************************************/
    $('#fADaily').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosA.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    //console.log(i+": "+datA[i]);
                }                
                
                if (datA[0] === "Bien"){
                    $('#mADaily').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertADaily").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    /**************************** LABORATORIO *********************************/    
    $('#fLADaily').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosLab.php",
            data: parametros,                
            success: function(datos) {                
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mLabDaily').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertLADaily").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    /**************************** LINEAS **************************************/
    $('#fLDaily').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosL.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    console.log(i+": "+datA[i]);
                }
                
                if (datA[0] === "Bien"){
                    $('#mLDaily').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertLDaily").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    /**************************** MANTENIMIENTO *******************************/
    $('#fMDaily').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosM.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    //console.log(i+": "+datA[i]);
                }                
                
                if (datA[0] === "Bien"){
                    $('#mMDaily').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertMDaily").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });

    /**************************** OFICINA *************************************/
    $('#fODaily').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosO.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    //console.log(i+": "+datA[i]);
                }                
                
                if (datA[0] === "Bien"){
                    $('#mODaily').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertLDaily").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    //FUNCIONES DE MODALS PARA MONTHLY
    /**************************** ALMACE **************************************/
    $('#fAMonthly1').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosAM.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    console.log(i+": "+datA[i]);
                }                
                
                if (datA[0] === "Bien"){
                    $('#mAMonthly1').modal('hide');
                    $('#mAMonthly2').modal({
                        show: true,
                        backdrop: false,  
                        keyboard: false
                    });                    
                } else {
                    $("#alertAMonthly1").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $('#fAMonthly2').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosAM2.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    console.log(i+": "+datA[i]);
                }                
                
                if (datA[0] === "Bien"){
                    $('#mAMonthly2').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertAMonthly2").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    /**************************** LABORATORIO *********************************/    
    $('#fLAMonthly1').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosLabM.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    console.log(i+": "+datA[i]);
                }                
                
                if (datA[0] === "Bien"){
                    $('#mLabMonthly1').modal('hide');
                    $('#mLabMonthly2').modal({
                        show: true,
                        backdrop: false,  
                        keyboard: false
                    });                     
                } else {
                    $("#alertLAMonthly1").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    $('#fLAMonthly2').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosLabM2.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                    console.log(i+": "+datA[i]);
                }                
                
                if (datA[0] === "Bien"){
                    $('#mLabMonthly2').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertLAMonthly2").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    /**************************** LINEAS **************************************/
    $('#fLMonthly1').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosLM.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }               
                
                if (datA[0] === "Bien"){
                    $('#mLMonthly1').modal('hide');
                    $('#mLMonthly2').modal({
                        show: true,
                        backdrop: false,  
                        keyboard: false
                    });
                } else {
                    $("#alertLMonthly1").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    $('#fLMonthly2').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosLM2.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }               
                
                if (datA[0] === "Bien"){
                    $('#mLMonthly2').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertLMonthly1").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    /**************************** MANTENIMIENTO *******************************/
    $('#fMMonthly1').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosMM.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mMMonthly1').modal('hide');
                    $('#mMMonthly2').modal({
                        show: true,
                        backdrop: false,  
                        keyboard: false
                    }); 
                } else {
                    $("#alertMMonthly1").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });

    $('#fMMonthly2').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosMM2.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mMMonthly2').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertMMonthly2").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });

    /**************************** OFICINA *************************************/
    $('#fOMonthly').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosOM.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }
                
                if (datA[0] === "Bien"){
                    $('#mOMonthly').modal('hide');
                    if (datA[1] < 85){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 75 && datA[1] > 84){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertOMonthly").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });

    //FUNCIONES DE MODALS PARA WEEKLY
    /**************************** ALMACE **************************************/
    $('#fAWeekly').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosAW.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mAWeekly').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertAWeekly").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    /**************************** LABORATORIO *********************************/  
    $('#fLAWeekly').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosLabW.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mLabWeekly').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertLAWeekly").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    /**************************** LINEAS **************************************/
    $('#fLWeekly').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosLW.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mLWeekly').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertLWeekly").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });

    /**************************** MANTENIMIENTO *******************************/
    $('#fMWeekly').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosMW.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mMWeekly').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertMWeekly").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });

    /**************************** OFICINA *************************************/
    $('#fOWeekly').submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/iRespuestas/iPuntosOW.php",
            data: parametros,                
            success: function(datos){
                var datA = [];
                var res = datos.trim();
                var a1 = res.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }                
                
                if (datA[0] === "Bien"){
                    $('#mOWeekly').modal('hide');
                    if (datA[1] < 80){
                        $('#scooreR').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaR').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    }else
                    if (datA[1] < 60){
                        $('#scooreM').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaM').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } else {
                        $('#scooreB').val("TOTAL: "+datA[1]);
                        $('#mPuntosAuditoriaB').modal({
                            show: true,
                            backdrop: false,  
                            keyboard: false
                        }); 
                    } 
                } else {
                    $("#alertOWeekly").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });    

    //FUNCION PARA EL LOGIN
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/cLogin.php",
            data: parametros,
            success: function(datos){
                $("#datos_ajax").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });       
    
    $( "#logOut" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./modals/accesos/exit.php",
            data: parametros,
            success: function(datos){
                $("#datos_ajax").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });   
    
    $( "#fTipoAuditoriaCh" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var datA = [];
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/auditoria/sTAuditChamp.php",
            data: parametros,
            success: function(respuesta) {
                $('#mTipoAuditoriaCh').modal('hide'); 
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i];
                }
                
                //TRANSFROMAMOS EL VALOR DE TIPO
                switch (datA[4]){
                    case 1: case "1":
                        tipo = "CHECKLIST";
                        break;
                    case 2: case "2":
                        tipo = "SEMANAL";
                        break;
                    case 3: case "3":
                        tipo = "MENSUAL";
                        break;
                    case 4: case "4":
                        tipo = "CONFIRMACION";
                        break;
                }
                
                //MANDAMOS LOS VALORES DE LA AUDITORIA 
                $('#idAuditoria').val(datA[0]);//ID 
                $('#areaAuditoria').val(datA[1]); //LINEA
                $('#auditado').val(datA[2]); //AUDITADO
                $('#tipoAudit').val(tipo); //TIPO AUDITORIA
                $('#tipoEv').val(datA[3]); //TIPO EVALUACION

                $('#mInfoAuditoria').modal({
                    show: true,
                    backdrop: false, 
                    keyboard: false
                });
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });   
    
    //OPL
    $('#mUOpl').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var idOpl = button.data('hallazgo1'); // Extraer la información de atributos de datos     
        var fInicio = button.data('hallazgo2'); // Extraer la información de atributos de datos
        var fCompromiso = button.data('hallazgo3'); // Extraer la información de atributos de datos
        var hallazgo = button.data('hallazgo'); // Extraer la información de atributos de datos
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var responsable = button.data('responsable'); // Extraer la información de atributos de datos
        var soporte = button.data('soporte'); // Extraer la información de atributos de datos
        var estado = button.data('estado'); // Extraer la información de atributos de datos

        //console.log("idOpl: "+idOpl+", fInicio: "+fInicio+", fCompromiso: "+fCompromiso+", hallazgo: "+hallazgo+", accion: "+accion+", responsable: "+responsable+", soporte: "+soporte+", estado: "+estado);

        var modal = $(this);
        modal.find('.modal-body #idOpl').val(idOpl);
        modal.find('.modal-body #fInicio').val(fInicio);
        modal.find('.modal-body #fCompromiso').val(fCompromiso);
        modal.find('.modal-body #hallazgo').val(hallazgo);
        modal.find('.modal-body #accion').val(accion);
        modal.find('.modal-body #responsable').val(responsable);
        modal.find('.modal-body #soporte').val(soporte);
        modal.find('.modal-body #estado').val(estado);

        $('.alert').hide();//Oculto alert
    });
    
    function miUser(){
        $('#mUMIU').modal({
            show: true,
            backdrop: false, 
            keyboard: false
        });
    }
    
    function loadOplA(){
        location.href="./pAbiertos.php";
        location.reload(true);
    }    
    
    $( "#fUOpl" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "db/opl/uOpl.php",
            data: parametros,
            beforeSend: function(objeto){
                $("#datos_ajax").html("Mensaje: Cargando...");
            },
            success: function(datos){
                $("#datos_ajax").html(datos);
                loadOplA();
            }
        });
        event.preventDefault();
    });
    
    $( "#fuMU" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "db/admin/uMiUsuario.php",
            data: parametros,
            beforeSend: function(objeto){
                $("#alert_uMIU").html("<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ACTUALIZACION REALIZADA </strong></div>");
            },
            success: function(datos) {                
                if (datos === '0' || datos === 0 ) { 
                    $("#alert_uMIU").html("<div class='alert alert-success' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>ACTUALIZACION REALIZADA </strong></div>");
                    loadOplA();
                } else { 
                    $("#alert_uMIU").html("<div class='alert alert-danger' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>NO SE PUEDE ACTUALIZAR: LOS CAMPOS DEBEN ESTAR LLENOS </strong></div>");
                } 
            } 
        });
        event.preventDefault();
    });

});

function index() {
    alert("index");
    location.href = "index.php";
}

function audit() {
    var datA = [];
    var tipo = "";
    //BOTONES DE LOS MODAL DE AUDITORIA    
    //AQUI HACEMOS LA CONSULTA PARA VER LAS AUDITORIAS 
    $.ajax({
        type: "POST",
        url: "./db/progAuditoria/cUserAuditoria.php",          
        success: function(datos){
            var respuesta = datos.trim();
            var res1 = respuesta.substr(0,1);

            //console.log("res: "+res1);
            switch(res1){
                case "0": case 0:
                    $('#NoneAuditoria').modal({
                        show: true,
                        backdrop: false,
                        keyboard: true
                    });
                    break;
                case "1": case 1:
                    var a1 = respuesta.split(",");
                    for (var i in a1) {
                        datA[i] = a1[i];
                        console.log(i+": "+datA[i]);
                    }                        
                    //TRANSFROMAMOS EL VALOR DE TIPO
                    switch (datA[4]){
                        case 1: case "1":
                            tipo = "CHECKLIST";
                            break;
                        case 2: case "2": 
                            tipo = "SEMANAL";
                            break;
                        case 3: case "3":
                            tipo = "MENSUAL";
                            break;
                        case 4: case "4": 
                            tipo = "CONFIRMACION";
                            break;
                    }                        
                    //MANDAMOS LOS VALORES DE LA AUDITORIA 
                    $('#idAuditoria').val(datA[1]);//ID 
                    $('#areaAuditoria').val(datA[2]); //LINEA
                    $('#auditado').val(datA[3]); //AUDITADO
                    $('#tipoAudit').val(tipo); //TIPO AUDITORIA
                    $('#tipoEv').val(datA[5]); //TIPO EVALUACION

                    $('#mInfoAuditoria').modal({
                        show: true,
                        backdrop: false, 
                        keyboard: false
                    });
                    break;
                case "2":  case 2: 
                    $("#cmbTipoAuditoriaCh").empty();
                    var a1 = respuesta.split(",");
                    for (var i in a1) {
                        datA[i] = a1[i];
                    }                   
                    
                    switch (datA[1]) {
                        case '1':
                            var dTipo1 = "CHECKLIST";
                            break;
                        case '2':
                            var dTipo1 = "SEMANAL";
                            break;
                        case '3':
                            var dTipo1 = "MENSUAL";
                            break;
                    }
                    
                    switch (datA[2]) {
                        case '1':
                            var dTipo2 = "CHECKLIST";
                            break;
                        case '2':
                            var dTipo2 = "SEMANAL";
                            break;
                        case '3':
                            var dTipo2 = "MENSUAL";
                            break;
                    }
                    
                    $("#cmbTipoAuditoriaCh").append('<option value='+datA[3]+'>'+dTipo1+'</option>');
                    $("#cmbTipoAuditoriaCh").append('<option value='+datA[4]+'>'+dTipo2+'</option>');
                    
                    $('#mTipoAuditoriaCh').modal({
                        show: true,
                        backdrop: false,
                        keyboard: false
                    });
                    break;
                default :
                    alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(champion.js: Line 1726)");
                    break;                        
            }
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
    //event.preventDefault();
}

function report() {
    location.href = "rDepto.php";
}

function calendar() {
    location.href = "calendar.php";
}

function info() {
    location.href = "info.php";
}

function opl() {
    location.href = "lOpl.php";
}

function r_area (){
    var anio = document.getElementById("anioA").value;
    var mes = document.getElementById("mesA").value;
    var area = document.getElementById("area").value;
    $.ajax({
        url: "contenedores/rArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contGeneral").html("");
            $("#hGeneral").hide();
            $("#hArea").show();
            jQuery("#contAreaCh").html(resultA);
        }
    });
}

function aOpl_miArea (){
    var anio = document.getElementById("anioAOpl").value;
    var mes = document.getElementById("mesAOpl").value;
    var area = document.getElementById("areaOpl").value;
    $.ajax({
        url: "contenedores/opl/aArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contAreaSOpl").html(resultA);
        }
    });
}

function aOpl_area (){
    var anio = document.getElementById("anioA").value;
    var mes = document.getElementById("mesA").value;
    var area = document.getElementById("area").value;
    $.ajax({
        url: "contenedores/opl/aArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contGeneral").html("");
            $("#hGeneral").hide();
            $("#hArea").show();
            jQuery("#contArea").html(resultA);
        }
    });
}

function aOpl_general(){    
    var anio = document.getElementById("anioG").value;
    var mes = document.getElementById("mesG").value;    
        
    $.ajax({
        url: "contenedores/opl/aGeneral.php",
        type: "post",
        data: { a: anio, m: mes},
        success: function (resultG) {
            jQuery("#contArea").html("");
            $("#hArea").hide();
            $("#hGeneral").show();
            jQuery("#contGeneral").html(resultG);
        }
    });
}

