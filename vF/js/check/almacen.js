/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Areli Pérez Calixto (PRR1TL)
 * 21/01/2019
 *  
 */

    $(window).on('load',function() { 
        //VALICACIONES PARA OPL
        //1
        $("input[name=checkAP1]").click(function () {
            var vP1 = $(this).val(); 
            if (vP1 == '1'){
                pnlOplCheckA1.className = 'no';
            } else if (cOpl1 !== 3){
                pnlOplCheckA1.className = 'si';                
            }            
        });
        
        //2
        $("input[name=checkAP2]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckA2.className = 'no';
            } else if (cOpl2 !== 3){
                pnlOplCheckA2.className = 'si';                
            }
        });
        
        //3
        $("input[name=checkAP3]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckA3.className = 'no';
            } else if (cOpl3 !== 3){
                pnlOplCheckA3.className = 'si';                
            }
        });
        
        //4
        $("input[name=checkAP4]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckA4.className = 'no';
            } else if (cOpl4 !== 3){
                pnlOplCheckA4.className = 'si';                
            }
        });
        
        //5
        $("input[name=checkAP5]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckA5.className = 'no';
            } else if (cOpl5 !== 3){
                pnlOplCheckA5.className = 'si';                
            }
        });
        
        //6
        $("input[name=checkAP6]").click(function () {
            var vP6 = $(this).val(); 
            if (vP6 == '1'){
                pnlOplCheckA6.className = 'no';
            } else if (cOpl6 !== 3){
                pnlOplCheckA6.className = 'si';                
            }
        });
        
        //7
        $("input[name=checkAP7]").click(function () {
            var vP7 = $(this).val(); 
            if (vP7 == '1'){
                pnlOplCheckA7.className = 'no';
            } else if (cOpl7 !== 3){
                pnlOplCheckA7.className = 'si';                
            }
        });
        
        //8
        $("input[name=checkAP8]").click(function () {
            var vP8 = $(this).val(); 
            if (vP8 == '1'){
                pnlOplCheckA8.className = 'no';
            } else if (cOpl8 !== 3){
                pnlOplCheckA8.className = 'si';                
            }
        });
        
        //9
        $("input[name=checkAP9]").click(function () {
            var vP9 = $(this).val(); 
            if (vP9 == '1'){
                pnlOplCheckA9.className = 'no';
            } else if (cOpl9 !== 3){
                pnlOplCheckA9.className = 'si';                
            }
        });
        
        //10
        $("input[name=checkAP10]").click(function () {
            var vP10 = $(this).val(); 
            if (vP10 == '1'){
                pnlOplCheckA10.className = 'no';
            } else if (cOpl10 !== 3){
                pnlOplCheckA10.className = 'si';                
            }
        });        
    });
    
    //FUNCIONES PARA EL PICKER
    $(window).on('load',function() { 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        
        if (dd < 10) {
            dd = '0' + dd;
        } 
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var today = dd+"/"+mm+"/"+yyyy;
  
        $( "#fFin1A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin2A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin3A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin4A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin5A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin6A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin7A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin8A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin9A").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin10A").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });        
    });
    
    function dA1() {
        $("#mDAP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dA2() {
        $("#mDAP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dA3() {
        $("#mDAP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dA4() {
        $("#mDAP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dA5() {
        $("#mDAP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dA6() {
        $("#mDAP6").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dA7() {
        $("#mDAP7").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dA8() {
        $("#mDAP8").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dA9() {
        $("#mDAP9").modal({
            show: true,
            backdrop: false 
        });
    }
    
    function dA10() {
        $("#mDAP10").modal({
            show: true,
            backdrop: false
        });
    }
    
    //CONTADORES PARA LOS INGRESOS DE OPL POR PREGUNTA
    var cOpl1 = 0;
    var cOpl2 = 0;
    var cOpl3 = 0;
    var cOpl4 = 0;
    var cOpl5 = 0;
    var cOpl6 = 0;
    var cOpl7 = 0;
    var cOpl8 = 0;
    var cOpl9 = 0;
    var cOpl10 = 0;
    
    //CUANDO SE HAGA CLIC EN OPL, SE JALAN LOS DATOS DE CADA OPL
    //FUNCIONES PARA PASO DE PARAMETTROS DE OPL A BD
    function oplCheckA1(){        
        var h1 = document.getElementById("hallazgo1A").value;
        var a1 = document.getElementById("accion1A").value;
        var r1 = document.getElementById("responsable1A").value;
        var s1 = document.getElementById("soporte1A").value;
        var fF1 = document.getElementById("fFin1A").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        var fFt1 = fF1.trim();
        
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl1++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl1 === 3 ){
                        pnlOplCheckA1.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar1A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones1A").append(cOpl1+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo1A").val("");
                    $("#accion1A").val("");
                    $("#responsable1A").val("");
                    $("#soporte1A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckA2(){
        var h2 = document.getElementById("hallazgo2A").value;
        var a2 = document.getElementById("accion2A").value;
        var r2 = document.getElementById("responsable2A").value;
        var s2 = document.getElementById("soporte2A").value;
        var fF2 = document.getElementById("fFin2A").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl2++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl2 === 3 ){
                        pnlOplCheckA2.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar2A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones2A").append(cOpl2+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo2A").val("");
                    $("#accion2A").val("");
                    $("#responsable2A").val("");
                    $("#soporte2A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckA3(){
        var h3 = document.getElementById("hallazgo3A").value;
        var a3 = document.getElementById("accion3A").value;
        var r3 = document.getElementById("responsable3A").value;
        var s3 = document.getElementById("soporte3A").value;
        var fF3 = document.getElementById("fFin3A").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl3++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl3 === 3 ){
                        pnlOplCheckA3.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar3A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones3A").append(cOpl3+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo3A").val("");
                    $("#accion3A").val("");
                    $("#responsable3A").val("");
                    $("#soporte3A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckA4(){
        var h4 = document.getElementById("hallazgo4A").value;
        var a4 = document.getElementById("accion4A").value;
        var r4 = document.getElementById("responsable4A").value;
        var s4 = document.getElementById("soporte4A").value;
        var fF4 = document.getElementById("fFin4A").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl4++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl4 === 3 ){
                        pnlOplCheckA4.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar4A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones4A").append(cOpl4+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo4A").val("");
                    $("#accion4A").val("");
                    $("#responsable4A").val("");
                    $("#soporte4A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckA5(){
        var h5 = document.getElementById("hallazgo5A").value;
        var a5 = document.getElementById("accion5A").value;
        var r5 = document.getElementById("responsable5A").value;
        var s5 = document.getElementById("soporte5A").value;
        var fF5 = document.getElementById("fFin5A").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl5++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl5 === 3 ){
                        pnlOplCheckA5.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar5A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones5A").append(cOpl5+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo5A").val("");
                    $("#accion5A").val("");
                    $("#responsable5A").val("");
                    $("#soporte5A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckA6(){
        var h6 = document.getElementById("hallazgo6A").value;
        var a6 = document.getElementById("accion6A").value;
        var r6 = document.getElementById("responsable6A").value;
        var s6 = document.getElementById("soporte6A").value;
        var fF6 = document.getElementById("fFin6A").value;
        
        var ht6 = h6.trim();
        var at6 = a6.trim();
        var rt6 = r6.trim();
        var st6 = s6.trim();
        var fFt6 = fF6.trim();
        
        if (ht6.length !== 0 && at6.length !== 0 && rt6.length !== 0 && st6.length !== 0 && fFt6.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h6, a: a6, r: r6, s: s6,fF: fF6 },                
                success: function(result){
                    cOpl6++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl6 === 3 ){
                        pnlOplCheckA6.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar6A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones6A").append(cOpl6+". "+a6+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo6A").val("");
                    $("#accion6A").val("");
                    $("#responsable6A").val("");
                    $("#soporte6A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckA7(){
        var h7 = document.getElementById("hallazgo7A").value;
        var a7 = document.getElementById("accion7A").value;
        var r7 = document.getElementById("responsable7A").value;
        var s7 = document.getElementById("soporte7A").value;
        var fF7 = document.getElementById("fFin7A").value;
        
        var ht7 = h7.trim();
        var at7 = a7.trim();
        var rt7 = r7.trim();
        var st7 = s7.trim();
        var fFt7 = fF7.trim();
        
        if (ht7.length !== 0 && at7.length !== 0 && rt7.length !== 0 && st7.length !== 0 && fFt7.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h7, a: a7, r: r7, s: s7,fF: fF7 },                
                success: function(result){
                    cOpl7++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl7 === 3 ){
                        pnlOplCheckA7.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar7A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones7A").append(cOpl7+". "+a7+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo7A").val("");
                    $("#accion7A").val("");
                    $("#responsable7A").val("");
                    $("#soporte7A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckA8(){
        var h8 = document.getElementById("hallazgo8A").value;
        var a8 = document.getElementById("accion8A").value;
        var r8 = document.getElementById("responsable8A").value;
        var s8 = document.getElementById("soporte8A").value;
        var fF8 = document.getElementById("fFin8A").value;
        
        var ht8 = h8.trim();
        var at8 = a8.trim();
        var rt8 = r8.trim();
        var st8 = s8.trim();
        var fFt8 = fF8.trim();
        
        if (ht8.length !== 0 && at8.length !== 0 && rt8.length !== 0 && st8.length !== 0 && fFt8.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h8, a: a8, r: r8, s: s8,fF: fF8 },                
                success: function(result){
                    cOpl8++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl8 === 3 ){
                        pnlOplCheckA8.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar8A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones8A").append(cOpl8+". "+a8+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo8A").val("");
                    $("#accion8A").val("");
                    $("#responsable8A").val("");
                    $("#soporte8A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckA9(){
        var h9 = document.getElementById("hallazgo9A").value;
        var a9 = document.getElementById("accion9A").value;
        var r9 = document.getElementById("responsable9A").value;
        var s9 = document.getElementById("soporte9A").value;
        var fF9 = document.getElementById("fFin9A").value;
        
        var ht9 = h9.trim();
        var at9 = a9.trim();
        var rt9 = r9.trim();
        var st9 = s9.trim();
        var fFt9 = fF9.trim();
        
        if (ht9.length !== 0 && at9.length !== 0 && rt9.length !== 0 && st9.length !== 0 && fFt9.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h9, a: a9, r: r9, s: s9,fF: fF9 },                
                success: function(result){
                    cOpl9++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl9 === 3 ){
                        pnlOplCheckA9.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar9A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones9A").append(cOpl9+". "+a9+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo9A").val("");
                    $("#accion9A").val("");
                    $("#responsable9A").val("");
                    $("#soporte9A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckA10(){
        var h10 = document.getElementById("hallazgo10A").value;
        var a10 = document.getElementById("accion10A").value;
        var r10 = document.getElementById("responsable10A").value;
        var s10 = document.getElementById("soporte10A").value;
        var fF10 = document.getElementById("fFin10A").value;
        
        var ht10 = h10.trim();
        var at10 = a10.trim();
        var rt10 = r10.trim();
        var st10 = s10.trim();
        var fFt10 = fF10.trim();
        
        if (ht10.length !== 0 && at10.length !== 0 && rt10.length !== 0 && st10.length !== 0 && fFt10.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h10, a: a10, r: r10, s: s10,fF: fF10 },                
                success: function(result){
                    cOpl10++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl10 === 3 ){
                        pnlOplCheckA10.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar10A.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones10A").append(cOpl10+". "+a10+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo10A").val("");
                    $("#accion10A").val("");
                    $("#responsable10A").val("");
                    $("#soporte10A").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
        
    //FUNCIONES PARA CERRAR OPLS
    function oplCheckAC1(){
        pnlOplCheckA1.className = 'no';
    }

    function oplCheckAC2(){
        pnlOplCheckA2.className = 'no';
    }
    
    function oplCheckAC3(){
        pnlOplCheckA3.className = 'no';
    }
    
    function oplCheckAC4(){
        pnlOplCheckA4.className = 'no';
    }
    
    function oplCheckAC5(){
        pnlOplCheckA5.className = 'no';
    }
    
    function oplCheckAC6(){
        pnlOplCheckA6.className = 'no';
    }
    
    function oplCheckAC7(){
        pnlOplCheckA7.className = 'no';
    }
    
    function oplCheckAC8(){
        pnlOplCheckA8.className = 'no';
    }
    
    function oplCheckAC9(){
        pnlOplCheckA9.className = 'no';
    }
    
    function oplCheckAC10(){
        pnlOplCheckA10.className = 'no';
    }



