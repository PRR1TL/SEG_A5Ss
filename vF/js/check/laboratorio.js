/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Areli Pérez Calixto (PRR1TL)
 * 21/01/2019
 *  
 */

    $(window).on('load',function() { 
        //VALIDACIONES PARA OPL
        //1
        $("input[name=checkLAP1]").click(function () {
            var vP1 = $(this).val(); 
            if (vP1 == '1'){
                pnlOplCheckLA1.className = 'no';
            } else if (cOpl1 !== 3){
                pnlOplCheckLA1.className = 'si';                
            }            
        });
        
        //2
        $("input[name=checkLAP2]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckLA2.className = 'no';
            } else if (cOpl2 !== 3){
                pnlOplCheckLA2.className = 'si';                
            }
        });
        
        //3
        $("input[name=checkLAP3]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckLA3.className = 'no';
            } else if (cOpl3 !== 3){
                pnlOplCheckLA3.className = 'si';                
            }
        });
        
        //4
        $("input[name=checkLAP4]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckLA4.className = 'no';
            } else if (cOpl4 !== 3){
                pnlOplCheckLA4.className = 'si';                
            }
        });
        
        //5
        $("input[name=checkLAP5]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckLA5.className = 'no';
            } else if (cOpl5 !== 3){
                pnlOplCheckLA5.className = 'si';                
            }
        });
        
        //6
        $("input[name=checkLAP6]").click(function () {
            var vP6 = $(this).val(); 
            if (vP6 == '1'){
                pnlOplCheckLA6.className = 'no';
            } else if (cOpl6 !== 3){
                pnlOplCheckLA6.className = 'si';                
            }
        });
        
        //7
        $("input[name=checkLAP7]").click(function () {
            var vP7 = $(this).val(); 
            if (vP7 == '1'){
                pnlOplCheckLA7.className = 'no';
            } else if (cOpl7 !== 3){
                pnlOplCheckLA7.className = 'si';                
            }
        });
        
        //8
        $("input[name=checkLAP8]").click(function () {
            var vP8 = $(this).val(); 
            if (vP8 == '1'){
                pnlOplCheckLA8.className = 'no';
            } else if (cOpl8 !== 3){
                pnlOplCheckLA8.className = 'si';                
            }
        });
        
        //9
        $("input[name=checkLAP9]").click(function () {
            var vP9 = $(this).val(); 
            if (vP9 == '1'){
                pnlOplCheckLA9.className = 'no';
            } else if (cOpl9 !== 3){
                pnlOplCheckLA9.className = 'si';                
            }
        });
        
        //10
        $("input[name=checkLAP10]").click(function () {
            var vP10 = $(this).val(); 
            if (vP10 == '1'){
                pnlOplCheckLA10.className = 'no';
            } else if (cOpl10 !== 3){
                pnlOplCheckLA10.className = 'si';                
            }
        });
    });
    
    //FUNCIONES PARA EL PICKER
    $(window).on('load',function() { 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        
        if (dd < 10) {
            dd = '0' + dd;
        }
        
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var today = dd+"/"+mm+"/"+yyyy;
        //console.log(today);
  
        $( "#fFin1LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin2LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin3LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin4LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin5LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin6LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin7LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin8LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin9LA").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin10LA").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });        
    });
    
    function dLA1() {
        $("#mDAP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLA2() {
        $("#mDAP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLA3() {
        $("#mDAP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLA4() {
        $("#mDAP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLA5() {
        $("#mDAP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLA6() {
        $("#mDAP6").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLA7() {
        $("#mDAP7").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLA8() {
        $("#mDAP8").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLA9() {
        $("#mDAP9").modal({
            show: true,
            backdrop: false 
        });
    }
    
    function dLA10() {
        $("#mDAP10").modal({
            show: true,
            backdrop: false
        });
    }
    
    //CONTADORES PARA LOS INGRESOS DE OPL POR PREGUNTA
    var cOpl1 = 0;
    var cOpl2 = 0;
    var cOpl3 = 0;
    var cOpl4 = 0;
    var cOpl5 = 0;
    var cOpl6 = 0;
    var cOpl7 = 0;
    var cOpl8 = 0;
    var cOpl9 = 0;
    var cOpl10 = 0;
    
    //CUANDO SE HAGA CLIC EN OPL, SE JALAN LOS DATOS DE CADA OPL
    //FUNCIONES PARA PASO DE PARAMETTROS DE OPL A BD
    function oplCheckLA1(){        
        var h1 = document.getElementById("hallazgo1LA").value;
        var a1 = document.getElementById("accion1LA").value;
        var r1 = document.getElementById("responsable1LA").value;
        var s1 = document.getElementById("soporte1LA").value;
        var fF1 = document.getElementById("fFin1LA").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        var fFt1 = fF1.trim();
        
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl1++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl1 === 3 ){
                        pnlOplCheckLA1.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar1LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones1LA").append(cOpl1+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo1LA").val("");
                    $("#accion1LA").val("");
                    $("#responsable1LA").val("");
                    $("#soporte1LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLA2(){
        var h2 = document.getElementById("hallazgo2LA").value;
        var a2 = document.getElementById("accion2LA").value;
        var r2 = document.getElementById("responsable2LA").value;
        var s2 = document.getElementById("soporte2LA").value;
        var fF2 = document.getElementById("fFin2LA").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl2++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl2 === 3 ){
                        pnlOplCheckLA2.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar2LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones2LA").append(cOpl2+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo2LA").val("");
                    $("#accion2LA").val("");
                    $("#responsable2LA").val("");
                    $("#soporte2LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLA3(){
        var h3 = document.getElementById("hallazgo3LA").value;
        var a3 = document.getElementById("accion3LA").value;
        var r3 = document.getElementById("responsable3LA").value;
        var s3 = document.getElementById("soporte3LA").value;
        var fF3 = document.getElementById("fFin3LA").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl3++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl3 === 3 ){
                        pnlOplCheckLA3.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar3LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones3LA").append(cOpl3+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo3LA").val("");
                    $("#accion3LA").val("");
                    $("#responsable3LA").val("");
                    $("#soporte3LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLA4(){
        var h4 = document.getElementById("hallazgo4LA").value;
        var a4 = document.getElementById("accion4LA").value;
        var r4 = document.getElementById("responsable4LA").value;
        var s4 = document.getElementById("soporte4LA").value;
        var fF4 = document.getElementById("fFin4LA").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl4++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl4 === 3 ){
                        pnlOplCheckLA4.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar4LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones4LA").append(cOpl4+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo4LA").val("");
                    $("#accion4LA").val("");
                    $("#responsable4LA").val("");
                    $("#soporte4LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLA5(){
        var h5 = document.getElementById("hallazgo5LA").value;
        var a5 = document.getElementById("accion5LA").value;
        var r5 = document.getElementById("responsable5LA").value;
        var s5 = document.getElementById("soporte5LA").value;
        var fF5 = document.getElementById("fFin5LA").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl5++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl5 === 3 ){
                        pnlOplCheckLA5.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar5LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones5LA").append(cOpl5+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo5LA").val("");
                    $("#accion5LA").val("");
                    $("#responsable5LA").val("");
                    $("#soporte5LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLA6(){
        var h6 = document.getElementById("hallazgo6LA").value;
        var a6 = document.getElementById("accion6LA").value;
        var r6 = document.getElementById("responsable6LA").value;
        var s6 = document.getElementById("soporte6LA").value;
        var fF6 = document.getElementById("fFin6LA").value;
        
        var ht6 = h6.trim();
        var at6 = a6.trim();
        var rt6 = r6.trim();
        var st6 = s6.trim();
        var fFt6 = fF6.trim();
        
        if (ht6.length !== 0 && at6.length !== 0 && rt6.length !== 0 && st6.length !== 0 && fFt6.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h6, a: a6, r: r6, s: s6,fF: fF6 },                
                success: function(result){
                    cOpl6++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl6 === 3 ){
                        pnlOplCheckLA6.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar6LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones6LA").append(cOpl6+". "+a6+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo6LA").val("");
                    $("#accion6LA").val("");
                    $("#responsable6LA").val("");
                    $("#soporte6LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLA7(){
        var h7 = document.getElementById("hallazgo7LA").value;
        var a7 = document.getElementById("accion7LA").value;
        var r7 = document.getElementById("responsable7LA").value;
        var s7 = document.getElementById("soporte7LA").value;
        var fF7 = document.getElementById("fFin7LA").value;
        
        var ht7 = h7.trim();
        var at7 = a7.trim();
        var rt7 = r7.trim();
        var st7 = s7.trim();
        var fFt7 = fF7.trim();
        
        if (ht7.length !== 0 && at7.length !== 0 && rt7.length !== 0 && st7.length !== 0 && fFt7.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h7, a: a7, r: r7, s: s7,fF: fF7 },                
                success: function(result){
                    cOpl7++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl7 === 3 ){
                        pnlOplCheckLA7.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar7LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones7LA").append(cOpl7+". "+a7+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo7LA").val("");
                    $("#accion7LA").val("");
                    $("#responsable7LA").val("");
                    $("#soporte7LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLA8(){
        var h8 = document.getElementById("hallazgo8LA").value;
        var a8 = document.getElementById("accion8LA").value;
        var r8 = document.getElementById("responsable8LA").value;
        var s8 = document.getElementById("soporte8LA").value;
        var fF8 = document.getElementById("fFin8LA").value;
        
        var ht8 = h8.trim();
        var at8 = a8.trim();
        var rt8 = r8.trim();
        var st8 = s8.trim();
        var fFt8 = fF8.trim();
        
        if (ht8.length !== 0 && at8.length !== 0 && rt8.length !== 0 && st8.length !== 0 && fFt8.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h8, a: a8, r: r8, s: s8,fF: fF8 },                
                success: function(result){
                    cOpl8++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl8 === 3 ){
                        pnlOplCheckLA8.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar8LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones8LA").append(cOpl8+". "+a8+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo8LA").val("");
                    $("#accion8LA").val("");
                    $("#responsable8LA").val("");
                    $("#soporte8LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLA9(){
        var h9 = document.getElementById("hallazgo9LA").value;
        var a9 = document.getElementById("accion9LA").value;
        var r9 = document.getElementById("responsable9LA").value;
        var s9 = document.getElementById("soporte9LA").value;
        var fF9 = document.getElementById("fFin9LA").value;
        
        var ht9 = h9.trim();
        var at9 = a9.trim();
        var rt9 = r9.trim();
        var st9 = s9.trim();
        var fFt9 = fF9.trim();
        
        if (ht9.length !== 0 && at9.length !== 0 && rt9.length !== 0 && st9.length !== 0 && fFt9.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h9, a: a9, r: r9, s: s9,fF: fF9 },                
                success: function(result){
                    cOpl9++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl9 === 3 ){
                        pnlOplCheckLA9.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar9LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones9LA").append(cOpl9+". "+a9+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo9LA").val("");
                    $("#accion9LA").val("");
                    $("#responsable9LA").val("");
                    $("#soporte9LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLA10(){
        var h10 = document.getElementById("hallazgo10LA").value;
        var a10 = document.getElementById("accion10LA").value;
        var r10 = document.getElementById("responsable10LA").value;
        var s10 = document.getElementById("soporte10LA").value;
        var fF10 = document.getElementById("fFin10LA").value;
        
        var ht10 = h10.trim();
        var at10 = a10.trim();
        var rt10 = r10.trim();
        var st10 = s10.trim();
        var fFt10 = fF10.trim();
        
        if (ht10.length !== 0 && at10.length !== 0 && rt10.length !== 0 && st10.length !== 0 && fFt10.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h10, a: a10, r: r10, s: s10,fF: fF10 },                
                success: function(result){
                    cOpl10++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl10 === 3 ){
                        pnlOplCheckLA10.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar10LA.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones10LA").append(cOpl10+". "+a10+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo10LA").val("");
                    $("#accion10LA").val("");
                    $("#responsable10LA").val("");
                    $("#soporte10LA").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
        
    //FUNCIONES PARA CERRAR OPLS
    function oplCheckLAC1(){
        pnlOplCheckLA1.className = 'no';
    }

    function oplCheckLAC2(){
        pnlOplCheckLA2.className = 'no';
    }
    
    function oplCheckLAC3(){
        pnlOplCheckLA3.className = 'no';
    }
    
    function oplCheckLAC4(){
        pnlOplCheckLA4.className = 'no';
    }
    
    function oplCheckLAC5(){
        pnlOplCheckLA5.className = 'no';
    }
    
    function oplCheckLAC6(){
        pnlOplCheckLA6.className = 'no';
    }
    
    function oplCheckLAC7(){
        pnlOplCheckLA7.className = 'no';
    }
    
    function oplCheckLAC8(){
        pnlOplCheckLA8.className = 'no';
    }
    
    function oplCheckLAC9(){
        pnlOplCheckLA9.className = 'no';
    }
    
    function oplCheckLAC10(){
        pnlOplCheckLA10.className = 'no';
    }



