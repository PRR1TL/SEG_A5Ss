/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Areli Pérez Calixto (PRR1TL)
 * 21/01/2019
 *  
 */

    $(window).on('load',function() { 
        //VALICACIONES PARA OPL
        //1
        $("input[name=checkLAWP1]").click(function () {
            var vP1 = $(this).val(); 
            if (vP1 == '1'){
                pnlOplCheckLAW1.className = 'no';
            } else if (cOpl1 !== 3){
                pnlOplCheckLAW1.className = 'si';                
            }            
        });
        
        //2
        $("input[name=checkLAWP2]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckLAW2.className = 'no';
            } else if (cOpl2 !== 3){
                pnlOplCheckLAW2.className = 'si';                
            }
        });
        
        //3
        $("input[name=checkLAWP3]").click(function () {
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckLAW3.className = 'no';
            } else if (cOpl3 !== 3){
                pnlOplCheckLAW3.className = 'si';                
            }
        });
        
        //4
        $("input[name=checkLAWP4]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckLAW4.className = 'no';
            } else if (cOpl4 !== 3){
                pnlOplCheckLAW4.className = 'si';                
            }
        });
        
        //5
        $("input[name=checkLAWP5]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckLAW5.className = 'no';
            } else if (cOpl5 !== 3){
                pnlOplCheckLAW5.className = 'si';                
            }
        });
        
        //6
        $("input[name=checkLAWP6]").click(function () {
            var vP6 = $(this).val(); 
            if (vP6 == '1'){
                pnlOplCheckLAW6.className = 'no';
            } else if (cOpl6 !== 3){
                pnlOplCheckLAW6.className = 'si';                
            }
        });
        
        //7
        $("input[name=checkLAWP7]").click(function () {
            var vP7 = $(this).val(); 
            if (vP7 == '1'){
                pnlOplCheckLAW7.className = 'no';
            } else if (cOpl7 !== 3){
                pnlOplCheckLAW7.className = 'si';                
            }
        });
        
        //8
        $("input[name=checkLAWP8]").click(function () {
            var vP8 = $(this).val(); 
            if (vP8 == '1'){
                pnlOplCheckLAW8.className = 'no';
            } else if (cOpl8 !== 3){
                pnlOplCheckLAW8.className = 'si';                
            }
        });
        
        //9
        $("input[name=checkLAWP9]").click(function () {
            var vP9 = $(this).val(); 
            if (vP9 == '1'){
                pnlOplCheckLAW9.className = 'no';
            } else if (cOpl9 !== 3){
                pnlOplCheckLAW9.className = 'si';                
            }
        });
        
        //10
        $("input[name=checkLAWP10]").click(function () {
            var vP10 = $(this).val(); 
            if (vP10 == '1'){
                pnlOplCheckLAW10.className = 'no';
            } else if (cOpl10 !== 3){
                pnlOplCheckLAW10.className = 'si';                
            }
        });        
    });
    
    //FUNCIONES PARA EL PICKER
    $(window).on('load',function() { 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        
        if (dd < 10) {
            dd = '0' + dd;
        } 
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var today = dd+"/"+mm+"/"+yyyy;
        //console.log(today);
  
        $( "#fFin1LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin2LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin3LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin4LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin5LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin6LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin7LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin8LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin9LAW").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFin10LAW").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });        
    });
    
    function dLAW1() {
        $("#mDAP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAW2() {
        $("#mDAP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAW3() {
        $("#mDAP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAW4() {
        $("#mDAP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAW5() {
        $("#mDAP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAW6() {
        $("#mDAP6").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAW7() {
        $("#mDAP7").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAW8() {
        $("#mDAP8").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dLAW9() {
        $("#mDAP9").modal({
            show: true,
            backdrop: false 
        });
    }
    
    function dLAW10() {
        $("#mDAP10").modal({
            show: true,
            backdrop: false
        });
    }
    
    //CONTADORES PARA LOS INGRESOS DE OPL POR PREGUNTA
    var cOpl1 = 0;
    var cOpl2 = 0;
    var cOpl3 = 0;
    var cOpl4 = 0;
    var cOpl5 = 0;
    var cOpl6 = 0;
    var cOpl7 = 0;
    var cOpl8 = 0;
    var cOpl9 = 0;
    var cOpl10 = 0;
    
    //CUANDO SE HAGA CLIC EN OPL, SE JALAN LOS DATOS DE CADA OPL
    //FUNCIONES PARA PASO DE PARAMETTROS DE OPL A BD
    function oplCheckLAW1(){        
        var h1 = document.getElementById("hallazgo1LAW").value;
        var a1 = document.getElementById("accion1LAW").value;
        var r1 = document.getElementById("responsable1LAW").value;
        var s1 = document.getElementById("soporte1LAW").value;
        var fF1 = document.getElementById("fFin1LAW").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        var fFt1 = fF1.trim();
        
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fFt1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl1++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl1 === 3 ){
                        pnlOplCheckLAW1.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar1LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones1LAW").append(cOpl1+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo1LAW").val("");
                    $("#accion1LAW").val("");
                    $("#responsable1LAW").val("");
                    $("#soporte1LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLAW2(){
        var h2 = document.getElementById("hallazgo2LAW").value;
        var a2 = document.getElementById("accion2LAW").value;
        var r2 = document.getElementById("responsable2LAW").value;
        var s2 = document.getElementById("soporte2LAW").value;
        var fF2 = document.getElementById("fFin2LAW").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl2++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl2 === 3 ){
                        pnlOplCheckLAW2.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar2LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones2LAW").append(cOpl2+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo2LAW").val("");
                    $("#accion2LAW").val("");
                    $("#responsable2LAW").val("");
                    $("#soporte2LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }        
    }
    
    function oplCheckLAW3(){
        var h3 = document.getElementById("hallazgo3LAW").value;
        var a3 = document.getElementById("accion3LAW").value;
        var r3 = document.getElementById("responsable3LAW").value;
        var s3 = document.getElementById("soporte3LAW").value;
        var fF3 = document.getElementById("fFin3LAW").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl3++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl3 === 3 ){
                        pnlOplCheckLAW3.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar3LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones3LAW").append(cOpl3+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo3LAW").val("");
                    $("#accion3LAW").val("");
                    $("#responsable3LAW").val("");
                    $("#soporte3LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLAW4(){
        var h4 = document.getElementById("hallazgo4LAW").value;
        var a4 = document.getElementById("accion4LAW").value;
        var r4 = document.getElementById("responsable4LAW").value;
        var s4 = document.getElementById("soporte4LAW").value;
        var fF4 = document.getElementById("fFin4LAW").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl4++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl4 === 3 ){
                        pnlOplCheckLAW4.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar4LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones4LAW").append(cOpl4+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo4LAW").val("");
                    $("#accion4LAW").val("");
                    $("#responsable4LAW").val("");
                    $("#soporte4LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLAW5(){
        var h5 = document.getElementById("hallazgo5LAW").value;
        var a5 = document.getElementById("accion5LAW").value;
        var r5 = document.getElementById("responsable5LAW").value;
        var s5 = document.getElementById("soporte5LAW").value;
        var fF5 = document.getElementById("fFin5LAW").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl5++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl5 === 3 ){
                        pnlOplCheckLAW5.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar5LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones5LAW").append(cOpl5+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo5LAW").val("");
                    $("#accion5LAW").val("");
                    $("#responsable5LAW").val("");
                    $("#soporte5LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAW6(){
        var h6 = document.getElementById("hallazgo6LAW").value;
        var a6 = document.getElementById("accion6LAW").value;
        var r6 = document.getElementById("responsable6LAW").value;
        var s6 = document.getElementById("soporte6LAW").value;
        var fF6 = document.getElementById("fFin6LAW").value;
        
        var ht6 = h6.trim();
        var at6 = a6.trim();
        var rt6 = r6.trim();
        var st6 = s6.trim();
        var fFt6 = fF6.trim();
        
        if (ht6.length !== 0 && at6.length !== 0 && rt6.length !== 0 && st6.length !== 0 && fFt6.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h6, a: a6, r: r6, s: s6,fF: fF6 },                
                success: function(result){
                    cOpl6++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl6 === 3 ){
                        pnlOplCheckLAW6.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar6LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones6LAW").append(cOpl6+". "+a6+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo6LAW").val("");
                    $("#accion6LAW").val("");
                    $("#responsable6LAW").val("");
                    $("#soporte6LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAW7(){
        var h7 = document.getElementById("hallazgo7LAW").value;
        var a7 = document.getElementById("accion7LAW").value;
        var r7 = document.getElementById("responsable7LAW").value;
        var s7 = document.getElementById("soporte7LAW").value;
        var fF7 = document.getElementById("fFin7LAW").value;
        
        var ht7 = h7.trim();
        var at7 = a7.trim();
        var rt7 = r7.trim();
        var st7 = s7.trim();
        var fFt7 = fF7.trim();
        
        if (ht7.length !== 0 && at7.length !== 0 && rt7.length !== 0 && st7.length !== 0 && fFt7.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h7, a: a7, r: r7, s: s7,fF: fF7 },                
                success: function(result){
                    cOpl7++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl7 === 3 ){
                        pnlOplCheckLAW7.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar7LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones7LAW").append(cOpl7+". "+a7+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo7LAW").val("");
                    $("#accion7LAW").val("");
                    $("#responsable7LAW").val("");
                    $("#soporte7LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAW8(){
        var h8 = document.getElementById("hallazgo8LAW").value;
        var a8 = document.getElementById("accion8LAW").value;
        var r8 = document.getElementById("responsable8LAW").value;
        var s8 = document.getElementById("soporte8LAW").value;
        var fF8 = document.getElementById("fFin8LAW").value;
        
        var ht8 = h8.trim();
        var at8 = a8.trim();
        var rt8 = r8.trim();
        var st8 = s8.trim();
        var fFt8 = fF8.trim();
        
        if (ht8.length !== 0 && at8.length !== 0 && rt8.length !== 0 && st8.length !== 0 && fFt8.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h8, a: a8, r: r8, s: s8,fF: fF8 },                
                success: function(result){
                    cOpl8++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl8 === 3 ){
                        pnlOplCheckLAW8.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar8LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones8LAW").append(cOpl8+". "+a8+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo8LAW").val("");
                    $("#accion8LAW").val("");
                    $("#responsable8LAW").val("");
                    $("#soporte8LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
    
    function oplCheckLAW9(){
        var h9 = document.getElementById("hallazgo9LAW").value;
        var a9 = document.getElementById("accion9LAW").value;
        var r9 = document.getElementById("responsable9LAW").value;
        var s9 = document.getElementById("soporte9LAW").value;
        var fF9 = document.getElementById("fFin9LAW").value;
        
        var ht9 = h9.trim();
        var at9 = a9.trim();
        var rt9 = r9.trim();
        var st9 = s9.trim();
        var fFt9 = fF9.trim();
        
        if (ht9.length !== 0 && at9.length !== 0 && rt9.length !== 0 && st9.length !== 0 && fFt9.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h9, a: a9, r: r9, s: s9,fF: fF9 },                
                success: function(result){
                    cOpl9++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl9 === 3 ){
                        pnlOplCheckLAW9.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar9LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones9LAW").append(cOpl9+". "+a9+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo9LAW").val("");
                    $("#accion9LAW").val("");
                    $("#responsable9LAW").val("");
                    $("#soporte9LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }

    function oplCheckLAW10(){
        var h10 = document.getElementById("hallazgo10LAW").value;
        var a10 = document.getElementById("accion10LAW").value;
        var r10 = document.getElementById("responsable10LAW").value;
        var s10 = document.getElementById("soporte10LAW").value;
        var fF10 = document.getElementById("fFin10LAW").value;
        
        var ht10 = h10.trim();
        var at10 = a10.trim();
        var rt10 = r10.trim();
        var st10 = s10.trim();
        var fFt10 = fF10.trim();
        
        if (ht10.length !== 0 && at10.length !== 0 && rt10.length !== 0 && st10.length !== 0 && fFt10.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h10, a: a10, r: r10, s: s10,fF: fF10 },                
                success: function(result){
                    cOpl10++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl10 === 3 ){
                        pnlOplCheckLAW10.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelar10LAW.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAcciones10LAW").append(cOpl10+". "+a10+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo10LAW").val("");
                    $("#accion10LAW").val("");
                    $("#responsable10LAW").val("");
                    $("#soporte10LAW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
    }
        
    //FUNCIONES PARA CERRAR OPLS
    function oplCheckLAWC1(){
        pnlOplCheckLAW1.className = 'no';
    }

    function oplCheckLAWC2(){
        pnlOplCheckLAW2.className = 'no';
    }
    
    function oplCheckLAWC3(){
        pnlOplCheckLAW3.className = 'no';
    }
    
    function oplCheckLAWC4(){
        pnlOplCheckLAW4.className = 'no';
    }
    
    function oplCheckLAWC5(){
        pnlOplCheckLAW5.className = 'no';
    }
    
    function oplCheckLAWC6(){
        pnlOplCheckLAW6.className = 'no';
    }
    
    function oplCheckLAWC7(){
        pnlOplCheckLAW7.className = 'no';
    }
    
    function oplCheckLAWC8(){
        pnlOplCheckLAW8.className = 'no';
    }
    
    function oplCheckLAWC9(){
        pnlOplCheckLAW9.className = 'no';
    }
    
    function oplCheckLAWC10(){
        pnlOplCheckLAW10.className = 'no';
    }



