/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Areli Pérez Calixto (PRR1TL)
 * 21/01/2019
 *  
 */

    $(window).on('load',function() {
        //VALICACIONES PARA OPL
        //1
        $("input[name=checkOWP1]").click(function () {            
            var vP1 = $(this).val();
            console.log("checkOP1: "+cOpl1);
            
            if (vP1 == 1){
                pnlOplCheckOW1.className = 'no';
            } else if (cOpl1 !== 3){
                pnlOplCheckOW1.className = 'si';
            }            
        });

        //2
        $("input[name=checkOWP2]").click(function () {
            var vP2 = $(this).val(); 
            if (vP2 == '1'){
                pnlOplCheckOW2.className = 'no';
            } else if (cOpl2 !== 3){
                pnlOplCheckOW2.className = 'si';                
            }
        });

        //3
        $("input[name=checkOWP3]").click(function () {
            //alert("Vamos bien Arelita :)");
            var vP3 = $(this).val(); 
            if (vP3 == '1'){
                pnlOplCheckOW3.className = 'no';
            } else if (cOpl3 !== 3){
                pnlOplCheckOW3.className = 'si';                
            }
        });

        //4
        $("input[name=checkOWP4]").click(function () {
            var vP4 = $(this).val(); 
            if (vP4 == '1'){
                pnlOplCheckOW4.className = 'no';
            } else if (cOpl4 !== 3){
                pnlOplCheckOW4.className = 'si';                
            }
        });

        //5
        $("input[name=checkOWP5]").click(function () {
            var vP5 = $(this).val(); 
            if (vP5 == '1'){
                pnlOplCheckOW5.className = 'no';
            } else if (cOpl5 !== 3){
                pnlOplCheckOW5.className = 'si';                
            }
        });

        //6
        $("input[name=checkOWP6]").click(function () {
            var vP6 = $(this).val(); 
            if (vP6 == '1'){
                pnlOplCheckOW6.className = 'no';
            } else if (cOpl6 !== 3){
                pnlOplCheckOW6.className = 'si';                
            }
        });

        //7
        $("input[name=checkOWP7]").click(function () {
            var vP7 = $(this).val(); 
            if (vP7 == '1'){
                pnlOplCheckOW7.className = 'no';
            } else if (cOpl7 !== 3){
                pnlOplCheckOW7.className = 'si';                
            }
        });

        //8
        $("input[name=checkOWP8]").click(function () {
            var vP8 = $(this).val(); 
            if (vP8 == '1'){
                pnlOplCheckOW8.className = 'no';
            } else if (cOpl8 !== 3){
                pnlOplCheckOW8.className = 'si';                
            }
        });

        //9
        $("input[name=checkOWP9]").click(function () {
            var vP9 = $(this).val(); 
            if (vP9 == '1'){
                pnlOplCheckOW9.className = 'no';
            } else if (cOpl9 !== 3){
                pnlOplCheckOW9.className = 'si';                
            }
        });

        //10
        $("input[name=checkOWP10]").click(function () {
            var vP10 = $(this).val(); 
            if (vP10 == '1'){
                pnlOplCheckOW10.className = 'no';
            } else if (cOpl10 !== 3){
                pnlOplCheckOW10.className = 'si';                
            }
        }); 
    });    
    
    //FUNCIONES PARA EL PICKER
    $(window).on('load', function() {        
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        
        if (dd < 10) {
            dd = '0' + dd;
        } 
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var today = dd+"/"+mm+"/"+yyyy;
  
        $( "#fFinOW1").datepicker({ 
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW2").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW3").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW4").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW5").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW6").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW7").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW8").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW9").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        $( "#fFinOW10").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: today
        });
        
    });
    
    
    //FUNCIONES PARA LAS AYUDAS VISUALES
    function dOW1() {
        //alert("algo");
        $("#mDOP1").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dOW2() {
        $("#mDOP2").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dOW3() {
        $("#mDOP3").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dOW4() {
        $("#mDOP4").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dOW5() {
        $("#mDOP5").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dOW6() {
        $("#mDOP6").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dOW7() {
        $("#mDOP7").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dOW8() {
        $("#mDOP8").modal({
            show: true,
            backdrop: false
        });
    }
    
    function dOW9() {
        $("#mDOP9").modal({
            show: true,
            backdrop: false 
        });
    }
    
    function dOW10() {
        $("#mDOP10").modal({
            show: true,
            backdrop: false
        });
    }
    
    //CONTADORES PARA LOS INGRESOS DE OPL POR PREGUNTA
    var cOpl1 = 0;
    var cOpl2 = 0;
    var cOpl3 = 0;
    var cOpl4 = 0;
    var cOpl5 = 0;
    var cOpl6 = 0;
    var cOpl7 = 0;
    var cOpl8 = 0;
    var cOpl9 = 0;
    var cOpl10 = 0;
    
    //CUANDO SE HAGA CLIC EN OPL, SE JALAN LOS DATOS DE CADA OPL
    //FUNCIONES PARA PASO DE PARAMETTROS DE OPL A BD
    function oplCheckOW1(){        
        var h1 = document.getElementById("hallazgo1OW").value;
        var a1 = document.getElementById("accion1OW").value;
        var r1 = document.getElementById("responsable1OW").value;
        var s1 = document.getElementById("soporte1OW").value;
        var fF1 = document.getElementById("fFinOW1").value;
        
        var ht1 = h1.trim();
        var at1 = a1.trim();
        var rt1 = r1.trim();
        var st1 = s1.trim();
        //alert("ALGO PASA EN OFICINA");
        console.log("h: "+h1+" a: "+a1+" r: "+r1+" s: "+s1+" f: "+fF1);
        
        if (ht1.length !== 0 && at1.length !== 0 && rt1.length !== 0 && st1.length !== 0 && fF1.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h1, a: a1, r: r1, s: s1,fF: fF1 },                
                success: function(result){
                    cOpl1++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl1 === 3 || cOpl1 > 3){
                        pnlOplCheckOW1.className = 'no';
                    }
                        
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW1.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW1").append(cOpl1+". "+a1+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo1OW").val("");
                    $("#accion1OW").val("");
                    $("#responsable1OW").val("");
                    $("#soporte1OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("Todos los campos deben estar llenos");
        }
        event.preventDefault();
    }
    
    function oplCheckOW2(){
        var h2 = document.getElementById("hallazgo2OW").value;
        var a2 = document.getElementById("accion2OW").value;
        var r2 = document.getElementById("responsable2OW").value;
        var s2 = document.getElementById("soporte2OW").value;
        var fF2 = document.getElementById("fFinOW2").value;
        
        var ht2 = h2.trim();
        var at2 = a2.trim();
        var rt2 = r2.trim();
        var st2 = s2.trim();
        var fFt2 = fF2.trim();
        
        if (ht2.length !== 0 && at2.length !== 0 && rt2.length !== 0 && st2.length !== 0 && fFt2.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h2, a: a2, r: r2, s: s2,fF: fF2 },                
                success: function(result){
                    cOpl2++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl2 === 3 ){
                        pnlOplCheckOW2.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW2.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW2").append(cOpl2+". "+a2+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo2OW").val("");
                    $("#accion2OW").val("");
                    $("#responsable2OW").val("");
                    $("#soporte2OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }
    
    function oplCheckOW3(){
        var h3 = document.getElementById("hallazgo3OW").value;
        var a3 = document.getElementById("accion3OW").value;
        var r3 = document.getElementById("responsable3OW").value;
        var s3 = document.getElementById("soporte3OW").value;
        var fF3 = document.getElementById("fFinOW3").value;
        
        var ht3 = h3.trim();
        var at3 = a3.trim();
        var rt3 = r3.trim();
        var st3 = s3.trim();
        var fFt3 = fF3.trim();
        
        if (ht3.length !== 0 && at3.length !== 0 && rt3.length !== 0 && st3.length !== 0 && fFt3.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h3, a: a3, r: r3, s: s3,fF: fF3 },                
                success: function(result){
                    cOpl3++;
                    
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl3 === 3 || cOpl3 > 3){
                        pnlOplCheckOW3.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW3.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW3").append(cOpl3+". "+a3+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo3OW").val("");
                    $("#accion3OW").val("");
                    $("#responsable3OW").val("");
                    $("#soporte3OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }

    function oplCheckOW4(){
        var h4 = document.getElementById("hallazgo4OW").value;
        var a4 = document.getElementById("accion4OW").value;
        var r4 = document.getElementById("responsable4OW").value;
        var s4 = document.getElementById("soporte4OW").value;
        var fF4 = document.getElementById("fFinOW4").value;
        
        var ht4 = h4.trim();
        var at4 = a4.trim();
        var rt4 = r4.trim();
        var st4 = s4.trim();
        var fFt4 = fF4.trim();
        
        if (ht4.length !== 0 && at4.length !== 0 && rt4.length !== 0 && st4.length !== 0 && fFt4.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h4, a: a4, r: r4, s: s4,fF: fF4 },                
                success: function(result){
                    cOpl4++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl4 === 3 ){
                        pnlOplCheckOW4.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW4.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW4").append(cOpl4+". "+a4+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo4OW").val("");
                    $("#accion4OW").val("");
                    $("#responsable4OW").val("");
                    $("#soporte4OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }

    function oplCheckOW5(){
        var h5 = document.getElementById("hallazgo5OW").value;
        var a5 = document.getElementById("accion5OW").value;
        var r5 = document.getElementById("responsable5OW").value;
        var s5 = document.getElementById("soporte5OW").value;
        var fF5 = document.getElementById("fFinOW5").value;
        
        var ht5 = h5.trim();
        var at5 = a5.trim();
        var rt5 = r5.trim();
        var st5 = s5.trim();
        var fFt5 = fF5.trim();
        
        if (ht5.length !== 0 && at5.length !== 0 && rt5.length !== 0 && st5.length !== 0 && fFt5.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h5, a: a5, r: r5, s: s5,fF: fF5 },                
                success: function(result){
                    cOpl5++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl5 === 3 ){
                        pnlOplCheckOW5.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW5.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW5").append(cOpl5+". "+a5+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo5OW").val("");
                    $("#accion5OW").val("");
                    $("#responsable5OW").val("");
                    $("#soporte5OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }
    
    function oplCheckOW6(){
        var h6 = document.getElementById("hallazgo6OW").value;
        var a6 = document.getElementById("accion6OW").value;
        var r6 = document.getElementById("responsable6OW").value;
        var s6 = document.getElementById("soporte6OW").value;
        var fF6 = document.getElementById("fFinOW6").value;
        
        var ht6 = h6.trim();
        var at6 = a6.trim();
        var rt6 = r6.trim();
        var st6 = s6.trim();
        var fFt6 = fF6.trim();
        
        if (ht6.length !== 0 && at6.length !== 0 && rt6.length !== 0 && st6.length !== 0 && fFt6.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h6, a: a6, r: r6, s: s6,fF: fF6 },                
                success: function(result){
                    cOpl6++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl6 === 3 ){
                        pnlOplCheckOW6.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW6.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW6").append(cOpl6+". "+a6+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo6OW").val("");
                    $("#accion6OW").val("");
                    $("#responsable6OW").val("");
                    $("#soporte6OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }
    
    function oplCheckOW7(){
        var h7 = document.getElementById("hallazgo7OW").value;
        var a7 = document.getElementById("accion7OW").value;
        var r7 = document.getElementById("responsable7OW").value;
        var s7 = document.getElementById("soporte7OW").value;
        var fF7 = document.getElementById("fFinOW7").value;
        
        var ht7 = h7.trim();
        var at7 = a7.trim();
        var rt7 = r7.trim();
        var st7 = s7.trim();
        var fFt7 = fF7.trim();
        
        if (ht7.length !== 0 && at7.length !== 0 && rt7.length !== 0 && st7.length !== 0 && fFt7.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h7, a: a7, r: r7, s: s7,fF: fF7 },                
                success: function(result){
                    cOpl7++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl7 === 3 ){
                        pnlOplCheckOW7.className = 'no';
                    }
                    
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW7.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW7").append(cOpl7+". "+a7+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo7OW").val("");
                    $("#accion7OW").val("");
                    $("#responsable7OW").val("");
                    $("#soporte7OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }
    
    function oplCheckOW8(){
        var h8 = document.getElementById("hallazgo8OW").value;
        var a8 = document.getElementById("accion8OW").value;
        var r8 = document.getElementById("responsable8OW").value;
        var s8 = document.getElementById("soporte8OW").value;
        var fF8 = document.getElementById("fFinOW8").value;
        
        var ht8 = h8.trim();
        var at8 = a8.trim();
        var rt8 = r8.trim();
        var st8 = s8.trim();
        var fFt8 = fF8.trim();
        
        if (ht8.length !== 0 && at8.length !== 0 && rt8.length !== 0 && st8.length !== 0 && fFt8.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h8, a: a8, r: r8, s: s8,fF: fF8 },                
                success: function(result){
                    cOpl8++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl8 === 3 ){
                        pnlOplCheckOW8.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW8.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW8").append(cOpl8+". "+a8+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo8OW").val("");
                    $("#accion8OW").val("");
                    $("#responsable8OW").val("");
                    $("#soporte8OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }
    
    function oplCheckOW9(){
        var h9 = document.getElementById("hallazgo9OW").value;
        var a9 = document.getElementById("accion9OW").value;
        var r9 = document.getElementById("responsable9OW").value;
        var s9 = document.getElementById("soporte9OW").value;
        var fF9 = document.getElementById("fFinOW9").value;
        
        var ht9 = h9.trim();
        var at9 = a9.trim();
        var rt9 = r9.trim();
        var st9 = s9.trim();
        var fFt9 = fF9.trim();
        
        if (ht9.length !== 0 && at9.length !== 0 && rt9.length !== 0 && st9.length !== 0 && fFt9.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h9, a: a9, r: r9, s: s9,fF: fF9 },                
                success: function(result){
                    cOpl9++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl9 === 3 ){
                        pnlOplCheckOW9.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW9.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW9").append(cOpl9+". "+a9+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo9OW").val("");
                    $("#accion9OW").val("");
                    $("#responsable9OW").val("");
                    $("#soporte9OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }

    function oplCheckOW10(){
        var h10 = document.getElementById("hallazgo10OW").value;
        var a10 = document.getElementById("accion10OW").value;
        var r10 = document.getElementById("responsable10OW").value;
        var s10 = document.getElementById("soporte10OW").value;
        var fF10 = document.getElementById("fFinOW1OW").value;
        
        var ht10 = h10.trim();
        var at10 = a10.trim();
        var rt10 = r10.trim();
        var st10 = s10.trim();
        var fFt10 = fF10.trim();
        
        if (ht10.length !== 0 && at10.length !== 0 && rt10.length !== 0 && st10.length !== 0 && fFt10.length !== 0 ){
            //MANDAMOS LOS DATOS A LA BD, POR AJAX
            $.ajax({
                type: "POST",
                url: "'../../db/opl/iOPL.php",
                data: {h: h10, a: a10, r: r10, s: s10,fF: fF10 },                
                success: function(result){
                    cOpl10++;
                    //SOLO PERMITE 3 OPL POR PREGUNTA NO MÁS
                    if (cOpl10 === 3 ){
                        pnlOplCheckOW10.className = 'no';
                    }
                    //Mandamos el ajax con l ainformacion y hacemos visible el boton de cancelar
                    btnCancelarOW10.className = 'si';            
                    //AGREGAMOS LOS COMPONENTES DE LOS HALLAZGOS Y ACCIONES A REALIZAR POR CADA OPL
                    $("#descAccionesOW10").append(cOpl10+". "+a10+"<br>");  
                    //LIMPIAMOS LOS COMPONENTES DESPUES DE GUARDAR Y TENER EL REGISTRO VISIBLE
                    $("#hallazgo10OW").val("");
                    $("#accion10OW").val("");
                    $("#responsable10OW").val("");
                    $("#soporte10OW").val(""); 
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                  
        } else {
            alert("TODOS LOS CAMPOS DEBEN ESTAR LLENOS");
        }
        event.preventDefault();
    }
        
    //FUNCIONES PARA CERRAR OPLS
    function oplCheckOWC1(){
        pnlOplCheckOW1.className = 'no';
    }

    function oplCheckOWC2(){
        pnlOplCheckOW2.className = 'no';
    }
    
    function oplCheckOWC3(){
        pnlOplCheckOW3.className = 'no';
    }
    
    function oplCheckOWC4(){
        pnlOplCheckOW4.className = 'no';
    }
    
    function oplCheckOWC5(){
        pnlOplCheckOW5.className = 'no';
    }
    
    function oplCheckOWC6(){
        pnlOplCheckOW6.className = 'no';
    }
    
    function oplCheckOWC7(){
        pnlOplCheckOW7.className = 'no';
    }
    
    function oplCheckOWC8(){
        pnlOplCheckOW8.className = 'no';
    }
    
    function oplCheckOWC9(){
        pnlOplCheckOW9.className = 'no';
    }
    
    function oplCheckOWC10(){
        pnlOplCheckOW10.className = 'no';
    }



