/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * CREATED  : 29-01-2019
 * 
 */

$(document).ready(function() {
    /* FUNCIONES GENERALES */
    var tipoEv = 0;
    $('.btn-logOut').on('click', function(e){
        $('#ModalLogOut').modal({
            show: true,
            backdrop: false
        });
    });     
    
    $('.btn-login').on('click', function(){
        $('#ModalLogin').modal({
            show: true,
            backdrop: false, 
            keyboard: false
        });
    });

    //BOTONES DE LOS MODAL DE AUDITORIA    
    $('.btn-new-audit').on('click', function(){
        var datA = [];
        var tipo = "";
        $.ajax({
            type: "POST",
            url: "./db/auditoria/sDeptoCadValor.php",
            //data: parametros,                
            success: function(datos) {                
                $("#cmbDeptoAuditoriaS").empty();                
                var respuesta = datos.trim();
                var a1 = respuesta.split(",");                
                
                for (var i in a1) {
                    datA[i] = a1[i];                    
                    $("#cmbDeptoAuditoriaS").append("<option value='"+datA[i]+"' >"+datA[i]+"</option>");
                }
                $('#mSupAuditoria').modal({
                    show: true
                });                
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        //event.preventDefault();  
    });

    //EVENTOS PARA LOS BOTONES DE MODALS INFORMATIVOS DE LAS AUDITORIAS   
    //BOTON DE MODAL PARA SELECCION DE AUDITORIA
    $('#fTipoAuditoria').submit(function( event ) { 
        var datA = [];
        var tipo = "";
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/auditoria/selecAuditoria.php",
            data: parametros,                
            success: function(datos){
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                switch(res1){
                    case "0":
                        $('#NoneAuditoria').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false                                
                        });
                        break;
                    case "1":   
                        $('#mTipoAuditoria').modal('hide');
                        var a1 = respuesta.split(",");
                        for (var i in a1) {
                            datA[i] = a1[i];
                        }

                        //TRANSFROMAMOS EL VALOR DE TIPO
                        switch (datA[4]){
                            case "1":
                                tipo = "SEMANAL";
                                break;
                            case "2":
                                tipo = "MENSUAL";
                                break;
                            case "3":
                                tipo = "CONFIRMACION";
                                break;
                        }
                        //MANDAMOS LOS VALORES DE LA AUDITORIA 
                        $('#idAuditoria').val(datA[1]);//ID 
                        $('#areaAuditoria').val(datA[2]); //LINEA
                        $('#auditado').val(datA[3]); //AUDITADO
                        $('#tipo').val(tipo); //TIPO AUDITORIA
                        $('#tipoEv').val(datA[5]); //TIPO EVALUACION

                        $('#mInfoAuditoria').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false
                        });
                        break;                        
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(iLine: Line 161)");
                        break;                        
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    }); 
    
    //BOTON DE MODAL INFORMATIVO DE AUDITORIA
    $('#fInfoAuditoria').submit(function( event ) { 
        var datA = [];
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/auditoria/sessionID.php",
            data: parametros,                
            success: function(datos) {
                var respuesta = datos.trim();
                var res1 = respuesta.substr(0,1);
                var a1 = respuesta.split(",");
                for (var i in a1) {
                    datA[i] = a1[i].trim();
                } 
                tipoEv = datA[4];

                switch(res1){
                    case "0":
                        alert("NO SE TIENE ID \n Error(supervisor.js: Line 166)");
                        break;
                    case "1":  
                        $('#mInfoAuditoria').modal('hide');
                        $('#ModalRules').modal({
                            show: true,
                            backdrop: false, 
                            keyboard: false
                        });
                        break; 
                    default :
                        alert("ALGO SALIO MAL, HACER CAPTURA DE PANTALLA \n Error(supervisor.js: Line 177)");
                        break;                        
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });
    
    //        AJAX PARA MANDAR A BD
    $('#fAreaAuditAdmin').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/admin/auditoriaVSE.php",
            data: parametros,          
            success: function(datos){
                var res = datos.trim();
                if (res === "Bien"){
                    //$('#mAreaAuditAdmin').modal('hide');
                    $('#mConfirmacion').modal({
                        show: true,
                        backdrop: false, 
                        keyboard: false
                    });
                } else {
                    $("#msg_AuditVSE").html(datos);
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });    
    
    $('#fSupAuditoria').submit(function( event ) { 
        
        var depto = document.getElementById("cmbDeptoAuditoriaS").value;
        var elem = document.getElementById("usuarioSVSE");
        //document.getElementById("usuarioSVSE").value
        var auditado = 0;
        
        if(typeof elem !== 'undefined' && elem !== null) {
            var auditado = document.getElementById("usuarioSVSE").value;
            var datA = [];    
            
            auditado = document.getElementById("usuarioSVSE").value ;
            console.log("!null");
            
            $.ajax({
                type: "POST",
                url: "./db/progAuditoria/iAuditSup.php",
                data: {tipoAudit: 4, area: depto, auditado: auditado },          
                success: function(datos) { 
                    var respuesta = datos.trim();
                    var a1 = respuesta.split(",");                

                    for (var i in a1) {
                        datA[i] = a1[i]; 
                    }

                    if (datA[0] === "Bien") { 
                        $('#mSupAuditoria').modal('hide');
                        tipoEv = datA[1]; 
                        $('#ModalRules').modal({ 
                            show: true, 
                            backdrop: false, 
                            keyboard: false 
                        });
                    } else {
                        $("#alertISup").html(datos);
                    }
                    //MANTENEMOS EL MODAL SIN HACER NADA 
                    event.preventDefault();
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });
            
        } else {
            auditado = 0;
            $("#alertISup").html("<div class='alert alert-danger' role='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>SE DEBE SELECCIONAS UNA AREA/LINEA<BR> </strong></div>");
            //console.log("null");
        }
        event.preventDefault();
    });    
    
    $('.btn-rules').on('click', function(){   
        $('#ModalRules').modal('hide');
        //EVALUAMOS EL TIPO DE EVALUACION
        console.log("tipoEv: "+tipoEv);        
        switch (tipoEv){
            case '1': //ALMACEN
                $('#mAConfirm').modal({                            
                    show: true,
                    backdrop: false, 
                    keyboard: false
                });
                break;
                
            case '2': //LABORATORIO
                $('#mLabConfirm').modal({                            
                    show: true,
                    backdrop: false,  
                    keyboard: false
                });
                break;
                
            case '3': //LINEA
                $('#mLConfirm').modal({                            
                    show: true,
                    backdrop: false,  
                    keyboard: false
                }); 
                break;
                
            case '4': //MANTENIMIENTO
                $('#mMConfirm').modal({                            
                    show: true,
                    backdrop: false,  
                    keyboard: false
                });                        
                break;
                
            case '5': //OFICINA
                $('#mOConfirm').modal({
                    show: true,
                    backdrop: false, 
                    keyboard: false
                });                        
                break;
        }
        //event.preventDefault();
    });
    
    
    //FUNCIONES PARA LA CABECERA LOS TOOLTIP    
    $('.btn-help').on('click', function(){
        $('#ModalHelp').modal({
            show: true,
            backdrop: false, 
            keyboard: false
        });
    });
    
    //FUNCION PARA EL LOGIN
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./db/cLogin.php",
            data: parametros,
            success: function(datos){
                //alert ("echo");
                $("#datos_ajax").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });       
    
    $( "#logOut" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./modals/accesos/exit.php",
            data: parametros,
            success: function(datos){
                $("#datos_ajax").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });   
    
    //OPL
    $('#mUOpl').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var idOpl = button.data('id'); // Extraer la información de atributos de datos     
        var fInicio = button.data('finicio'); // Extraer la información de atributos de datos
        var fCompromiso = button.data('fcompromiso'); // Extraer la información de atributos de datos
        var hallazgo = button.data('hallazgo'); // Extraer la información de atributos de datos
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var responsable = button.data('resp'); // Extraer la información de atributos de datos
        var soporte = button.data('sop'); // Extraer la información de atributos de datos
        var estado = button.data('estado'); // Extraer la información de atributos de datos

        console.log("idOpl: "+idOpl+", fInicio: "+fInicio+", fCompromiso: "+fCompromiso+", hallazgo: "+hallazgo+", accion: "+accion+", responsable: "+responsable+", soporte: "+soporte+", estado: "+estado);

        var modal = $(this);
        modal.find('.modal-body #idOpl').val(idOpl);
        modal.find('.modal-body #fInicio').val(fInicio);
        modal.find('.modal-body #fCompromiso').val(fCompromiso);
        modal.find('.modal-body #hallazgo').val(hallazgo);
        modal.find('.modal-body #accion').val(accion);
        modal.find('.modal-body #responsable').val(responsable);
        modal.find('.modal-body #soporte').val(soporte);
        modal.find('.modal-body #estado').val(estado);

       // $('.alert').hide();//Oculto alert
    }); 
    
    $('#mDOpl').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var idOpl = button.data('id'); // Extraer la información de atributos de datos
        var hallazgo = button.data('hallazgo'); // Extraer la información de atributos de datos
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var responsable = button.data('resp'); // Extraer la información de atributos de datos

        console.log("idOpl: "+idOpl+", hallazgo: "+hallazgo+", accion: "+accion+", responsable: "+responsable);

        var modal = $(this); 
        modal.find('.modal-body #idD').val(idOpl); 
        modal.find('.modal-body #hallazgoD').val('<b>HALLAZGO: </b>'+hallazgo); 
        modal.find('.modal-body #accionD').val('<b>ACCION: </b>'+accion); 
        modal.find('.modal-body #respD').val('<b>RESPONSABLE: </b>'+responsable); 
    }); 
    
    $( "#fUOpl" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "db/opl/uOpl.php",
            data: parametros,
            beforeSend: function(objeto){
                $("#datos_ajax").html("Mensaje: Cargando...");
            },
            success: function(datos){
                $("#datos_ajax").html(datos);
                loadOplA();
            }
        });
        event.preventDefault();
    });
    
    function loadOplA(){
        location.href="./pAbiertos.php";
        location.reload(true);
    } 
});

function index() {
    location.href = "./index.php";
}

function s_depto() {
    var depto = document.getElementById("cmbDeptoAuditoriaS").value;
    $.ajax({
        type: "POST",
        url: "./db/admin/usuariosDepto.php",
        data: {depto: depto}, 
        success: function(datos) {
            $("#pnlAuditadoS").html(datos); 
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
    event.preventDefault();
}


function areaVSE(){
    var area = document.getElementById("areaSVSE").value;
    $.ajax({
        type: "POST",
        url: "./db/admin/deptos.php",
        data: {area: area},          
        success: function(datos){
            $("#pnlDepto").html(datos);            
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
    event.preventDefault();
}

function deptoVSE(){
    var depto = document.getElementById("deptoSVSE").value;
    $.ajax({
        type: "POST",
        url: "./db/admin/usuariosDepto.php",
        data: {depto: depto},          
        success: function(datos){
            $("#pnlUsuario").html(datos);            
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
    event.preventDefault();
}

function audit() {    
  
}

function report() {
    location.href = "report.php";
}

function calendar() {
    location.href = "calendar.php";
}

function info() {
    location.href = "info.php";
}

function opl() {
    location.href = "opl.php";
}

function fPrivilegio() {        
    var x = document.getElementById("privilegio").value;        
    switch(x){
        case '1': //ADMINISTRADOR
            pnlAdmin.className = 'si';
            pnlSup.className = 'no';
            pnlChamp.className = 'no'; 
            datosGeneralesI.className = 'si';
            break;                
        case '2'://SUP / GERENTES
            pnlAdmin.className = 'no';
            pnlSup.className = 'si';
            pnlChamp.className = 'no'; 
            datosGeneralesI.className = 'si';
            break;
        case '3': //CHAMPION
            pnlAdmin.className = 'no';
            pnlSup.className = 'no';
            pnlChamp.className = 'si';
            datosGeneralesI.className = 'si';
            break;
    }        
}
    
function fCValorSIU(){
    var id = document.getElementById('areaSIU').value;
    var dataString = 'action='+ id;
    pnlDeptoIU.className = 'no';

    $.ajax({
        type: "post",
        url: './db/admin/cadValor.php',
        data: dataString,
        cache: false,
        success: function(r){
            $("#cadValorIU").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}    

function fCValorS(){
    var id = document.getElementById('areaS').value;
    var dataString = 'action='+ id;
    pnlDepto.className = 'no';

    $.ajax({
        type: "post",
        url: './db/admin/cadValor.php',
        data: dataString,
        cache: false,
        success: function(r){
            $("#cadValor").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function fDeptosIU(){
    var id = document.getElementById('cadValorIU').value;
    var area = document.getElementById('areaSIU').value;
    var dataString = 'area='+area;
    
    if (id === 'N/A' || id === '-' ) {
        pnlDeptoIU.className = 'si';
        $("#deptoSIU").empty();
        $.ajax({
            type: "post",
            url: './db/admin/depSinCValor.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#deptoSIU").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
    } else {
        pnlDeptoIU.className = 'no';            
    }
}

function fDeptosUU() {
    var id = document.getElementById('cadValorSU').value;
    var area = document.getElementById('areaSU').value;
    var dataString = 'area='+area;
    
    //alert("Areliuts id:"+id+" area: "+area);
    if (id === 'N/A' || id === '-' ) {
        pnlDeptoUU.className = 'si';
        $("#deptoSUU").empty();
        $.ajax({
            type: "post",
            url: './db/admin/depSinCValor.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#deptoSUU").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
    } else {
        pnlDeptoUU.className = 'no';            
    }
}

function fDeptos(){
    var id = document.getElementById('cadValor').value;
    var area = document.getElementById('areaS').value;
    var dataString = 'cValor='+ id+', '+area;
    //alert("Vamos bien :)");
    if (id === 'N/A'){
        pnlDepto.className = 'si';
        alert("Vamos bien :)");
        $("#deptoS").empty();
        $.ajax({
            url: './db/admin/deptos.php',
            data: dataString,
            cache: false,
            success: function(r){
                $("#deptoS").html(r);
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
    } else {
        pnlDepto.className = 'no';            
    }
}

function fDeptosCh(){        
    var area = document.getElementById('areaCh').value;
    var dataString = 'cValor='+area;        
    $("#deptoCh").empty();
    $.ajax({
        url: './db/admin/deptos.php',
        data: dataString,
        cache: false,
        success: function(r){
            $("#deptoCh").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function r_areaU (){
    var anio = document.getElementById("anioAU").value;
    var mes = document.getElementById("mesAU").value;
    var area = document.getElementById("areaU").value;
    $.ajax({
        url: "contenedores/rArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contGeneralU").html("");
            $("#hGeneralU").hide();
            $("#hAreaU").show();
            jQuery("#contAreaU").html(resultA);
        }
    });
}

function r_generalU(){    
    var anio = document.getElementById("anioGU").value;
    var mes = document.getElementById("mesGU").value;         
    $.ajax({
        url: "contenedores/rGeneral.php",
        type: "post",
        data: { a: anio, m: mes},
        success: function (resultG) {
            jQuery("#contAreaU").html("");
            $("#hAreaU").hide();
            $("#hGeneralU").show();
            jQuery("#contGeneralU").html(resultG);
        }
    });
}

function fPrivilegioU() {        
    var x = document.getElementById("privilegioU").value;        
    switch(x){
        case '1': //ADMINISTRADOR
            pnlAdminU.className = 'si'; 
            pnlSupU.className = 'no'; 
            pnlChampU.className = 'no';
            pnlDatosG.className = 'si';
            break;  
        
        case '2'://SUP / GERENTES
            pnlAdminU.className = 'no';
            pnlSupU.className = 'si';
            pnlChampU.className = 'no';      
            pnlDatosG.className = 'si';
            break;
            
        case '3': //CHAMPION
            pnlAdminU.className = 'no';
            pnlSupU.className = 'no';
            pnlChampU.className = 'si';
            pnlDatosG.className = 'si';
            break;
    }        
}
    
function fCValorSU(){
    var id = document.getElementById('areaSU').value;
    var dataString = 'action='+ id;
    $.ajax({
        url: './db/admin/cadValor.php',
        data: dataString,
        cache: false,
        success: function(r) { 
            $("#cadValorSU").html(r);            
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function fDeptosChU(){        
    var area = document.getElementById('areaChU').value;
    var dataString = 'area='+area;        
    $("#deptoChU").empty();
    $.ajax({
        type: "POST",
        url: './db/admin/deptos.php',
        data: dataString,
        cache: false,
        success: function(r){
            $("#deptoChU").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function fCValorDepto(){
    var id = document.getElementById('areaDep').value;
    var dataString = 'action='+ id;
    $.ajax({
        type: "POST",
        url: './db/admin/cadValorArea.php',
        data: dataString,
        cache: false,
        success: function(r) { 
            $("#cadValorDep").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function fCValorDeptoU(){
    var id = document.getElementById('areaDepU').value;
    var dataString = 'action='+ id;
    $.ajax({
        type: "POST",
        url: './db/admin/cadValorArea.php',
        data: dataString,
        cache: false,
        success: function(r) { 
            $("#cadValorDepU").html(r);
        } 
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        if (jqXHR.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + jqXHR.responseText);
        }
    });
}

function r_area (){
    var anio = document.getElementById("anioA").value;
    var mes = document.getElementById("mesA").value;
    var area = document.getElementById("area").value;
    $.ajax({
        url: "contenedores/rArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contGeneral").html("");
//            jQuery("#hGeneral").html("");
            $("#hGeneral").hide();
            $("#hArea").show();
            jQuery("#contArea").html(resultA);
        }
    });
}

function r_general(){    
    var anio = document.getElementById("anioG").value;
    var mes = document.getElementById("mesG").value;    
        
    $.ajax({
        url: "contenedores/rGeneral.php",
        type: "post",
        data: { a: anio, m: mes},
        success: function (resultG) {
            jQuery("#contArea").html("");
            $("#hArea").hide();
            $("#hGeneral").show();
            jQuery("#contGeneral").html(resultG);
        }
    });
}

function aOpl_area (){
    var anio = document.getElementById("anioA").value;
    var mes = document.getElementById("mesA").value;
    var area = document.getElementById("area").value;
    $.ajax({
        url: "contenedores/opl/aArea.php",
        type: "post",
        data: { a: anio, m: mes, area: area },
        success: function (resultA) {
            jQuery("#contGeneral").html("");
            $("#hGeneral").hide();
            $("#hArea").show();
            jQuery("#contArea").html(resultA);
        }
    });
}

function aOpl_general(){    
    var anio = document.getElementById("anioG").value;
    var mes = document.getElementById("mesG").value;    
        
    $.ajax({
        url: "contenedores/opl/aGeneral.php",
        type: "post",
        data: { a: anio, m: mes},
        success: function (resultG) {
            jQuery("#contArea").html("");
            $("#hArea").hide();
            $("#hGeneral").show();
            jQuery("#contGeneral").html(resultG);
        }
    });
}


