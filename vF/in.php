<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    session_start(); 
    date_default_timezone_set("America/Mexico_City"); 
    $_SESSION['path'] = $_SERVER['PHP_SELF']; 
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])) { 
        $userName = $_SESSION['userName']; 
        $name = $_SESSION['name']; 
        $typeUser = $_SESSION['tipo']; 
        $typeEv = $_SESSION['ev']; 
    } else { 
        $userName = ''; 
        $name = 'INICIAR SESION'; 
        $typeUser = 0; 
        $typeEv = 0; 
    } 

?>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 

    <!-- LIBRERIAS DE BOOSTRAP --> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
    
    <!-- LIBRERIAS DE JS --> 
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" > 
    <link rel="stylesheet" href="../css/style.css" > 
    <script src="../js/modernizr.js" ></script> 
    <script src="../js/bootstrap.min.js" ></script> 
    <script src="../js/jquery.mCustomScrollbar.concat.min.js" ></script> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" > 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    
    <!-- ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS --> 
    <?php 
        //ECHO $typeUser ;
        switch ($typeUser) { 
            case 0: //SOLO VISUALIZACION
            ?>
                <script src="js/index.js"> </script> 
            <?php 
                break; 
            case 1: //ADMINISTRADOR
            ?> 
                <script src="js/admin.js"> </script> 
            <?php 
                break; 
            case 2: //PM 
            ?> 
                <script src="js/supervisor.js"> </script> 
            <?php 
                break; 
            case 3: //HOD 
            ?> 
                <script src="js/supervisor.js"> </script> 
            <?php 
                break; 
            case 4: // SUPERVISOR / GERENTE
            ?> 
                <script src="js/supervisor.js"> </script> 
            <?php 
                break;
            case 5: // ING PROCESOS
            ?> 
                <script src="js/supervisor.js"> </script> 
            <?php 
                break; 
            case 6: //CHAMPIONS
            ?> 
                <script src="js/champion.js"> </script> 
            <?php 
                break; 
        } ?> 
    
    <script src="js/check/almacen.js"> </script>
    <script src="js/check/laboratorio.js"> </script>    
    <script src="js/check/linea.js"> </script>
    <script src="js/check/mantenimiento.js"> </script>
    <script src="js/check/oficina.js"> </script>
    
    <script src="js/weekly/almacen.js"> </script>
    <script src="js/weekly/laboratorio.js"> </script>    
    <script src="js/weekly/linea.js"> </script>
    <script src="js/weekly/mantenimiento.js"> </script>
    <script src="js/weekly/oficina.js"> </script>
    
    <script src="js/monthly/almacen.js"> </script>
    <script src="js/monthly/laboratorio.js"> </script>    
    <script src="js/monthly/linea.js"> </script>
    <script src="js/monthly/mantenimiento.js"> </script>
    <script src="js/monthly/oficina.js"> </script>
    
    <link rel="stylesheet" href="css/index.css">
<head>
    <?php 
    
        include './modals/informativos/help.php';
        
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MOD AL INFORMATIVOS
        include './modals/accesos/login.php';
        include './modals/accesos/cerrarSesion.php';
        include './modals/informativos/rules.php';
        
        //MODALS PARA LA SELECCION DE AUDITORIAS
        //DE ACUERDO A LOS PRIVILEGIOS DE USUARIOS
        include './modals/vse/AreaAudit.php';
        switch ($typeUser) {
            case 1:
                include './modals/vse/AreaAudit.php'; 
                include './modals/vse/DeptoAudit.php'; 
                include './modals/vse/mAuditCon.php'; 
                break;
            
            case 2: 
                include './modals/supervisor/DeptoAudit.php'; 
                break;
            
            case 6:                
                include './modals/informativos/TipoAuditoriaCh.php';
                break;
        }
        
        include './modals/accesos/configU.php';
        
        include './modals/informativos/InfoAuditoria.php';
        include './modals/informativos/NoneAuditoria.php';        
        
        include './modals/auditoria/daily/checkAlmacen.php';
        include './modals/auditoria/daily/checkLab.php';
        include './modals/auditoria/daily/checkLinea.php';
        include './modals/auditoria/daily/checkMantenimiento.php';
        include './modals/auditoria/daily/checkOficina.php';
        
        include './modals/auditoria/monthly/almacen.php';
        include './modals/auditoria/monthly/almacen2.php';
        include './modals/auditoria/monthly/laboratorio.php';
        include './modals/auditoria/monthly/laboratorio2.php';
        include './modals/auditoria/monthly/linea.php';
        include './modals/auditoria/monthly/linea2.php';
        include './modals/auditoria/monthly/mantenimiento.php';
        include './modals/auditoria/monthly/mantenimiento2.php';
        include './modals/auditoria/monthly/oficina.php';
        
        include './modals/auditoria/weekly/almacen.php';
        include './modals/auditoria/weekly/laboratorio.php';
        include './modals/auditoria/weekly/linea.php';
        include './modals/auditoria/weekly/mantenimiento.php';
        include './modals/auditoria/weekly/oficina.php';
        
        //CONFIRMACIONALES
        include './modals/auditoria/confirmacion/almacen.php'; 
        include './modals/auditoria/confirmacion/laboratorio.php'; 
        include './modals/auditoria/confirmacion/linea.php'; 
        include './modals/auditoria/confirmacion/mantenimiento.php'; 
        include './modals/auditoria/confirmacion/oficina.php'; 
        
        //AYUDAS VISUALES DE PREGUNTAS DE DAILY
        //ALMACEN
        include './modals/informativos/dAlmacen/mDP1.php';
        include './modals/informativos/dAlmacen/mDP2.php';
        include './modals/informativos/dAlmacen/mDP3.php';
        include './modals/informativos/dAlmacen/mDP4.php';
        include './modals/informativos/dAlmacen/mDP5.php';
        include './modals/informativos/dAlmacen/mDP6.php';
        include './modals/informativos/dAlmacen/mDP7.php';
        include './modals/informativos/dAlmacen/mDP8.php';
        include './modals/informativos/dAlmacen/mDP9.php';
        include './modals/informativos/dAlmacen/mDP10.php';
        
        //LABORATORIO
        include './modals/informativos/dLaboratorio/mDP1.php';
        include './modals/informativos/dLaboratorio/mDP2.php';
        include './modals/informativos/dLaboratorio/mDP3.php';
        include './modals/informativos/dLaboratorio/mDP4.php';
        include './modals/informativos/dLaboratorio/mDP5.php';
        include './modals/informativos/dLaboratorio/mDP6.php';
        include './modals/informativos/dLaboratorio/mDP7.php';
        include './modals/informativos/dLaboratorio/mDP8.php';
        include './modals/informativos/dLaboratorio/mDP9.php';
        include './modals/informativos/dLaboratorio/mDP10.php';
        
        //LINEA
        include './modals/informativos/dLinea/mDP1.php';
        include './modals/informativos/dLinea/mDP2.php';
        include './modals/informativos/dLinea/mDP3.php';
        include './modals/informativos/dLinea/mDP4.php';
        include './modals/informativos/dLinea/mDP5.php';
        include './modals/informativos/dLinea/mDP6.php';
        include './modals/informativos/dLinea/mDP7.php';
        include './modals/informativos/dLinea/mDP8.php';
        include './modals/informativos/dLinea/mDP9.php';
        include './modals/informativos/dLinea/mDP10.php';
        
        //MANTENIMIENTO
        include './modals/informativos/dMantenimiento/mDP1.php'; 
        include './modals/informativos/dMantenimiento/mDP2.php'; 
        include './modals/informativos/dMantenimiento/mDP3.php'; 
        include './modals/informativos/dMantenimiento/mDP4.php'; 
        include './modals/informativos/dMantenimiento/mDP5.php'; 
        include './modals/informativos/dMantenimiento/mDP6.php'; 
        include './modals/informativos/dMantenimiento/mDP7.php'; 
        include './modals/informativos/dMantenimiento/mDP8.php';
        include './modals/informativos/dMantenimiento/mDP9.php';
        include './modals/informativos/dMantenimiento/mDP10.php';
        
        //OFICINA
        include './modals/informativos/dOficina/mDP1.php'; 
        include './modals/informativos/dOficina/mDP2.php'; 
        include './modals/informativos/dOficina/mDP3.php'; 
        include './modals/informativos/dOficina/mDP4.php'; 
        include './modals/informativos/dOficina/mDP5.php'; 
        include './modals/informativos/dOficina/mDP6.php'; 
        include './modals/informativos/dOficina/mDP7.php'; 
        include './modals/informativos/dOficina/mDP8.php'; 
        include './modals/informativos/dOficina/mDP9.php'; 
        include './modals/informativos/dOficina/mDP10.php'; 
                
        //MODAL DE PUNTAJE
        include './modals/informativos/PuntosAuditoriaB.php';
        include './modals/informativos/PuntosAuditoriaR.php';
        include './modals/informativos/PuntosAuditoriaM.php';
        
        //OPL 
        include './modals/opl/mIOPL.php'; 
    ?>    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align = center id="headerFixed" class="contenedor">
        <div class='fila0'>
            <img src="imagenes/logo.jpg" >
        </div>
        <h3 class="tituloPortal" >
            PORTAL 5S's
        </h3 >
        <div class="fila1" >
            <?php if ($name != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" onclick="logiin()" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 
</head> 

<body style="overflow-y: hidden;" > 
    <div aling = "center" id = "pnlHoja" > 
        <div id = "menuPrincipal" > 
            <?php if ($typeUser > 0) { ?> 
            <button type="button" id="miUser" class="btn glyphicon btn-miU" style="margin-left: 96%; margin-top: -5%" onclick="miUser()" > <img src="imagenes/adjust.png"> </button>
            <?php } ?>
            <?php if ($typeUser == 0) { ?> 
                <div id = "wrap" class = "row" > 
                    <div class="contenidoCentrado" onclick="report()" > 
                        <a href="report.php" >
                            <img class="card-img-top" src="imagenes/report.png" style="width: 320px; height: 200px" >
                        </a> 
                        <br><br> 
                        <h4> 
                            <a href="report.php" >
                                <h1>Reportes</h1>
                            </a> 
                        </h4> 
                    </div> 
                    <div class="contenidoCentrado" onclick="calendar()" >
                        <a href="calendar.php" >
                            <img class="card-img-top" src="imagenes/calendario.png" style="width: 350px; height: 200px" >
                        </a>
                        <br><br>
                        <h4>
                            <a href="calendar.php" ><h1>Calendario</h1></a>
                        </h4>
                    </div>
                    <div class="contenidoCentrado" onclick="info()" >
                        <a href="info.php" ><img class="card-img-top" src="imagenes/inf.png" style="width: 180px; height: 200px" ></a>
                        <br><br>
                        <h4>
                            <a href="info.php" >
                                <h1>Información</h1>
                            </a>
                        </h4> 
                    </div> 
                </div>
                <br><br><br>
                <div id="wrap" class="row">                    
                    <div class="contenidoCentrado" onclick="opl()" > 
                        <a href="opl.php" > 
                            <img class="card-img-top" src="imagenes/opl.png" style="width: 200px; height: 150px" > 
                        </a> 
                        <br><br> 
                        <h4> 
                            <a href="opl.php" ><h1>OPL</h1></a> 
                        </h4> 
                    </div>
                </div>
            <?php } else if ($typeUser == 1){?>
                <div id = "wrap" class = "row" >
                    <div class="contenidoCentrado" onclick="admin()" >
                        <a href="admin.php" ><img class="card-img-top" src="imagenes/admin.png" style="width: 350px; height: 200px" ></a>                    
                        <br><br> 
                        <h4>
                            <a href="admin.php" ><h1>Administración</h1></a>
                        </h4> 
                    </div>
                    <div class="contenidoCentrado btn-new-audit" onclick="audit()"  >
                        <a class="btn-new-audit" ><img class="card-img-top" src="imagenes/audit.png" style="width: 350px; height: 200px" ></a>
                        <br><br>                    
                        <h4>
                            <a class="btn-new-audit" ><h1>Nueva Auditoria</h1></a>
                        </h4> 
                    </div>
                    <div class="contenidoCentrado" onclick="report()" >
                        <a href="report.php" ><img class="card-img-top" src="imagenes/report.png" style="width: 350px; height: 200px" ></a>
                        <br><br> 
                        <h4>
                            <a href="report.php" ><h1>Reportes</h1></a>
                        </h4> 
                    </div>
                </div>            
                <br><br><br>
                <div id="wrap" class="row">
                    <div class="contenidoCentrado" onclick="calendar()" >
                        <a href="calendar.php" ><img class="card-img-top" src="imagenes/calendario.png" style="width: 350px; height: 200px" ></a>
                        <br><br> 
                        <h4> 
                            <a href="calendar.php" > <h1>Calendario</h1> </a> 
                        </h4> 
                    </div>
                    <div class="contenidoCentrado" onclick="info()" >
                        <a href="info.php" ><img class="card-img-top" src="imagenes/inf.png" style="width: 180px; height: 150px" ></a>
                        <br><br>                    
                        <h4>
                            <a href="info.php" ><h1>Información</h1></a>
                        </h4> 
                    </div>
                    <div class="contenidoCentrado" onclick="opl()" >
                        <a href="opl.php" ><img class="card-img-top" src="imagenes/opl.png" style="width: 200px; height: 150px" ></a>
                        <br><br>                    
                        <h4>
                            <a href="opl.php" ><h1>OPL</h1></a>
                        </h4> 
                    </div>
                </div>
            <?php } else{ ?>
                <div id = "wrap" class = "row" >                    
                    <div class="contenidoCentrado btn-new-audit" onclick="audit()"  >
                        <a class="btn-new-audit" ><img class="card-img-top" src="imagenes/audit.png" style="width: 350px; height: 200px" ></a>
                        <br><br>                    
                        <h4>
                            <a class="btn-new-audit"> <h1> Nueva Auditoria </h1> </a>
                        </h4> 
                    </div>
                    <div class="contenidoCentrado" onclick="report()" >
                        <a href="rDepto.php" ><img class="card-img-top" src="imagenes/report.png" style="width: 350px; height: 200px" ></a>
                        <br><br> 
                        <h4> 
                            <a href="rDepto.php"> <h1> Reportes <h1> </a> 
                        </h4> 
                    </div> 
                    <div class="contenidoCentrado" onclick="calendar()" >
                        <a href="calendar.php" ><img class="card-img-top" src="imagenes/calendario.png" style="width: 350px; height: 200px" ></a>
                        <br><br>                   
                        <h4>
                            <a href="calendar.php" > <h1> Calendario </h1></a> 
                        </h4> 
                    </div> 
                </div> 
                <br><br><br>
                <div id="wrap" class="row"> 
                    <div class="contenidoCentrado" onclick="info()" > 
                        <a href="info.php" ><img class="card-img-top" src="imagenes/inf.png" style="width: 180px; height: 150px" ></a> 
                        <br><br> 
                        <h4> 
                            <a href="info.php" > <h1> Información </h1> </a> 
                        </h4> 
                    </div> 
                    <div class="contenidoCentrado" > 
                        <a href="lOpl.php" ><img class="card-img-top" src="imagenes/opl.png" style="width: 200px; height: 150px" ></a> 
                        <br><br> 
                        <h4>
                            <a href="lOpl.php" > <h1> OPL </h1> </a>
                        </h4> 
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
</body>
