<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //CONFIGURACION PARA MAILS 
    require("../../smtp/class.phpmailer.php");
    $mail = new PHPMailer();

    $mail->isSMTP();
    $mail->SMTPDebug = 2;
    $mail->Debugoutput = 'html';
    
    //CONFIGURACION DE HOST DE SEG
    $mail->Host = "smtp.sg.lan";
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->SMTPSecure = false;
    $mail->setFrom("no-reply@seg-automotive.com", "Portal auditoria 5S's");

    session_start();
    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];
    $typeEv = $_SESSION["tipoEv"]; 
    $typeAuditoria = $_SESSION["tipoAuditoria"];
    $puntos = $_SESSION["puntosAM1"];
    
    $today = date('Y-m-d');
    $w = date('W');
    $m = date('m');
    $y = date('Y');
    
    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    $query = "SELECT id FROM a_puntajeAuditoria WHERE id = '$id';";
    $result = sqlsrv_query($conn,$query);

    if ($result === false ){
        echo "error <br>";
    } else {
        //CONSULTAMOS SI ESXISTES REGISTROS PARA ESA AUDITORIA
        if ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
            $countRegAuditoria = 1 ; //contador de registros que trae despues de la consulta        
        } else {
            $countRegAuditoria = 0;
        }
    }
    
    //PRIMER
    if (isset($_POST["checkAMP11"]) && isset($_POST["checkAMP12"]) && isset($_POST["checkAMP13"]) &&
        isset($_POST["checkAMP14"]) && isset($_POST["checkAMP15"]) ){
        
        $p11 = $_POST["checkAMP11"];
        $p12 = $_POST["checkAMP12"];
        $p13 = $_POST["checkAMP13"];
        $p14 = $_POST["checkAMP14"];
        $p15 = $_POST["checkAMP15"];
        
        $puntos += $p11+$p12+$p13+$p14+$p15;
        
        //CONSULTA OPL
        $query = "SELECT hallazgo FROM a_opl WHERE idAud = '$id' ;";
        $result = sqlsrv_query($conn, $query);
        $cont = 0;
        $mensaje2 = "<br>";
        
        while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
            $cont++;
            $fCompromiso = $row['fCompromiso']->format('d/m/Y');
            $hallazgo = $row['hallazgo'];
            $accion = $row['accion'];
            $responsable = $row['responsable'];
            
            if ($cont > 1){
                $mensaje2 = $mensaje2."<tr>
                      <td>$cont</td>                      
                      <td>$hallazgo</td>
                      <td>$accion</td>
                      <td>$responsable</td>
                      <td>$fCompromiso</td>
                    </tr>";
            } else {
                $mensaje2 = "<tr>
                        <td>$cont</td>                      
                        <td>$hallazgo</td>
                        <td>$accion</td>
                        <td>$responsable</td>
                        <td>$fCompromiso</td>
                    </tr>";
            }        
        }
        
        if($cont >= (15-$puntos)){        
            if ($puntos > 0){
                 $puntaje = round(($puntos * 100)/15,1);
            } else {
                $puntaje = 0;
            }           
            
            $queryUPuntos = "UPDATE a_puntosEvaluacion SET p11 = '$p11', p12 = '$p12', p13 = '$p13', p14 = '$p14', p15 = '$p15' WHERE id = '$id' ";
            $resultUPuntos = sqlsrv_query($conn, $queryUPuntos); 
            
            //INSERT O ACTUALIZACION DE AUDITORIA EN BASE DE DATOS        
            if($countRegAuditoria != 0 ){
                //ACTUALIZACION DE PUNTOS PARA REPORTAR EN GRAFICA
                $queryUPAudit = "UPDATE a_puntajeAuditoria SET puntaje = '$puntaje'";
                $resultUPAudit = sqlsrv_query($conn, $queryUPAudit);            
            } else {
                //INSERT PARA LOS PUNTOS A REPORTAR EN LA AUDOTORIA
                $queryIPAudit = "INSERT INTO a_puntajeAuditoria (id, depto, tipoEval, tipoAudit, fecha, semana, mes, anio, puntaje) VALUES ('$id','$depto','$typeEv','$typeAuditoria','$today','$w','$m','$y','$puntaje');";
                $resultIPAudit = sqlsrv_query($conn, $queryIPAudit);
            }
        
            //MODIFICACION DE ESTADO DE AUDITORIA
            $queryUPAudit = "UPDATE a_progAuditoria SET estado = 1 WHERE id='$id' ";
            $resultUPAudit = sqlsrv_query($conn, $queryUPAudit);    

            //CONSULTA DE E-MAIL DEL AUDITADO
            $queryCorreo = "SELECT u.nombre, u.correo FROM a_usuarios as u, a_progAuditoria as prog where prog.id = '$id' AND prog.auditado = u.usuario;";
            $resultCorreo = sqlsrv_query($conn, $queryCorreo);    

            while($row = sqlsrv_fetch_array($resultCorreo, SQLSRV_FETCH_ASSOC)){
                $nombreAuditado = $row['nombre']; //contador de registros que trae despues de la consulta        
                $correoAuditado = $row['correo']; //contador de registros que trae despues de la consulta        
            }
            $mensaje = $nombreAuditado. "<br> El puntaje de la auditoria que se te realizo es: ".$puntaje;

            if ($cont > 0){
                $mensaje = $mensaje." <br><br><b> En esta auditoria surgieron los siguientes puntos: </b>";
                $mensaje = $mensaje.'<br>'.$mensaje2;
            }
            $mensaje = $mensaje."<br><br><b> *** EN CASO DE ALGUNA INCONFORMIDAD, FAVOR DE HACERLO SABER AL DEPARTAMENTO DE VSE </b>***";
            
            //ENVIAMOS LA NOTIFICACION DE LA AUDITORIA
            $mail->addAddress("$correoAuditado", "Recepient Name");
            $mail->isHTML(true);

            $mail->Subject = "Evaluacion 5S's";
            $mail->Body = "$mensaje";
            $mail->AltBody = "This is the plain text version of the email content";

            //echo $mensaje;
            if(!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                echo "Bien,";
            }
            echo $puntaje;
        } else {
            $errors []= "SE DEBEN REGISTRAR LAS ACCIONES A REALIZAR EN LOS PUNTOS SELECCIONADOS EN <b>'NO'</b>";
        }
    } else {
        $errors []= "SE DEBE MARCAR UNA OPCION EN CADA PREGUNTA";
    }
    
//    MODULO PARA IMPRIMIR ERRORES
if (isset($errors)){
?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
    </div>
<?php } ?>









