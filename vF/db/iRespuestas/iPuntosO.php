<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //CONFIGURACION PARA MAILS 
    require("../../smtp/class.phpmailer.php");
    $mail = new PHPMailer();

    $mail->isSMTP();
    $mail->SMTPDebug = 2;
    $mail->Debugoutput = 'html';
    
    //CONFIGURACION DE HOST DE SEG
    $mail->Host = "smtp.sg.lan";
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->SMTPSecure = false;
    $mail->setFrom("no-reply@seg-automotive.com", "Portal auditoria 5S's");

    session_start();
    //OBTENEMOS LOS VALORES DE LOS COMPONENTES DEL MODAL
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"];
    $typeEv = $_SESSION["tipoEv"]; 
    $typeAuditoria = $_SESSION["tipoAuditoria"];
    
    $today = date('Y-m-d');
    $w = date('W');
    $m = date('m');
    $y = date('Y');
    
    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    $query = "SELECT id FROM a_puntosEvaluacion WHERE id = '$id';";
    $result = sqlsrv_query($conn,$query);

    if ($result === false ){
        echo "error <br>";
    } else {
        //CONSULTAMOS SI ESXISTES REGISTROS PARA ESA AUDITORIA
        if ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
            $countRegAuditoria = 1 ; //contador de registros que trae despues de la consulta        
        } else {
            $countRegAuditoria = 0;
        }
    }
    
    //PRIMER
    if (isset($_POST["checkOP1"]) && isset($_POST["checkOP2"]) && isset($_POST["checkOP3"]) &&
        isset($_POST["checkOP4"]) && isset($_POST["checkOP5"]) && isset($_POST["checkOP6"]) &&
        isset($_POST["checkOP7"]) && isset($_POST["checkOP8"]) && isset($_POST["checkOP9"]) &&
        isset($_POST["checkOP10"]) ){
        
        $p1 = $_POST["checkOP1"];
        $p2 = $_POST["checkOP2"];
        $p3 = $_POST["checkOP3"];
        $p4 = $_POST["checkOP4"];
        $p5 = $_POST["checkOP5"];
        $p6 = $_POST["checkOP6"];
        $p7 = $_POST["checkOP7"];
        $p8 = $_POST["checkOP8"];
        $p9 = $_POST["checkOP9"];
        $p10 = $_POST["checkOP10"];
        
        $puntos = $p1+$p2+$p3+$p4+$p5+$p6+$p7+$p8+$p9+$p10;
        
        //CONSULTA OPL
        $query = "SELECT fCompromiso, hallazgo, accion, responsable, soporte FROM a_opl WHERE idAud = '$id' ;";
        $result = sqlsrv_query($conn, $query);
        $cont = 0;
        
        while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
            $cont++;
            $fCompromiso = $row['fCompromiso']->format('d/m/Y');
            $hallazgo = $row['hallazgo'];
            $accion = $row['accion'];
            $responsable = $row['responsable'];
            $soporte = $row['soporte'];
            
            if ($cont > 1){
                $mensaje2 = $mensaje2."<tr>
                      <td>$cont</td>                      
                      <td>$hallazgo</td>
                      <td>$accion</td>
                      <td>$responsable</td>
                      <td>$soporte</td>
                      <td>$fCompromiso</td>
                    </tr>";
            } else {
                $mensaje2 = "<tr>
                        <td>$cont</td>                      
                        <td>$hallazgo</td>
                        <td>$accion</td>
                        <td>$responsable</td>
                        <td>$soporte</td>
                        <td>$fCompromiso</td>
                    </tr>";
            }  
        }
        
        if($cont >= (10-$puntos)){        
            if ($puntos > 0){
                $puntaje = $puntos * 10;
            } else {
                $puntaje = 0;
            }           
            //INSERT O ACTUALIZACION DE AUDITORIA EN BASE DE DATOS        
            if($countRegAuditoria != 0 ){
                $queryUPuntos = "UPDATE a_puntosEvaluacion SET p1 = '$p1', p2 = '$p2', p3 = '$p3', p4 = '$p4', p5 = '$p5', p6 = '$p6', p7 = '$p7', p8 = '$p8', p9 = '$p9', p10 = '$p10' WHERE id = '$id' ";
                $resultUPuntos = sqlsrv_query($conn, $queryUPuntos); 

                //ACTUALIZACION DE PUNTOS PARA REPORTAR EN GRAFICA
                $queryUPAudit = "UPDATE a_puntajeAuditoria SET puntaje = '$puntaje'";
                $resultUPAudit = sqlsrv_query($conn, $queryUPAudit);            
            } else {
                $queryIPuntos = "INSERT INTO a_puntosEvaluacion (id, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, tipoEv) VALUES ('$id','$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$typeEv');";
                $resultIPuntos = sqlsrv_query($conn, $queryIPuntos);

                //INSERT PARA LOS PUNTOS A REPORTAR EN LA AUDOTORIA
                $queryIPAudit = "INSERT INTO a_puntajeAuditoria (id, depto, tipoEval, tipoAudit, fecha, semana, mes, anio, puntaje) VALUES ('$id','$depto','$typeEv','$typeAuditoria','$today','$w','$m','$y','$puntaje');";
                $resultIPAudit = sqlsrv_query($conn, $queryIPAudit);
            }
        
            //MODIFICACION DE ESTADO DE AUDITORIA
            $queryUPAudit = "UPDATE a_progAuditoria SET estado = 1 WHERE id='$id' ";
            $resultUPAudit = sqlsrv_query($conn, $queryUPAudit);    

            //CONSULTA DE E-MAIL DEL AUDITADO
            $queryCorreo = "SELECT u.nombre, u.correo FROM a_usuarios as u, a_progAuditoria as prog where prog.id = '$id' AND prog.auditado = u.usuario;";
            $resultCorreo = sqlsrv_query($conn, $queryCorreo);    

            while($row = sqlsrv_fetch_array($resultCorreo, SQLSRV_FETCH_ASSOC)){
                $nombreAuditado = $row['nombre']; //contador de registros que trae despues de la consulta        
                $correoAuditado = $row['correo']; //contador de registros que trae despues de la consulta        
            }
            
            $mensaje = "<b>".$nombreAuditado. "</b><br> El puntaje de la auditoria que se te realizo es: ".$puntaje;
            if ($cont > 0){
                $mensaje = $mensaje." <br><br><b> En esta auditoria surgieron los siguientes puntos: </b>";                
                $mensaje = $mensaje."<table border='1' style='border-collapse: collapse;'>
                    <tr>
                        <th>No.</th> 
                        <th>Hallazgo</th>
                        <th>Accion</th>
                        <th>Responsable</th>
                        <th>Soporte</th>
                        <th>F.Compromiso</th>
                    </tr>".$mensaje2."</table>";
            }
            $mensaje = $mensaje."<h4><br><br><b>**EN CASO DE ALGUNA DUDA HACERLO SABER AL DEPARTAMENTO DE VSE</b></h4>**";
            
            //ENVIAMOS LA NOTIFICACION DE LA AUDITORIA
            $mail->addAddress("$correoAuditado", "Recepient Name");
            $mail->isHTML(true);

            $mail->Subject = "Evaluacion 5S's";
            $mail->Body = "$mensaje";
            $mail->AltBody = "This is the plain text version of the email content";

            //echo $mensaje;
            if(!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                echo "Bien,";
            }
            echo $puntaje;
        } else {
            $errors []= "SE DEBEN REGISTRAR LAS ACCIONES A REALIZAR EN LOS PUNTOS SELECCIONADOS EN <b>'NO'</b>";
        }
    } else {
        $errors []= "SE DEBE MARCAR UNA OPCION EN CADA PREGUNTA";
    }
    
//    MODULO PARA IMPRIMIR ERRORES
if (isset($errors)){
?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
    </div>
<?php } ?>









