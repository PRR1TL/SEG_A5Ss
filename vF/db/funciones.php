<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'conexion.php';
//Devuelve un array multidimensional con el resultado de la consulta
function getArraySQL($sql) {
    $connectionObj = new ServerConfig();
    $connectionStr = $connectionObj -> serverConnection();
    
    if (!$result = sqlsrv_query($connectionStr, $sql)) die();
    $rawdata = array();
    $i = 0;
    while ($row = sqlsrv_fetch_array($result)) {
        $rawdata[$i] = $row;
        $i++;
    }
    $connectionObj ->serverDisconnect();
    return $rawdata;
}

//CONSULTA PARA LOGIN
function cUsuarioContrasena($usuario, $contrasena){
    $sql = "SELECT usuario, nombre, privilegio, estado, tipoEv, depto, correo FROM a_usuarios WHERE usuario = '$usuario' AND contrasenia = '$contrasena'";
    return getArraySQL($sql);
}

//CONSULTA PARA USUARIOS
function cUAdministradores() {
    $sql = "SELECT depto, tipoEv, usuario, nombre, correo FROM a_usuarios WHERE privilegio = 1 AND estado = 1";
    return getArraySQL($sql);
}

function cUAdministradoresNoActivos() {
    $sql = "SELECT depto, tipoEv, usuario, nombre, correo FROM a_usuarios WHERE privilegio = 1 AND estado = 0 ";
    return getArraySQL($sql);
}

function cUSubChampion($depto) {
    $sql = "SELECT usuario, nombre, correo FROM a_usuarios WHERE depto = '$depto' AND privilegio = 7 AND estado = 1 ";
    return getArraySQL($sql);
}

function cUSubChampionNoActivos($depto) {
    $sql = "SELECT usuario, nombre, correo FROM a_usuarios WHERE depto = '$depto' AND privilegio = 7 AND estado = 0 ";
    return getArraySQL($sql);
}

//CONSULTA DE NOMBRE Y CORREO
function cUNuevoRol($usuario) { 
    $sql = "SELECT nombre, correo FROM a_usuarios WHERE usuario = '$usuario' ";
    return getArraySQL($sql);
}

function cAuditRoles($depto, $fecha) {
    $sql = "SELECT a.nWeek, concat(a.fIni,' a ', a.fFin) as period, u.nombre, a.tipoAuditoria FROM a_usuarios as u, a_progAuditoria as a WHERE a.depAuditado = '$depto' AND a.depAuditado = u.depto AND u.usuario = a.auditor AND a.fFin >= '$fecha' ";
    return getArraySQL($sql);
}


//EVENTOS SEMANALES POR USUARIO 
function cEvenSemanalUsuario($usuario, $fIni, $fFin){
    $sql = "SELECT id, nWeek, depAuditado, auditado, auditor, tipoEvaluacion FROM a_progAuditoria WHERE auditor = '$usuario' AND fIni = '$fIni' AND fFin = '$fFin' AND estado = 0";
    return getArraySQL($sql);
}

//EVENTOS MENSUALES POR USUARIO
function cEvenMensualUsuario($usuario, $fIni, $fFin){
    $sql = "SELECT id, nWeek, depAuditado, auditado, auditor, tipoEvaluacion FROM a_progAuditoria WHERE auditor = '$usuario' AND fIni > '$fIni' AND fFin = '$fFin' AND estado = 0 ";
    return getArraySQL($sql);
}

//AUDITORIAS
function cUAuditoriaChamp(){
    $sql = "SELECT depto,usuario, nombre, tipoEv FROM a_usuarios WHERE privilegio = 3";
    return getArraySQL($sql);
}

function cUAuditoriaChampPiso(){
    $sql = "SELECT depto, usuario, nombre, tipoEv, correo FROM a_usuarios WHERE privilegio = 3 AND tipoEv BETWEEN 1 AND 4 AND estado = 1";
    return getArraySQL($sql);
}

function cUAuditoriaChampOficina(){
    $sql = "SELECT depto, usuario, nombre, tipoEv, correo FROM a_usuarios WHERE privilegio = 3 AND tipoEv = 5 AND estado = 1 ";
    return getArraySQL($sql);
}

function cUAuditoriaChampNOActivos(){
    $sql = "SELECT depto,usuario, nombre, tipoEv, correo FROM a_usuarios WHERE privilegio = 3 AND estado = 0";
    return getArraySQL($sql);
}

function cLastAuditProgram ($anio){
    $sql = "SELECT max(nWeek) FROM a_progAuditoria WHERE anio LIKE '$anio'";
    return getArraySQL($sql);
} 

function cUAuditoriaSupPiso(){
    $sql = "SELECT depto, usuario, nombre, tipoEv, correo FROM a_usuarios WHERE privilegio = 2 AND tipoEv BETWEEN 1 AND 4 AND estado = 1 ";
    return getArraySQL($sql);
}

function cUAuditoriaSupOficina(){
    $sql = "SELECT depto, usuario, nombre, tipoEv, correo FROM a_usuarios WHERE privilegio = 2 AND tipoEv = 5 AND estado = 1";
    return getArraySQL($sql);
}

function cUAuditoriaSupNOActivo(){
    $sql = "SELECT depto, usuario, nombre, tipoEv, correo FROM a_usuarios WHERE privilegio = 2 AND estado = 0";
    return getArraySQL($sql);
}

//DELETE DE USUARIO
function dUsuario ($usuario) {
    $sql = "DELETE FROM a_usuarios WHERE usuario = '$usuario' ";
    return getArraySQL($sql);
}

//CONSULTA PARA LAS AREAS
function cDepartamentosPiso(){
    $sql = "SELECT * FROM a_departamento WHERE tipoEv BETWEEN 1 AND 4 ORDER BY tipoEv";
    return getArraySQL($sql);
}

function cDepartamentosOficina(){
    $sql = "SELECT * FROM a_departamento WHERE tipoEv = 5 ";
    return getArraySQL($sql);
}

//CONSULTA PARA TIPO DE EVALUACION POR DEPARTAMENTO
function cTEvalDepto($depto) {
    $sql = "SELECT tipoEv FROM a_departamento WHERE depto = '$depto' ";
    return getArraySQL($sql);
}

//CONSULTAS PARA LAS AUDITORIAS (ADMINISTRADOR)
function cAuditoriaAdmin($semana,$anio){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au where au.usuario = pa.auditor AND nWeek >= '$semana' AND anio >= '$anio' ORDER BY pa.depAuditado, pa.tipoAuditoria";
    return getArraySQL($sql);
}

function cAuditoriaId($id){
    $sql = "SELECT us.nombre, us.tipoEv FROM a_progAuditoria as prog, a_usuarios as us WHERE prog.id = '$id' AND prog.auditado = us.usuario";
    return getArraySQL($sql);
}

//CONSULTA DE AUDITORIAS
function cAuditoriasProgramadasPiso($semana,$anio){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), concat(us.nombre,' (',us.depto,')'), concat(au.nombre,' (',au.depto,')' ), pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au, a_usuarios as us WHERE au.usuario = pa.auditado AND us.usuario = pa.auditor AND pa.nWeek >= '$semana' AND anio >= '$anio' AND pa.tipoEvaluacion BETWEEN 1 AND 4 AND pa.tipoAuditoria >= 2 order by us.depto";
    return getArraySQL($sql);
}

function cAuditoriasProgramadasOficina($semana,$anio){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), us.depto, us.nombre , concat(au.nombre,' (',au.depto,')' ), pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au, a_usuarios as us WHERE au.usuario = pa.auditado AND us.usuario = pa.auditor AND pa.nWeek >= '$semana' AND anio >= '$anio' AND pa.tipoEvaluacion = 5 AND pa.tipoAuditoria >= 2 order by us.depto";
    return getArraySQL($sql);
}

function cAuditoriaUser($semana,$anio, $usuario){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au where au.usuario = pa.auditado AND nWeek >= '$semana' AND anio >= '$anio' AND auditor LIKE '$usuario' ORDER BY pa.depAuditado, pa.tipoAuditoria";
    return getArraySQL($sql);
}

function cAuditorUser($semana,$anio, $usuario){
    $sql = "SELECT concat(pa.fIni,' a ', pa.fFin), au.depto,au.nombre, pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria as pa, a_usuarios as au WHERE au.usuario = pa.auditor AND nWeek >= '$semana' AND anio >= '$anio' AND auditado LIKE '%$usuario%' AND tipoAuditoria = 2 ORDER BY pa.depAuditado, pa.tipoAuditoria";
    return getArraySQL($sql);
}

//CONSULTAS PARA PROGRAMACION DE AUDITORIAS
function cUUsuariosSup (){
    $sql = "SELECT usuario, depto FROM a_usuarios WHERE estado = 1 AND tipoEv < 7 AND privilegio = 2 ORDER BY depto,usuario ASC";
    return getArraySQL($sql);
}

function cUUsuariosChampPiso (){
    $sql = "SELECT usuario, depto, tipoEv FROM a_usuarios WHERE tipoEv BETWEEN 1 AND 4 AND privilegio = 3  ORDER BY usuario ASC";
    return getArraySQL($sql);
}

function cUUsuariosChampMantenimiento (){
    $sql = "SELECT usuario, depto, tipoEv FROM a_usuarios WHERE estado = 1 AND tipoEv = 4 AND privilegio = 3 ORDER BY tipoEv,usuario ASC";
    return getArraySQL($sql);
}

function cUUsuariosChampOficina (){
    $sql = "SELECT usuario, depto, tipoEv FROM a_usuarios WHERE estado = 1 AND tipoEv = 5 AND privilegio = 3 ORDER BY tipoEv,usuario ASC";
    return getArraySQL($sql);
}

function cUsuarioPAuditoriaSup ($cValor) {
    $sql = "SELECT u.usuario FROM a_usuarios as u, a_departamento AS d WHERE U.privilegio = 3 AND d.depto = u.depto AND d.cValor = '$cValor'";
    return getArraySQL($sql);
}

function cDeptosArea () {
    $sql = "SELECT tipoEv, depto FROM a_departamento GROUP BY tipoEv, depto ORDER BY tipoEv, depto";
    return getArraySQL($sql);
}



function cUsuarioDeptos ($depto) {
    $sql = "SELECT usuario FROM a_usuarios WHERE depto LIKE '$depto' AND privilegio = 6 AND tipoEv <> 6 AND estado <> 0 GROUP BY usuario ORDER BY usuario ASC";
    return getArraySQL($sql);
}

function cUltimoUsuarioAuditorDeptos ($depto, $fInicio) {
    $sql = "SELECT auditor FROM a_progAuditoria WHERE depAuditado LIKE '$depto' AND fIni = '$fInicio' AND tipoAuditoria = 1";
    return getArraySQL($sql);
}

function cUltimoUsuarioAuditadoDepto ($depto, $fInicio) {
    $sql = "SELECT auditado FROM a_progAuditoria WHERE depAuditado LIKE '$depto' AND fIni = '$fInicio' AND tipoAuditoria = 2";
    return getArraySQL($sql);
}

function cUsuarioPAuditoriaChampionPiso ($usuario, $anio, $fInicio){
    $sql = "SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
                SELECT a.auditor FROM a_progAuditoria as a WHERE u.usuario = a.auditor AND a.fIni = '$fInicio' AND a.tipoAuditoria = 2
            ) AND u.usuario NOT LIKE '%$usuario%' AND u.tipoEv BETWEEN 1 AND 4 AND u.privilegio = 3 AND estado <> 0 GROUP BY u.usuario
            UNION
            SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
                SELECT b.auditor FROM a_progAuditoria as b WHERE u.usuario = b.auditor AND b.anio = '$anio' AND b.tipoAuditoria = 2
            ) AND u.usuario NOT LIKE '%$usuario%' AND u.tipoEv BETWEEN 1 AND 4 AND u.privilegio = 3 AND estado <> 0 GROUP BY u.usuario"; 
    return getArraySQL($sql);    
}

function cUsuarioPAuditoriaChampionOficina($usuario, $anio, $fInicio) {
    $sql = "SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
                SELECT a.auditor FROM a_progAuditoria as a WHERE u.usuario = a.auditor AND a.fIni = '2019-04-15' AND a.tipoAuditoria = 3
            ) AND u.usuario NOT LIKE '%$usuario%' AND u.tipoEv = 5 AND u.privilegio = 6 AND estado <> 0 GROUP BY u.usuario
            UNION
            SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
                SELECT b.auditor FROM a_progAuditoria as b WHERE u.usuario = b.auditor AND b.anio = '2019' AND b.tipoAuditoria = 3
            ) AND u.usuario NOT LIKE '%$usuario%' AND u.tipoEv = 5 AND u.privilegio = 6 AND estado <> 0 GROUP BY u.usuario";
    return getArraySQL($sql);
}

function cUsuarioNoRelacionDepto($depto, $fecha) {
//    $sql = "SELECT u.usuario FROM a_usuarios as u, a_progAuditoria as p 
//            WHERE u.estado = 1 AND u.privilegio = 6 AND u.depto <> '$depto' AND u.usuario <> ( 
//                    SELECT pa.auditor FROM a_progAuditoria as pa WHERE pa.depAuditado <> '$depto' GROUP BY pa.auditor
//            ) GROUP BY u.usuario
//            
//    $sql = "SELECT u.usuario 
//            FROM a_usuarios as u
//            WHERE NOT EXISTS (
//             SELECT pa.auditor FROM a_progAuditoria as pa WHERE pa.fIni = '2019-04-01' GROUP BY pa.auditor
//            ) AND u.estado = 1 AND u.privilegio = 6 AND u.depto <> '$depto' ";
//    $sql = "SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
//                SELECT a.auditor FROM a_progAuditoria as a WHERE u.usuario = a.auditor AND a.fIni = '$fecha' AND a.tipoAuditoria = 3
//            ) AND u.usuario NOT LIKE '%$usuario%' AND u.privilegio = 6 AND estado <> 0 GROUP BY u.usuario
//            UNION
//            SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
//                SELECT b.auditor FROM a_progAuditoria as b WHERE u.usuario = b.auditor AND b.anio = '2019' AND b.tipoAuditoria = 3
//            ) AND u.usuario NOT LIKE '%$usuario%' AND u.privilegio = 6 AND estado <> 0 GROUP BY u.usuario ";
//    $sql = "SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
//	SELECT a.auditor FROM a_progAuditoria as a WHERE u.usuario = a.auditor AND u.depto <> a.depAuditado AND a.fIni >= '$fecha' AND a.tipoAuditoria = 3
//) AND u.depto NOT LIKE '%$depto%' AND u.privilegio = 6 AND estado <> 0 GROUP BY u.usuario
//UNION
//SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
//	SELECT b.auditor FROM a_progAuditoria as b WHERE u.usuario = b.auditor AND u.depto <> b.depAuditado AND b.anio = '2019' AND b.tipoAuditoria = 3
//) AND u.depto NOT LIKE '%$depto%' AND  u.privilegio = 6 AND estado <> 0 GROUP BY u.usuario";
    $sql = "SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
	SELECT a.auditor FROM a_progAuditoria as a WHERE u.usuario = a.auditor AND a.fIni >= '$fecha' AND a.tipoAuditoria = 3
) AND u.depto NOT LIKE '%$depto%' AND u.privilegio = 6 AND estado <> 0 GROUP BY u.usuario
UNION
SELECT u.usuario FROM a_usuarios as u WHERE NOT EXISTS (
	SELECT b.auditor FROM a_progAuditoria as b WHERE u.usuario = b.auditor AND b.anio = '2019' AND b.tipoAuditoria = 3
) AND u.depto NOT LIKE '%$depto%' AND  u.privilegio = 6 AND estado <> 0 GROUP BY u.usuario";
    
    return getArraySQL($sql);
}

//function cLastAuditProgram ($anio){
//    $sql = "SELECT max(nWeek) FROM a_progAuditoria WHERE anio LIKE '$anio'";
//    return getArraySQL($sql);
//}


function cLastWAuditProgram ($anio){
    $sql = "SELECT max(nWeek) FROM a_progAuditoria WHERE anio LIKE '$anio'";
    return getArraySQL($sql);
}

function cLastDAuditProgram ($anio){
    $sql = "SELECT max(fIni) FROM a_progAuditoria WHERE anio LIKE '$anio'";
    return getArraySQL($sql);
}

//function iProgAuditoria ($fIni, $fFin, $anio, $mes, $sem, $depto, $auditado, $auditor, $tipoAuditoria, $tipoEv ){
//    $sql = "INSERT INTO a_progAuditoria (fIni, fFin, anio, mes, nWeek, depAuditado, auditado, auditor, tipoAuditoria, tipoEvaluacion, estado) VALUES ('$fIni', '$fFin', '$anio', '$mes', '$sem', '$depto', '$auditado', '$auditor', '$tipoAuditoria', '$tipoEv', '0');";
//    return getArraySQL($sql);
//}

function iProgAuditoria ($id, $fIni, $fFin, $anio, $mes, $sem, $depto, $auditado, $auditor, $tipoAuditoria, $tipoEv, $aux ) {
    //echo $id,', ',$fIni,', ',$fFin,', ',$anio,', ',$mes,', ',$sem,', ',$depto,', ',$auditado,', ',$auditor,', ',$tipoAuditoria,', ',$tipoEv,', ',$aux;
    $sql = "INSERT INTO a_progAuditoria (id, fIni, fFin, anio, mes, nWeek, depAuditado, auditado, auditor, tipoAuditoria, tipoEvaluacion, estado, aux) VALUES ('$id', '$fIni', '$fFin', '$anio', '$mes', '$sem', '$depto', '$auditado', '$auditor', '$tipoAuditoria', '$tipoEv', '0','$aux');";
    return getArraySQL($sql);
}

function cAuditoriaSUPandADMIN ($userName){
    $sql = "SELECT id FROM a_progAuditoria WHERE auditor = '$userName' AND estado = 0 and tipoAuditoria = 4 ";
    return getArraySQL($sql);
}

function cLIdDaily () {
    //$sql = "SELECT MAX(id) FROM a_progAuditoria WHERE tipoAuditoria = 1 AND fIni = (SELECT MAX (fIni) FROM a_progAuditoria where tipoAuditoria = 1 ); ";
    $sql = "SELECT MAX(aux) FROM a_progAuditoria WHERE tipoAuditoria = 1 AND fIni = (SELECT MAX (fIni) FROM a_progAuditoria where tipoAuditoria = 1 ); ";
    return getArraySQL($sql);
}

function cLIdWeekly () {
    //$sql = "SELECT MAX(id) FROM a_progAuditoria WHERE tipoAuditoria = 2 AND fIni = (SELECT MAX (fIni) FROM a_progAuditoria where tipoAuditoria = 2 ); ";
    $sql = "SELECT MAX(aux) FROM a_progAuditoria WHERE tipoAuditoria = 2 AND fIni = (SELECT MAX (fIni) FROM a_progAuditoria where tipoAuditoria = 2 ); ";
    return getArraySQL($sql);
}

function cLIdMonthly () {
    //$sql = "SELECT MAX(id) FROM a_progAuditoria WHERE tipoAuditoria = 3 AND fIni = (SELECT MAX (fIni) FROM a_progAuditoria where tipoAuditoria = 3 ); ";
    $sql = "SELECT MAX(aux) FROM a_progAuditoria WHERE tipoAuditoria = 3 ; ";
    return getArraySQL($sql);
}

//ULTIMO AUDITADO DEL AREA
function cLChampDaily ($depto) {
    $sql = "SELECT auditado FROM a_progAuditoria WHERE depAuditado = '$depto' AND fIni = (SELECT MAX(fIni) FROM a_progAuditoria WHERE tipoAuditoria = 1 AND depAuditado = '$depto' ) AND tipoAuditoria = 1; ";
    return getArraySQL($sql);
}

function cLChampWeekly ($depto) {
    $sql = "SELECT auditado FROM a_progAuditoria WHERE depAuditado = '$depto' AND fIni = (SELECT MAX(fIni) FROM a_progAuditoria WHERE tipoAuditoria = 2 AND depAuditado = '$depto' ) AND tipoAuditoria = 2; ";
    return getArraySQL($sql);
}

function cLChampMonthly ($depto) {
    $sql = "SELECT auditado FROM a_progAuditoria WHERE depAuditado = '$depto' AND fIni = (SELECT MAX(fIni) FROM a_progAuditoria WHERE tipoAuditoria = 3 AND depAuditado = '$depto' ) AND tipoAuditoria = 3; ";
    return getArraySQL($sql);
}

function cLAuxSemanal ( ) {
    $sql = "SELECT MAX(aux) from a_progAuditoria WHERE tipoAuditoria = 2 ";
    return getArraySQL($sql);
}

//AUDITORIAS PROGRAMADAS / PENDIENTES POR USUARIO
function cAuditoriasTipoUsuario($fInicio, $usuario){
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.nWeek, CAST(CONVERT(date, pa.fIni, 103) as VARCHAR(15)) as fini, pa.tipoEvaluacion FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fFin >= '$fInicio' AND pa.auditor LIKE '%$usuario%' AND pa.estado = 0 ORDER BY pa.tipoAuditoria, pa.fFin";
    return getArraySQL($sql);
}

function cAuditoriasUsuario($fInicio, $usuario){
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.nWeek, CAST(CONVERT(date, pa.fIni, 103) as VARCHAR(15)) as fini, pa.tipoEvaluacion FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fFin >= '$fInicio' AND pa.auditor LIKE '%$usuario%' AND pa.estado = 0 AND pa.tipoAuditoria < 4 ORDER BY pa.tipoAuditoria, pa.fFin";
    return getArraySQL($sql);
}

function cDatAuditoriaAdmin($fecha, $usuario, $tipo) { 
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.nWeek, pa.tipoEvaluacion FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fFin >= '$fecha' AND pa.auditor LIKE '%$usuario%' AND pa.tipoAuditoria = '$tipo' AND pa.estado = 0 ORDER BY pa.tipoAuditoria, pa.fFin";
    return getArraySQL($sql);
}

//FUNCIONES PARA CONSULTA DE LAS AUDITORIAS DE USUARIOS NO ADMINISTRADORES
function cAuditPorRealizarUsuario ($fecha , $usuario){
    $sql = "SELECT p.id, p.fIni, p.fIni, p.depAuditado, u.nombre, p.tipoAuditoria FROM a_progAuditoria as p, a_usuarios as u WHERE p.auditor = '$usuario' AND p.fFin > '$fecha' AND p.estado = 0 AND tipoAuditoria < 4 AND u.usuario = p.auditado ";
    return getArraySQL($sql);
}

function cAuditPorRealizarmeUsuario ($fecha , $usuario){
    $sql = "SELECT p.id, p.fIni, p.fIni, p.depAuditado, u.nombre, p.tipoAuditoria FROM a_progAuditoria as p, a_usuarios as u WHERE p.auditado = '$usuario' AND p.fFin >= '$fecha' AND p.estado = 0 AND tipoAuditoria > 2 AND u.usuario = p.auditor ";
    return getArraySQL($sql);
}


//AUDITORIA PENDIENTE POR USUARIO
function cDatAuditoriaUsuario($fInicio, $usuario){
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.tipoAuditoria FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fIni >= '$fInicio' AND pa.auditor LIKE '%$usuario%' AND pa.estado = 0";
    return getArraySQL($sql);
}

function cAuditoriaFechaUsuario($fInicio, $usuario, $tipoAuditoria){
    $sql = "SELECT pa.id, pa.depAuditado, au.nombre, pa.tipoAuditoria, CAST(CONVERT(date, pa.fIni, 103) as VARCHAR(15)) as fIni, CAST(CONVERT(date, pa.fFin, 103) as VARCHAR(15)) as fFin FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.fFin >= '$fInicio' AND pa.auditor LIKE '%$usuario%' AND pa.tipoAuditoria LIKE '%$tipoAuditoria%' AND pa.estado = 0";
    return getArraySQL($sql);
}

function cAuditoriaIDUsuario($id){
    $sql = "SELECT pa.depAuditado, au.nombre, pa.tipoAuditoria, pa.tipoEvaluacion FROM a_progAuditoria AS pa, a_usuarios AS au WHERE au.usuario = pa.auditado AND pa.id = '$id'";
    return getArraySQL($sql);
}

function cExistRegistroAuditoria($id){
    $sql = "SELECT id FROM a_puntosEvaluacion WHERE id = '$id'";
    return getArraySQL($sql);
}

/************************* CONSULTAS PARA REPORTTES ***************************/
function cCadValorArea($tipo){
    $sql = "SELECT cValor, tipoEv FROM a_cadValor WHERE tipoEv = '$tipo' ORDER BY cValor ASC";
    return getArraySQL($sql);
}

function cCadValorPiso(){
    $sql = "SELECT cValor, tipoEv FROM a_cadValor WHERE tipoEv BETWEEN 1 AND 4 ORDER BY cValor ASC";
    return getArraySQL($sql);
}

function cCadValorOficina(){
    $sql = "SELECT cValor, tipoEv FROM a_cadValor WHERE tipoEv = 5 ORDER BY cValor ASC";
    return getArraySQL($sql);
}

function cTotalDeptos(){
    $sql = "SELECT depto FROM a_departamento GROUP BY depto ORDER BY depto ASC";
    return getArraySQL($sql);
}

function cDeptoCadValor($cValor){
    $sql = "SELECT depto FROM a_departamento WHERE cValor = '$cValor' GROUP BY depto ";
    return getArraySQL($sql);
}

function cDeptoSinCadValorP(){
    $sql = "SELECT depto FROM a_departamento WHERE cValor = '-' AND tipoEv BETWEEN 1 AND 4";
    return getArraySQL($sql);
}

function cDeptoSinCadValorO(){
    $sql = "SELECT depto FROM a_departamento WHERE cValor = '-' AND tipoEv = 5";
    return getArraySQL($sql);
}

function cDeptosPiso(){
    $sql = "SELECT depto FROM a_departamento WHERE tipoEv BETWEEN 1 AND 4 GROUP BY depto ORDER BY depto ASC";
    return getArraySQL($sql);
}

function cDeptosOficina(){
    $sql = "SELECT depto FROM a_departamento WHERE tipoEv = 5 GROUP BY depto ORDER BY depto ASC";
    return getArraySQL($sql);
}

function cDeptoRegistro($anio){
    $sql = "SELECT depto FROM a_puntajeAuditoria WHERE anio = '$anio' GROUP BY depto";
    return getArraySQL($sql);
}

function cDeptoPuntosMes($anio, $depto){
    $sql = "SELECT month(fecha) AS mes, puntaje FROM a_puntajeAuditoria WHERE anio = '$anio' AND depto = '$depto' " ;
    return getArraySQL($sql);
}

//CONSULTAS PARA GRAFICAS
function cPAreaMes($anio){
    //$sql = "SELECT depto, month(fecha) AS mes, AVG(puntaje) FROM a_puntajeAuditoria WHERE anio = '$anio' GROUP BY depto, month(fecha)";
    $sql = "SELECT prog.depAuditado, month(prog.fIni) AS mes, AVG(point.puntaje) AS puntos FROM a_progAuditoria AS prog, a_puntajeAuditoria as point WHERE prog.depAuditado = point.depto AND prog.id = point.id AND prog.anio = '$anio' GROUP BY prog.depAuditado, month(prog.fIni)";
    return getArraySQL($sql);
}

function cAPlanAreaMes($anio){
    $sql = "SELECT depAuditado, month(fFin) AS mes, COUNT(id) FROM a_progAuditoria WHERE anio = '$anio' GROUP BY depAuditado, month(fFin)";
    return getArraySQL($sql);
}

function cARealAreaMes($anio){
    $sql = "SELECT depAuditado, month(fFin) AS mes, COUNT(id) FROM a_progAuditoria WHERE anio = '$anio' AND estado = '1' GROUP BY depAuditado, month(fFin)";
    return getArraySQL($sql);
}

//GRAFICA GENERAL
    //PISO POR ANIO
function cPuntosPisoGY($anio){
    $sql = "SELECT depto, AVG(puntaje) FROM a_puntajeAuditoria WHERE tipoAudit BETWEEN 1 AND 3 AND anio = '$anio' GROUP BY depto";
    return getArraySQL($sql);
}

function cAPlanPisoGY($anio){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoAuditoria BETWEEN 1 AND 3 AND anio = '$anio' GROUP BY depAuditado";
    return getArraySQL($sql);
}

function cARealPisoGY($anio){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoAuditoria BETWEEN 1 AND 3 AND anio = '$anio' AND estado = '1' GROUP BY depAuditado";
    return getArraySQL($sql);
}
    //OFICINA POR ANIO
function cPuntosOficinaGY($anio){
    $sql = "SELECT depto, AVG(puntaje) FROM a_puntajeAuditoria WHERE tipoEval = 5 AND anio = '$anio' GROUP BY depto";
    return getArraySQL($sql);
}

function cAPlanOficinaGY($anio){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoAuditoria BETWEEN 1 AND 3 AND anio = '$anio' GROUP BY depAuditado";
    return getArraySQL($sql);
}

function cARealOficinaGY($anio){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoAuditoria BETWEEN 1 AND 3 AND anio = '$anio' AND estado = '1' GROUP BY depAuditado";
    return getArraySQL($sql);
}

/**anio mes**/
    //PISO POR ANIO-MES
function cPuntosPisoGYM($anio, $mes){
    $sql = "SELECT pua.depto, AVG(pua.puntaje) FROM a_puntajeAuditoria as pua, a_progAuditoria as pra WHERE pra.anio = '$anio' AND pra.mes  = '$mes' AND pra.id = pua.id AND pra.tipoAuditoria BETWEEN 1 AND 3 GROUP BY pua.depto";
    return getArraySQL($sql);
}

function cAPlanPisoGYM($anio, $mes){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoAuditoria BETWEEN 1 AND 3 AND anio = '$anio' AND mes = '$mes' GROUP BY depAuditado";
    return getArraySQL($sql);
}

function cARealPisoGYM($anio, $mes){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoAuditoria BETWEEN 1 AND 3 AND anio = '$anio' AND mes = '$mes' AND estado = '1' GROUP BY depAuditado";
    return getArraySQL($sql);
}
    //OFICINA POR ANIO-MES
function cPuntosOficinaGYM($anio, $mes){
    $sql = "SELECT pua.depto, AVG(pua.puntaje) FROM a_puntajeAuditoria as pua, a_progAuditoria as pra WHERE pra.tipoEval = 5 AND pra.anio = '$anio' AND pra.mes  = '$mes' GROUP BY depto";
    return getArraySQL($sql, $mes);
}

function cAPlanOficinaGYM($anio, $mes){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoAuditoria BETWEEN 1 AND 3 AND anio = '$anio' AND mes = '$mes' GROUP BY depAuditado";
    return getArraySQL($sql);
}

function cARealOficinaGYM($anio, $mes){
    $sql = "SELECT depAuditado, COUNT(id) FROM a_progAuditoria WHERE tipoAuditoria BETWEEN 1 AND 3 AND anio = '$anio' AND mes = '$mes' AND estado = '1' GROUP BY depAuditado";
    return getArraySQL($sql);
}


//CONSULTAS PARA OPL
//function cOPLAbiertosArea($depto, $fCompromiso){
//    $sql = "SELECT id, convert (varchar(15), fInicio) as inicio, convert (varchar(15),fCompromiso) as compromiso, hallazgo, accion, responsable, soporte, estado FROM a_opl WHERE depto LIKE '%$depto%' AND fCompromiso >= '$fCompromiso' AND estado <> 4";
//    return getArraySQL($sql);
//}
//
//function cOPLCerradosArea($depto, $fCompromiso){
//    $sql = "SELECT id, convert (varchar(15), fInicio) as inicio, convert (varchar(15),fCompromiso) as compromiso, convert (varchar(15),fTerminacion) as terminacion, hallazgo, accion, responsable, soporte, estado FROM a_opl WHERE depto LIKE '%$depto%' AND fCompromiso >= '$fCompromiso' AND estado = 4";
//    return getArraySQL($sql);
//}
//
//function cOPLTodosArea($depto, $fCompromiso){
//    $sql = "SELECT id, convert (varchar(15), fInicio) as inicio, convert (varchar(15),fCompromiso) as compromiso, convert (varchar(15),fTerminacion) as terminacion, hallazgo, accion, responsable, soporte, estado FROM a_opl WHERE depto LIKE '%$depto%' AND fCompromiso >= '$fCompromiso' ";
//    return getArraySQL($sql);
//}

//CONSULTA DE AUDITORIAS PROGRAMADAS 

// LISTADOS PARA LINEAS DE SUPERVISORES
function listadoDeptosSup_Gerentes($usuario){
    $sql = "SELECT d.depto FROM a_usuarios AS u, a_departamento AS d WHERE u.depto = d.cValor AND u.usuario LIKE '%$usuario%' OR u.depto = d.depto AND u.usuario LIKE '%$usuario%'";
    return getArraySQL($sql);
}

//DATOS DE LA AUDITORIA DE ACUERDO A EL AREA SELECCIONADA SUP-ADMINASTRAR
function cUsuarioAuditarSup_Admin ($depto){
    $sql = "SELECT usuario, nombre FROM a_usuarios WHERE depto LIKE '%$depto%'";
    return getArraySQL($sql);
}

function uAuditoriaSup_Admin($fIn, $fFin, $anio, $mes, $week, $depto, $auditado, $id){
    $sql = "UPDATE a_progAuditoria SET fIni = '$fIn', fFin = '$fFin', anio = '$anio', mes = '$mes', nWeek = '$week', depAuditado = '$depto', auditado = '$auditado' WHERE id = '$id'";
    return getArraySQL($sql);
}

//APARTADO PARA EL MODULO DE USUARIOS
function iUsuario($tipoEv, $depto,$usuario,$nombre,$correo,$contrasenia,$privilegio ) {
    $sql = "INSERT INTO a_usuarios (tipoEv, depto, usuario, nombre, correo, contrasenia, privilegio, estado) VALUES ('$tipoEv','$depto','$usuario','$nombre','$correo','$contrasenia','$privilegio',1)";
    return getArraySQL($sql);
}

function cUsuario($usuario ){
    $sql = "SELECT usuario FROM a_usuarios WHERE usuario LIKE '%$usuario%' ";
    return getArraySQL($sql);
}

function uUsuarioFull($tipoEv, $depto, $nombre, $correo, $pass, $privilegio, $estado, $usuario ){
    $sql = "UPDATE a_usuarios SET tipoEv = '$tipoEv', depto = '$depto', nombre = '$nombre', correo = '$correo', contrasenia = '$pass', privilegio = '$privilegio', estado = '$estado' WHERE usuario = '$usuario'";
    return getArraySQL($sql);
}

function uUsuarioSinPass($tipoEv, $depto, $nombre, $correo, $privilegio, $estado, $usuario ){
    $sql = "UPDATE a_usuarios SET tipoEv = '$tipoEv', depto = '$depto', nombre = '$nombre', correo = '$correo', privilegio = '$privilegio', estado = '$estado' WHERE usuario = '$usuario' ";
    return getArraySQL($sql);
}

function uMiUsuarioPass($pass, $usuario){
    $sql = "UPDATE a_usuarios SET contrasenia = '$pass' WHERE usuario = '$usuario' ";
    return getArraySQL($sql);
}
function uMiUsuarioCorreo($correo, $usuario){
    $sql = "UPDATE a_usuarios SET correo = '$correo' WHERE usuario = '$usuario' ";
    return getArraySQL($sql);
}

function uMiUsuarioPC($correo, $pass, $usuario ){
    $sql = "UPDATE a_usuarios SET correo = '$correo', contrasenia = '$pass' WHERE usuario = '$usuario' ";
    return getArraySQL($sql);
}


//TipoEv de acuerdo a cadena de valor
function cTipoEv_CadValor($cadValor ){
    $sql = "SELECT tipoEv FROM a_cadValor WHERE cValor = '$cadValor'";
    return getArraySQL($sql);
}

function cTipoEv_Depto($depto ){
    $sql = "SELECT tipoEv FROM a_departamento WHERE depto = '$depto'";
    return getArraySQL($sql);
}

//INSERT EN OPL
function iOPL ($id, $depto, $hallazgo, $accion, $responsable, $soporte, $fIni, $fFin, $respAudit){
    $sql = "INSERT INTO a_opl (idAud, depto, fInicio, fCompromiso, fTerminacion, hallazgo, accion, responsable, soporte, estado, respAudit) VALUES ('$id', '$depto', '$fIni', '$fFin', NULL, '$hallazgo', '$accion', '$responsable', '$soporte', 1,'$respAudit');";
    return getArraySQL($sql);
}

//CONSULTAS DE OPL
function cOplGTotalesAnio ($anio) {
    $sql = "SELECT depto, COUNT(id) FROM a_opl WHERE YEAR(fCompromiso) = '$anio' GROUP by depto ";
    return getArraySQL($sql);
}

function cOplGTotalesAnioMes ($mes, $anio) {
    $sql = "SELECT depto, COUNT(id) FROM a_opl WHERE YEAR(fCompromiso) = '$anio' AND MONTH (fCompromiso) = '$mes' GROUP by depto ";
    return getArraySQL($sql);
}

function cOplGAbiertosAnio ($anio) {
    $sql = "SELECT depto, COUNT(id) FROM a_opl WHERE YEAR(fCompromiso) = '$anio' AND estado < 4 GROUP by depto ";
    return getArraySQL($sql);
} 

function cOplGAbiertosAnioMes ($mes, $anio){
    $sql = "SELECT depto, COUNT(id) FROM a_opl WHERE YEAR(fCompromiso) = '$anio' AND MONTH (fCompromiso) = '$mes' AND estado < 4 GROUP by depto ";
    return getArraySQL($sql);
}

function cOplGCerradosAnio ( $anio) {
    $sql = $sql = "SELECT depto, COUNT(id) FROM a_opl WHERE YEAR(fCompromiso) = '$anio' AND estado = 4 GROUP by depto ";
    return getArraySQL($sql);
} 

function cOplGCerradosAnioMes ($mes, $anio) {
    $sql =  "SELECT depto, COUNT(id) FROM a_opl WHERE YEAR(fCompromiso) = '$anio' AND MONTH (fCompromiso) = '$mes' AND estado = 4 GROUP by depto ";
    return getArraySQL($sql);
}

    //AREA
function cOplATotalesAnioArea ($anio, $depto) {
    $sql = "SELECT depto, MONTH(fCompromiso) mes ,COUNT(id) puntos FROM a_opl WHERE YEAR(fCompromiso) = '$anio' and depto = '$depto' GROUP BY depto, MONTH(fCompromiso) ";
    return getArraySQL($sql);
}

function cOplAAbiertosAnioArea ( $anio, $depto) {
    $sql = "SELECT depto, MONTH(fCompromiso) mes ,COUNT(id) puntos FROM a_opl WHERE YEAR(fCompromiso) = '$anio' and depto = '$depto' AND estado < 4 GROUP BY depto, MONTH(fCompromiso) ";
    return getArraySQL($sql);
}

function cOplACerradosAnioArea ( $anio, $depto) {
    $sql = "SELECT depto, MONTH(fCompromiso) mes ,COUNT(id) puntos FROM a_opl WHERE YEAR(fCompromiso) = '$anio' and depto = '$depto' AND estado = 4 GROUP BY depto, MONTH(fCompromiso)  ";
    return getArraySQL($sql);
}

function cOplATotalesAnioMesArea ($mes, $anio, $depto) {
    $sql = "SELECT DATEPART(week,fCompromiso) semana , COUNT(id) puntos FROM a_opl WHERE YEAR(fCompromiso) = '$anio' AND MONTH(fCompromiso) = '$mes' AND depto = '$depto' GROUP BY DATEPART(week,fCompromiso) ";
    return getArraySQL($sql);
}

function cOplAAbiertosAnioMesArea ($mes, $anio, $depto) {
    $sql = "SELECT DATEPART(week,fCompromiso) semana , COUNT(id) puntos FROM a_opl WHERE YEAR(fCompromiso) = '$anio' AND MONTH(fCompromiso) = '$mes' AND depto = '$depto' AND estado < 4 GROUP BY DATEPART(week,fCompromiso) ";
    return getArraySQL($sql);
}

function cOplACerradosAnioMesArea ($mes, $anio, $depto) {
    $sql = "SELECT DATEPART(week,fCompromiso) semana , COUNT(id) puntos FROM a_opl WHERE YEAR(fCompromiso) = '$anio' AND MONTH(fCompromiso) = '$mes' AND depto = '$depto' AND estado = 4 GROUP BY DATEPART(week,fCompromiso) ";
    return getArraySQL($sql);
}

//LISTA DE OPL
function cListOpl ($fecha, $depto) { 
    //$sql = "SELECT * FROM a_opl WHERE fCompromiso >= '$fecha' AND depto = '$depto' AND estado < 4 ";
    $sql = "SELECT fInicio, fCompromiso, hallazgo, accion, responsable, soporte, estado, SUBSTRING(respAudit, 1, 10) AS resp, id FROM a_opl WHERE fCompromiso >= '$fecha' AND depto = 'ICO' AND estado < 4 ";
    return getArraySQL($sql);
}

//CONSULTAS DE PUNTUACION
function cPuntosAreaMes ($mes, $anio, $depto){
    $sql = "SELECT semana, puntaje, COUNT(id) FROM a_puntajeAuditoria WHERE mes = '$mes' AND anio = '$anio' AND depto = '$depto' GROUP BY semana, puntaje";
    return getArraySQL($sql);
}

function cPuntosAreaAnio ($anio, $depto){
    $sql = "SELECT pra.mes, SUM(pua.puntaje), COUNT(pua.id) FROM a_progAuditoria as pra, a_puntajeAuditoria as pua 
            WHERE pua.depto = '$depto' AND pra.anio = '$anio' AND pua.depto = pra.depAuditado AND pua.id = pra.id GROUP by pra.mes";
    return getArraySQL($sql);
}

//CONSULTA DE ULTIMO ID POR TIPO 
function cLastAuditConfirm (){
    $sql = "SELECT aux FROM a_progAuditoria WHERE id LIKE '%co-%' AND fFin =( SELECT MAX(fFin) FROM a_progAuditoria WHERE id like '%co-%') AND estado = 1; ";
    return getArraySQL($sql);
}

//ULTIMO ID DE CHECKLIST
//function cLastAuditCheckList (){
//    $sql = "SELECT id from a_progAuditoria where id like '%cl-%' AND fFin =( SELECT MAX(fFin) FROM a_progAuditoria WHERE id like '%cl-%') ;";
//    return getArraySQL($sql);
//}
//
////ULTIMO ID DE SEMANAL
//function cLastAuditWeek (){
//    $sql = "SELECT id from a_progAuditoria where id like '%s-%' AND fFin =( SELECT MAX(fFin) FROM a_progAuditoria WHERE id like '%s-%') ;";
//    return getArraySQL($sql);
//}
//
////ULTIMO ID DE MENSUAL
//function cLastAuditMonth (){
//    $sql = "SELECT id from a_progAuditoria where id like '%m-%' AND fFin =( SELECT MAX(fFin) FROM a_progAuditoria WHERE id like '%m-%') ;";
//    return getArraySQL($sql);
//}

//CONSULTA DE ID LIBRE (DISPONIBLE) SOLO APLICA PARA CONFIRMACIONAL
function cIdAuditConfirm ($usuario){
    $sql = "SELECT id FROM a_progAuditoria WHERE auditor = '$usuario' AND id like '%co-%' AND estado = 0; ";
    return getArraySQL($sql);
}

/* MODULO DE CADENA DE VALOR */
//INSERTAR
function iCadValor($tipoEv, $cadValor ){
    $sql = "INSERT INTO a_cadValor (tipoEv, cValor) VALUES ('$tipoEv','$cadValor')";
    return getArraySQL($sql);
}

//CONSULTA
function cCadValor( $cadValor ){
    $sql = "SELECT * FROM a_cadValor WHERE cValor = '$cadValor'";
    return getArraySQL($sql);
}

//MODIFICAR CADENA DE VALOR
function uCadValor( $cadValor, $cValorO ){
    $sql = "UPDATE a_cadValor SET cValor = '$cadValor' WHERE cValor = '$cValorO' ";
    return getArraySQL($sql);    
}

function uCadValorDepto( $cadValor, $cValorO ){
    $sql = "UPDATE a_departamento SET cValor = '$cadValor' WHERE cValor = '$cValorO' ";
    return getArraySQL($sql);    
}

/* MODULO DE DEPARTAMENTOS */
//INSERT
function iDepto($tipoEv, $depto, $descripcion, $cValor ){
    $sql = "INSERT INTO a_departamento (tipoEv, depto, descripcion, cValor) VALUES ('$tipoEv','$depto','$descripcion','$cValor')";
    return getArraySQL($sql);
}

//CONSULTA
function cDepto( $depto ){
    $sql = "SELECT * FROM a_departamento WHERE depto = '$depto' ";
    return getArraySQL($sql);
}

//UPDATE
function uDepto( $tipoEv, $depto, $descripcion, $cValor, $id ){
    $sql = "UPDATE a_departamento SET tipoEv = '$tipoEv', depto = '$depto', descripcion = '$descripcion', cValor = '$cValor' WHERE id = '$id' ";
    return getArraySQL($sql);
}

//ELIMINAR DEPARTAMENTO
function dDepto( $id ){
    $sql = "DElETE FROM a_departamento WHERE id = '$id'";
    return getArraySQL($sql);
}


/******************************************************************************/
function cSupDepto( $depto ) { 
    $sql = "SELECT usuario FROM a_usuarios WHERE depto = '$depto' AND privilegio = 4 ";
    return getArraySQL($sql);
}

function cIngProces( $depto ) { 
    $sql = "SELECT usuario FROM a_usuarios WHERE depto = '$depto' AND privilegio = 5 ";
    return getArraySQL($sql);
}

//function cChampDepto( $depto ) { 
//    $sql = "SELECT usuario FROM a_usuarios WHERE depto = '$depto' AND privilegio = 6 AND estado = 1";
//    return getArraySQL($sql);
//}

/******************************************************************************/

function cLOPLCerrados($depto, $diaI, $diaF) { 
    $sql = "SELECT id FROM a_opl WHERE estado < 4  AND fCompromiso >= '$diaI' AND fCompromiso < '$diaF' AND depto = '$depto' ";
    return getArraySQL($sql);
}

function cLAuditRealizada($depto, $diaI, $diaF) { 
    $sql = "SELECT estado FROM a_progAuditoria WHERE estado = 0 AND fFin >= '$diaI' AND fFin < '$diaF' AND depAuditado = '$depto'";
    return getArraySQL($sql);
}


//NUEVA PROGRAMACXION DE AUDITORIA
function cDeptosWithChamp () {
    $sql = "SELECT d.tipoEv, d.depto FROM a_departamento as d, a_usuarios as u  WHERE d.depto = u.depto AND u.privilegio >= 6 AND u.estado = 1 GROUP BY d.tipoEv, d.depto ORDER BY d.tipoEv, d.depto";
    return getArraySQL($sql);
}

function cChampDepto ($depto) {
    $sql = "SELECT usuario, privilegio FROM a_usuarios WHERE privilegio >= 6 AND estado = 1 AND depto = '$depto' ORDER BY privilegio";
    return getArraySQL($sql);
}

function cDeptosNoRelacionados ($depto, $fecha){
    $sql = "SELECT d.depto FROM a_departamento as d, a_usuarios as u 
WHERE not EXISTS (
	SELECT a.depAuditor FROM a_progAuditoria as a 
	WHERE a.fIni = '$fecha' AND a.tipoAuditoria = 3 and d.depto = a.depAuditor
) AND d.depto <> '$depto' AND  d.depto = u.depto AND u.privilegio >= 6 AND u.estado = 1 GROUP BY d.depto "; 
    return getArraySQL($sql);    
}

function iAuditoria ($id, $fIni, $fFin, $anio, $mes, $nWeek, $depAuditado, $auditado, $depAuditor, $auditor, $tipoAuditoria, $tipoEvaluacion, $aux) {
    //INSERT INTO a_progAuditoria (fIni, fFin, anio, mes, nWeek, depAuditado, auditado, auditor, tipoAuditoria, tipoEvaluacion, estado) VALUES ('$fIni', '$fFin', '$anio', '$mes', '$sem', '$depto', '$auditado', '$auditor', '$tipoAuditoria', '$tipoEv', '0');";
    //echo '<br>', $id,', ',$fIni,', ',$fFin,', ',$anio,', ',$mes,', ',$nWeek,', ',$depAuditado,', ',$auditado,', ',$depAuditor,', ',$auditor,', ',$tipoAuditoria,', ',$tipoEvaluacion,', ',$estado,', ',$aux;
    $sql = "INSERT INTO a_progAuditoria (id, fIni, fFin, anio, mes, nWeek, depAuditado, auditado, depAuditor, auditor, tipoAuditoria, tipoEvaluacion, estado, aux ) VALUES ('$id', '$fIni', '$fFin', '$anio', '$mes', '$nWeek', '$depAuditado', '$auditado', '$depAuditor', '$auditor', '$tipoAuditoria', '$tipoEvaluacion', '0', '$aux')";
    return getArraySQL($sql);
}

function iAuditoriaConfirmacional ($id, $fIni, $fFin, $anio, $mes, $nWeek, $depAuditor, $auditor, $tipoAuditoria, $aux) {
    $sql = "INSERT INTO a_progAuditoria (id, fIni, fFin, anio, mes, nWeek, depAuditor, auditor, tipoAuditoria, estado, aux ) VALUES ('$id', '$fIni', '$fFin', '$anio', '$mes', '$nWeek', '$depAuditor', '$auditor', '$tipoAuditoria', '0', '$aux')";
    return getArraySQL($sql);
}

function listadoAuditores($mes, $anio){
    //$sql = "SELECT prog.auditor, u.nombre , u.correo FROM a_usuarios as u, a_progAuditoria as prog WHERE u.usuario = prog.auditor AND prog.nWeek > '$week' AND prog.anio = '$anio' AND prog.tipoAuditoria = 3 GROUP BY prog.auditor, u.nombre, u.correo";
    $sql = "SELECT u.usuario, u.nombre, u.correo FROM a_progAuditoria as p, a_usuarios as u WHERE p.mes >= '$mes' AND p.anio = '$anio' AND u.usuario = p.auditor AND tipoAuditoria = 3 GROUP by u.usuario, u.nombre, u.correo";
    return getArraySQL($sql);
}

function auditoriasProgramadasUsuario($usuario, $anio, $mes){
    $sql = "SELECT concat(p.fIni,' a ', p.fFin) as period, p.depAuditado, u.nombre FROM a_progAuditoria as p, a_usuarios as u 
            WHERE p.mes >= '$mes' AND p.anio = '$anio' AND u.usuario = p.auditado AND p.tipoAuditoria = 3 AND p.auditor = '$usuario'
            GROUP by concat(p.fIni,' a ', p.fFin) , p.depAuditado, u.nombre, u.correo, p.auditor";
    return getArraySQL($sql);
}

function cAuditAdmin($id) { 
    $sql = "SELECT p.depAuditado, u.nombre, p.tipoAuditoria, p.tipoEvaluacion FROM a_progAuditoria as p, a_usuarios as u WHERE u.usuario = p.auditado AND p.id = '$id'";
    return getArraySQL($sql);
} 

