<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start(); 
    include '../funciones.php';     
    $depto = $_SESSION["deptoAuditoria"]; 
    
    //echo $depto ;
    //$depto = "ICO"; 
    
    # OBTENCION DE FECHA ACTUAL 
    $y = date('Y'); 
    $m = date('m'); 
    $d = date('d'); 
    $fNow = date('Y-m-d'); 
    
    # Obtenemos el día de la semana de la fecha dada
    $diaSemana = date("w",mktime(0,0,0,$m,$d,$y));
    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
    $diaI = date("Y-m-d",strtotime('+1 day',mktime(0,0,0,$m,$d-$diaSemana,$y)));
    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
    $diaF = date("Y-m-d",mktime(0,0,0,$m,$d+(6-$diaSemana),$y)); 
    
    $bnOpl = 1;
    $bnAudit = 1;
    
    # CONSULTA DE OPL CERRADOS
    $cOplCerrados = cLOPLCerrados($depto, $diaI, $diaF);
    //echo " opl: ", count($cOplCerrados);
    if (count($cOplCerrados) > 0 ) { 
        $bnOpl = 0; 
    } 
    
    # CONSULTA DE ULTIMA AUDITORIA
    $cAudit = cLAuditRealizada($depto, $diaI, $diaF);
    //echo " audit: ",count($cAudit) ;
    if (count($cAudit) > 0) { 
        $bnAudit = 0; 
    } 
    
    echo $bnOpl,', ', $bnAudit;
    
    
    