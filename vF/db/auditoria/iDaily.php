<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    
    $fecha = date('Y-m-d');
    $dia = date('d');
    $mes = date('m');    
    $anio = date('Y');
    $semana = date('W');
    
//    echo $dia,', ', $mes,', ', $anio,', ', $semana, '<br>';
    
    $id = $_SESSION["idAuditoria"];
    $depto = $_SESSION["deptoAuditoria"]; 
    $tipoAuditoria = $_SESSION["tipoAuditoria"]; 
    $tipoEv = $_SESSION["tipoEv"];
    
    //CONEXION A LA BASE DE DATOS
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);
    
    //EVALUAMOS SI EL CHECK LIST ESTA LLENO O NO
    if (isset($_POST["checkP1"]) || isset($_POST["checkP2"]) || 
        isset($_POST["checkP3"]) || isset($_POST["checkP4"]) || 
        isset($_POST["checkP5"]) || isset($_POST["checkP6"]) || 
        isset($_POST["checkP7"]) || isset($_POST["checkP8"]) ||
        isset($_POST["checkP9"]) || isset($_POST["checkP10"])) {
        
        //PASAMOS LOS VALORES A VARIABLES PARA PODER HACER EL UPDATE 
        $p1 = $_POST["checkP1"];
        $p2 = $_POST["checkP2"];
        $p3 = $_POST["checkP3"];
        $p4 = $_POST["checkP4"];
        $p5 = $_POST["checkP5"];
        $p6 = $_POST["checkP6"];
        $p7 = $_POST["checkP7"];
        $p8 = $_POST["checkP8"];
        $p9 = $_POST["checkP9"];
        $p10 = $_POST["checkP10"];
        
        //MANDAMOS LOS PUNTOS A LA BASE DE DATOS
        $sum = $p1+$p2+$p3+$p4+$p5+$p6+$p7+$p8+$p9+$p10;

        if ( $sum > 0 ){
            $puntos = $sum * 10;
        } else {
            $puntos = 0;
        }
        
        //INSERT DE VALORES EN LA BASE DE DATOS
        $query = "SELECT id FROM a_puntajeAuditoria WHERE id = '$id'";
        $result = sqlsrv_query($conn, $query);

        if ( $result === false ){
            echo "error <br>";
        } else {
            if ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
                //ACTUALIZAMOS INFORMACION
                //PUNTOS POR PREGUNTA
                $queryIPuntosEv = "UPDATE a_puntosEvaluacion SET p1 = '$p1', p2 = '$p2', p3 = '$p3', p4 = '$p4', p5 = '$p5', p6 = '$p6', p7 = '$p7', p8 = '$p8', p9 = '$p9', p10 = '$p10' WHERE id = '$id' ";
                $resultIPuntosEv = sqlsrv_query($conn, $queryIPuntosEv);
                
                //PUNTAJE TOTAL
                $queryIPuntosAudit= "UPDATE a_puntajeAuditoria SET puntaje = '$puntos' WHERE id = '$id'";
                $resultIPuntosAudit = sqlsrv_query($conn, $queryIPuntosAudit);
                
                
            } else {
                //INSERTAMOS LOS VALORES
                //PUNTOS POR PREGUNTA
                $queryIPuntosEv = "INSERT INTO a_puntosEvaluacion (id, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) VALUES ('$id', '$p1', '$p2', '$p3', '$p4', '$p5', '$p6', '$p7', '$p8', '$p9', '$p10')";
                $resultIPuntosEv = sqlsrv_query($conn, $queryIPuntosEv); 
                
                //PUNTAJE TOTAL
                $queryIPuntos = "INSERT INTO a_puntajeAuditoria (id, depto, tipoEval, tipoAudit, fecha, semana, mes, anio, puntaje) VALUES ('$id','$depto','$tipoEv','$tipoAuditoria','$fecha','$semana','$mes','$anio','$puntos');";
                $resultIPuntos = sqlsrv_query($conn, $queryIPuntos);
            }
            
            //CAMBIAMOS EL ETADO DE LA PROGRAMACION DE LA AUDITORIA
            $queryUAudit = "UPDATE a_progAuditoria SET estado = '1' WHERE id = '$id' ";
            $resultUAudit = sqlsrv_query($conn, $queryUAudit);
            
        }
    } else {
?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php 
            echo "REVISAR: TODOS LOS PUNTOS A EVALUAR DEBEN ESTAR MARCADOS";
        ?> </strong>     
    </div>
<?php
    }



