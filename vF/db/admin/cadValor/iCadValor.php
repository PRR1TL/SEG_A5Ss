<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../funciones.php';

    if (isset($_POST["depto"]) && isset($_POST["acronimo"])){
        $tipoEv = $_POST["depto"];
        $cadValor = strtoupper($_POST["acronimo"]);
        
        //CONSULTA PARA QUE NO SE REPITA EL ACRONIMO
        $cCadValor = cCadValor($cadValor);        
        if (count($cCadValor) == 0){
            //INSERT DE DATOS
            iCadValor($tipoEv, $cadValor);
            echo "Bien";
        } else {
            $errors[]="AL PARECER EL ACRONIMO DE LA CADENA DE VALOR YA EXISTE";
        }
    } else {
        $errors[] = "TODOS LOS CAMPOS DEBEN ESTAR LLENOS";
    }    

if (isset($errors)){
?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
    </div>
<?php } ?>