<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../funciones.php';

    if (isset($_POST["acronimoU"])){
        $cadValor = strtoupper(trim($_POST["acronimoU"]));
        $cadValorO = strtoupper($_POST["acronimoU2"]);        
                
        if (empty($cadValor)){
            $errors[] = "NO SE PUEDE HACER EL CAMBIO, REVISAR ACRONIMO";
        } else {
            //CONSULTAMOS QUE NO EXISTA LA CADENA DE VALOR
            $cCadValor = cCadValor($cadValor);        
            if (count($cCadValor) == 0){
                //MANDAMOS LA MOFICIACION A LA BASE DE DATOS
                //MODIFICACION DEPARTAMENTO
                uCadValorDepto($cadValor, $cadValorO);

                //MODIFICACION CADENA DE VALOR
                uCadValor($cadValor, $cadValorO);
                echo "Bien";
            } else {
                $errors[] = "NO SE PUEDE HACER EL CAMBIO, AL PARECER LA CADENA DE VALOR YA EXISTE";
            }
        }
    } else {
        $errors[] = "NO SE PUEDE HACER EL CAMBIO, REVISAR ACRONIMO";
    }
    
    if (isset($errors)) { 
?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
    </div>

    <?php }
    
    
    