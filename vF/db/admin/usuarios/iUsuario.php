<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../funciones.php';

    //RECIBIMOS LOS PARAMETROS DE LOS CAMPOS DEL FORMULARIO    
    if (isset($_POST["privilegio"])){
        $privilegio = $_POST["privilegio"];
        
        switch ($privilegio){
            case 1:
                $tipoAdmin = $_POST["tipoA"];
                if ($tipoAdmin == 1){
                    $tipoEv = 6;
                } else {
                    $tipoEv = 5;
                }

                $depto = 'VSE';
                break;
                
            case 2: 
                $tipoEv = $_POST["areaSIU"];
                if ($_POST["cadValorIU"] == '-'){
                    $depto = $_POST["deptoSIU"];
                    $tEv = cTipoEv_Depto($depto);
                    for ($i = 0; $i < count($tEv); $i++){
                        $tipoEv = $tEv[$i][0];
                    }
                } else {
                    $depto = $_POST["cadValorIU"];
                    $tEv = cTipoEv_CadValor($depto);
                    for ($i = 0; $i < count($tEv); $i++){
                        $tipoEv = $tEv[$i][0];
                    }                
                }            
                break;
                
            case 3:
                $tipoEv = $_POST["areaCh"];            
                $depto = $_POST["deptoCh"];             
                            
                break;
        }
        
        $usuario = strtoupper($_POST["usuarioI"]); 
        $nombre = $_POST["nombreI"]; 
        $correo = $_POST["correoI"]; 
        $pass = $_POST["passI"]; 
        
        if (empty($tipoEv)){
            $errors []= "SE DEBE SELECCIONAR UNA ÁREA";
        } 
        if (empty ($depto)){
            $errors []= "SE DEBE SELECCIONAR UN DEPARTAMENTO";
        } 
        if (empty ($usuario)){
            $errors []= "SE DEBE INGRESAR UN USUARIO";
        } else {
            $cUsuario = cUsuario($usuario);
            if (count($cUsuario) > 0){
               $errors []= "EL USUARIO YA EXISTE"; 
            }
        }
        if (empty ($nombre)){
            $errors []= "SE DEBE INGRESAR NOMBRE DEL USUARIO";
        } 
        if (empty ($correo)){
            $errors []= "SE DEBE INGRESARR CORREO";
        } 
        if (empty ($pass)){
            $errors []= "SE DEBE INGRESAR UNA CONTRASEÑA";
        }   
    } else {
        $errors []= "SE DEBE SELECCIONAR PRIVILEGIO";
    }
    
    if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong> 
        </div>
<?php } else {     
    //MANDAMOS EL INSERT A LA BASE DE DATOS 
    iUsuario($tipoEv, $depto, $usuario, $nombre, $correo, $pass, $privilegio);
    echo "Bien," ;
    //echo $privilegio,': ',$tipoEv,', ', $depto,', ', $usuario,', ', $nombre,', ',$correo,', ',$pass;    
    ?>
        <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php  echo "REGISTRO GUARDADO CORRECTAMENTE",'<BR>'; ?> </strong>     
        </div>
<?php

    switch ($privilegio){
        case '1':
            echo ", ./admin.php";
            break;
        case '2':
            echo ", ./gerentes.php";
            break;
        case '3':
            echo ", ./champions.php";
            break;        
    }

}
    
    
    
    
