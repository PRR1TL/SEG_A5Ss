<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    include '../../funciones.php';
    
    $usuario = strtoupper($_POST["usuarioSChamp"]);
    $nombre = strtoupper($_POST["nombreSChamp"]);
    $correo = strtolower($_POST["correoSChamp"]);
    $pass = strtolower($_POST["passSChamp"]);
    //$depto = "ICO";
    $depto = strtoupper($_SESSION["depto"]);
    
    if (!isset($usuario) || !isset($nombre) || !isset($correo) || !isset($pass) || !isset($depto)) {
        $errors []= "SE DEBEN LLENAR TODOS LOS DATOS ";
    } 
    
    //CONSULTA SI EL USUARIO EXISTE
    $cUsuario = cUsuario($usuario); 
    if (count($cUsuario) > 0) { 
        $errors []= "EL USUARIO QUE DESEA AGREGAR YA EXITE"; 
    } 
    
    if (isset($errors)) { 
?>
        <div class="alert alert-danger" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong>
            <?php foreach ($errors as $error) { 
                echo $error,'<BR>'; 
            } ?> 
        </strong> 
        </div> 
<?php 
    } else {     
        //MANDAMOS EL INSERT A LA BASE DE DATOS 
        iUsuario(5, $depto, $usuario, $nombre, $correo, $pass, 7);
?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>USUARIO AGREGADO CORRECTAMENTE</strong>     
        </div>
        <script>
            location.href = "<?php echo "./aChampion.php" ;?>"; 
        </script> 
        
<?php    }

?>