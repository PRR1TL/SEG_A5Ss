<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../funciones.php';

    //RECIBIMOS LOS PARAMETROS DE LOS CAMPOS DEL FORMULARIO 
    if (isset($_POST["privilegioU"])) {
        $privilegio = $_POST["privilegioU"]; 
        switch ($privilegio){
            case '1':
                $tipoAdmin = $_POST["tipoAU"];
                if ($tipoAdmin == 1) {
                    $tipoEv = 6; 
                } else {
                    $tipoEv = 5; 
                }
                $depto = 'VSE'; 
                break;
                
            case '2': 
                if (isset($_POST["cadValorSU"])){
                    if ($_POST["cadValorSU"] == 'N/A' || $_POST["cadValorSU"] == '-') {                        
                        $depto = $_POST["deptoSUU"]; 
                        $tEv = cTipoEv_Depto($depto); 
                        for ($i = 0; $i < count($tEv); $i++) {
                            $tipoEv = $tEv[$i][0];
                        }
                    } else {
                        $depto = $_POST["cadValorSU"];
                        $tEv = cTipoEv_CadValor($depto);
                        for ($i = 0; $i < count($tEv); $i++){
                            $tipoEv = $tEv[$i][0];
                        }                
                    }
                } else {
                    $depto = $_POST["deptoSUO"];
                    $tEv = cTipoEv_Depto($depto);
                    //EVALUAMOS SI EL ACRONIMO PERTENECE A DEPARTAMENTO O CADENA DE VALOR
                    if (count($tEv) > 0){
                        for ($i = 0; $i < count($tEv); $i++){
                            $tipoEv = $tEv[$i][0];
                        } 
                    } else {
                        $tEv = cTipoEv_CadValor($depto);
                        for ($i = 0; $i < count($tEv); $i++){
                            $tipoEv = $tEv[$i][0];
                        } 
                    }                    
                }
                                         
                break;
                
            case '3':
                //$tipoEv = $_POST["areaChU"];            
                $depN = $_POST["deptoChU"];           
                $depO = $_POST["deptoChO"];
                
                if ($depN == '0'){
                    $depto = $depO;
                } else {
                    $depto = $depN;
                }
                
                $tEv = cTipoEv_Depto($depto);
                for ($i = 0; $i < count($tEv); $i++){
                    $tipoEv = $tEv[$i][0];
                }
                
                break;
        }
        
        $estado = $_POST["estadoU"];
        $usuario = strtoupper($_POST["usuarioU"]);
        $nombre = strtoupper($_POST["nombreU"]);
        $correo = strtolower($_POST["correoU"]);
        $pass = strtolower($_POST["passU"]);
        
        if (empty($tipoEv)){
            $errors []= "SE DEBE SELECCIONAR UNA ÁREA";
        } 
        if (empty ($depto)){
            $errors []= "SE DEBE SELECCIONAR UN DEPARTAMENTO";
        } 
        
        if (empty ($nombre)){
            $errors []= "SE DEBE INGRESAR NOMBRE DEL USUARIO";
        } 
        if (empty ($correo)){
            $errors []= "SE DEBE INGRESARR CORREO";
        } 
          
    } else {
        $errors []= "SE DEBE SELECCIONAR PRIVILEGIO";
    }
    
    if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong> 
        </div>
<?php } else {     
    //MANDAMOS EL INSERT A LA BASE DE DATOS 
    if (empty ($pass)){
        //UPDATE DE DATOS SIN CONTRASEÑA
        uUsuarioSinPass($tipoEv, $depto, $nombre, $correo, $privilegio, $estado, $usuario);
        //echo '1. ',$privilegio,': ',$tipoEv,', ', $depto,', ', $usuario,', ', $nombre,', ',$correo,', ',$pass,', ',$estado; 
    } else {
        //UPDATE DE DATOS CON CONTRASEÑA
        uUsuarioFull($tipoEv, $depto, $nombre, $correo, $pass, $privilegio, $estado, $usuario);
        //echo '2. ',$privilegio,': ',$tipoEv,', ', $depto,', ', $usuario,', ', $nombre,', ',$correo,', ',$pass,', ',$estado; 
    }
    
    echo "Bien," ;
    
    switch ($privilegio){
        case '1':
            echo "./admin.php";
            break;
        case '2':
            echo "./gerentes.php";
            break;
        case '3':
            echo "./champions.php";
            break;        
    }

}
    
    
    
    
