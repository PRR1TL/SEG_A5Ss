<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    session_start();
    include '../../funciones.php';

    //RECIBIMOS LOS PARAMETROS DE LOS CAMPOS DEL FORMULARIO 
    
    $estado = $_POST["estadoSChamp"];
    $privilegio = $_POST["privilegioSChamp"];
    $usuario = strtoupper($_POST["usuarioSChampU"]);
    $nombre = strtoupper($_POST["nombreSChampU"]);
    $correo = strtolower($_POST["correoSChampU"]);
    $pass = strtolower($_POST["passSChampU"]);
    $depto = strtoupper($_SESSION["depto"]);

    if (empty ($nombre)){
        $errors []= "SE DEBE INGRESAR NOMBRE DEL USUARIO";
    } 
    if (empty ($correo)){
        $errors []= "SE DEBE INGRESARR CORREO";
    } 
    
    if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong> 
        </div>
<?php } else {     
    //MANDAMOS EL INSERT A LA BASE DE DATOS 
    if (empty ($pass)){
        //UPDATE DE DATOS SIN CONTRASEÑA
        uUsuarioSinPass(5, $depto, $nombre, $correo, $privilegio, $estado, $usuario);
        //echo '1. ',$privilegio,': ',$tipoEv,', ', $depto,', ', $usuario,', ', $nombre,', ',$correo,', ',$pass,', ',$estado; 
    } else {
        //UPDATE DE DATOS CON CONTRASEÑA
        uUsuarioFull(5, $depto, $nombre, $correo, $pass, $privilegio, $estado, $usuario);
        //echo '2. ',$privilegio,': ',$tipoEv,', ', $depto,', ', $usuario,', ', $nombre,', ',$correo,', ',$pass,', ',$estado; 
    }
    
    echo "Bien," ;
    
    switch ($privilegio){
        case '1':
            echo "./admin.php";
            break;
        case '2':
            echo "./gerentes.php";
            break;
        case '3':
            echo "./champions.php";
            break;        
    }

}
    
    
    
    
