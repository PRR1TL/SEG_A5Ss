<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../db/funciones.php';

    if (isset($_REQUEST["depto"]) && !empty($_REQUEST["depto"])){
        $tipoEv = $_REQUEST["depto"];
    } else {
        $errors []= "SE DEBE SELECCIONAR UNA OPCION DE TIPO DE AREA";
    }
    
    if (isset($_REQUEST["acronimo"]) && !empty($_REQUEST["acronimo"])){
        $cadValor = $_REQUEST["acronimo"];
    } else {
        $errors []= "SE DEBE INGRESAR UN ACRONIMO";
    }
    
    if (isset($errors)){
?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
        </div>
<?php } else { 
        iCadValor($tipoEv, $cadValor);        
?>
        <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php 
            echo "REGISTRO GUARDADO CORRECTAMENTE",'<BR>';
        ?> </strong>     
        </div>
<?php 
}    
    