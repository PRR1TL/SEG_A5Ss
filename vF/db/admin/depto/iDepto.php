<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../funciones.php';

    if (isset($_POST["areaDep"]) && isset($_POST["cadValorDep"]) && isset($_POST["deptoU"]) && isset($_POST["descU"]) ){
        $tipoEv = $_POST["areaDep"];
        $cadValor = $_POST["cadValorDep"];
        $acronimo = strtoupper($_POST["deptoU"]);
        $desc = $_POST["descU"];
        
        $ac = trim($acronimo);
        $ds = trim($desc);
        
        if (empty($ds)) {
            $desc = '-';
        }
        
        if (!empty($ac)){
            //CONSULTA PARA QUE NO SE REPITA EL ACRONIMO
            $cDepto = cDepto($acronimo); 
            if (count($cDepto) == 0){
                //INSERT DE DATOS
                iDepto($tipoEv, $acronimo, $desc, $cadValor);
                echo "Bien";
            } else {
                $errors[]="AL PARECER EL ACRONIMO DE LA CADENA DE VALOR YA EXISTE";
            }
        } else {
            $errors[]="AL PARECER EL ACRONIMO DE LA CADENA DE VALOR YA EXISTE";
        }
        
    } else {
        $errors[] = "TODOS LOS CAMPOS DEBEN ESTAR LLENOS";
    }    

if (isset($errors)){
?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
    </div>
<?php } ?>