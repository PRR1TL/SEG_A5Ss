<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../funciones.php';

    if (isset($_POST["areaDepU"]) && isset($_POST["cadValorDepU"]) && isset($_POST["deptoU"]) && isset($_POST["descU"]) ){
        $id = $_POST["idU"];
        $acronimoO = $_POST["acronimo"];
        
        $tipoEv = $_POST["areaDepU"];
        $cadValor = $_POST["cadValorDepU"];
        $acronimo = strtoupper($_POST["deptoU"]);
        $desc = $_POST["descU"];
        
        $ac = trim($acronimo);
        $ds = trim($desc);
        
        if (empty($ds)){
            $desc = '-';
        }
        
        //DESCRIPCION PUEDE QUEDAR VACIA PERO NO EL ACRONIMO 
        if (!empty($ac)){
            //CONSULTA PARA QUE NO SE REPITA EL ACRONIMO
            if ($acronimo == $acronimoO){
                 uDepto($tipoEv, $acronimo, $desc, $cadValor, $id);
            } else {            
                $cDepto = cDepto($acronimo);        
                if (count($cDepto) == 0){
                    //UPDATE
                    uDepto($tipoEv, $acronimo, $desc, $cadValor, $id);
                } else {
                    $errors[]="AL PARECER EL ACRONIMO DE LA CADENA DE VALOR YA EXISTE";
                }
            }
        } else {
            $errors[]="AL PARECER EL ACRONIMO DE LA CADENA DE VALOR YA EXISTE";
        }
        
    } else {
        $errors[] = "TODOS LOS CAMPOS DEBEN ESTAR LLENOS";
    }    

if (isset($errors)){
?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error,'<BR>';
        } ?> </strong>     
    </div>
<?php } else { 
    echo "Bien"; 
} ?>