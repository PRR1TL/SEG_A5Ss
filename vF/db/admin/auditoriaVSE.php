<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../db/funciones.php';
    session_start();
    
    $today = date("Y-m-d");
    $y = date("Y");
    $m = date("m");
    $w = date('W');
    
    $userName = $_SESSION['userName'];
    $tipoEv = $_POST['typEvaluacion'];
        
    if (isset($_POST["areaSVSE"]) && isset($_POST["deptoSVSE"]) && isset($_POST["usuarioSVSE"])){
        $area = $_POST["areaSVSE"];
        $depto = $_POST["deptoSVSE"];
        $auditado = $_POST["usuarioSVSE"];
        //INCIAMOS LA PARTE DE REGISTRO DE ID DE LA AUDITORIA
        //CONSULTAMOS SI EXITE UN ID LIBRE (DISPONIBLE)
        $cIdAuditConfirm = cIdAuditConfirm($userName);
        
        if (count($cIdAuditConfirm) > 0 ){ //CUANDO SE TIENE UN ID SIN OCUPAR        
            $id = $cIdAuditConfirm[0][0]; 
            
            //HACEMOS EL UPDATE DE LA AUDITORIA
            uAuditoriaSup_Admin($today, $today, $y, $m, $w, $depto, $auditado, $id);
            
        } else {//SI NO SE TIENE ID LIBRE 
            //HACE LA CONSULTA DEL ULTIMO ID DE LA AUDITORIA CONFIRMACIONAL
            $lastIdConfirm = cLastAuditConfirm();

            if(count($lastIdConfirm) > 0 ){
                //CORTAMOS EL ID QUE OBTENEMOS POR QUE TRAE LAS LETRAS QUE LO DISTINGUE
                $cLastId = split ("-", $lastIdConfirm[0][0]); 
                $lastId = $cLastId[1];
                $id = 'CO-'.$lastId+1;
            } else {
                $id = 'CO-0';
            }
            
            //HACEMOS EL INSERT DEL NUEVO ID
            iProgAuditoria($id, $today, $today, $y, $m, $w, $depto, $_POST["usuarioSVSE"], $userName, 4, $tipoEv);
            
        }
        //CREAMOS LAS SESIONES PARA LA AUDITORIA, PARA PODER USARLAS EN EL PROCESO
        $_SESSION["tipoEv"] = $tipoEv;    
        $_SESSION["idAuditoria"] = $id;
        $_SESSION["deptoAuditado"] = $depto;
        
        echo 'Bien';
        
    } else{
    ?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong> DEBEN LLENARSE TODO LOS DATOS DE LA AUDITORIA A REALIZAR </strong>
        </div>
    <?php
    } 
        
    
    
    
    
    
    
    