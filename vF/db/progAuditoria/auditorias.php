<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//CONEXION A LA BASE DE DATOS
include '../funciones.php';

    $date = new DateTime;
    $f = date('2018-11-03');
    //$f = date('Y-m-d');
    $year = date("Y", strtotime('+1 day',strtotime($f))); 
    $nuevafecha = date ('m/d/Y' , strtotime('+1 day',strtotime($f)));
    
    # Establecemos la fecha segun el estandar ISO 8601 (numero de semana)
    $date->setISODate("$year", 53);
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
    if($date->format("W") == 53){
        $numSemanas = 53;
    }else{
        $numSemanas = 52;
    }

    for($i = 10; $i < $numSemanas+1; $i++ ){
        $sem[$i] = + $i;
        for($day = 0; $day < 7; $day++){
            if ($day == 0){
                $dISemana[$i] = date('Y-m-d', strtotime($year."W".$sem[$i].$day));
            } else if ($day == 6){
                $dFSemana[$i] = date('Y-m-d', strtotime($year."W".$sem[$i].$day));
            }
        }
    }
    
    //PRIMERAS AUDITORIAS
    $iFijo = $dISemana[10];
    $fFijo = $dFSemana[10];
    //PARTIMOS DE LA AUDITORIA 10 Y DE AHÍ RESTAMOS DIAS
    for($i = 1; $i < 10; $i++ ){
        $sem[$i] = $i;
        $d = 7*$i; 
        for($day = 0; $day < 7; $day++){
            if ($day == 0){                
                $dISemana[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.' day', strtotime ($iFijo)));
            } else if ($day == 6){
                $dFSemana[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.' day', strtotime ($fFijo)));
            }
        }
    }
    
    $fecha = new DateTime(date($nuevafecha));
    $semana = $fecha->format('W');     
    $bnCont = 0;
    
    //echo $semana,' de ', $numSemanas;
    //CADA CUATRO SEMANAS SE VA ACTUALIZAR LA PROGRAMACION DE LAS AUDITORIAS
    if (($sem[$semana] % 4) == 0){
        $lastAuditProgram = cLastAuditProgram($year);
        if ($lastAuditProgram[0][0] <= $sem[$semana]){
            //SE HACE LA PROGRAMACION DE LA AUDITORIA
            if ($sem[$semana] != 52 ){
                /**** PROGRAMACION DE LAS SIGUIENTES 4 SEMANAS A LA ACTUAL ****/               
                $listDeptos = cDeptosArea();
                //echo count($listDeptos),'<br>';                
                for($i = 0; $i < count($listDeptos); $i++){
                    $tipoEv[$i] = $listDeptos[$i][0];
                    $depto[$i] = $listDeptos[$i][1];
                    $cUserDepto = cUsuarioDeptos($depto[$i]);    
                    //echo $depto[$i],'<br>';
                    if (count($cUserDepto) > 0 ){
                        echo '<br>',$i,': ',$tipoEv[$i],', ',$depto[$i],'( ',count($cUserDepto),'): ';
                        /* PROG. AUDITORIA SEMANAL (CHAMPIONS / TEAM LEADERS) */
                        switch (count($cUserDepto)){
                            case 1;     
                                for ($j = 0; $j < count($cUserDepto); $j++  ){
                                    $auditado[$i][$j+1] = $cUserDepto[$j][0];
                                    for ($x = 1; $x < 5; $x++){                        
                                        $mes[$i] =  (int) date("m",strtotime($dISemana[$semana+$x])); 
                                        iProgAuditoria($dISemana[$semana+$x],$dFSemana[$semana+$x], $year, $mes[$i], $sem[$semana+$x], $depto[$i], $auditado[$i][1], $cUserDepto[$j][0], 1, $tipoEv[$i],0);
                                    }
                                    if ($tipoEv[$i] < 5){
                                        $uAuditoriaChamp = cUsuarioPAuditoriaChampionPiso($auditado[$i][1], $year, $dISemana[$semana+2]);                    
                                        for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                            $auditor[$j][$x] = $uAuditoriaChamp[$x][0];
                                        } 

                                        $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4]));
                                        $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                        //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                        if(isset($auditor[$j][$vRandom])){
                                            iProgAuditoria($dISemana[$semana+2],     $dFSemana[$semana+4],     $year,     $mesS[$i],     $sem[$semana+4],     $depto[$i],     $auditado[$i][1],           $auditor[$j][$vRandom],     2,     $tipoEv[$i],0);
                                        }
                                    } else if ($tipoEv[$i] == 5){
                                        $uAuditoriaChamp = cUsuarioPAuditoriaChampionOficina($auditado[$i][1], $year, $dISemana[$semana+2]);                    
                                        for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                            $auditor[$j][$x] = $uAuditoriaChamp[$x][0];
                                        } 

                                        $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4]));
                                        $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                        //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                        if(isset($auditor[$j][$vRandom])){
                                            iProgAuditoria($dISemana[$semana+2],     $dFSemana[$semana+4],     $year,     $mesS[$i],     $sem[$semana+4],     $depto[$i],     $auditado[$i][1],     $auditor[$j][$vRandom], 2, $tipoEv[$i],0);
                                        }
                                    }
                                }
                                break;

                            case 2; 
                                $cUAuditado = cUltimoUsuarioAuditadoDepto($depto[$i], $dISemana[$semana-2]);
                                for ($c = 0; $c < count($cUAuditado); $c++ ){
                                    $lUsuarioM = $cUAuditado[$c][0];
                                }
                                
                                for ($j = 0; $j < count($cUserDepto); $j++  ){
                                   $auditado[$j+1] = $cUserDepto[$j][0];
                                   if(isset($lUsuarioM)) {
                                        if ($lUsuarioM != $auditado[$j+1] ){
                                            $auditadoM = $auditado[$j+1];
                                        }
                                    } else {
                                        $auditadoM = $auditado[1];
                                    }
                                }
                                
                                for ($x = 1; $x < 5; $x++){                        
                                    $mes[$i] =  (int) date("m",strtotime($dISemana[$semana+$x])); 
                                    if (($x % 2) == 1) { //impar
                                        iProgAuditoria($dISemana[$semana+$x],$dFSemana[$semana+$x], $year, $mes[$i], $sem[$semana+$x], $depto[$i], $auditado[1], $auditado[1], 1, $tipoEv[$i],0);
                                    } else {//par
                                        iProgAuditoria($dISemana[$semana+$x],$dFSemana[$semana+$x], $year, $mes[$i], $sem[$semana+$x], $depto[$i], $auditado[2], $auditado[2], 1, $tipoEv[$i],0);
                                    } 
                                }
                                
                                //TRAEMOS EL ULTIMO AUDITADO DE FORMA MENSUAL DEL DEPARTAMENTO       
                                if($tipoEv[$i] < 5){
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionPiso($auditadoM, $year, $dISemana[$semana+2]);       
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana[$semana+2],$dFSemana[$semana+4], $year, $mesS[$i], $sem[$semana+4], $depto[$i], $auditadoM, $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    } 
                                } else if ($tipoEv[$i] == 5 ){
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionOficina($auditadoM, $year, $dISemana[$semana+2]);
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana[$semana+2],$dFSemana[$semana+4], $year, $mesS[$i], $sem[$semana+4], $depto[$i], $auditado[$j], $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    } 
                                } 
                                break;

                            case 3: 
                                $bnCont = 0;
                                $cUltimoUsuario = cUltimoUsuarioAuditorDeptos($depto[$i], $dISemana[$semana]);                                    
                                for ($c = 0; $c < count($cUltimoUsuario); $c++ ){
                                    $lUsuario = $cUltimoUsuario[$c][0];
                                }  

                                for ($j = 0; $j < count($cUserDepto); $j++  ){                                        
                                    $auditor[$j+1] = $cUserDepto[$j][0];
                                    if(isset($lUsuario) && $lUsuario == $auditor[$j+1] ){
                                       $bnCont = $j+1;
                                    } 
                                }
                                
                                switch ($bnCont){
                                    case 0:
                                        $uAuditor[1] = $auditor[1];
                                        $uAuditor[2] = $auditor[2];
                                        $uAuditor[3] = $auditor[3];
                                        break;
                                    case 1:
                                        $uAuditor[1] = $auditor[2];
                                        $uAuditor[2] = $auditor[3];
                                        $uAuditor[3] = $auditor[1];
                                        break;
                                    case 2:
                                        $uAuditor[1] = $auditor[3];
                                        $uAuditor[2] = $auditor[1];
                                        $uAuditor[3] = $auditor[2];
                                        break;
                                    case 3:
                                        $uAuditor[1] = $auditor[1];
                                        $uAuditor[2] = $auditor[2];
                                        $uAuditor[3] = $auditor[3];
                                        break;
                                }
                                
                                $uAuditor[4] = $auditor[1];

                                for ($x = 1; $x < 5; $x++){                        
                                    $mes[$i] =  (int) date("m",strtotime($dISemana[$semana+$x])); 
                                    iProgAuditoria($dISemana[$semana+$x],$dFSemana[$semana+$x], $year, $mes[$i], $sem[$semana+$x], $depto[$i], $uAuditor[$x], $uAuditor[$x], 1, $tipoEv[$i], 0);
                                }

                                if ($tipoEv[$i] < 5 ){
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionPiso($auditor[3], $year, $dISemana[$semana+2]);       
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana[$semana+2],$dFSemana[$semana+4], $year, $mesS[$i], $sem[$semana+4], $depto[$i], $uAuditor[3], $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    }
                                } else if ($tipoEv == 5) {
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionOficina($auditor[3], $year, $dISemana[$semana+2]);       
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana[$semana+2],$dFSemana[$semana+4], $year, $mesS[$i], $sem[$semana+4], $depto[$i], $uAuditor[3], $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    }
                                }          
                                break;

                            case 4;   
                                $bnCont = 0;
                                $cUAuditado = cUltimoUsuarioAuditadoDepto($depto[$i], $dISemana[$semana-2]);
                                for ($c = 0; $c < count($cUAuditado); $c++ ){
                                    $lUsuarioM = $cUAuditado[$c][0];
                                }
                                
                                for ($j = 0; $j < count($cUserDepto); $j++  ){
                                    $auditado[$j+1] = $cUserDepto[$j][0];
                                    if(isset($lUsuarioM) && $lUsuarioM != $auditado[$j+1] ){
                                        $bnCont = $j+1;
                                    }                                    
                                }
                                
                                for ($x = 1; $x < 5; $x++){                        
                                    $mes[$i] =  (int) date("m",strtotime($dISemana[$semana+$x])); 
                                    iProgAuditoria($dISemana[$semana+$x],     $dFSemana[$semana+$x],     $year,     $mes[$i],      $sem[$semana+$x],     $depto[$i], $auditado[$x], $auditado[$x], 1, $tipoEv[$i], 0);
                                }
                                
                                switch ($bnCont){
                                    case 0:
                                        $uAuditor = $auditado[1];
                                        break;
                                    case 1:
                                        $uAuditor = $auditado[2];
                                        break;
                                    case 2:
                                        $uAuditor = $auditado[3];
                                        break;
                                    case 3:
                                        $uAuditor = $auditado[4];
                                        break;
                                    case 4:
                                        $uAuditor = $auditado[1];
                                        break;
                                }
                                
                                if ($tipoEv[$i] < 5 ){
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionPiso($uAuditor, $year, $dISemana[$semana+2]);       
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana[$semana+2],$dFSemana[$semana+4], $year, $mesS[$i], $sem[$semana+4], $depto[$i], $uAuditor, $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    }
                                } else if ($tipoEv[$i] == 5){ 
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionOficina($uAuditor, $year, $dISemana[$semana+2]);      
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana[$semana+2],$dFSemana[$semana+4], $year, $mesS[$i], $sem[$semana+4], $depto[$i], $uAuditor, $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    }
                                }                                
                                break;
                        }
                    }
                } 
            } else {
                //AQUI ENTRAMOS PARA EL PROXIMO AÑO LAS PRIMERAS SEMANAS                
                //PRIMERAS AUDITORIAS
                    //SOLO METEMOS LA PROGRAMACION DE LA ULTIMA SEMANA (53) DE ESE AÑO
                    $year2 = date("Y",strtotime ( '+1 year', strtotime(date($f)))); 
                    $date = new DateTime;
                    # Establecemos la fecha segun el estandar ISO 8601 (numero de semana)
                    $date->setISODate("$year2", 53);
                    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
                    if($date->format("W") == 53){
                        $numSemanas2 = 53;
                    }else{
                        $numSemanas2 = 52;
                    }

                    for($i = 10; $i < $numSemanas+1; $i++ ){
                        $sem2[$i] = + $i;
                        for($day = 0; $day < 7; $day++){
                            if ($day == 0){
                                $dISemana2[$i] = date('Y-m-d', strtotime($year2."W".$sem2[$i].$day));
                            } else if ($day == 6){
                                $dFSemana2[$i] = date('Y-m-d', strtotime($year2."W".$sem2[$i].$day));
                            }
                        }
                    }                    
                    
                    $iFijo2 = $dISemana2[10];
                    $fFijo2 = $dFSemana2[10];
                    
                    //PARTIMOS DE LA AUDITORIA 10 Y DE AHÍ RESTAMOS DIAS
                    for($i = 1; $i < 10; $i++ ){
                        $sem2[$i] = $i;
                        $d = 7*$i; 
                        for($day = 0; $day < 7; $day++){
                            if ($day == 0){                
                                $dISemana2[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.' day', strtotime ($iFijo2)));
                            } else if ($day == 6){
                                $dFSemana2[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.' day', strtotime ($fFijo2)));
                            }
                        }
                    }                    
                
                /************* PROGRAMACION DE AUDITORIAS EN PISO *************/
                $listDeptos = cDeptosArea();
                for($i = 0; $i < count($listDeptos); $i++){
                    $tipoEv[$i] = $listDeptos[$i][0];
                    $depto[$i] = $listDeptos[$i][1];
                    $cUserDepto = cUsuarioDeptos($depto[$i]);                      
                    if (count($cUserDepto) > 0 ){
                        /* PROG. AUDITORIA SEMANAL (CHAMPIONS / TEAM LEADERS) */
                        switch (count($cUserDepto)){
                            case 1;     
                                for ($j = 0; $j < count($cUserDepto); $j++  ){
                                    $auditado[$i][$j+1] = $cUserDepto[$j][0];                                    
                                    for ($x = $sem[$semana]+1; $x <= $numSemanas ; $x++ ){
                                        $mes[$x] =  (int) date("m",strtotime($dISemana[$x])); 
                                        iProgAuditoria($dISemana[$x],$dFSemana[$x], $year, $mes[$x], $sem[$x], $depto[$i], $auditado[$i][1], $cUserDepto[$j][0], 1, $tipoEv[$i],0);                                            
                                    }

                                    for ($x = 1; $x < 5; $x++){                        
                                        $mes[$i] =  (int) date("m",strtotime($dISemana2[$x])); 
                                        iProgAuditoria($dISemana2[$x],$dFSemana2[$x], $year2, $mes[$i], $sem2[$x], $depto[$i], $auditado[$i][1], $cUserDepto[$j][0], 1, $tipoEv[$i],0);
                                    }

                                    if ($tipoEv[$i] < 5){
                                        $uAuditoriaChamp = cUsuarioPAuditoriaChampionPiso($auditado[$i][1], $year2, $dISemana2[2]);                    
                                        for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                            $auditor[$j][$x] = $uAuditoriaChamp[$x][0];
                                        } 

                                        $mesS[$i] = (int) date("m",strtotime($dISemana2[4]));
                                        $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                        //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                        if(isset($auditor[$j][$vRandom])){
                                            iProgAuditoria($dISemana2[2],     $dFSemana2[4],     $year2,     $mesS[$i],     $sem[4],     $depto[$i],     $auditado[$i][1],           $auditor[$j][$vRandom],     2,     $tipoEv[$i],0);
                                        }
                                    } else if ($tipoEv[$i] == 5){
                                        $uAuditoriaChamp = cUsuarioPAuditoriaChampionOficina($auditado[$i][$j+1], $year2, $dISemana2[2]);                    
                                        for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                            $auditor[$j][$x] = $uAuditoriaChamp[$x][0];
                                        } 

                                        $mesS[$i] = (int) date("m",strtotime($dISemana2[4]));
                                        $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                        //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                        if(isset($auditor[$j][$vRandom])){
                                            iProgAuditoria($dISemana2[2],     $dFSemana2[4],     $year2,     $mesS[$i],     $sem[4],     $depto[$i],     $auditado[$i][$j+1],     $auditor[$j][$vRandom], 2, $tipoEv[$i],0);
                                        }
                                    }
                                }
                                break;

                            case 2; 
                                $cUAuditado = cUltimoUsuarioAuditadoDepto($depto[$i], $dISemana[$semana-2]);
                                for ($c = 0; $c < count($cUAuditado); $c++ ){
                                    $lUsuarioM = $cUAuditado[$c][0];
                                }
                                
                                for ($j = 0; $j < count($cUserDepto); $j++  ){
                                   $auditado[$j+1] = $cUserDepto[$j][0];
                                   if(isset($lUsuarioM)) {
                                        if ($lUsuarioM != $auditado[$j+1] ){
                                            $auditadoM = $auditado[$j+1];
                                        }
                                    } else {
                                        $auditadoM = $auditado[1];
                                    }
                                }
                                
                                for ($x = $sem[$semana]+1; $x <= $numSemanas ; $x++ ){
                                    $mes[$x] =  (int) date("m",strtotime($dISemana2[$x])); 
                                    if (($x % 2) == 1) { //impar
                                        iProgAuditoria($dISemana[$x],     $dFSemana[$x],      $year,      $mes[$x], $sem[$x], $depto[$i], $auditado[1], $auditado[1], 1, $tipoEv[$i],0);
                                    } else {//par
                                        iProgAuditoria($dISemana2[$x],$dFSemana2[$x], $year, $mes[$i], $sem[$x], $depto[$i], $auditado[2], $auditado[2], 1, $tipoEv[$i],0);
                                    }                                        
                                    
                                }

                                for ($x = 1; $x < 5; $x++){                        
                                    $mes[$i] =  (int) date("m",strtotime($dISemana2[$x])); 
                                    if (($x % 2) == 1) { //impar
                                        iProgAuditoria($dISemana2[$x],$dFSemana2[$x], $year2, $mes[$i], $sem[$x], $depto[$i], $auditado[1], $auditado[1], 1, $tipoEv[$i],0);
                                    } else {//par
                                        iProgAuditoria($dISemana2[$x],$dFSemana2[$x], $year2, $mes[$i], $sem[$x], $depto[$i], $auditado[2], $auditado[2], 1, $tipoEv[$i],0);
                                    }                                        
                                    
                                }
                                
                                //TRAEMOS EL ULTIMO AUDITADO DE FORMA MENSUAL DEL DEPARTAMENTO       
                                if($tipoEv[$i] < 5){
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionPiso($auditadoM, $year2, $dISemana2[2]);       
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana2[4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana2[2],$dFSemana2[4], $year2, $mesS[$i], $sem[4], $depto[$i], $auditadoM, $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    } 
                                } else if ($tipoEv[$i] == 5 ){
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionOficina($auditadoM, $year2, $dISemana2[2]);
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana2[4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana2[2],$dFSemana2[4], $year2, $mesS[$i], $sem[4], $depto[$i], $auditado[$j], $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    } 
                                } 
                                break;

                            case 3: 
                                $bnCont = 0;
                                $cUltimoUsuario = cUltimoUsuarioAuditorDeptos($depto[$i], $dISemana2[$semana]);                                    
                                for ($c = 0; $c < count($cUltimoUsuario); $c++ ){
                                    $lUsuario = $cUltimoUsuario[$c][0];
                                }  

                                for ($j = 0; $j < count($cUserDepto); $j++  ){                                        
                                    $auditor[$j+1] = $cUserDepto[$j][0];
                                    if(isset($lUsuario) && $lUsuario == $auditor[$j+1] ){
                                       $bnCont = $j+1;
                                    } 
                                }
                                
                                switch ($bnCont){
                                    case 0:
                                        $uAuditor[1] = $auditor[1];
                                        $uAuditor[2] = $auditor[2];
                                        $uAuditor[3] = $auditor[3];
                                        break;
                                    case 1:
                                        $uAuditor[1] = $auditor[2];
                                        $uAuditor[2] = $auditor[3];
                                        $uAuditor[3] = $auditor[1];
                                        break;
                                    case 2:
                                        $uAuditor[1] = $auditor[3];
                                        $uAuditor[2] = $auditor[1];
                                        $uAuditor[3] = $auditor[2];
                                        break;
                                    case 3:
                                        $uAuditor[1] = $auditor[1];
                                        $uAuditor[2] = $auditor[2];
                                        $uAuditor[3] = $auditor[3];
                                        break;
                                }
                                
                                $uAuditor[4] = $auditor[1];

                                for ($x = $sem[$semana]+1; $x <= $numSemanas ; $x++ ){                       
                                    $mes[$x] =  (int) date("m",strtotime($dISemana[$x])); 
                                    iProgAuditoria($dISemana[$x],$dFSemana[$x], $year, $mes[$x], $sem[$x], $depto[$i], $uAuditor[1], $uAuditor[1], 1, $tipoEv[$i], 0);
                                }

                                for ($x = 1; $x < 5; $x++){                        
                                    $mes[$x] =  (int) date("m",strtotime($dISemana2[$x])); 
                                    iProgAuditoria($dISemana2[$x],$dFSemana2[$x], $year2, $mes[$i], $sem[$x], $depto[$i], $uAuditor[$x], $uAuditor[$x], 1, $tipoEv[$i], 0);
                                }

                                if ($tipoEv[$i] < 5 ){
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionPiso($auditor[3], $year2, $dISemana2[2]);       
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana2[4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana2[2],$dFSemana2[4], $year2, $mesS[$i], $sem[4], $depto[$i], $uAuditor[3], $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    }
                                } else if ($tipoEv == 5) {
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionOficina($auditor[3], $year2, $dISemana2[2]);       
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana2[4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana2[2],$dFSemana2[4], $year2, $mesS[$i], $sem[4], $depto[$i], $uAuditor[3], $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    }
                                }          
                                break;

                            case 4;   
                                $bnCont = 0;
                                $cUAuditado = cUltimoUsuarioAuditadoDepto($depto[$i], $dISemana[$semana-2]);
                                for ($c = 0; $c < count($cUAuditado); $c++ ){
                                    $lUsuarioM = $cUAuditado[$c][0];
                                }
                                
                                for ($j = 0; $j < count($cUserDepto); $j++  ){
                                   $auditado[$j+1] = $cUserDepto[$j][0];
                                   if(isset($lUsuarioM) && $lUsuarioM != $auditado[$j+1] ){
                                        $bnCont = $j+1;
                                    }                                    
                                }

                                for ($x = $sem[$semana]+1; $x <= $numSemanas ; $x++ ){                        
                                    $mes[$x] = (int) date("m",strtotime($dISemana[$x])); 
                                    iProgAuditoria($dISemana[$x], $dFSemana[$x], $year, $mes[$x], $sem[$x], $depto[$i], $auditado[1], $auditado[1], 1, $tipoEv[$i], 0);
                                }
                                
                                for ($x = 1; $x < 5; $x++){                        
                                    $mes[$x] = (int) date("m",strtotime($dISemana2[$x])); 
                                    iProgAuditoria($dISemana2[$x], $dFSemana2[$x], $year2, $mes[$x], $sem[$x], $depto[$i], $auditado[$x], $auditado[$x], 1, $tipoEv[$i], 0);
                                    //echo '<br>1.',$x,': ',$dISemana2[$x],', ',$dFSemana2[$x],', ',$year2,', ',$mes[$i],', ', $sem[$x],', ',$depto[$i],', ',$auditado[$x],', ',$auditado[$x],', ',1,', ', $tipoEv[$i],0;
                                }
                                
                                switch ($bnCont){
                                    case 0:
                                        $uAuditor = $auditado[1];
                                        break;
                                    case 1:
                                        $uAuditor = $auditado[2];
                                        break;
                                    case 2:
                                        $uAuditor = $auditado[3];
                                        break;
                                    case 3:
                                        $uAuditor = $auditado[4];
                                        break;
                                    case 4:
                                        $uAuditor = $auditado[1];
                                        break;
                                }
                                
                                if ($tipoEv[$i] < 5 ){
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionPiso($uAuditor, $year2, $dISemana2[2]);       
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana2[4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana2[2],$dFSemana2[4], $year2, $mesS[$i], $sem[4], $depto[$i], $uAuditor, $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    }
                                } else if ($tipoEv[$i] == 5){ 
                                    $uAuditoriaChamp = cUsuarioPAuditoriaChampionOficina($uAuditor, $year2, $dISemana2[2]);      
                                    for($x = 0; $x < count($uAuditoriaChamp); $x++ ){
                                        $auditorM[$x] = $uAuditoriaChamp[$x][0];
                                    }

                                    $mesS[$i] = (int) date("m",strtotime($dISemana2[4]));
                                    $vRandom = rand(0,count($uAuditoriaChamp)-1);        
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditorM[$vRandom])){
                                        iProgAuditoria($dISemana2[2],$dFSemana2[4], $year2, $mesS[$i], $sem[4], $depto[$i], $uAuditor, $auditorM[$vRandom], 2, $tipoEv[$i],0);
                                    }
                                }                                
                                break;
                        }
                    }
                }
            }
        }
    } 
?>




