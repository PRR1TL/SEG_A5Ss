<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //CONFIGURACION PARA CORREO
    require("../../smtp/class.phpmailer.php");
    $mail = new PHPMailer();

    $mail->isSMTP();
    $mail->SMTPDebug = 2;
    $mail->Debugoutput = 'html';
    
    //CONFIGURACION DE HOST DE SEG
    $mail->Host = "smtp.sg.lan";
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->SMTPSecure = false;
    $mail->setFrom("no-reply@seg-automotive.com", "ROL DE AUDITORIA 5S's");

    //PARTE DE BASE DE DATOS
    include '../funciones.php';

    session_start();
    $depto = $_SESSION["depto"];
    
    //CONSULTA DE ULTIMO ID PARA SEMANALES
    $cLAux = cLAuxSemanal();
    $aux = $cLAux[0][0];
    
    $contI = 0;
            
    $date = new DateTime;
    $f = date('Y-m-d'); 
    $year = date("Y", strtotime('+1 day',strtotime($f))); 
    
    # Establecemos la fecha segun el estandar ISO 8601 (numero de semana)
    $date->setISODate("$year", 53);
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
    if($date->format("W") == 53) {
        $numSemanas = 53;
    }else{
        $numSemanas = 52;
    }    
    
    for($i = 10; $i < $numSemanas+1; $i++ ){
        $sem[$i] = + $i;
        for($day = 0; $day < 7; $day++) {
            if ($day == 0){
                $dISemana[$i] = date('Y-m-d', strtotime('+1 day', strtotime($year."W".$sem[$i].$day)));
            } 
            if ($day == 6){
                $dFSemana[$i] = date('Y-m-d', strtotime('+1 day', strtotime($year."W".$sem[$i].$day)));
            }
        }
    }

    //PRIMERAS AUDITORIAS
    $iFijo = $dISemana[10];
    $fFijo = $dFSemana[10];

    //PARTIMOS DE LA AUDITORIA 10 Y DE AHÍ RESTAMOS DIAS
    for($i = 1; $i < 10; $i++ ){
        $sem[$i] = $i;
        $d = 7*$i; 
        for($day = 0; $day < 7; $day++){
            if ($day == 0) { 
                $dISemana[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.'+1 day', strtotime ($iFijo)));
            } 
            if ($day == 6) { 
                $dFSemana[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.'+1 day', strtotime ($fFijo)));
            } 
        } 
    } 

    $contU = 1;
    
    for ($i = 1; $i < 11; $i++){
        $uReg[$i] = $_POST["subChamp".$i]; 
    }
    
    for ($i = 1; $i < 11; $i++){
        if ($i == 1 && $uReg[$i] != '-' ) {            
            $uAudit[$contU] = $uReg[$i];
            $contU++;
        } else if ($uReg[$i] != '-' ) {
            $bnUExis = 0; 
            for($j = 1; $j < $contU; $j++ ) { 
                if ($uReg[$i] == $uAudit[$j]) {
                    $bnUExis = 1;
                }
            }
            if ($bnUExis == 0){                 
                $uAudit[$contU] = $uReg[$i];
                $contU++;
            }
        } 
    } 
    
    for ($i = 1; $i < $contU; $i++) { 
        //OBTENEMOS NOMBRE Y CORREO DE LOS USUARIOS 
        $datU = cUNuevoRol($uAudit[$i]); 
        for ($j = 0; $j < count($datU); $j++) { 
            $uNombre[$i] = $datU[$j][0];
            $uCorreo[$i] = $datU[$j][1]; 
        } 
    } 
    
    //RECIBIMOS PARAMETROS
    if (isset($_POST["subChamp1"]) && $_POST["subChamp1"] != '-') {
        $auditor = "";
        $nW = explode("-", $_POST["nWeek1"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek])); 
        $mes = date("m", strtotime($dFSemana[$nWeek])); 
        $aux++;
        $id = "W-".$aux; 
        $contI++;
        
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp1"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
        if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom' >$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom' >$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom' >$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td>$sem[$nWeek]</td>
                            <td>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td>$auditor</td>                     
                        </tr>" ;
        }
        
        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp1"], $depto, $_POST["subChamp1"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp2"]) && $_POST["subChamp2"] != '-') {
        $auditor = "";
        $nW = explode("-", $_POST["nWeek2"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "W-".$aux; 
         
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp2"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
        if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom' >$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }

        
        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp2"], $depto, $_POST["subChamp2"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp3"]) && $_POST["subChamp3"] != '-') { 
        $auditor = "";
        $nW = explode("-", $_POST["nWeek3"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "W-".$aux; 
        
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp3"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
        if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }

        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp3"], $depto, $_POST["subChamp3"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp4"]) && $_POST["subChamp4"] != '-') { 
        $auditor = "";
        $nW = explode("-", $_POST["nWeek1"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "W-".$aux; 
        
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp4"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
        if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }

        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp4"], $depto, $_POST["subChamp4"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp5"]) && $_POST["subChamp5"] != '-') { 
        $auditor = ""; 
        $nW = explode("-", $_POST["nWeek5"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "W-".$aux; 
        
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp5"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
        if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }

        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp5"], $depto, $_POST["subChamp5"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp6"]) && $_POST["subChamp6"] != '-') { 
        $auditor = "";
        $nW = explode("-", $_POST["nWeek6"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "W-".$aux; 
        
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp6"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
        if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }

        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp6"], $depto, $_POST["subChamp6"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp7"]) && $_POST["subChamp7"] != '-') { 
        $auditor = "";
        $nW = explode("-", $_POST["nWeek7"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "W-".$aux; 
        
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp7"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
        if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }
        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp7"], $depto, $_POST["subChamp7"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp8"]) && $_POST["subChamp8"] != '-') { 
        $auditor = "";
        $nW = explode("-", $_POST["nWeek8"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "W-".$aux; 
        
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp8"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
       if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }

        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp8"], $depto, $_POST["subChamp8"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp9"]) && $_POST["subChamp9"] != '-') { 
        $auditor = ""; 
        $nW = explode("-", $_POST["nWeek9"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "W-".$aux; 
        
        for ($i = 1; $i < $contU; $i++) { 
            if ($_POST["subChamp9"] == $uAudit[$i]){
                $auditor = $uNombre[$i];
            }
        }
        
        if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }
        
        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp9"], $depto, $_POST["subChamp9"], 2, 5, $aux);
    } 
    if (isset($_POST["subChamp10"]) && $_POST["subChamp10"] != '-') { 
        $auditor = "";
        $nW = explode("-", $_POST["nWeek10"]);    
        $nWeek = $nW[1];
        $anio = date("Y", strtotime($dFSemana[$nWeek]));
        $mes = date("m", strtotime($dFSemana[$nWeek]));
        $aux++;
        $id = "C-".$aux; 
        
       if (isset($rowIAuditoria)) {
            $rowIAuditoria = $rowIAuditoria."<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        } else {
            $rowIAuditoria = "<tr>                                              
                            <td 'width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$sem[$nWeek]</td>
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$dISemana[$nWeek] a $dFSemana[$nWeek]</td>                     
                            <td 'width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>$auditor</td>                     
                        </tr>" ;
        }
        
        iAuditoria($id, $dISemana[$nWeek], $dISemana[$nWeek], $anio, $mes, $sem[$nWeek], $depto, $_POST["subChamp10"], $depto, $_POST["subChamp10"], 2, 5, $aux);
    } 
       
    if ($contI > 0) { 
        $mensaje = "<b>Nuevo rol de auditoria </b><br>";
        $mensaje = $mensaje." <br><br>";                
                $mensaje = $mensaje."<table border='1' style='width: 60%; border-collapse: collapse;'>
                    <tr> 
                        <th style='background: #eee; width: 10%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>SEMANA</th>             
                        <th style='background: #eee; width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>PERIODO</th> 
                        <th style='background: #eee; width: 25%; text-align: left; vertical-align: top; border: 1px solid #000; border-collapse: collapse; padding: 0.3em; caption-side: bottom'>RESPONSABLE</th> 
                    </tr>".$rowIAuditoria."</table>";
                
        $mensaje = $mensaje."<h4><br><br><b>**EN CASO DE CUALQUIER DUDA, FAVOR DE HACERLO SABER AL DEPARTAMENTO DE VSE</b></h4>**";
        //ENVIAMOS LA NOTIFICACION DE LA AUDITORIA
        
        $cEnvCorreo = 1;
        for ($i = 1; $i < $contU; $i++) { 
            //echo '<BR>',$uNombre[$i],', ',$uCorreo[$i]; 
            $mail->addAddress("$uCorreo[$i]", "Recepient Name"); 
            $mail->isHTML(true);

            $mail->Subject = "Rol de auditoria 5Ss";
            $mail->Body = "$mensaje";
            $mail->AltBody = "This is the plain text version of the email content";

            //echo $mensaje;
            if(!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                $cEnvCorreo++;
            } 
        } 
        
        //echo $cEnvCorreo,', ',$contU;
        if ($cEnvCorreo == $contU ){
           echo "Bien";
        }            
    }
    
    //CONTENIDO DE CORREO 
    
    
    
    
    
    
    
    
    
    
    