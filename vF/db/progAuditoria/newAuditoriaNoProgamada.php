<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../funciones.php';
    date_default_timezone_set("America/Mexico_City");  
    session_start();
    
    //$userName = 'GOI4TL';
    $userName = $_SESSION['userName'];
    $name = $_SESSION['name'];
    $typeUser = $_SESSION['tipo'];    
    $typeEv = $_SESSION['ev'];
    $depto = $_SESSION["depto"];
    
    if ($typeUser == 2){ //SUPERVISOR
        $typeAuditoria = 3;
    } else { //ADMINISTRADOR
        $typeAuditoria = 4;
    }
    
    $y = date('Y');
    $m = date('m');
    $d = date('d');
    $fNow = date('Y-m-d');
    $nuevafecha = date ( 'm/d/Y' , strtotime('+1 day',strtotime($fNow)));
    $fecha = new DateTime(date($nuevafecha));
    $semana = $fecha->format('W');    
    $bnContador = 0;
    
    # Obtenemos el día de la semana de la fecha dada
    $diaSemana = date("w",mktime(0,0,0,$m,$d,$y));
    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
    $primerDia = date("Y-m-d",mktime(0,0,0,$m,$d-$diaSemana,$y));
    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
    $ultimoDia = date("Y-m-d",mktime(0,0,0,$m,$d+(6-$diaSemana),$y));
    
    $auditoriasUsuario = cAuditoriaSUPandADMIN($userName);
    //echo count($auditoriasUsuario);
    if (count($auditoriasUsuario) == 0) {
        //echo "<br>fI: ",$primerDia,", ",$ultimoDia,", ",$y,", ", $m,", ", $semana,", ",$depto,", ",$userName,", ",$typeAuditoria,", ",$typeEv,"<br>"; 
        iProgAuditoria($primerDia, $ultimoDia, $y, $m, $semana, $depto, $userName, $userName, $typeAuditoria, $typeEv);
    }
    
    //CONSULTA DE SUS AREAS DE ACUERDO AL TIPO USUARIO 
    if ($typeUser == 2 ){
        $listadoDeptosSup_Gerentes = listadoDeptosSup_Gerentes($userName);
        for($i = 0; $i < count($listadoDeptosSup_Gerentes); $i++ ){
            if ($i == 0){
                echo $listadoDeptosSup_Gerentes[$i][0];
            } else {
                echo ', '.$listadoDeptosSup_Gerentes[$i][0];
            }            
        }
    } else {
        echo '2';
    }
    
?>