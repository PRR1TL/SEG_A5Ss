<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../funciones.php';
    
    $date = new DateTime;
    //$f = date('2018-12-1');
    $f = date('Y-m-d');
    $year = date("Y", strtotime('+1 day',strtotime($f))); 
    $nuevafecha = date ('m/d/Y' , strtotime('+1 day',strtotime($f)));
    
    # Establecemos la fecha segun el estandar ISO 8601 (numero de semana)
    $date->setISODate("$year", 53);
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
    if($date->format("W") == 53){
        $numSemanas = 53;
    }else{
        $numSemanas = 52;
    }

    for($i = 10; $i < $numSemanas+1; $i++ ){
        $sem[$i] = + $i;
        for($day = 0; $day < 7; $day++){
            if ($day == 0){
                $dISemana[$i] = date('Y-m-d', strtotime($year."W".$sem[$i].$day));
            } else if ($day == 6){
                $dFSemana[$i] = date('Y-m-d', strtotime($year."W".$sem[$i].$day));
            }
        }
    }
    
    //PRIMERAS AUDITORIAS
    $iFijo = $dISemana[10];
    $fFijo = $dFSemana[10];
    //PARTIMOS DE LA AUDITORIA 10 Y DE AHÍ RESTAMOS DIAS
    for($i = 1; $i < 10; $i++ ){
        $sem[$i] = $i;
        $d = 7*$i; 
        for($day = 0; $day < 7; $day++){
            if ($day == 0){                
                $dISemana[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.' day', strtotime ($iFijo)));
            } else if ($day == 6){
                $dFSemana[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.' day', strtotime ($fFijo)));
            }
        }
    }
    
    $fecha = new DateTime(date($nuevafecha));
    $semana = (int) $fecha->format('W');     
    $bnCont = 0;
    
    echo $semana,' de ', $numSemanas, ': ', $dISemana[$semana],' a ', $dFSemana[$semana],'<br>';
    