<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //header("Refresh: 90; URL='newProgAudit.php'");

    //CONEXION A LA BASE DE DATOS
    include '../funciones.php';
    require("../../smtp/class.phpmailer.php");
    $date = new DateTime;
    //$f = date('2018-12-10');
    $f = date('2019-03-18');
    //$f = date('2019-04-18');
    //$f = date('Y-m-d');    
    $year = date("Y", strtotime('+1 day',strtotime($f))); 
    $ldY = $year."-12-31";
    $nuevafecha = date ('m/d/Y' , strtotime('+1 day',strtotime($f)));
    
    //INCIALIZACION DE CONTADORES PARA LOS ID PR TIPO
    $cIdDaily = 0;
    $cIdWeekly = 0;
    $cIdMontly = 0;
    
    # Establecemos la fecha segun el estandar ISO 8601 (numero de semana)
    $date->setISODate("$year", 53);
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
    if($date->format("W") == 53) {
        $numSemanas = 53;
    }else{
        $numSemanas = 52;
    }

    $fecha = new DateTime(date($nuevafecha));
    $semana = (int) $fecha->format('W');     
    $bnCont = 0;
    
    
    for($i = 10; $i < $numSemanas+1; $i++ ){
        $sem[$i] = + $i;
        for($day = 0; $day < 7; $day++) {
            if ($day == 0){
                $dISemana[$i] = date('Y-m-d', strtotime('+1 day', strtotime($year."W".$sem[$i].$day)));
            } else if ($day == 6){
                $dFSemana[$i] = date('Y-m-d', strtotime('+1 day', strtotime($year."W".$sem[$i].$day)));
            }
        }
    }

    //PRIMERAS AUDITORIAS
    $iFijo = $dISemana[10];
    $fFijo = $dFSemana[10];

    //PARTIMOS DE LA AUDITORIA 10 Y DE AHÍ RESTAMOS DIAS
    for($i = 1; $i < 10; $i++ ){
        $sem[$i] = $i;
        $d = 7*$i; 
        for($day = 0; $day < 7; $day++){
            if ($day == 0) { 
                $dISemana[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.'+1 day', strtotime ($iFijo)));
            } else if ($day == 6) { 
                $dFSemana[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.'+1 day', strtotime ($fFijo)));
            } 
        } 
    } 
    
    if ($semana >= 48 ){
        $year = $year +1;
        $date->setISODate("$year", 53);
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53) {
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        for($i = 10; $i < $numSemanas+1; $i++ ){
            $sem[$i] = + $i;
            for($day = 0; $day < 7; $day++) {
                if ($day == 0){
                    $dISemana2[$i] = date('Y-m-d', strtotime('+1 day', strtotime($year."W".$sem[$i].$day)));
                } else if ($day == 6){
                    $dFSemana2[$i] = date('Y-m-d', strtotime('+1 day', strtotime($year."W".$sem[$i].$day)));
                }
            }
        }

        //PRIMERAS AUDITORIAS
        $iFijo2 = $dISemana2[10];
        $fFijo2 = $dFSemana2[10];

        //PARTIMOS DE LA AUDITORIA 10 Y DE AHÍ RESTAMOS DIAS
        for($i = 1; $i < 10; $i++ ){
            $sem[$i] = $i;
            $d = 7*$i; 
            for($day = 0; $day < 7; $day++) {
                if ($day == 0) { 
                    $dISemana2[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.'+1 day', strtotime ($iFijo2)));
                } else if ($day == 6) { 
                    $dFSemana2[10-$i] = date ( 'Y-m-d', strtotime ('-'.$d.'+1 day', strtotime ($fFijo2)));
                } 
            } 
        } 
    }
    
    $lastAuditProgram = cLastDAuditProgram($year);
    if(!isset($lastAuditProgram[0][0])){
        $cLDayAudit = $f;
    } else {
        $cLDayAudit = $lastAuditProgram[0][0]->format('Y-m-d');
    }    
    
    //CONSULTA DE ULTIMOS ID PARA LA PROGRAMACION DE AUDITORIAS
    $cLIdDaily = cLIdDaily();
    //echo "cIdDay: ", count($cLIdDaily),"<br>" ;
    if (count($cLIdDaily) > 0 && isset($cLIdDaily[0][0])){
        //SEPARAMOS EL NUMERO DEL ACRONIMO 
        //$corte = explode("-", $cLIdDaily[0][0]);
        //$cIdDaily = $corte[1]+1;
        $cIdDaily = $cLIdDaily[0][0]+1;
    }
    
    $cLIdWeekly = cLIdWeekly();
    //echo "cIdWeekly: ", count($cLIdWeekly),"<br>" ;
    if (count($cLIdWeekly) > 0  && isset($cLIdWeekly[0][0])){
        //SEPARAMOS EL NUMERO DEL ACRONIMO
        //$corte = explode("-", $cLIdWeekly[0][0]);
        //$cIdWeely = $corte[1]+1;
        $cIdWeely = $cLIdWeekly[0][0];
    }
    
    $cLIdMonthly = cLIdMonthly();
    //echo "cIdMonth: ", count($cLIdMonthly),"<br>" ;
    if (count($cLIdMonthly) && isset($cLIdMonthly[0][0])){
        //SEPARAMOS EL NUMERO DEL ACRONIMO
        //$corte = explode("-", $cLIdMonthly[0][0]);
        //$cIdMontly = $corte[1]+1;
        $cIdMontly = $cLIdMonthly[0][0];
    }
    
    echo "<br>ID d: ",$cIdDaily," s: ",$cIdWeekly," m: ",$cIdMontly,'<br><br>';
    
    echo $semana,' de ', $numSemanas, ': ', $dISemana[$semana],' a ', $dFSemana[$semana]," - ( ",$cLDayAudit," . ",$dISemana[$semana],') <br>';
    //CADA CUATRO SEMANAS SE VA ACTUALIZAR LA PROGRAMACION DE LAS AUDITORIAS
    if (($semana % 4) == 0) {
        if ($cLDayAudit <= $dISemana[$semana]) { 
            //SE HACE LA PROGRAMACION DE LA AUDITORIA 
            if ($semana != 52 ) { 
                /**** PROGRAMACION DE LAS SIGUIENTES 4 SEMANAS A LA ACTUAL ****/ 
                $listDeptos = cDeptosArea();
                for($i = 0; $i < count($listDeptos); $i++) { 
                //for($i = 0; $i < 2; $i++) { 
                    $tipoEv[$i] = $listDeptos[$i][0]; 
                    $depto[$i] = $listDeptos[$i][1]; 
                    //CONSULTAS PARA LOS TIPOS DE USUARIOS QUE SE TIENEN EN EL AREA
                    $cChampsDepto = cChampDepto($depto[$i]);
                    $cSupDepto = cSupDepto($depto[$i]); 
                    $cIPDepto = cIngProces($depto[$i]); 
                    
                    echo "entra ";
                    //CONSULTAMOS CUANTOS USUARIOS ESTAN COMO CHAMPIONS EN EL DEPARTAMENTO 
                    //DE ACUERDO AL TIPO DE EVALUACION SE MANDAN LAS AUDITORIAS 
                    switch ($tipoEv[$i]) {
                        case '1'://ALMACEN
                            echo "<br> -------------------------- ALMACEN -------------------------- <br>";
                            echo $depto[$i],', ',count($cChampsDepto),': ',$tipoEv[$i],'<br>'; 
                            //EVALUAMOS A LOS CHAMPION
                            if (count($cChampsDepto)){
                                //EVALUAMOS EL AUDITOR DELUsuarioNoRelacionDepto MES 
                                $cOptionsAuditores = cUsuarioNoRelacionDepto($depto[$i],$dISemana[$semana+2]); 
                                echo "<br>", count($cOptionsAuditores);
                                for ($k = 0; $k < count($cOptionsAuditores); $k++){
                                    $auditor[$k] = $cOptionsAuditores[$k][0];
                                    echo "<br>$k ",$auditor[$k] ;                                        
                                } 

                                $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4])); 
                                $vRandom = rand(0,count($cOptionsAuditores)-1);  

                                echo "<br>vRand: ", $vRandom,' -> ',$auditor[$vRandom],'<br>';  
                            }
                                                      
                            switch (count($cChampsDepto)) {
                                case 1:
                                    //echo " ******* ALMACEN CHAMP(1) ******* <br>"; 
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 

                                        $idW = "W-".$cIdWeekly; 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        //ENCABEZADO DE SEPARACION
                                        //echo $k,':  ',$dISemana[$k],' a ',$dFSemana[$k],' -> '; 
                                        
                                        //PARTE PARA CHECKLIST DIARIO 
                                        for($day = 0; $day < 7; $day++) { 
                                            if ($day < 5) { 
                                                $idD = "CL-".$cIdDaily; 
                                                //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cUserDepto[$j][0],', ',$cUserDepto[$j][0],', ',$tipoEv[$i] ;
                                                echo '<br> d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i],', ',$cIdDaily ;
                                                iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 1, $tipoEv[$i],$cIdDaily);
                                                $cIdDaily++; 
                                            } 
                                        } 
                                        //PARTE PARA SEMANALES 
                                        //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i],', ',$cIdWeekly;
                                        iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 2, $tipoEv[$i],0,$cIdWeekly);
                                        //echo '<br><br>'; 
                                        $cIdWeekly++; 
                                    }

                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo 'm ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$auditor[$vRandom],', ',$tipoEv[$i],', ',$cIdMontly,'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dmaFSena[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;    
                                    }                                  
                                    break;
                                
                                case 2: 
                                    //echo " ******* ALMACEN CHAMP (2) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW = $uAudit[1]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break; 
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        //$cLAuditM = $cLChampM[0][0];
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } 
                                                                        
                                    //SI NO ESTA NADA DEFINIDO (SI ES NUEVO EL DEPARTAMENTO, O NO EXISTEN AUDITORIAS) 
                                    if (!isset($cLAuditW) && !isset($cLAuditM) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            switch ($j) {
                                                case 0:
                                                    $cLAuditW = $uAudit[0];
                                                    break;
                                                case 1:
                                                    $cLAuditM = $uAudit[1];
                                                    break;
                                            }
                                        }
                                    } else if(!isset($cLAuditW)) {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditM == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } else {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditW == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    }

                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        if (($k % 2) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                                    echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i],', ',$cIdDaily ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                            //echo '<br><br>';
                                            $cIdWeekly++;
                                        } else {
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i],', ',$cIdDaily ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i],', ',$cIdWeekly ;
                                            //echo '<br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 2, $tipoEv[$i],0,$cIdWeekly);
                                            
                                            $cIdWeekly++;
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$auditor[$vRandom],', ',$tipoEv[$i],', ',$cIdMontly,'<br>' ;
                                        $cIdMontly++;                                                
                                    }
                                    break;
                                
                                case 3:
                                    //echo " ******* ALMACEN CHAMP (3) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        break; 
                                                }
                                            }
                                        }
                                        //echo '<br> 0.CL.R: ',$cLChampW[0][0],' - ', $cLAuditW1,'<br>';
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW3 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW3 = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                       // echo '<br> 1.CL.R: ',$cLChampM[0][0],' - ', $cLAuditW3,'<br>';
                                    }                                     
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW3)) {
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW3) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW3 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW3 = $uAudit[1];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else {
                                        //DEFINIMOS SOLO EL 3 AUDITOR Y AUDITADO
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 != $cChampsDepto[$j][0] && $cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        
                                        if (($k % 3) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                            //echo '<br><br>';
                                            $cIdWeekly++;
                                        } else {
                                            if ($count == 0){
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                //echo '<br>';
                                                $cIdWeekly++;
                                                $count++;
                                            } else {
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                //echo '<br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                            } 
                                        } 
                                    } 
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])) { 
                                        $idM = "M-".$cIdMontly; 
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly); 
                                        $cIdMontly++; 
                                    } 
                                    break;
                                
                                case 4:
                                    //echo " ******* ALMACEN CHAMP (4) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0]; 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL
                                    $cLChampW = cLChampWeekly($depto[$i]); 
                                    if(count($cLChampW)) { 
                                        echo $cLChampW[0][0],'<br>' ;
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) { 
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break; 
                                                } 
                                            } 
                                        }  
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2:
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3:
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    //echo "<br>+ W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3: ',$cLAuditW3,'  - W4: ',$cLAuditW4,'<br>';
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW4)) {
                                        //echo "!nodefinidos <br>";
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                        $cLAuditW4 = $uAudit[3];
                                    } else if (!isset($cLAuditW1) ) {
                                        //echo "!cLAuditW1 <br>";
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW4) ) {
                                        //echo "!cLAuditW4 <br>";
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1: 
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2: 
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3: 
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    //echo "<br>- W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3: ',$cLAuditW3,'  - W4: ',$cLAuditW4,'<br>';                                     
                                    
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        //echo "<br>";
                                        switch ($count) {
                                            case 0:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                        echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i],', ',$cIdDaily ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                        
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                
                                                //PARTE PARA SEMANALES
                                                echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i],', ',$cIdWeekly ;
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);
                                                //echo '<br><br>';
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            
                                            case 1:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                //echo '<br><br>';
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            
                                            case 2:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                //echo '<br><br>';
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            
                                            case 3:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;                                            
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        //echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }                                     
                                    break;
                            }
                            
                            //EVALUACION DE SUPERVISORES 
                            
                            break;
                            
                        case '2'://LABORATORIO
                            echo "<br> ------------------- LABORATORIOS ------------- <br> ";
                            echo $depto[$i],', ',count($cChampsDepto),': ',$tipoEv[$i],'<br>'; 
                            
                            if (count($cChampsDepto)){
                                //EVALUAMOS EL AUDITOR DEL MES 
                                $cOptionsAuditores = cUsuarioNoRelacionDepto($depto[$i],$dISemana[$semana+2]);
                                echo "<br>", count($cOptionsAuditores);
                                for ($k = 0; $k < count($cOptionsAuditores); $k++) {
                                    $auditor[$k] = $cOptionsAuditores[$k][0]; 
                                    echo "<br>$k ",$auditor[$k]; 
                                } 

                                $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4])); 
                                $vRandom = rand(0,count($cOptionsAuditores)-1);  

                                echo "<br>vRand: ", $vRandom,' -> ',$auditor[$vRandom],'<br>';  
                            }
                            
                            //EVALUACION DE CHAMPIONS
                            switch (count($cChampsDepto)) {
                                case 1:
                                    //echo " ******* LAB CHAMP(1) ******* <br>";
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        //ENCABEZADO DE SEPARACION 
                                        ////echo $k,':  ',$dISemana[$k],' a ',$dFSemana[$k],' -> '; 
                                        
                                        //PARTE PARA CHECKLIST DIARIO 
                                        for($day = 0; $day < 7; $day++) { 
                                            if ($day < 5) { 
                                                $idD = "CL-".$cIdDaily;
                                                //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cUserDepto[$j][0],', ',$cUserDepto[$j][0],', ',$tipoEv[$i] ;
                                                //echo '<br> d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i] ;
                                                iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 1, $tipoEv[$i],$cIdDaily);
                                                $cIdDaily++; 
                                            } 
                                        } 
                                        
                                        //PARTE PARA SEMANALES
                                        //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i] ;
                                        iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 2, $tipoEv[$i],0,$cIdWeekly);
                                        //echo '<br><br>';
                                        $cIdWeekly++;
                                    }

                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        //echo 'm ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;    
                                    }                                  
                                    break;
                                    
                                case 2: 
                                    //echo " ******* LAB CHAMP (2) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW = $uAudit[1]; 
                                                        break;
                                                    case 1:
                                                        echo "1. ";
                                                        $cLAuditW = $uAudit[0];
                                                        break; 
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        //$cLAuditM = $cLChampM[0][0];
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } 
                                                                        
                                    //SI NO ESTA NADA DEFINIDO (SI ES NUEVO EL DEPARTAMENTO, O NO EXISTEN AUDITORIAS) 
                                    if (!isset($cLAuditW) && !isset($cLAuditM) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            switch ($j) {
                                                case 0:
                                                    $cLAuditW = $uAudit[0];
                                                    break;
                                                case 1:
                                                    $cLAuditM = $uAudit[1];
                                                    break;
                                            }
                                        }
                                    } else if(!isset($cLAuditW)) {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditM == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } else {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditW == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                                        
                                    //echo "<br>- w:", $cLAuditW,' m: ',$cLAuditM,'<br>' ; 
                                    
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        if (($k % 2) == 0) { 
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                            //echo '<br><br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        } else {
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                            echo '<br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }
                                    break;
                                
                                case 3:
                                    //echo " ******* LAB CHAMP (3) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        break; 
                                                }
                                            }
                                        }
                                        //echo '<br> 0.CL.R: ',$cLChampW[0][0],' - ', $cLAuditW1,'<br>';
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW3 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW3 = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                        echo '<br> 1.CL.R: ',$cLChampM[0][0],' - ', $cLAuditW3,'<br>';
                                    }                                     
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW3)) {
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW3) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW3 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW3 = $uAudit[1];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else {
                                        //DEFINIMOS SOLO EL 3 AUDITOR Y AUDITADO
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 != $cChampsDepto[$j][0] && $cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    //echo "<br>- W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3 o M: ',$cLAuditW3,'<br>'; 
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        
                                        if (($k % 3) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                            //echo '<br><br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        } else {
                                            if ($count == 0){
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                echo '<br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                            } else {
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                echo '<br>';
                                                $cIdWeekly++;
                                            } 
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }
                                    break;
                                
                                case 4:
                                    //echo " ******* LAB CHAMP (4) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0]; 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL
                                    $cLChampW = cLChampWeekly($depto[$i]); 
                                    if(count($cLChampW)) { 
                                        echo $cLChampW[0][0],'<br>' ;
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) { 
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break; 
                                                } 
                                            } 
                                        }  
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2:
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3:
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    }                                     
                                    //echo "<br>+ W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3: ',$cLAuditW3,'  - W4: ',$cLAuditW4,'<br>';
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW4)) {
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                        $cLAuditW4 = $uAudit[3];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW4) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1: 
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2: 
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3: 
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    }                                    
                                    //echo "<br>- W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3: ',$cLAuditW3,'  - W4: ',$cLAuditW4,'<br>';                                     
                                    
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        echo "<br>";
                                        switch ($count) {
                                            case 0:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);

                                                //echo '<br><br>';
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 1:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                //echo '<br><br>';
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 2:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                //echo '<br><br>'
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 3:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;                                            
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }
                                    
                                    break;
                            }
                            
                            //EVALUACION DE ISUPERVISORES
                            
                            break;
                            
                        case '3'://LINEA
                            echo " <br> ******* LINEA ******* <br>";
                            echo $depto[$i],', ',count($cChampsDepto),': ',$tipoEv[$i],'<br>'; 
                            
                            if (count($cChampsDepto)){
                                //EVALUAMOS EL AUDITOR DEL MES 
                                $cOptionsAuditores = cUsuarioNoRelacionDepto($depto[$i],$dISemana[$semana+2]); 
                                echo "<br>", count($cOptionsAuditores);
                                for ($k = 0; $k < count($cOptionsAuditores); $k++){
                                    $auditor[$k] = $cOptionsAuditores[$k][0];
                                    echo "<br>$k ",$auditor[$k]; 
                                } 

                                $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4])); 
                                $vRandom = rand(0,count($cOptionsAuditores)-1);  

                                echo "<br>vRand: ", $vRandom,' -> ',$auditor[$vRandom],'<br>';  
                            }
                            
                            //EVALUACION DE CHAMPIONS
                            switch (count($cChampsDepto)) {
                                case 1:
                                    //echo " ******* LINEA CHAMP(1) ******* <br>";
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 

                                        $idW = "W-".$cIdWeekly; 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        //ENCABEZADO DE SEPARACION
                                        //echo $k,':  ',$dISemana[$k],' a ',$dFSemana[$k],' -> '; 

                                        //PARTE PARA CHECKLIST DIARIO
                                        for($day = 0; $day < 7; $day++) { 
                                            if ($day < 5) { 
                                                $idD = "CL-".$cIdDaily;
                                                //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cUserDepto[$j][0],', ',$cUserDepto[$j][0],', ',$tipoEv[$i] ;
                                                //echo '<br> d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i] ;
                                                iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 1, $tipoEv[$i],$cIdDaily);
                                                $cIdDaily++;
                                            } 
                                        } 
                                        //PARTE PARA SEMANALES
                                        //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i] ;
                                        //echo '<br><br>';
                                        iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 2, $tipoEv[$i],0,$cIdWeekly);
                                        $cIdWeekly++;
                                    }

                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;    
                                    }                                  
                                    break;
                                    
                                case 2: 
                                    //echo " ******* LINEA CHAMP (2) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:  
                                                        $cLAuditW = $uAudit[1]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break; 
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        //$cLAuditM = $cLChampM[0][0];
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } 
                                                                        
                                    //SI NO ESTA NADA DEFINIDO (SI ES NUEVO EL DEPARTAMENTO, O NO EXISTEN AUDITORIAS) 
                                    if (!isset($cLAuditW) && !isset($cLAuditM) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            switch ($j) {
                                                case 0:
                                                    $cLAuditW = $uAudit[0];
                                                    break;
                                                case 1:
                                                    $cLAuditM = $uAudit[1];
                                                    break;
                                            }
                                        }
                                    } else if(!isset($cLAuditW)) {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditM == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } else {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditW == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                                        
                                    //echo "<br>- w:", $cLAuditW,' m: ',$cLAuditM,'<br>' ; 
                                    
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        if (($k % 2) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 2, $tipoEv[$i],0,$cIdWeekly);
                                            //echo '<br><br>';
                                            $cIdWeekly++;
                                        } else {
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                            //echo '<br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++; 
                                    }
                                    break;
                                
                                case 3:
                                    //echo " ******* LINEA CHAMP (3) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        break; 
                                                }
                                            }
                                        }
                                        echo '<br> 0.CL.R: ',$cLChampW[0][0],' - ', $cLAuditW1,'<br>';
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW3 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW3 = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                        echo '<br> 1.CL.R: ',$cLChampM[0][0],' - ', $cLAuditW3,'<br>';
                                    }                                     
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW3)) {
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW3) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW3 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW3 = $uAudit[1];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else {
                                        //DEFINIMOS SOLO EL 3 AUDITOR Y AUDITADO
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 != $cChampsDepto[$j][0] && $cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    echo "<br>- W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3 o M: ',$cLAuditW3,'<br>';                                     
                                    
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        
                                        if (($k % 3) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                            //echo '<br><br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        } else {
                                            if ($count == 0){
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                //echo '<br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                            } else {
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                //echo '<br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                            } 
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }
                                    break;
                                
                                case 4:
                                    //echo " ******* LINEA CHAMP (4) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0]; 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL
                                    $cLChampW = cLChampWeekly($depto[$i]); 
                                    if(count($cLChampW)) { 
                                        echo $cLChampW[0][0],'<br>' ;
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) { 
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break; 
                                                } 
                                            } 
                                        }  
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2:
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3:
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    }                                     
                                    //echo "<br>+ W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3: ',$cLAuditW3,'  - W4: ',$cLAuditW4,'<br>';
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW4)) {
                                        echo "!nodefinidos <br>";
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                        $cLAuditW4 = $uAudit[3];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW4) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1: 
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2: 
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3: 
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    }                                    
                                    //echo "<br>- W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3: ',$cLAuditW3,'  - W4: ',$cLAuditW4,'<br>';                                     
                                   
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        //echo "<br>";
                                        switch ($count) {
                                            case 0:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 1:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                //echo '<br><br
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 2:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 3:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;                                            
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])) {
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }                                    
                                    break;
                            }
                            
                            //EVALUACION DE ING DE PROCESOS                            
                            
                            break;
                        
                        case '4'://MANTENIMIENTO
                            echo " <br> ******* MANTENIMIENTO ******* <br>";
                            echo $depto[$i],', ',count($cChampsDepto),': ',$tipoEv[$i],'<br>'; 
                            
                            //EVALUAMOS EL AUDITOR DEL MES 
                            if (count($cChampsDepto)){
                                //EVALUAMOS EL AUDITOR DEL MES 
                                $cOptionsAuditores = cUsuarioNoRelacionDepto($depto[$i],$dISemana[$semana+2]); 
                                echo "<br>", count($cOptionsAuditores);
                                for ($k = 0; $k < count($cOptionsAuditores); $k++){
                                    $auditor[$k] = $cOptionsAuditores[$k][0];
                                    echo "<br>$k ",$auditor[$k] ;                                        
                                } 

                                $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4])); 
                                $vRandom = rand(0,count($cOptionsAuditores)-1);  

                                echo "<br>vRand: ", $vRandom,' -> ',$auditor[$vRandom],'<br>';  
                            }
                            
                            //EVALUACION DE CHAMPIONS
                            switch (count($cChampsDepto)) {
                                case 1:
                                    //echo " ******* MANTENIMIENTO CHAMP(1) ******* <br>";
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 

                                        $idW = "W-".$cIdWeekly; 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        //ENCABEZADO DE SEPARACION
                                        ////echo $k,':  ',$dISemana[$k],' a ',$dFSemana[$k],' -> '; 

                                        //PARTE PARA CHECKLIST DIARIO
                                        for($day = 0; $day < 7; $day++) { 
                                            if ($day < 5) { 
                                                $idD = "CL-".$cIdDaily;
                                                //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cUserDepto[$j][0],', ',$cUserDepto[$j][0],', ',$tipoEv[$i] ;
                                                //echo '<br> d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i] ;
                                                iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 1, $tipoEv[$i],$cIdDaily);
                                                $cIdDaily++;
                                            } 
                                        } 
                                        //PARTE PARA SEMANALES
                                        //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i] ;
                                      //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                        iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 2, $tipoEv[$i],0,$cIdWeekly);
                                        //echo '<br><br>';
                                        $cIdWeekly++;
                                    }

                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;    
                                    }                                  
                                    break;
                                    
                                case 2: 
                                    //echo " ******* MANTENIMIENTO CHAMP (2) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW = $uAudit[1]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break; 
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        //$cLAuditM = $cLChampM[0][0];
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } 
                                                                        
                                    //SI NO ESTA NADA DEFINIDO (SI ES NUEVO EL DEPARTAMENTO, O NO EXISTEN AUDITORIAS) 
                                    if (!isset($cLAuditW) && !isset($cLAuditM) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            switch ($j) {
                                                case 0:
                                                    $cLAuditW = $uAudit[0];
                                                    break;
                                                case 1:
                                                    $cLAuditM = $uAudit[1];
                                                    break;
                                            }
                                        }
                                    } else if(!isset($cLAuditW)) {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditM == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } else {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditW == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                                        
                                    //echo "<br>- w:", $cLAuditW,' m: ',$cLAuditM,'<br>' ; 

                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        if (($k % 2) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                            //echo '<br><br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        } else {
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                            //echo '<br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }
                                    break;
                                
                                case 3:
                                    //echo " ******* MANTENIMIENTO CHAMP (3) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        break; 
                                                }
                                            }
                                        }
                                        echo '<br> 0.CL.R: ',$cLChampW[0][0],' - ', $cLAuditW1,'<br>';
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW3 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW3 = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                        echo '<br> 1.CL.R: ',$cLChampM[0][0],' - ', $cLAuditW3,'<br>';
                                    }                                     
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW3)) {
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW3) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW3 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW3 = $uAudit[1];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else {
                                        //DEFINIMOS SOLO EL 3 AUDITOR Y AUDITADO
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 != $cChampsDepto[$j][0] && $cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    //echo "<br>- W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3 o M: ',$cLAuditW3,'<br>';                                     
                                    
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        
                                        if (($k % 3) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                            //echo '<br><br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        } else {
                                            if ($count == 0){
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                //echo '<br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                            } else {
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                //echo '<br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                            } 
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }
                                    break;
                                
                                case 4:
                                    //echo " ******* MANTENIMIENTO CHAMP (4) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0]; 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL
                                    $cLChampW = cLChampWeekly($depto[$i]); 
                                    if(count($cLChampW)) { 
                                        //echo $cLChampW[0][0],'<br>' ;
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) { 
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break; 
                                                } 
                                            } 
                                        }  
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2:
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3:
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    }                                     
                                    //echo "<br>+ W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3: ',$cLAuditW3,'  - W4: ',$cLAuditW4,'<br>';
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW4)) {
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                        $cLAuditW4 = $uAudit[3];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW4) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1: 
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2: 
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3: 
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        //echo "<br>";
                                        switch ($count) {
                                            case 0:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 1:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 2:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 3:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;                                            
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }                                    
                                    break;
                            }
                            
                            //EVALUACION DE SUPERVISORES
                            
                            break;
                        
                        case '5'://OFICINA
                            echo "<br> ******* OFICINA ******* <br>";
                            echo $depto[$i],', ',count($cChampsDepto),': ',$tipoEv[$i],'<br>'; 
                            
                            if (count($cChampsDepto)){
                                //EVALUAMOS EL AUDITOR DEL MES 
                                $cOptionsAuditores = cUsuarioNoRelacionDepto($depto[$i],$dISemana[$semana+2]); 
                                echo "<br>", count($cOptionsAuditores);
                                for ($k = 0; $k < count($cOptionsAuditores); $k++){
                                    $auditor[$k] = $cOptionsAuditores[$k][0];
                                    echo "<br>$k ",$auditor[$k] ;                                        
                                } 

                                $mesS[$i] = (int) date("m",strtotime($dISemana[$semana+4])); 
                                $vRandom = rand(0,count($cOptionsAuditores)-1);  

                                echo "<br>vRand: ", $vRandom,' -> ',$auditor[$vRandom],'<br>';  
                            } 
                            
                            //EVALUACION DE CHAMPIONS
                            switch (count($cChampsDepto)) {
                                case 1:
                                    //echo " ******* OFICINA CHAMP(1) ******* <br>";
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 

                                        $idW = "W-".$cIdWeekly; 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        //ENCABEZADO DE SEPARACION
                                        ////echo $k,':  ',$dISemana[$k],' a ',$dFSemana[$k],' -> '; 

                                        //PARTE PARA CHECKLIST DIARIO
                                        for($day = 0; $day < 7; $day++) { 
                                            if ($day < 5) { 
                                                $idD = "CL-".$cIdDaily;
                                                //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cUserDepto[$j][0],', ',$cUserDepto[$j][0],', ',$tipoEv[$i] ;
                                                //echo '<br> d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i] ;
                                                //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 1, $tipoEv[$i],$cIdDaily);
                                                $cIdDaily++;
                                            } 
                                        } 
                                        //PARTE PARA SEMANALES
                                        //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$cChampsDepto[0][0],', ',$tipoEv[$i] ;
                                        //echo '<br><br>';
                                        iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $cChampsDepto[0][0], 2, $tipoEv[$i],0,$cIdWeekly);
                                        $cIdWeekly++;
                                    }

                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cChampsDepto[0][0],', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cChampsDepto[0][0], $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;    
                                    }                                  
                                    break;
                                    
                                case 2: 
                                    //echo " ******* OFICINA CHAMP (2) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW = $uAudit[1]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break; 
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        //$cLAuditM = $cLChampM[0][0];
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } 
                                                                        
                                    //SI NO ESTA NADA DEFINIDO (SI ES NUEVO EL DEPARTAMENTO, O NO EXISTEN AUDITORIAS) 
                                    if (!isset($cLAuditW) && !isset($cLAuditM) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            switch ($j) {
                                                case 0:
                                                    $cLAuditW = $uAudit[0];
                                                    break;
                                                case 1:
                                                    $cLAuditM = $uAudit[1];
                                                    break;
                                            }
                                        }
                                    } else if(!isset($cLAuditW)) {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditM == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    } else {
                                        //SE DEFINE AUDITOR DE LA SEMANA
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLAuditW == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditM = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditM = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                                        
                                    //echo "<br>- w:", $cLAuditW,' m: ',$cLAuditM,'<br>' ; 

                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        if (($k % 2) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW,', ',$cLAuditW,', ',$tipoEv[$i] ;
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW, $cLAuditW, 2, $tipoEv[$i],0,$cIdWeekly);
                                            //echo '<br><br>';
                                            $cIdWeekly++;
                                        } else {
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$i,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                    //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $cLAuditM, 2, $tipoEv[$i],0,$cIdWeekly);
                                            //echo '<br>';
                                            $cIdWeekly++;
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditM, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }
                                    break;
                                
                                case 3:
                                    //echo " ******* OFICINA CHAMP (3) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0];
                                    }
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL 
                                    $cLChampW = cLChampWeekly($depto[$i]);
                                    if(count($cLChampW)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        break; 
                                                }
                                            }
                                        }
                                        //echo '<br> 0.CL.R: ',$cLChampW[0][0],' - ', $cLAuditW1,'<br>';
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[1];
                                                        break;
                                                    case 1:
                                                        $cLAuditW3 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW3 = $uAudit[0];
                                                        break;
                                                }
                                            }
                                        }
                                        //echo '<br> 1.CL.R: ',$cLChampM[0][0],' - ', $cLAuditW3,'<br>';
                                    }                                     
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW3)) {
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW3) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW3 = $uAudit[0];
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2: 
                                                        $cLAuditW3 = $uAudit[1];
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else {
                                        //DEFINIMOS SOLO EL 3 AUDITOR Y AUDITADO
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 != $cChampsDepto[$j][0] && $cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW2 = $uAudit[1];
                                                        break;
                                                    case 1: 
                                                        $cLAuditW2 = $uAudit[2];
                                                        break;
                                                    case 2:
                                                        $cLAuditW2 = $uAudit[0];
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    //echo "<br>- W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3 o M: ',$cLAuditW3,'<br>';                                     
                                    
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        
                                        if (($k % 3) == 0) {
                                            //PARTE PARA CHECKLIST DIARIO 
                                            for($day = 0; $day < 7; $day++) { 
                                                if ($day < 5) { 
                                                    $idD = "CL-".$cIdDaily;
                                                    //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                    //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                    //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 1, $tipoEv[$i],$cIdDaily);
                                                    $cIdDaily++;
                                                } 
                                            } 
                                            //PARTE PARA SEMANALES
                                            //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                            //echo '<br><br>';
                                            iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);
                                            $cIdWeekly++;
                                        } else {
                                            if ($count == 0){
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                //echo '<br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                            } else {
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        //echo '<br> 1. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditM,', ',$cLAuditM,', ',$tipoEv[$i] ;
                                                        //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                //echo '<br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                            } 
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++;                                                
                                    }
                                    break;
                                
                                case 4:
                                    //echo " ******* OFICINA CHAMP (4) ******* <br>";
                                    for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                        $uAudit[$j] = $cChampsDepto[$j][0]; 
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE SEMANAL
                                    $cLChampW = cLChampWeekly($depto[$i]); 
                                    if(count($cLChampW)) { 
                                        //echo $cLChampW[0][0],'<br>' ;
                                        for ($j = 0; $j < count($cChampsDepto); $j++) { 
                                            if ($cLChampW[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) { 
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break; 
                                                } 
                                            } 
                                        }  
                                    } 
                                    
                                    //CONSULTA ULTIMO AUDITOR DE MES
                                    $cLChampM = cLChampMonthly($depto[$i]);
                                    if(count($cLChampM)) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLChampM[0][0] == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0:
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1:
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2:
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3:
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    }                                     
                                    //echo "<br>+ W1: ",$cLAuditW1,'  - W2: ',$cLAuditW2,'  - W3: ',$cLAuditW3,'  - W4: ',$cLAuditW4,'<br>';
                                    
                                    //***** ANALISIS DE LOS AUDITORES DEFINIDOS Y NO DEFINIDOS 
                                    if (!isset($cLAuditW1) && !isset($cLAuditW4)) {
                                        $cLAuditW1 = $uAudit[0];
                                        $cLAuditW2 = $uAudit[1];
                                        $cLAuditW3 = $uAudit[2];
                                        $cLAuditW4 = $uAudit[3];
                                    } else if (!isset($cLAuditW1) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW3 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        $cLAuditW4 = $uAudit[0]; 
                                                        break; 
                                                    case 1: 
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        $cLAuditW4 = $uAudit[1]; 
                                                        break; 
                                                    case 2: 
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        $cLAuditW4 = $uAudit[2]; 
                                                        break; 
                                                    case 3: 
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        $cLAuditW4 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } else if (!isset($cLAuditW4) ) {
                                        for ($j = 0; $j < count($cChampsDepto); $j++) {                                         
                                            if ($cLAuditW1 == $cChampsDepto[$j][0] ) { 
                                                //EVALUAMOS A CUAL ES IGUAL Y DE AHÍ SOLO EVALUAMOS EL USUARIO QUE SIGUE
                                                switch ($j) {
                                                    case 0: 
                                                        $cLAuditW4 = $uAudit[1];
                                                        $cLAuditW1 = $uAudit[2]; 
                                                        $cLAuditW2 = $uAudit[3]; 
                                                        $cLAuditW3 = $uAudit[0]; 
                                                        break;
                                                    case 1: 
                                                        $cLAuditW4 = $uAudit[2];
                                                        $cLAuditW1 = $uAudit[3]; 
                                                        $cLAuditW2 = $uAudit[0]; 
                                                        $cLAuditW3 = $uAudit[1]; 
                                                        break;
                                                    case 2: 
                                                        $cLAuditW4 = $uAudit[3];
                                                        $cLAuditW1 = $uAudit[0]; 
                                                        $cLAuditW2 = $uAudit[1]; 
                                                        $cLAuditW3 = $uAudit[2]; 
                                                        break;
                                                    case 3: 
                                                        $cLAuditW4 = $uAudit[0];
                                                        $cLAuditW1 = $uAudit[1]; 
                                                        $cLAuditW2 = $uAudit[2]; 
                                                        $cLAuditW3 = $uAudit[3]; 
                                                        break;
                                                } 
                                            } 
                                        } 
                                    } 
                                    
                                    $count = 0;
                                    //EVALUACION PARA FECHAS Y TIPO DE EVALUACIONES
                                    //AUDITORIAS DIARIAS Y SEMANALES 
                                    for ($k = $semana; $k < $semana+4; $k++ ) { 
                                        //PARTE PARA CHECKLIST SEMANAL 
                                        $fInicio = date("d-m-Y", strtotime($dISemana[$k])); 
                                        $idW = "W-".$cIdWeekly; 
                                        //echo "<br>";
                                        switch ($count) {
                                            case 0:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$cLAuditW1,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $cLAuditW1, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 1:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW2,', ',$cLAuditW2,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW2, $cLAuditW2, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 2:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW3,', ',$cLAuditW3,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW3, $cLAuditW3, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;
                                            case 3:
                                                for($day = 0; $day < 7; $day++) { 
                                                    if ($day < 5) { 
                                                        $idD = "CL-".$cIdDaily;
                                                        //echo '<br>',$k,':  ',$dISemana[$k],' + ',$day,' -> ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                        //echo '<br> 0. d ',$idD,": ",date("Y-m-d",strtotime($fInicio."+ ".$day." days")),' a ',date("Y-m-d",strtotime($fInicio."+ ".$day." days")),', ',date("Y",strtotime($fInicio."+ ".$day." days")),', ',date("m",strtotime($fInicio."+ ".$day." days")),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditCl,', ',$cLAuditCl,', ',$tipoEv[$i] ;
                                                        //iProgAuditoria($idD,date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y-m-d",strtotime($fInicio."+ ".$day." days")), date("Y",strtotime($fInicio."+ ".$day." days")), date("m",strtotime($fInicio."+ ".$day." days")), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 1, $tipoEv[$i],$cIdDaily);
                                                        $cIdDaily++;
                                                    } 
                                                } 
                                                //PARTE PARA SEMANALES
                                                //echo '<br> s ',$idW,": ",$dISemana[$k],' a ',$dFSemana[$k],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW4,', ',$cLAuditW4,', ',$tipoEv[$i] ;
                                                //echo '<br><br>';
                                                iProgAuditoria($idW, $dISemana[$k], $dFSemana[$k], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW4, $cLAuditW4, 2, $tipoEv[$i],0,$cIdWeekly);
                                                $cIdWeekly++;
                                                $count++;
                                                break;                                            
                                        }
                                    }
                                    
                                    //PROGRAMACION PARA LA MENSUAL (POR OTROS CHAMPIONS)
                                    if(isset($auditor[$vRandom])){
                                        $idM = "M-".$cIdMontly;
                                        echo '<br>m ',$idM,": ",$dISemana[$semana+2],' a ',$dFSemana[$semana+3],', ',date("Y",strtotime($dISemana[$k])),', ',date("m",strtotime($dISemana[$k])),', ',$sem[$k],', ',$depto[$i],', ',$cLAuditW1,', ',$auditor[$vRandom],', ',$tipoEv[$i],'<br>' ;
                                        iProgAuditoria($idM, $dISemana[$semana+2], $dFSemana[$semana+3], date("Y",strtotime($dISemana[$k])), date("m",strtotime($dISemana[$k])), $sem[$k], $depto[$i], $cLAuditW1, $auditor[$vRandom], 3, $tipoEv[$i],0,$cIdMontly);
                                        $cIdMontly++; 
                                    } 
                                    break; 
                            } 
                            
                            //EVALUACION DE ING DE GERENTES
                            
                            break;
                    } 
                }
            }
        }
    }
