<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../funciones.php';
    date_default_timezone_set("America/Mexico_City"); 
    session_start();
    
    $today = date("Y-m-d");
    $y = date("Y");
    $m = date("m");
    $w = date('W');
    
    $auditor = $_SESSION['userName'];
    $auditado = $_POST['auditado'];
    $depto = $_POST['area'];
    
    if (isset($auditado) && $auditado != '0' ) { 
        //OBTENEMOS EL TIPO DE EVALUACION
        $cTEv = cTEvalDepto($depto); 
        $tipoEv = $cTEv[0][0]; 
        
        $cIdAuditConfirm = cIdAuditConfirm($auditor); 
        if (count($cIdAuditConfirm) > 0 ){ //CUANDO SE TIENE UN ID SIN OCUPAR 
            $id = $cIdAuditConfirm[0][0]; 

            //HACEMOS EL UPDATE DE LA AUDITORIA
            uAuditoriaSup_Admin($today, $today, $y, $m, $w, $depto, $auditado, $id);
            echo 'Bien,',$tipoEv;            
        } else {//SI NO SE TIENE ID LIBRE 
            //HACE LA CONSULTA DEL ULTIMO ID DE LA AUDITORIA CONFIRMACIONAL
            $lastIdConfirm = cLastAuditConfirm();
            if(count($lastIdConfirm) > 0 ) { 
                //CORTAMOS EL ID QUE OBTENEMOS POR QUE TRAE LAS LETRAS QUE LO DISTINGUE
                $cLastId = split ("-", $lastIdConfirm[0][0]); 
                $lastId = $cLastId[1]; 
                $id = 'CO-'.$lastId+1; 
            } else { 
                $id = 'CO-0'; 
            } 

            //HACEMOS EL INSERT DEL NUEVO ID
            iProgAuditoria($id, $today, $today, $y, $m, $w, $depto, $auditado, $auditor, 4, $tipoEv); 
            echo 'Bien,',$tipoEv; 
        } 
    } else { 
    ?>
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong> DEBEN LLENARSE TODO LOS DATOS DE LA AUDITORIA A REALIZAR </strong>
    </div>
    <?php
    } 
    


