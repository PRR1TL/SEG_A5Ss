<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    date_default_timezone_set("America/Mexico_City");    
    $_SESSION['path'] = $_SERVER['PHP_SELF'];
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
        $typeEv = $_SESSION['ev'];   
        
        if($typeUser != 1 ){
            //echo '<script>location.href = "./index.php";</script>';
        }        
    } else {
        $userName = '';
        $name = 'INICIAR SESION';
        $typeUser = 0;
        $typeEv = 0;
        //echo '<script>location.href = "./index.php";</script>';
    } 
    
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    
    <!-- librerias adicionales -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <!--LINK PARA ICONOS-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    
    <!--ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS-->
    <link rel="stylesheet" href="css/modulo.css">
    <!--<script src="js/index.js"> </script>-->
    <link rel="stylesheet" href="../css/style.css" >
    <script src="js/admin.js"> </script>
    
    <?php 
        include './modals/informativos/help.php';
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MOD AL INFORMATIVOS
        include './modals/accesos/login.php';
        include './modals/accesos/cerrarSesion.php';
        include './modals/informativos/rules.php';
        
        //MODALS PARA LA SELECCION DE AUDITORIAS
        //DE ACUERDO A LOS PRIVILEGIOS DE USUARIOS
//        switch ($typeUser) {
//            case 1:
                include './modals/vse/AreaAudit.php';
                include './modals/vse/DeptoAudit.php';                
//                break;
//            case 2: 
                include './modals/supervisor/DeptoAudit.php';
//                include './modals/informativos/InfoAuditoria.php';
//                break;
//            case 3:
                include './modals/informativos/NoneAuditoria.php';
                include './modals/informativos/InfoAuditoria.php';
                include './modals/informativos/TipoAuditoriaCh.php';
//                break;
//        }
        
        include './modals/auditoria/daily/checkAlmacen.php';
        include './modals/auditoria/daily/checkLab.php';
        include './modals/auditoria/daily/checkLinea.php';
        include './modals/auditoria/daily/checkMantenimiento.php';
        include './modals/auditoria/daily/checkOficina.php';
        
        include './modals/auditoria/monthly/almacen.php';
        include './modals/auditoria/monthly/almacen2.php';
        include './modals/auditoria/monthly/laboratorio.php';
        include './modals/auditoria/monthly/laboratorio2.php';
        include './modals/auditoria/monthly/linea.php';
        include './modals/auditoria/monthly/linea2.php';
        include './modals/auditoria/monthly/mantenimiento.php';
        include './modals/auditoria/monthly/mantenimiento2.php';
        include './modals/auditoria/monthly/oficina.php';
        
        include './modals/auditoria/weekly/almacen.php';
        include './modals/auditoria/weekly/laboratorio.php';
        include './modals/auditoria/weekly/linea.php';
        include './modals/auditoria/weekly/mantenimiento.php';
        include './modals/auditoria/weekly/oficina.php';
        
        //AYUDAS VISUALES DE PREGUNTAS DE DAILY
        //ALMACEN
        include './modals/informativos/dAlmacen/mDP1.php';
        include './modals/informativos/dAlmacen/mDP2.php';
        include './modals/informativos/dAlmacen/mDP3.php';
        include './modals/informativos/dAlmacen/mDP4.php';
        include './modals/informativos/dAlmacen/mDP5.php';
        include './modals/informativos/dAlmacen/mDP6.php';
        include './modals/informativos/dAlmacen/mDP7.php';
        include './modals/informativos/dAlmacen/mDP8.php';
        include './modals/informativos/dAlmacen/mDP9.php';
        include './modals/informativos/dAlmacen/mDP10.php';
        
        //LABORATORIO
        include './modals/informativos/dLaboratorio/mDP1.php';
        include './modals/informativos/dLaboratorio/mDP2.php';
        include './modals/informativos/dLaboratorio/mDP3.php';
        include './modals/informativos/dLaboratorio/mDP4.php';
        include './modals/informativos/dLaboratorio/mDP5.php';
        include './modals/informativos/dLaboratorio/mDP6.php';
        include './modals/informativos/dLaboratorio/mDP7.php';
        include './modals/informativos/dLaboratorio/mDP8.php';
        include './modals/informativos/dLaboratorio/mDP9.php';
        include './modals/informativos/dLaboratorio/mDP10.php';
        
        //LINEA
        include './modals/informativos/dLinea/mDP1.php';
        include './modals/informativos/dLinea/mDP2.php';
        include './modals/informativos/dLinea/mDP3.php';
        include './modals/informativos/dLinea/mDP4.php';
        include './modals/informativos/dLinea/mDP5.php';
        include './modals/informativos/dLinea/mDP6.php';
        include './modals/informativos/dLinea/mDP7.php';
        include './modals/informativos/dLinea/mDP8.php';
        include './modals/informativos/dLinea/mDP9.php';
        include './modals/informativos/dLinea/mDP10.php';
        
        //MANTENIMIENTO
        include './modals/informativos/dMantenimiento/mDP1.php';
        include './modals/informativos/dMantenimiento/mDP2.php';
        include './modals/informativos/dMantenimiento/mDP3.php';
        include './modals/informativos/dMantenimiento/mDP4.php';
        include './modals/informativos/dMantenimiento/mDP5.php';
        include './modals/informativos/dMantenimiento/mDP6.php';
        include './modals/informativos/dMantenimiento/mDP7.php';
        include './modals/informativos/dMantenimiento/mDP8.php';
        include './modals/informativos/dMantenimiento/mDP9.php';
        include './modals/informativos/dMantenimiento/mDP10.php';
        
        //OFICINA
        include './modals/informativos/dOficina/mDP1.php';
        include './modals/informativos/dOficina/mDP2.php';
        include './modals/informativos/dOficina/mDP3.php';
        include './modals/informativos/dOficina/mDP4.php';
        include './modals/informativos/dOficina/mDP5.php';
        include './modals/informativos/dOficina/mDP6.php';
        include './modals/informativos/dOficina/mDP7.php';
        include './modals/informativos/dOficina/mDP8.php';
        include './modals/informativos/dOficina/mDP9.php';
        include './modals/informativos/dOficina/mDP10.php';
        
        //MODAL DE PUNTAJE
        include './modals/informativos/PuntosAuditoriaB.php';
        include './modals/informativos/PuntosAuditoriaR.php';
        include './modals/informativos/PuntosAuditoriaM.php';        
        
        //USUARIOS
        include './modals/usuarios/mDUsuario.php';
        include './modals/usuarios/mIUsuario.php';
        include './modals/usuarios/mUUsuario.php';
        
        //OPL
        include './modals/opl/mIOPL.php';
        
        include './db/funciones.php';       
   
        //LLAMAMOS LAS CONSULTAS           
        $cUAdminChampP = cUAuditoriaChampPiso();
        $cUAdminChampO = cUAuditoriaChampOficina(); 
        $cUAdminChampNoActivo = cUAuditoriaChampNOActivos();
        
    ?>    
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align = center id="headerFixed" class="contenedor"> 
        <div class='fila0 col-lg-6'>
            <img src="imagenes/log.jpg" style="height: 5.2vh; margin-top: 2.7vh" >
        </div>        
        <div class="fila1 col-lg-6" >
            <h3 class="tituloPortal" >
                PORTAL 5S's: Auditorias
            </h3 >
            <?php if ($name != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" onclick="logiin()" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 
</head>

<body>    
    <div id="main" class="menuModulo" style="border: 1px solid #FFF;" >
        <ul <?php if ($typeUser == 1) { ?> id="navA" <?php } else { ?> id="navC" <?php } ?> >
            <li><a href="index.php">Inicio</a></li>            
            <li><a class="btn-new-audit" >Auditoria</a></li>            
            <li><a href="report.php">Reportes</a></li>
            <li><a href="calendar.php">Calendario</a></li>
            <li><a href="info.php">Informacion</a></li>
            <li><a href="opl.php">Opl</a></li>
        </ul>
    </div>   
    <div id="menuPrincipal" >            
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" >
            <li class="nav-item">
                <a href="admin.php">Administradores</a>
            </li>
            <li class="nav-item">
                <a href="gerentes.php">Gerentes/Supervisores</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="champions.php">Champions</a>
            </li>
            <li class="nav-item">
                <a href="auditorias.php">Auditorias</a>
            </li>
            <li class="nav">
                <a href="deptos.php">Deptos</a>
            </li>            
            <li class="nav-item">
                <a  href="cadValor.php">Cadena de valor</a>
            </li>
            <li class="nav-item">
                <a href="rolVSE.php">Rol VSE</a>
            </li>
        </ul>
        <!-- Tab panes -->            
        <div class="tab-content" style="width: 98%; margin-left: 1%">           
            <button type="button" class="btn btn-warning btnAdd" data-toggle="modal" data-target="#mIUsuario"  >
                Agregar
            </button> 
            <br><br>
            <h3 class="text-center all-tittles" style="margin-top: -35px" >PISO</h3>
            <table>
                <thead >
                    <tr>
                        <th class="tblHxs" >DEPTO</th>
                        <th class="tblHxs" >USUARIO</th>
                        <th class="tblHxl" >NOMBRE</th>
                        <th class="tblHs" >ACCIÓN</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 0; $i < count($cUAdminChampP); $i++) { ?>
                        <tr>
                            <td class="tblBxs"> <?php echo $cUAdminChampP[$i][0] ?> </td>
                            <td class="tblBxs"> <?php echo $cUAdminChampP[$i][1] ?> </td>
                            <td class="tblBxl"> <?php echo $cUAdminChampP[$i][2] ?> </td>
                            <td class="tblBs"> 
                                <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#mUUsuario" 
                                    data-priv = "3"
                                    data-area = "1"
                                    data-estado ="1"
                                    data-depto = "<?php echo $cUAdminChampP[$i][0]?>" 
                                    data-usuario = "<?php echo $cUAdminChampP[$i][1]?>" 
                                    data-nombre = "<?php echo $cUAdminChampP[$i][2]?>" 
                                    data-correo = "<?php echo $cUAdminChampP[$i][4]?>" >
                                    <i class='glyphicon glyphicon-edit'></i> Modificar </button>
                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" 
                                    data-target="#mDUsuario" 
                                    data-usuario = "<?php echo $cUAdminChampP[$i][1]?>" 
                                    data-nombre = "<?php echo $cUAdminChampP[$i][2]?>" ><i class='glyphicon glyphicon-trash'></i> Eliminar</button>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

            <hr>
            <h3 class="text-center all-tittles">OFICINA</h3>
            <table>
                <thead >
                    <tr>
                        <th class="tblHxs" >DEPTO</th>
                        <th class="tblHxs" >USUARIO</th>
                        <th class="tblHxl" >NOMBRE</th>
                        <th class="tblHs" >ACCIÓN</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 0; $i < count($cUAdminChampO); $i++) { ?>
                        <tr>
                            <td class="tblBxs"> <?php echo $cUAdminChampO[$i][0] ?> </td>
                            <td class="tblBxs"> <?php echo $cUAdminChampO[$i][1] ?> </td>
                            <td class="tblBxl"> <?php echo $cUAdminChampO[$i][2] ?> </td>
                            <td class="tblBs">  
                                <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#mUUsuario" 
                                    data-priv = "3"
                                    data-area = "1"
                                    data-estado ="1"
                                    data-depto = "<?php echo $cUAdminChampO[$i][0]?>" 
                                    data-usuario = "<?php echo $cUAdminChampO[$i][1]?>" 
                                    data-nombre = "<?php echo $cUAdminChampO[$i][2]?>" 
                                    data-correo = "<?php echo $cUAdminChampO[$i][4]?>" >
                                    <i class='glyphicon glyphicon-edit'></i> Modificar </button>
                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" 
                                    data-target="#mDUsuario" 
                                    data-usuario = "<?php echo $cUAdminChampO[$i][1]?>" 
                                    data-nombre = "<?php echo $cUAdminChampO[$i][2]?>" ><i class='glyphicon glyphicon-trash'></i> Eliminar</button>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>            
        
            <hr>            
            <h3 class="text-center all-tittles">NO ACTIVOS</h3>
            <table>
                <thead >
                    <tr>
                        <th class="tblHxs" >DEPTO</th>
                        <th class="tblHxs" >USUARIO</th>
                        <th class="tblHxl" >NOMBRE</th>
                        <th class="tblHs" >ACCIÓN</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 0; $i < count($cUAdminChampNoActivo); $i++) { ?>
                        <tr>
                            <td class="tblBxs"> <?php echo $cUAdminChampNoActivo[$i][0] ?> </td>
                            <td class="tblBxs"> <?php echo $cUAdminChampNoActivo[$i][1] ?> </td>
                            <td class="tblBxl"> <?php echo $cUAdminChampNoActivo[$i][2] ?> </td>
                            <td class="tblBs">  
                                <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#mUUsuario" 
                                    data-priv = "3"
                                    data-area = "1"
                                    data-estado ="0"
                                    data-depto = "<?php echo $cUAdminChampNoActivo[$i][0]?>" 
                                    data-usuario = "<?php echo $cUAdminChampNoActivo[$i][1]?>" 
                                    data-nombre = "<?php echo $cUAdminChampNoActivo[$i][2]?>" 
                                    data-correo = "<?php echo $cUAdminChampNoActivo[$i][4]?>" >
                                    <i class='glyphicon glyphicon-edit'></i> Modificar </button>
                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" 
                                    data-target="#mDUsuario" 
                                    data-usuario = "<?php echo $cUAdminChampNoActivo[$i][1]?>" 
                                    data-nombre = "<?php echo $cUAdminChampNoActivo[$i][2]?>" ><i class='glyphicon glyphicon-trash'></i> Eliminar</button>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>             
            <br>            
        </div>
    </div>    
</body>
