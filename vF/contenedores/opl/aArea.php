<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
    <!-- LIBRERIAS PARA HIGHCHART -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>    
    
<?php 
    include '../../db/funciones.php';
    
    $anio = date("Y");   

    // RECIBIMOS LO PARAMETROS QUE SE SOLICITAN
    $anio = $_POST["a"];
    $mes = $_POST["m"];
    $area = $_POST["area"];
    
//    $anio = 2018;
//    $mes = 12;
//    $area = 'ICO';
    
    if($mes < 10){
        $mes = '0'.$mes;
    }

    //$tipoInfo = 0;
    //$fecha = $anio.'-'.$mes.'-01' ;   
    
    if($mes > 0){
        $tipoInfo = 1;
        $cResultArea = cPuntosAreaMes($mes, $anio, $area);
        
        $date = new DateTime;
        //$f = date('2018-12-1');
        $f = date('Y-m-d');
        $year = date("Y", strtotime('+1 day',strtotime($f))); 

        # Establecemos la fecha segun el estandar ISO 8601 (numero de semana)
        $date->setISODate("$year", 53);
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }        
        
        $fI = date ( 'm/d/Y' , strtotime('+1 day',strtotime($anio.'-'.$mes.'-01')));
        $feI = new DateTime(date($fI));
        $semanaI = (int)$feI->format('W');
        
        $lastday = date("d", mktime(0,0,0, $mes+1, 0, $anio)); 
        $fF = date ( 'm/d/Y' , strtotime('+1 day',strtotime($anio.'-'.$mes.'-'.$lastday)));
        $feF = new DateTime(date($fF));
        $semanaF = (int)$feF->format('W');
        
        $countDatos = 0;
        
        if ($semanaI < $semanaF ) {            
            for ($i = $semanaI; $i <= $semanaF; $i++) {
                $lbl[$i] = "W-".$i;
                $countDatos++;
                $pTotales[$i] = 0;
                $pAbiertos[$i] = 0;
                $pCerrados[$i] = 0;
                $prom[$i] = 0;
                $target[$i] = 100;
            }
        } else {
            //echo "sI: ",$semanaI," sF: ",$semanaF,"<br>" ;
            for ($i = $semanaI; $i <= $numSemanas; $i++) {
                $lbl[$i] = "W-".$i;
                $countDatos++;
                $pTotales[$i] = 0;
                $pAbiertos[$i] = 0;
                $pCerrados[$i] = 0;
                $prom[$i] = 0;
                $target[$i] = 100;
                //echo $i,', ', $pAbiertos[$i],', ', $pCerrados[$i],', ', $pTotales[$i],'<br>';
            }
            for ($i = 1; $i <= $semanaF; $i++) {
                $lbl[$i] = "W-".$i;
                $countDatos++;
                $pTotales[$i] = 0;
                $pAbiertos[$i] = 0;
                $pCerrados[$i] = 0;
                $prom[$i] = 0;
                $target[$i] = 100;
                //echo $i,', ', $pAbiertos[$i],', ', $pCerrados[$i],', ', $pTotales[$i],'<br>';
            } 
        } 
        
        //CUANDO SE SELECCIONA UN MES 
        //echo $fecha;
        $cOplTotales = cOplATotalesAnioMesArea($mes, $anio, $area);
        for ($i = 0; $i < count($cOplTotales); $i++) {
            $s = $cOplTotales[$i][0];
            $pTotales[$s] = $cOplTotales[$i][1]; 
        }
        
        $cOplAbiertos = cOplAAbiertosAnioMesArea($mes, $anio,$area);
        for ($i = 0; $i < count($cOplAbiertos); $i++) {
            $s = $cOplAbiertos[$i][0];
            $pAbiertos[$s] = $cOplAbiertos[$i][1];
        }
        
        $cOplCerrados = cOplACerradosAnioMesArea($mes, $anio, $area);
        for ($i = 0; $i < count($cOplCerrados); $i++) {
            $s = $cOplCerrados[$i][0];
            $pCerrados[$s] = $cOplCerrados[$i][1];
        } 
        
        for ($i = 0; $i < count($cOplTotales); $i++) {
            $s = $cOplTotales[$i][0];
            $prom[$s] = @round(($pCerrados[$s]*100)/$pTotales[$s],2);
            //echo "s: ",$s," pT: ",$pTotales[$s]," pA: ",$pAbiertos[$s]," pC: ",$pCerrados[$s]," -> ",$prom[$s],"<br>";
        }         
    } else {        
        $tipoInfo = 2;
        
        for ($i = 1; $i <= 12 ; $i++){
            $pTotales[$i] = 0;
            $pAbiertos[$i] = 0;
            $pCerrados[$i] = 0;
            $prom[$i] = 0;
            $target[$i] = 100;
        }
        
        $cResultArea = cPuntosAreaAnio( $anio, $area); 
        $countDatos = 12;
        $lbl[1]= "JAN";
        $lbl[2]= "FEB";
        $lbl[3]= "MAR";
        $lbl[4]= "APR";
        $lbl[5]= "MAY";
        $lbl[6]= "JUN";
        $lbl[7]= "JUL";
        $lbl[8]= "AUG";
        $lbl[9]= "SEP";
        $lbl[10]= "OCT";
        $lbl[11]= "NOV";
        $lbl[12]= "DEC";
        
        //CONSULTAS PARA CUANDO SE TIENE FULL EL AÑO
        $cOplTotales = cOplATotalesAnioArea($anio, $area);
        for ($i = 0; $i < count($cOplTotales); $i++) { 
            $m = $cOplTotales[$i][1]; 
            $pTotales[$m] = $cOplTotales[$i][2]; 
        }
        
        $cOplAbiertos = cOplAAbiertosAnioArea($anio, $area); 
        for ($i = 0; $i < count($cOplAbiertos); $i ++) { 
            $m = $cOplAbiertos[$i][1]; 
            $pAbiertos[$m] = $cOplAbiertos[$i][2]; 
        } 
        
        $cOplCerrados = cOplACerradosAnioArea($anio, $area);
        for ($i = 0; $i < count($cOplCerrados); $i++) {
            $m = $cOplCerrados[$i][1];
            $pCerrados[$m] = $cOplCerrados[$i][2]; 
        }
        
        for ($i = 0; $i < count($cOplTotales); $i++) {
            $m = $cOplTotales[$i][1];
            $prom[$m] = @round(($pCerrados[$m]*100)/$pTotales[$m],1);
            //echo "m: ",$m," pT: ",$pTotales[$m]," pA: ",$pAbiertos[$m]," pC: ",$pCerrados[$m]," -> ",$prom[$m],"<br>";
        }         
    }    
//    
//    if ($tipoInfo == 1 ){
//        if ($semanaI < $semanaF ) {            
//            for ($i = $semanaI; $i <= $semanaF; $i++) {
//                echo $lbl[$i],', ', $pAbiertos[$i],', ', $pCerrados[$i],', ', $pTotales[$i],'<br>';
//            }
//        } else {
//            for ($i = $semanaI; $i <= $numSemanas; $i++) {
//                echo $lbl[$i],', ', $pAbiertos[$i],', ', $pCerrados[$i],', ', $pTotales[$i],'<br>';
//            }
//            for ($i = 1; $i <= $semanaF; $i++) {
//                echo $lbl[$i],', ', $pAbiertos[$i],', ', $pCerrados[$i],', ', $pTotales[$i],'<br>';
//            } 
//        } 
//    }
//    
    
?>
    
    <div id = "chartdivA" style="width: 100%; height: 350px;">
        <script>
            var chart = AmCharts.makeChart("chartdivA", {
                "type": "serial",
                "theme": "light", 
                "titles": [{
                    "text": "<?php echo $area ?>"
                }],
                "dataProvider": [
                <?PHP if ($tipoInfo == 1 ) { 
                    if ($semanaI > $semanaF){
                        for ($i = $semanaI; $i <= $numSemanas; $i++) { ?>
                            {
                                "month": "<?php echo $lbl[$i]; ?>",
                                "madurez": <?php echo $prom[$i]; ?>,
                                "meta": <?php echo $target[$i]; ?>
                            },
                        <?php } for ($i = 1; $i <= $semanaF; $i++) { ?>           
                            {
                                "month": "<?php echo $lbl[$i]; ?>",
                                "madurez": <?php echo $prom[$i]; ?>,
                                "meta": <?php echo $target[$i]; ?>
                            },   
                <?PHP } 
                        } else { 
                        for ($i = $semanaI; $i <= $semanaF; $i++) {  ?> 
                        {
                            "month": "<?php echo $lbl[$i]; ?>",
                            "madurez": <?php echo $prom[$i]; ?>,
                            "meta": <?php echo $target[$i]; ?>
                        },
                <?php
                }}} else { 
                    for($j = 1; $j <= $countDatos; $j++){ ?>                    
                    {                    
                        "month": "<?php echo $lbl[$j]; ?>",
                        "madurez": <?php echo $prom[$j]; ?>,
                        "meta": <?php echo $target[$j]; ?>
                    }, 
                <?php } } ?>],
                "graphs": [{
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "madurez",
                    "title": "Cumplimiento",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
                }, {  
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "lineThickness": 2,
                    "lineColor": "#E82C0C",
                    "type": "smoothedLine",
                    "dashLength": 5,
                    "title": "Meta",
                    "useLineColorForBulletBorder": true,
                    "valueField": "meta",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"     
                }],
                "categoryField": "month",
                "categoryAxis": {
                    "tickLength": 20,
                    "labelRotation": 0
                },
                "legend": {
                    "equalWidths": false,
                    "useGraphSettings": true,
                    "valueAlign": "left",
                    "valueWidth": 50
                }
            });
        </script>
    </div>
    
    <div >
        <table >
            <thead >                
                <tr>
                    <th class="tblHxs" > PUNTOS </th>
                <?php 
                    if ($tipoInfo == 2 ) { 
                        for ($i = 1; $i <= $countDatos; $i++) { ?>
                        <th class="tblHxs-0" > <?php echo $lbl[$i]; ?> </th>
                <?php   } 
                    } else {
                        if ($semanaI < $semanaF ) {            
                            for ($i = $semanaI; $i <= $semanaF; $i++) {
                                ?>
                               <th class="tblHs-0" > <?php echo $lbl[$i];  ?> </th>
                <?php       }
                        } else {
                            for ($i = $semanaI; $i <= $numSemanas; $i++) { ?>
                                <th class="tblHs-0" > <?php echo $lbl[$i]; ?> </th>
                <?php       }
                            for ($i = 1; $i <= $semanaF; $i++) { ?>
                                <th class="tblHs-0" > <?php echo $lbl[$i];  ?> </th>
                <?php       }
                        }
                     }  ?>                        
                </tr>
            </thead>
            <tbody style="height: 78px" >
                <?php 
                    if ($tipoInfo == 2 ) { ?>
                            <tr>
                                <td class="tblBxs" > Abiertos </td>
                <?php       for ($i = 1; $i <= $countDatos; $i++) { ?>
                                <td class="tblBxs-0" > <?php if ($prom[$i] > 0 ){ $pFalt = 100-$prom[$i]; } else { $pFalt = 0; } echo $pAbiertos[$i],' - ',$pFalt,'%' ?> </th>
                <?php       }  ?>
                            </tr>
                            <tr>
                                <td class="tblBxs" > Cerrados </td>
                <?php       for ($i = 1; $i <= $countDatos; $i++) { ?>
                                <td class="tblBxs-0" > <?php echo $pCerrados[$i],' - ',$prom[$i],'%'  ?> </th>
                <?php       }  ?>  
                            </tr>
                            <tr>
                                <td class="tblBxs" > Totales </td>
                <?php       for ($i = 1; $i <= $countDatos; $i++) { ?>
                                <td class="tblBxs-0" > <?php echo $pTotales[$i]; ?> </th>
                <?php       }  ?>                    
                            </tr>        
                <?php   } else {
                        if ($semanaI < $semanaF ) { ?>
                            <tr>
                                <td class="tblBxs" > Abiertos </td>
                <?php       for ($i = $semanaI; $i <= $semanaF; $i++) { ?>
                                <td class="tblBs-0" > <?php if ($prom[$i] > 0 ){ $pFalt = 100-$prom[$i]; } else { $pFalt = 0; } echo $pAbiertos[$i],' - ',$pFalt,'%' ?> </th>
                <?php       }  ?>
                            </tr>
                            <tr>
                                <td class="tblBxs" > Cerrados </td>
                <?php       for ($i = $semanaI; $i <= $semanaF; $i++) { ?>
                                <td class="tblBs-0" > <?php echo $pCerrados[$i],' - ',$prom[$i],'%' ?> </th>
                <?php       }  ?>  
                            </tr>
                            <tr>
                                <td class="tblBxs" > Totales </td>
                <?php       for ($i = $semanaI; $i <= $semanaF; $i++) { ?>
                                <td class="tblBs-0" > <?php echo $pTotales[$i]; ?> </th>
                <?php       }  ?>                    
                            </tr>  
                <?php   } else { ?>
                            <tr>
                                <td class="tblBxs" > Abiertos </td>
                <?php       for ($i = $semanaI; $i <= $numSemanas; $i++) { ?>
                                <td class="tblBs-0" > <?php if ($prom[$i] > 0 ){ $pFalt = 100-$prom[$i]; } else { $pFalt = 0; } echo $pAbiertos[$i],' - ',$pFalt,'%' ?> </th>
                <?php       }  ?>                                    
                <?php       for ($i = 1; $i <= $semanaF; $i++) { ?>
                                <td class="tblBs-0" > <?php if ($prom[$i] > 0 ){ $pFalt = 100-$prom[$i]; } else { $pFalt = 0; } echo $pAbiertos[$i],' - ',$pFalt,'%' ?> </th>
                <?php       }  ?>
                            </tr>
                            
                            <tr>
                                <td class="tblBxs" > Cerrados </td>
                <?php       for ($i = $semanaI; $i <= $numSemanas; $i++) { ?>
                                <td class="tblBs-0" > <?php echo $pCerrados[$i],' - ',$prom[$i],'%' ?> </th>
                <?php       }  ?>  
                <?php       for ($i = 1; $i <= $semanaF; $i++) {  ?>
                                <td class="tblBs-0" > <?php echo $pCerrados[$i],' - ',$prom[$i],'%' ?> </th>
                <?php       }  ?>  
                            </tr>
                            
                            <tr>
                                <td class="tblBxs" > Totales </td>
                <?php       for ($i = $semanaI; $i <= $numSemanas; $i++) { ?>
                                <td class="tblBs-0" > <?php echo $pTotales[$i]; ?> </th>
                <?php       }  ?>                    
                <?php       for ($i = 1; $i <= $semanaF; $i++) { ?>
                                <td class="tblBs-0" > <?php echo $pTotales[$i]; ?> </th>
                <?php       }  ?>                    
                            </tr> 
                <?php   }   }   ?>
                            
            </tbody>
        </table>
    </div>
               