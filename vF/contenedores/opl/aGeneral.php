<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
 <!-- LIBRERIAS PARA HIGHCHART -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>    
<?php 
    include '../../db/funciones.php';
    
//    $anio = $_POST["a"];
//    $mes = $_POST["m"];
    $anio = 2019;
    $mes = 1;
    
    if ($mes < 10){
        $mes = '0'.$mes;
    }
    
    //$anio = date("Y");         
    $month[1]= "JAN";
    $month[2]= "FEB";
    $month[3]= "MAR";
    $month[4]= "APR";
    $month[5]= "MAY";
    $month[6]= "JUN";
    $month[7]= "JUL";
    $month[8]= "AUG";
    $month[9]= "SEP";
    $month[10]= "OCT";
    $month[11]= "NOV";
    $month[12]= "DEC"; 
    
    //CONTADOR DE DEPARTAMENTOS 
    $cDeptosPiso = cDeptosPiso();        
    for ($i = 0; $i < count($cDeptosPiso); $i++){
        $deptoPY[$i+1] = $cDeptosPiso[$i][0];
        $pTotalesP[$i+1] = 0;
        $pAbiertosP[$i+1] = 0;
        $pCerradosP[$i+1] = 0;
        $promP[$i+1] = 0;
    }
    
    //CONTADOR DE DEPARTAMENTOS
    $cDeptosOficina = cDeptosOficina();
    for ($i = 0; $i < count($cDeptosOficina); $i++){
        $deptoOY[$i+1] = $cDeptosOficina[$i][0];
        $pTotalesO[$i+1] = 0;
        $pAbiertosO[$i+1] = 0;
        $pCerradosO[$i+1] = 0;
        $promO[$i+1] = 0;
    }

    if ($mes == 0){
        /******************* PISO *********************/ 
        $cOplTotales = cOplGTotalesAnio($anio);
        for ($i = 0; $i < count($cOplTotales); $i++) { 
            for ($j = 1; $j <= count($cDeptosPiso); $j++) {
                if ($cOplTotales[$i][0] == $deptoPY[$j]) {
                    $pTotalesP[$j] = $cOplTotales[$i][1];
                }
            }
            
            for ($j = 1; $j <= count($cDeptosOficina); $j++) {
                if ($cOplTotales[$i][0] == $deptoOY[$j]){
                    $pTotalesO[$j] = $cOplTotales[$i][1];
                }
            }
        }
        
        //PUNTOS ABIERTOS
        $cOplAbiertos = cOplGAbiertosAnio($anio);
        for ($i = 0; $i < count($cOplAbiertos); $i++){
            for ($j = 1; $j <= count($cDeptosPiso); $j++) {
                if ($cOplAbiertos[$i][0] == $deptoPY[$j]) {
                    $pAbiertosP[$j] = $cOplAbiertos[$i][1];
                }
            }
            
            for ($j = 1; $j <= count($cDeptosOficina); $j++) {
                if ($cOplAbiertos[$i][0] == $deptoOY[$j]){
                    $pAbiertosO[$j] = $cOplAbiertos[$i][1];
                }
            }
        }
        
        //PUNTOS CERRADOS
        $cOplCerrados = cOplGCerradosAnio($anio);
        for ($i = 0; $i < count($cOplCerrados); $i++){
            for ($j = 1; $j <= count($cDeptosPiso); $j++) {
                if ($cOplCerrados[$i][0] == $deptoPY[$j]) {
                    $pCerradosP[$j] = $cOplCerrados[$i][1];
                    $promP[$j] = @round(($pCerradosP[$j] *100)/$pTotalesP[$j],1);
                }
            }
            
            for ($j = 1; $j <= count($cDeptosOficina); $j++) {
                if ($cOplCerrados[$i][0] == $deptoOY[$j]){
                    $pCerradosO[$j] = $cOplCerrados[$i][1];
                    $promO[$j] = @round(($pCerradosO[$j] *100)/$pTotalesO[$j],1);
                }
            }
        }
//        
//        //OBTENEMOS EL PROCENTAJE POR DEPARTAMENTO
//        for ($j = 1; $j <= count($cDeptosPiso); $j++) {
//            //echo $deptoPY[$j],' -> ',$promP[$j],'<br>';            
//        }
//
//        for ($j = 1; $j <= count($cDeptosOficina); $j++) {
//            //echo $deptoOY[$j],' -> ',$promO[$j],'<br>';
//        }

    } else {
        /******************* PISO *********************/
        $cOplTotales = cOplGTotalesAnioMes($mes, $anio);
        for ($i = 0; $i < count($cOplTotales); $i++) { 
            for ($j = 1; $j <= count($cDeptosPiso); $j++) {
                if ($cOplTotales[$i][0] == $deptoPY[$j]) {
                    $pTotalesP[$j] = $cOplTotales[$i][1];
                }
            }
            
            for ($j = 1; $j <= count($cDeptosOficina); $j++) {
                if ($cOplTotales[$i][0] == $deptoOY[$j]){
                    $pTotalesO[$j] = $cOplTotales[$i][1];
                }
            }
        }
        
        //PUNTOS ABIERTOS
        $cOplAbiertos = cOplGAbiertosAnioMes($mes, $anio);
        for ($i = 0; $i < count($cOplAbiertos); $i++) {
            for ($j = 1; $j <= count($cDeptosPiso); $j++) {
                if ($cOplAbiertos[$i][0] == $deptoPY[$j]) {
                    $pAbiertosP[$j] = $cOplAbiertos[$i][1];
                }
            }
            
            for ($j = 1; $j <= count($cDeptosOficina); $j++) {
                if ($cOplAbiertos[$i][0] == $deptoOY[$j]) {
                    $pAbiertosO[$j] = $cOplAbiertos[$i][1];
                }
            }
        }
        
        //PUNTOS CERRADOS
        $cOplCerrados = cOplGCerradosAnioMes($mes, $anio);
        for ($i = 0; $i < count($cOplCerrados); $i++) {
            for ($j = 1; $j <= count($cDeptosPiso); $j++) {
                if ($cOplCerrados[$i][0] == $deptoPY[$j]) {
                    $pCerradosP[$j] = $cOplCerrados[$i][1];
                    $promP[$j] = @round(($pCerradosP[$j] *100)/$pTotalesP[$j],1);
                }
            }
            
            for ($j = 1; $j <= count($cDeptosOficina); $j++) {
                if ($cOplCerrados[$i][0] == $deptoOY[$j]){
                    $pCerradosO[$j] = $cOplCerrados[$i][1];
                    $promO[$j] = @round(($pCerradosO[$j] *100)/$pTotalesO[$j],1);
                }
            }
        }
        
        //OBTENEMOS EL PROCENTAJE POR DEPARTAMENTO
//        for ($j = 1; $j <= count($cDeptosPiso); $j++) {
//            echo $deptoPY[$j],' -> ',$promP[$j],'<br>';            
//        }
//
//        for ($j = 1; $j <= count($cDeptosOficina); $j++) {
//            echo $deptoOY[$j],' -> ',$promO[$j],'<br>';
//        }
        
    } 

?>    

    <div id = "chartdiv1" style="width: 100%; height: 320px;">
        <script>
            var chart = AmCharts.makeChart("chartdiv1", {
                "type": "serial",
                "theme": "light", 
                "titles": [{
                    "text": "<?php echo " PISO " ?>"
                }],
                "dataProvider": [
                <?PHP for($j = 1; $j <= count($cDeptosPiso); $j++){ ?>
                    {
                        "month": "<?php echo $deptoPY[$j] ?>",
                        "madurez": <?php echo $promP[$j];?>,
                        "meta": 99
                    },
                <?PHP } ?>],
                "graphs": [{
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "madurez",
                    "title": "Madurez",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>",
                    "max": 101
                }, {  
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "lineThickness": 2,
                    "lineColor": "#E82C0C",
                    "type": "smoothedLine",
                    "dashLength": 5,
                    "title": "Meta",
                    "useLineColorForBulletBorder": true,
                    "valueField": "meta",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"     
                }],
                "categoryField": "month",
                "categoryAxis": {
                    "tickLength": 1,
                    "labelRotation": 90
                }
            });
        </script>
    </div>
    
<!--    <div style="margin-left: -2%">
        <table >
            <thead >                
                <tr>
                    <th class="tblHxsty" > PUNTOS </th>
                <?php 
                    for ($i = 1; $i <= count($cDeptosPiso); $i++) { ?>
                        <th class="tblHxsty-0" > <?php echo ""; //$deptoPY[$i]; ?> </th>
                <?php } ?>                                          
                </tr>
            </thead>
            <tbody style="height: 78px" >                
                <tr>
                    <td class="tblHxsty" > Abiertos </td>
                <?php 
                    for ($i = 1; $i <= count($cDeptosPiso); $i++) { ?>
                        <td class="tblBxsty-0" > <?php if ($promP[$i] > 0 ){ $pFalt = 100-$promP[$i]; } else { $pFalt = 0; } echo $pAbiertosP[$i],' - ',$pFalt,'%' ?> </td>
                <?php } ?>
                </tr>
                <tr>
                    <td class="tblHxsty" > Cerrados </td>
                <?php 
                    for ($i = 1; $i <= count($cDeptosPiso); $i++) { ?>
                        <td class="tblBxsty-0" > <?php echo $pCerradosP[$i]; ?> </td>
                <?php } ?>    
                </tr>
                
                <tr>
                    <td class="tblHxsty" > Totales </td>
                <?php   for ($i = 1; $i <= count($cDeptosPiso); $i++) { ?>
                        <td class="tblBxsty-0" > <?php echo $pTotalesP[$i]; ?> </th>
                <?php }  ?>                    
                </tr>                            
            </tbody>
        </table>
    </div>-->
    
    <div id = "chartdiv2" style="width: 100%; height: 320px;">
        <script>
            var chart = AmCharts.makeChart("chartdiv2", {
                "type": "serial",
                "theme": "light", 
                "max": 100,
                "titles": [{
                    "text": "<?php echo " OFICINA " ?>"
                }],
                "dataProvider": [
                <?PHP for($j = 1; $j <= count($cDeptosOficina); $j++){ ?>
                    {
                        "month": "<?php echo $deptoOY[$j] ?>",
                        "madurez": <?php echo $promO[$j];?>,
                        "meta": 99
                    },
                <?PHP } ?>],
                "graphs": [{
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "madurez",
                    "title": "Madurez",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>",
                    "max": 101
                }, {  
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "lineThickness": 2,
                    "lineColor": "#E82C0C",
                    "type": "smoothedLine",
                    "dashLength": 5,
                    "title": "Meta",
                    "useLineColorForBulletBorder": true,
                    "valueField": "meta",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>",
                    "max": 100
                }],
                "categoryField": "month",                
                "categoryAxis": {
                    "tickLength": 1,
                    "labelRotation": 90
                }
                //,
//                "legend": {
//                    "equalWidths": false,
//                    "useGraphSettings": true,
//                    "valueAlign": "left",
//                    "valueWidth": 50
//                }
            });
        </script>
    </div>            
    

<!--    <div style="margin-left: -2%">
        <table >
            <thead >                
                <tr>
                    <th class="tblHxsty" > PUNTOS </th>
                <?php 
                    for ($i = 1; $i <= count($cDeptosOficina); $i++) { ?>
                        <th class="tblHxsty-0" > <?php echo ""; //$deptoPY[$i]; ?> </th>
                <?php } ?>                                          
                </tr>
            </thead>
            <tbody style="height: 78px" > 
                <tr> 
                    <td class="tblHxsty" > Abiertos </td> 
                <?php 
                    for ($i = 1; $i <= count($cDeptosOficina); $i++) { ?>
                        <td class="tblBxsty-0" > <?php if ($promO[$i] > 0 ){ $pFalt = 100-$promO[$i]; } else { $pFalt = 0; } echo $pAbiertosO[$i],' - ',$pFalt,'%' ?> </td>
                <?php } ?>
                </tr> 
                
                <tr> 
                    <td class="tblHxsty" > Cerrados </td> 
                <?php 
                    for ($i = 1; $i <= count($cDeptosOficina); $i++) { ?> 
                        <td class="tblBxsty-0" > <?php echo $pCerradosO[$i]; ?> </td> 
                <?php } ?> 
                </tr> 
                
                <tr> 
                    <td class="tblHxsty" > Totales </td> 
                <?php   for ($i = 1; $i <= count($cDeptosOficina); $i++) { ?> 
                        <td class="tblBxsty-0" > <?php echo $pTotalesO[$i]; ?> </th> 
                <?php } ?> 
                </tr> 
            </tbody> 
        </table>
    </div>-->
    
    