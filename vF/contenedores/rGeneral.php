<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

 <!-- LIBRERIAS PARA HIGHCHART -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
<!--    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />-->
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    
    <!--<link rel="stylesheet" href="../css/modulo.css">-->
    
<?php 
    include '../db/funciones.php';    
    
    if (isset($_POST["a"]) && isset($_POST["m"])){
        $anio = $_POST["a"];
        $mes = $_POST["m"];
    } else {
        $anio = date("Y");
        $mes = date("m");
    }
    
    //$anio = date("Y");         
    $month[1]= "JAN";
    $month[2]= "FEB";
    $month[3]= "MAR";
    $month[4]= "APR";
    $month[5]= "MAY";
    $month[6]= "JUN";
    $month[7]= "JUL";
    $month[8]= "AUG";
    $month[9]= "SEP";
    $month[10]= "OCT";
    $month[11]= "NOV";
    $month[12]= "DEC"; 
    
    //CONTADOR DE DEPARTAMENTOS 
    $cDeptosPiso = cDeptosPiso();        
    for ($i = 0; $i < count($cDeptosPiso); $i++){
        $deptoPY[$i+1] = $cDeptosPiso[$i][0];
        $pDeptoPY[$i+1] = 0;
        $cantAuditPlanPY[$i+1] = 0;
        $cantAuditRealPY[$i+1] = 0;
        $cumplimientoPY[$i+1] = 0; 
    }
    
    //CONTADOR DE DEPARTAMENTOS
    $cDeptosOficina = cDeptosOficina();
    for ($i = 0; $i < count($cDeptosOficina); $i++){
        $deptoOY[$i+1] = $cDeptosOficina[$i][0];
        $pDeptoOY[$i+1]= 0;
        $cantAuditPlanOY[$i+1]= 0;
        $cantAuditRealOY[$i+1]= 0;
        $cumplimientoOY[$i+1]= 0;

        //echo $deptoOY[$i+1],'<br>';
    }

    
    if ($mes == 0){
        /******************* PISO *********************/ 
        //MADUREZ
        $cPuntosPY = cPuntosPisoGY($anio);
        for($i = 0; $i < count($cPuntosPY); $i++){
            for ($j = 1; $j <= count($cDeptosPiso); $j++){
                if ($cPuntosPY[$i][0] == $deptoPY[$j]){
                    $pDeptoPY[$j] = $cPuntosPY[$i][1];
                }
            }
        }

        //CUMPLIMIENTO
        $cAuditPlaneadasPY = cAPlanPisoGY($anio);
        for($i = 0; $i < count($cAuditPlaneadasPY); $i++){
            for ($j = 1; $j <= count($cDeptosPiso); $j++){
                if ($cAuditPlaneadasPY[$i][0] == $deptoPY[$j]){
                    $cantAuditPlanPY[$j] = $cAuditPlaneadasPY[$i][1];
                }
            }
        }

        $cAuditRealPY = cARealPisoGY($anio);
        for($i = 0; $i < count($cAuditRealPY); $i++){
            for ($j = 1; $j <= count($cDeptosPiso); $j++){
                if ($cAuditRealPY[$i][0] == $deptoPY[$j]){
                    //$cantAuditReal[$j] = $cAudioriasReal[$i][1];
                    $cumplimientoPY[$j] = ($cAuditRealPY[$j] * 100 ) / $cantAuditPlanPY[$j];
                }
            }
        }

        /******************* OFICINA *********************/        
        //MADUREZ
        $cPuntosOY = cPuntosPisoGY($anio);
        for($i = 0; $i < count($cPuntosOY); $i++){
            for ($j = 1; $j <= count($cDeptosOficina); $j++){
                //echo $cPuntosOY[$i][0],', ', $deptoOY[$j],'<br>' ;
                if ($cPuntosOY[$i][0] == $deptoOY[$j]){
                    $pDeptoOY[$j] = $cPuntosOY[$i][1];
                }
            }
        }

        //CUMPLIMIENTO
        $cAuditPlaneadasOY = cAPlanPisoGY($anio);
        for($i = 0; $i < count($cAuditPlaneadasOY); $i++){
            for ($j = 1; $j <= count($cDeptosOficina); $j++){
                if ($cAuditPlaneadasOY[$i][0] == $deptoPY[$j]){
                    $cantAuditPlanOY[$j] = $cAuditPlaneadasOY[$i][1];
                }
            }
        }

        $cAuditRealOY = cARealPisoGY($anio);
        for($i = 0; $i < count($cAuditRealOY); $i++){
            for ($j = 1; $j <= count($cDeptosOficina); $j++){
                if ($cAuditRealOY[$i][0] == $deptoPY[$j]){
                    $cumplimientoOY[$j] = ($cAuditRealOY[$j] * 100 ) / $cantAuditPlanOY[$j];
                }
            }
        }
    } else {
        /******************* PISO *********************/
        //MADUREZ
        $cPuntosPY = cPuntosPisoGYM($anio, $mes);
        for($i = 0; $i < count($cPuntosPY); $i++){
            for ($j = 1; $j <= count($cDeptosPiso); $j++){
                if ($cPuntosPY[$i][0] == $deptoPY[$j]){
                    $pDeptoPY[$j] = $cPuntosPY[$i][1];
                }
            }
        }

        //CUMPLIMIENTO
        $cAuditPlaneadasPY = cAPlanPisoGYM($anio, $mes);
        for($i = 0; $i < count($cAuditPlaneadasPY); $i++){
            for ($j = 1; $j <= count($cDeptosPiso); $j++){
                if ($cAuditPlaneadasPY[$i][0] == $deptoPY[$j]){
                    $cantAuditPlanPY[$j] = $cAuditPlaneadasPY[$i][1];
                }
            }
        }

        $cAuditRealPY = cARealPisoGYM($anio, $mes);
        for($i = 0; $i < count($cAuditRealPY); $i++){
            for ($j = 1; $j <= count($cDeptosPiso); $j++){
                if ($cAuditRealPY[$i][0] == $deptoPY[$j]){
                    //$cantAuditReal[$j] = $cAudioriasReal[$i][1];
                    $cumplimientoPY[$j] = ($cAuditRealPY[$j] * 100 ) / $cantAuditPlanPY[$j];
                }
            }
        }

        /******************* OFICINA *********************/
        //MADUREZ
        $cPuntosOY = cPuntosPisoGYM($anio, $mes);
        for($i = 0; $i < count($cPuntosOY); $i++){
            for ($j = 1; $j <= count($cDeptosOficina); $j++){
                //echo $cPuntosOY[$i][0],', ', $deptoOY[$j],'<br>' ;
                if ($cPuntosOY[$i][0] == $deptoOY[$j]){
                    $pDeptoOY[$j] = $cPuntosOY[$i][1];
                }
            }
        }

        //CUMPLIMIENTO
        $cAuditPlaneadasOY = cAPlanPisoGYM($anio, $mes);
        for($i = 0; $i < count($cAuditPlaneadasOY); $i++){
            for ($j = 1; $j <= count($cDeptosOficina); $j++){
                if ($cAuditPlaneadasOY[$i][0] == $deptoPY[$j]){
                    $cantAuditPlanOY[$j] = $cAuditPlaneadasOY[$i][1];
                }
            }
        }

        $cAuditRealOY = cARealPisoGYM($anio, $mes);
        for($i = 0; $i < count($cAuditRealOY); $i++){
            for ($j = 1; $j <= count($cDeptosOficina); $j++){
                if ($cAuditRealOY[$i][0] == $deptoPY[$j]){
                    $cumplimientoOY[$j] = ($cAuditRealOY[$j] * 100 ) / $cantAuditPlanOY[$j];
                }
            }
        }
    } 

?>    

    <div id = "chartdiv1" style="width: 100%; height: 380px;">
        <script>
            var chart = AmCharts.makeChart("chartdiv1", {
                "type": "serial",
                "theme": "light", 
                "titles": [{
                    "text": "<?php echo " PISO " ?>"
                }],
                "dataProvider": [
                <?PHP for($j = 1; $j <= count($cDeptosPiso); $j++){ ?>
                    {
                        "month": "<?php echo $deptoPY[$j] ?>",
                        "madurez": <?php echo $pDeptoPY[$j];?>,
                        "cumplimiento": <?php echo $cumplimientoPY[$j];?>,
                        "meta": 85
                    },
                <?PHP } ?>],
                "valueAxes": [{
                    "maximum":100
                }],
                "graphs": [{
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "madurez",
                    "title": "Madurez",
                    "gridCount": 100, 
                    "fillAlphas": 0.85, 
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>",
                    "max": 100
                }, {    
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "hideBulletsCount": 50,
                    "lineThickness": 2,
                    "lineColor": "#43A66E",
                    "type": "smoothedLine",
                    "title": "Cumplimiento",
                    "useLineColorForBulletBorder": true,
                    "valueField": "cumplimiento",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
                }, {  
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "lineThickness": 2,
                    "lineColor": "#E82C0C",
                    "type": "smoothedLine",
                    "dashLength": 5,
                    "title": "Meta",
                    "useLineColorForBulletBorder": true,
                    "valueField": "meta",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"     
                }],
                "categoryField": "month",
                "categoryAxis": {
                    "tickLength": 1,
                    "labelRotation": 90
                }
            });
        </script>
    </div>
    
    <div id = "chartdiv2" style="width: 100%; height: 380px;">
        <script>
            var chart = AmCharts.makeChart("chartdiv2", {
                "type": "serial",
                "theme": "light",
                "titles": [{
                    "text": "<?php echo " OFICINA " ?>"
                }],
                "dataProvider": [
                <?PHP for($j = 1; $j <= count($cDeptosOficina); $j++) { ?>
                    {
                        "month": "<?php echo $deptoOY[$j] ?>",
                        "madurez": <?php echo $pDeptoOY[$j];?>,
                        "cumplimiento": <?php echo $cumplimientoOY[$j];?>,
                        "meta": 85
                    },
                <?PHP } ?>],
                "valueAxes": [{
                    "maximum":100
                }],
                "graphs": [{
                    "fillAlphas": 0.85, 
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "madurez",
                    "title": "Madurez",                    
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
                }, {    
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "hideBulletsCount": 50,
                    "lineThickness": 2,
                    "lineColor": "#43A66E",
                    "type": "smoothedLine",
                    "title": "Cumplimiento",
                    "useLineColorForBulletBorder": true,
                    "valueField": "cumplimiento",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
                }, {  
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "lineThickness": 2,
                    "lineColor": "#E82C0C",
                    "type": "smoothedLine",
                    "dashLength": 5,
                    "title": "Meta",
                    "useLineColorForBulletBorder": true,
                    "valueField": "meta",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
                }],
                "categoryField": "month",                
                "categoryAxis": {
                    "tickLength": 1,
                    "labelRotation": 90
                }
            });
            chart.events.on("ready", function(ev) {
                valueAxis.max = 100;
            });
        </script>
    </div>            