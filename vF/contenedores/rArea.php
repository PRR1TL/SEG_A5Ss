<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
    <!-- LIBRERIAS PARA HIGHCHART -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
        
<?php 
    include '../db/funciones.php';
    
    $anio = date("Y");         
     
    // RECIBIMOS LO PARAMETROS QUE SE SOLICITAN
    $anio = $_POST["a"];
    $mes = $_POST["m"];
    $area = $_POST["area"];
    
//    $anio = 2019;
//    $mes = 4;
//    $area = "ICO";
//    
    //CONSULTA PARA AREA    
    for ($i = 1; $i <= 12 ; $i++){
        $madurez[$i] = 0;
        $cumplimiento[$i] = 0;
        $target[$i] = 90;
    }
    
    if($mes > 0) { 
        $cResultArea = cPuntosAreaMes($mes, $anio, $area);
        
        $date = new DateTime;
        //$f = date('2018-12-1');
        $f = date('Y-m-d');
        $year = date("Y", strtotime('+1 day',strtotime($f))); 

        # Establecemos la fecha segun el estandar ISO 8601 (numero de semana)
        $date->setISODate("$year", 53);
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }        
        
        $fI = date ( 'm/d/Y' , strtotime('+1 day',strtotime($anio.'-'.$mes.'-01')));
        $feI = new DateTime(date($fI));
        $semanaI = (int)$feI->format('W');
        
        $lastday = date("d", mktime(0,0,0, $mes+1, 0, $anio)); 
        $fF = date ( 'm/d/Y' , strtotime($anio.'-'.$mes.'-'.$lastday));
        $feF = new DateTime(date($fF));
        $semanaF = (int)$feF->format('W');
        
        //echo "<br>fI: ",$fI," fF: ",$fF," -> ",$semanaI,", ",$semanaF,"<br>";        
        
        $countDatos = 1;
        if ($semanaI < $semanaF ) {            
            for ($i = $semanaI; $i <= $semanaF; $i++) { 
                $lbl[$countDatos] = "W-".$i; 
                //echo $countDatos,' -> ',$lbl[$countDatos],'<br>';
                $countDatos++; 
            }
        } else {
            for ($i = $semanaI; $i <= $numSemanas; $i++) { 
                $lbl[$i] = "W-".$i;
                $countDatos++;
            }
            for ($i = 0; $i <= $semanaF; $i++) {
                $lbl[$i] = "W-".$i;
                $countDatos++;
            }
        }
        
        $x = 1;
        for ($i = 0; $i < count($cResultArea); $i++) { 
            $madurez[$x] = @round($cResultArea[$i][1]/$cResultArea[$i][2],2);            
            //echo '<br>',$x,', ',$madurez[$x];
            $x++;
        } 
    } else { 
        $cResultArea = cPuntosAreaAnio( $anio, $area); 
        $countDatos = 13; 
        $lbl[1]= "JAN"; 
        $lbl[2]= "FEB"; 
        $lbl[3]= "MAR"; 
        $lbl[4]= "APR";
        $lbl[5]= "MAY";
        $lbl[6]= "JUN";
        $lbl[7]= "JUL";
        $lbl[8]= "AUG";
        $lbl[9]= "SEP";
        $lbl[10]= "OCT";
        $lbl[11]= "NOV";
        $lbl[12]= "DEC";
        
        //CONSULTA PARA DESGLOSE DE DATOS DEL MES
        //MADUREZ
        for($i = 0; $i < count($cResultArea); $i++){
            $mes = $cResultArea[$i][0];
            $madurez[$mes] = $cResultArea[$i][1]/$cResultArea[$i][2];
        } 
        //CUMPLIMIENTO (CUANTAS AUDITORIAS ESTAN REALIZANDO)
        
    }
?>
    
    <div id = "chartdivA" style="width: 100%; height: 350px;">
        <script>
            var chart = AmCharts.makeChart("chartdivA", {
                "type": "serial",
                "theme": "light", 
                "titles": [{
                    "text": "<?php echo $area ?>"
                }],
                "dataProvider": [
                <?PHP for($j = 1; $j < $countDatos; $j++){ ?>
                    {
                        "month": "<?php echo $lbl[$j]; ?>",
                        "madurez": <?php echo $madurez[$j]; ?>,
                        "cumplimiento": <?php echo $cumplimiento[$j]; ?>,
                        "meta": <?php echo $target[$j]; ?>
                    },
                <?PHP } ?>],
                "valueAxes": [{
                    "maximum":100
                }],
                "graphs": [{
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "madurez",
                    "title": "Madurez",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
                }, {    
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "hideBulletsCount": 50,
                    "lineThickness": 2,
                    "lineColor": "#43A66E",
                    "type": "smoothedLine",
                    "title": "Cumplimiento",
                    "useLineColorForBulletBorder": true,
                    "valueField": "cumplimiento",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
                }, {  
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "lineThickness": 2,
                    "lineColor": "#E82C0C",
                    "type": "smoothedLine",
                    "dashLength": 5,
                    "title": "Meta",
                    "useLineColorForBulletBorder": true,
                    "valueField": "meta",
                    "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"     
                }],
                "categoryField": "month",
                "categoryAxis": {
                    "tickLength": 20,
                    "labelRotation": 0
                },
                "legend": {
                    "equalWidths": false,
                    "useGraphSettings": true,
                    "valueAlign": "left",
                    "valueWidth": 50
                }
            });
        </script>
    </div>
               