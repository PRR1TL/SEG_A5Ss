<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    date_default_timezone_set("America/Mexico_City");    
    $_SESSION['path'] = $_SERVER['PHP_SELF'];
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
        $typeEv = $_SESSION['ev'];
        $depto = $_SESSION["depto"];
        
        if($typeUser == 0  ){
            echo '<script>location.href = "./index.php";</script>';
        } 
    } else {
        $userName = ''; 
        $name = 'INICIAR SESION'; 
        $typeUser = 0; 
        $typeEv = 0; 
        $depto = 'ICO'; 
        echo '<script>location.href = "./index.php";</script>';
    }
    
    $dia = date("Y-m-d");
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  
    
    <!--ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS-->
    <link rel="stylesheet" href="css/mod.css">
    <script src="js/champion.js"> </script>
    
     <?php 
        include './modals/informativos/help.php';
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MOD AL INFORMATIVOS
        include './modals/accesos/login.php';
        include './modals/accesos/cerrarSesion.php';
        include './modals/informativos/rules.php';
        
        //MODALS PARA LA SELECCION DE AUDITORIAS
        //DE ACUERDO A LOS PRIVILEGIOS DE USUARIOS
        include './modals/vse/AreaAudit.php';
        switch ($typeUser) {
            case 1:
                include './modals/vse/AreaAudit.php'; 
                include './modals/vse/DeptoAudit.php'; 
                include './modals/vse/mAuditCon.php'; 
                break;
            
            case 2: 
                include './modals/supervisor/DeptoAudit.php'; 
                break;
            
            case 6:                
                include './modals/informativos/TipoAuditoriaCh.php';
                break;
        }
        
        include './modals/accesos/configU.php';
        
        include './modals/informativos/InfoAuditoria.php';
        include './modals/informativos/NoneAuditoria.php';        
        
        include './modals/auditoria/daily/checkAlmacen.php';
        include './modals/auditoria/daily/checkLab.php';
        include './modals/auditoria/daily/checkLinea.php';
        include './modals/auditoria/daily/checkMantenimiento.php';
        include './modals/auditoria/daily/checkOficina.php';
        
        include './modals/auditoria/monthly/almacen.php';
        include './modals/auditoria/monthly/almacen2.php';
        include './modals/auditoria/monthly/laboratorio.php';
        include './modals/auditoria/monthly/laboratorio2.php';
        include './modals/auditoria/monthly/linea.php';
        include './modals/auditoria/monthly/linea2.php';
        include './modals/auditoria/monthly/mantenimiento.php';
        include './modals/auditoria/monthly/mantenimiento2.php';
        include './modals/auditoria/monthly/oficina.php';
        
        include './modals/auditoria/weekly/almacen.php';
        include './modals/auditoria/weekly/laboratorio.php';
        include './modals/auditoria/weekly/linea.php';
        include './modals/auditoria/weekly/mantenimiento.php';
        include './modals/auditoria/weekly/oficina.php';      
        
        include './modals/informativos/PuntosAuditoriaB.php';
        include './modals/informativos/PuntosAuditoriaR.php';
        include './modals/informativos/PuntosAuditoriaM.php';
        
        //AYUDAS VISUALES DE PREGUNTAS DE DAILY
        //ALMACEN
        include './modals/informativos/dAlmacen/mDP1.php';
        include './modals/informativos/dAlmacen/mDP2.php';
        include './modals/informativos/dAlmacen/mDP3.php';
        include './modals/informativos/dAlmacen/mDP4.php';
        include './modals/informativos/dAlmacen/mDP5.php';
        include './modals/informativos/dAlmacen/mDP6.php';
        include './modals/informativos/dAlmacen/mDP7.php';
        include './modals/informativos/dAlmacen/mDP8.php';
        include './modals/informativos/dAlmacen/mDP9.php';
        include './modals/informativos/dAlmacen/mDP10.php';
        
        //LABORATORIO
        include './modals/informativos/dLaboratorio/mDP1.php';
        include './modals/informativos/dLaboratorio/mDP2.php';
        include './modals/informativos/dLaboratorio/mDP3.php';
        include './modals/informativos/dLaboratorio/mDP4.php';
        include './modals/informativos/dLaboratorio/mDP5.php';
        include './modals/informativos/dLaboratorio/mDP6.php';
        include './modals/informativos/dLaboratorio/mDP7.php';
        include './modals/informativos/dLaboratorio/mDP8.php';
        include './modals/informativos/dLaboratorio/mDP9.php';
        include './modals/informativos/dLaboratorio/mDP10.php';
        
        //LINEA
        include './modals/informativos/dLinea/mDP1.php';
        include './modals/informativos/dLinea/mDP2.php';
        include './modals/informativos/dLinea/mDP3.php';
        include './modals/informativos/dLinea/mDP4.php';
        include './modals/informativos/dLinea/mDP5.php';
        include './modals/informativos/dLinea/mDP6.php';
        include './modals/informativos/dLinea/mDP7.php';
        include './modals/informativos/dLinea/mDP8.php';
        include './modals/informativos/dLinea/mDP9.php';
        include './modals/informativos/dLinea/mDP10.php';
        
        //MANTENIMIENTO
        include './modals/informativos/dMantenimiento/mDP1.php';
        include './modals/informativos/dMantenimiento/mDP2.php';
        include './modals/informativos/dMantenimiento/mDP3.php';
        include './modals/informativos/dMantenimiento/mDP4.php';
        include './modals/informativos/dMantenimiento/mDP5.php';
        include './modals/informativos/dMantenimiento/mDP6.php';
        include './modals/informativos/dMantenimiento/mDP7.php';
        include './modals/informativos/dMantenimiento/mDP8.php';
        include './modals/informativos/dMantenimiento/mDP9.php';
        include './modals/informativos/dMantenimiento/mDP10.php';
        
        //OFICINA
        include './modals/informativos/dOficina/mDP1.php';
        include './modals/informativos/dOficina/mDP2.php';
        include './modals/informativos/dOficina/mDP3.php';
        include './modals/informativos/dOficina/mDP4.php';
        include './modals/informativos/dOficina/mDP5.php';
        include './modals/informativos/dOficina/mDP6.php';
        include './modals/informativos/dOficina/mDP7.php';
        include './modals/informativos/dOficina/mDP8.php';
        include './modals/informativos/dOficina/mDP9.php';
        include './modals/informativos/dOficina/mDP10.php';
        
        //MODAL DE OPL
        include './modals/opl/mUOpl.php';
        include './modals/opl/mIOpl.php';
        include './modals/opl/mDOpl.php';
        
        //FUCNIONES   
        include './db/funciones.php';
    
    ?>
    
    <script src="js/check/almacen.js"> </script>
    <script src="js/check/laboratorio.js"> </script>    
    <script src="js/check/linea.js"> </script>
    <script src="js/check/mantenimiento.js"> </script>
    <script src="js/check/oficina.js"> </script>
    
    <script src="js/weekly/almacen.js"> </script>
    <script src="js/weekly/laboratorio.js"> </script>    
    <script src="js/weekly/linea.js"> </script>
    <script src="js/weekly/mantenimiento.js"> </script>
    <script src="js/weekly/oficina.js"> </script>
    
    <script src="js/monthly/almacen.js"> </script>
    <script src="js/monthly/laboratorio.js"> </script>    
    <script src="js/monthly/linea.js"> </script>
    <script src="js/monthly/mantenimiento.js"> </script>
    <script src="js/monthly/oficina.js"> </script>
    
    <!--ELEMENTOS PARA LA CABECERA-->
   <!--ELEMENTOS PARA LA CABECERA-->
    <a align = center id="headerFixed" class="contenedor"> 
        <div class='fila0 col-lg-6'>
            <img src="imagenes/log.jpg" style="height: 5.2vh; margin-top: 3.7vh" >
        </div>        
        <div class="fila1 col-lg-6" >
            <h3 class="tituloPortal" >
                PORTAL 5S's: OPL
            </h3 >
            <?php if ($name != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" onclick="logiin()" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 
</head>

<body>
    <br>
    <div style="border: 1px solid #FFF; margin-top: -.9%" >
        <ul <?php if ($typeUser == 1) { ?> id="navA" <?php } else { ?> id="navC" <?php } ?> >
            <li><a href="index.php">Inicio</a></li>            
            <li><a onclick="audit()" class="btn-new-audit" >Auditoria</a></li> 
            <li><a href="report.php">Reportes</a></li>
            <li><a href="calendar.php">Calendario</a></li>
            <li><a href="info.php">Informacion</a></li>
        </ul>
    </div>   
    <div id="menuPrincipal" > 
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" > 
            <li class="nav-item"> 
                <a class="nav-link active" data-toggle="tab" href="champions.php">Mis Opl</a> 
            </li> 
            <li class="nav-item"> 
                <a href="sOpl.php">Estadisticas</a> 
            </li> 
        </ul> 
        <!-- Tab panes -->            
        <div class="tab-content" style="width: 98%; margin-left: 1%"> 
            <button type="button" class="btn btn-warning btnAdd" data-toggle="modal" data-target="#mIUsuario"  >
                Agregar
            </button> 
            <br><br>
            <h3 class="text-center all-tittles" style="margin-top: -35px" >PISO</h3>
            <table class="tbl">
                <thead class="headt">
                    <tr class="trt" >
                        <th class="tht tblHxs-00" >NO.</th>
                        <th class="tht tblHs" >HALLAZGO</th>
                        <th class="tht tblHs" >ACCION</th>
                        <th class="tht tblHxs-0" >RESPONSABLE</th>
                        <th class="tht tblHxs-0" >SOPORTE</th>
                        <th class="tht tblHxs-0" >INICIO</th>
                        <th class="tht tblHxs-0" >COMPROMISO</th>                        
                        <th class="tht tblHxs-0" >AUDITADO</th>
                        <th class="tht tblHxs-00"></th>
                        <?php if ($typeUser > 0) { ?>
                        <th class="tht tblHs-0" >ACCION</th>
                        <?php } ?>
                    </tr>
                </thead>
                
                <?php 
                    $cListOpl = cListOpl($dia, $depto); 
                ?>
                
                <tbody style="height: 530px" class="bodyt">
                    <?php for ($i = 0; $i < count($cListOpl); $i++) { ?>
                    <tr class="trt" >
                            <td class="tdt tblBxs-00"> <?php echo $i+1 ?> </td>
                            <td class="tdt tblBs"> <?php echo $cListOpl[$i][2]; ?> </td>
                            <td class="tdt tblBs"> <?php echo $cListOpl[$i][3]; ?> </td>
                            <td class="tdt tblBxs-0"> <?php echo $cListOpl[$i][4]; ?> </td>
                            <td class="tdt tblBxs-0"> <?php echo $cListOpl[$i][5]; ?> </td>
                            <td class="tdt tblBxs-0"> <?php echo $cListOpl[$i][0]->format('d-M'); ?> </td>
                            <td class="tdt tblBxs-0"> <?php echo $cListOpl[$i][1]->format('d-M'); ?> </td>
                            <td class="tdt tblBxs-0"> <?php echo $cListOpl[$i][7]; ?> </td>
                            <td class="tdt tblBxs-00"> 
                                <?php                                     
                                    switch ($cListOpl[$i][6]){
                                        case '0';
                                            ?>
                                            <img src="./imagenes/opl/opl0.png" style="width: 3vh">
                                            <?php
                                            break;
                                        case '1';
                                            ?>
                                            <img src="./imagenes/opl/opl1.png" style="width: 3vh">
                                            <?php
                                            break;
                                        case '2';
                                            ?>
                                            <img src="./imagenes/opl/opl2.png" style="width: 3vh">
                                            <?php
                                            break; 
                                        case '3'; 
                                            ?>
                                            <img src="./imagenes/opl/opl3.png" style="width: 3vh" > 
                                            <?php
                                            break; 
                                    }
                                ?>
                            </td>
                            <?php if ($typeUser > 0) { ?>
                            <td class="tdt tblBs-0"> 
                                <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#mUOpl" 
                                    data-id = "<?php echo $cListOpl[$i][8]; ?>" 
                                    data-finicio = "<?php echo $cListOpl[$i][0] ->format('Y-m-d'); ?>" 
                                    data-fcompromiso = "<?php echo $cListOpl[$i][1] ->format('Y-m-d'); ?>" 
                                    data-hallazgo = "<?php echo $cListOpl[$i][2] ?>" 
                                    data-accion = "<?php echo $cListOpl[$i][3] ?>" 
                                    data-resp = "<?php echo $cListOpl[$i][4] ?>" 
                                    data-sop = "<?php echo $cListOpl[$i][5] ?>" 
                                    data-estado = "<?php echo $cListOpl[$i][6] ?>" > 
                                    <i class='glyphicon glyphicon-edit'></i> Modificar </button> 
                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#mDOpl" 
                                    data-id = "<?php echo $cListOpl[$i][8] ?>" 
                                    data-hallazgo = "<?php echo $cListOpl[$i][2] ?>" 
                                    data-accion = "<?php echo $cListOpl[$i][3] ?>" 
                                    data-resp = "<?php echo $cListOpl[$i][4] ?>" >
                                    <i class='glyphicon glyphicon-trash'></i> Eliminar </button> 
                            </td>
                            <?php } ?>
                        </tr> 
                        <?php } ?>
                </tbody>
            </table>
            <br>            
        </div>
    </div>    
</body>
