<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
    session_start();
    date_default_timezone_set("America/Mexico_City");
    $_SESSION['path'] = $_SERVER['PHP_SELF'];
    
    if (isset($_SESSION['userName']) && isset($_SESSION['tipo'])){
        $userName = $_SESSION['userName'];
        $name = $_SESSION['name'];
        $typeUser = $_SESSION['tipo'];    
        $typeEv = $_SESSION['ev']; 
        $depto = $_SESSION["depto"];
        
    } else {
        $userName = '';
        $name = 'INICIAR SESION';
        $typeUser = 0;
        $typeEv = 0;
    }
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  
    
    <!-- LIBRERIAS PARA HIGHCHART -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>    
    
    <!--ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS-->
    <link rel="stylesheet" href="css/mod.css">
    <script src="js/champion.js" ></script>
    
    <script type="text/javascript">
        jQuery().ready(            
            function() {
                getResult();
                setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
            }
        );
    
        function getResult() {
            //CUANDO SE ACTIVA LA HOJA TENDREMOS QUE MANDAR SOLO EL TEMA, PARA PODER HACER LA CONSULTA ADECUADA
            //PARA ELLO SE UTILIZA AJAX 
            var anio = document.getElementById("anioA").value;
            var mes = document.getElementById("mesA").value;
            var area = document.getElementById("area").value;
            //var area = "<?php //echo $depto ?>";
            $.ajax({
                url: "contenedores/rArea.php",
                type: "post",
                data: { a: anio, m: mes, area: area },
                success: function (resultA) {
                    jQuery("#contGeneral").html("");
                    $("#hGeneral").hide();
                    $("#hArea").show();
                    jQuery("#contAreaCh").html(resultA);
                }
            });    
        }
    </script>

    <?php 
        include './modals/informativos/help.php';
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MOD AL INFORMATIVOS
        include './modals/accesos/login.php';
        include './modals/accesos/cerrarSesion.php';
        include './modals/informativos/rules.php';
        
        //MODALS PARA LA SELECCION DE AUDITORIAS
        //DE ACUERDO A LOS PRIVILEGIOS DE USUARIOS
        include './modals/vse/AreaAudit.php';
        switch ($typeUser) {
            case 1:
                include './modals/vse/AreaAudit.php'; 
                include './modals/vse/DeptoAudit.php'; 
                include './modals/vse/mAuditCon.php'; 
                break;
            
            case 2: 
                include './modals/supervisor/DeptoAudit.php'; 
                break;
            
            case 6:                
                include './modals/informativos/TipoAuditoriaCh.php';
                break;
        }
        
        include './modals/accesos/configU.php';
        
        include './modals/informativos/InfoAuditoria.php';
        include './modals/informativos/NoneAuditoria.php';        
        
        include './modals/auditoria/daily/checkAlmacen.php';
        include './modals/auditoria/daily/checkLab.php';
        include './modals/auditoria/daily/checkLinea.php';
        include './modals/auditoria/daily/checkMantenimiento.php';
        include './modals/auditoria/daily/checkOficina.php';
        
        include './modals/auditoria/monthly/almacen.php';
        include './modals/auditoria/monthly/almacen2.php';
        include './modals/auditoria/monthly/laboratorio.php';
        include './modals/auditoria/monthly/laboratorio2.php';
        include './modals/auditoria/monthly/linea.php';
        include './modals/auditoria/monthly/linea2.php';
        include './modals/auditoria/monthly/mantenimiento.php';
        include './modals/auditoria/monthly/mantenimiento2.php';
        include './modals/auditoria/monthly/oficina.php';
        
        include './modals/auditoria/weekly/almacen.php';
        include './modals/auditoria/weekly/laboratorio.php';
        include './modals/auditoria/weekly/linea.php';
        include './modals/auditoria/weekly/mantenimiento.php';
        include './modals/auditoria/weekly/oficina.php';      
        
        include './modals/informativos/PuntosAuditoriaB.php';
        include './modals/informativos/PuntosAuditoriaR.php';
        include './modals/informativos/PuntosAuditoriaM.php';
        
        //AYUDAS VISUALES DE PREGUNTAS DE DAILY
        //ALMACEN
        include './modals/informativos/dAlmacen/mDP1.php';
        include './modals/informativos/dAlmacen/mDP2.php';
        include './modals/informativos/dAlmacen/mDP3.php';
        include './modals/informativos/dAlmacen/mDP4.php';
        include './modals/informativos/dAlmacen/mDP5.php';
        include './modals/informativos/dAlmacen/mDP6.php';
        include './modals/informativos/dAlmacen/mDP7.php';
        include './modals/informativos/dAlmacen/mDP8.php';
        include './modals/informativos/dAlmacen/mDP9.php';
        include './modals/informativos/dAlmacen/mDP10.php';
        
        //LABORATORIO
        include './modals/informativos/dLaboratorio/mDP1.php';
        include './modals/informativos/dLaboratorio/mDP2.php';
        include './modals/informativos/dLaboratorio/mDP3.php';
        include './modals/informativos/dLaboratorio/mDP4.php';
        include './modals/informativos/dLaboratorio/mDP5.php';
        include './modals/informativos/dLaboratorio/mDP6.php';
        include './modals/informativos/dLaboratorio/mDP7.php';
        include './modals/informativos/dLaboratorio/mDP8.php';
        include './modals/informativos/dLaboratorio/mDP9.php';
        include './modals/informativos/dLaboratorio/mDP10.php';
        
        //LINEA
        include './modals/informativos/dLinea/mDP1.php';
        include './modals/informativos/dLinea/mDP2.php';
        include './modals/informativos/dLinea/mDP3.php';
        include './modals/informativos/dLinea/mDP4.php';
        include './modals/informativos/dLinea/mDP5.php';
        include './modals/informativos/dLinea/mDP6.php';
        include './modals/informativos/dLinea/mDP7.php';
        include './modals/informativos/dLinea/mDP8.php';
        include './modals/informativos/dLinea/mDP9.php';
        include './modals/informativos/dLinea/mDP10.php';
        
        //MANTENIMIENTO
        include './modals/informativos/dMantenimiento/mDP1.php';
        include './modals/informativos/dMantenimiento/mDP2.php';
        include './modals/informativos/dMantenimiento/mDP3.php';
        include './modals/informativos/dMantenimiento/mDP4.php';
        include './modals/informativos/dMantenimiento/mDP5.php';
        include './modals/informativos/dMantenimiento/mDP6.php';
        include './modals/informativos/dMantenimiento/mDP7.php';
        include './modals/informativos/dMantenimiento/mDP8.php';
        include './modals/informativos/dMantenimiento/mDP9.php';
        include './modals/informativos/dMantenimiento/mDP10.php';
        
        //OFICINA
        include './modals/informativos/dOficina/mDP1.php';
        include './modals/informativos/dOficina/mDP2.php';
        include './modals/informativos/dOficina/mDP3.php';
        include './modals/informativos/dOficina/mDP4.php';
        include './modals/informativos/dOficina/mDP5.php';
        include './modals/informativos/dOficina/mDP6.php';
        include './modals/informativos/dOficina/mDP7.php';
        include './modals/informativos/dOficina/mDP8.php';
        include './modals/informativos/dOficina/mDP9.php';
        include './modals/informativos/dOficina/mDP10.php';
        
        //FUCNIONES   
        include './db/funciones.php';
        
        $cDepartamentos = cTotalDeptos();
                
        $anio = date("Y");
        $month[1]= "JAN";
        $month[2]= "FEB";
        $month[3]= "MAR";
        $month[4]= "APR";
        $month[5]= "MAY";
        $month[6]= "JUN";
        $month[7]= "JUL";
        $month[8]= "AUG";
        $month[9]= "SEP";
        $month[10]= "OCT";
        $month[11]= "NOV";
        $month[12]= "DEC";
    
    ?>
    
    <script src="js/check/almacen.js"> </script>
    <script src="js/check/laboratorio.js"> </script>    
    <script src="js/check/linea.js"> </script>
    <script src="js/check/mantenimiento.js"> </script>
    <script src="js/check/oficina.js"> </script>
    
    <script src="js/weekly/almacen.js"> </script>
    <script src="js/weekly/laboratorio.js"> </script>    
    <script src="js/weekly/linea.js"> </script>
    <script src="js/weekly/mantenimiento.js"> </script>
    <script src="js/weekly/oficina.js"> </script>
    
    <script src="js/monthly/almacen.js"> </script>
    <script src="js/monthly/laboratorio.js"> </script>    
    <script src="js/monthly/linea.js"> </script>
    <script src="js/monthly/mantenimiento.js"> </script>
    <script src="js/monthly/oficina.js"> </script>
    
    <!--ELEMENTOS PARA LA CABECERA-->
   <!--ELEMENTOS PARA LA CABECERA-->
    <a align = center id="headerFixed" class="contenedor"> 
        <div class='fila0 col-lg-6'>
            <img src="imagenes/log.jpg" style="height: 5.2vh; margin-top: 3.7vh" >
        </div>        
        <div class="fila1 col-lg-6" >
            <h3 class="tituloPortal" >
                PORTAL 5S's: Reporte
            </h3 >
            <?php if ($name != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" onclick="logiin()" style="color: #ffffff;" ><?php echo " $name" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 
</head>

<body>
    <br>
    <div style="border: 1px solid #FFF; margin-top: -.9%" >
        <ul <?php if ($typeUser == 1) { ?> id="navA" <?php } else { ?> id="navC" <?php } ?> >
            <li ><a href="./index.php">Inicio</a></li>
            <?php if ($typeUser == 1) { ?>
            <li><a href="admin.php">Administracion</a> </li>
            <?php } ?>
            <?php if ($typeUser != 0) { ?>
            <li onclick="audit()" ><a class="btn-new-audit" >Auditoria</a></li>
            <?php } ?>
            <li><a href="calendar.php">Calendario</a></li>
            <li><a href="info.php">Informacion</a></li>
            <li><a <?php if ($typeUser == 1) { ?> href="opl.php" <?php } else if ($typeUser == 6 ) { ?> href="lOpl.php" <?php } ?> >OPL</a></li>
        </ul>
        
    </div>   
    <div id="menuPrincipal" >
        <!-- Tab panes -->
        <div id="r_area" class="container tab-pane" ><br>
            <div id="hArea" style="margin-left: -14%; margin-top: -2%;">            
                <ul class="nav">
                    <li>
                        ANIO:
                        <select id="anioA" onchange="r_area()" > 
                            <option value="<?PHP echo $anio ?>" ><?php echo $anio ?></option>
                            <option value="<?PHP echo $anio-1 ?>" ><?php echo $anio-1 ?></option>
                        </select>
                    </li>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <li>
                        MES: 
                        <select id="mesA" onchange="r_area()" >
                            <option value="0">ALL</option> 
                            <?php for ($i = 1; $i < 13; $i++){ ?>
                            <option value="<?PHP echo $i ?>" ><?php echo $month[$i] ?></option> 
                            <?php } ?>
                        </select>
                    </li>
                    &nbsp;&nbsp;&nbsp;&nbsp; 
                    <li>                     
                        <input type="hidden" id="area" value="<?php echo $depto ?>">
                    </li>
                </ul>
            </div>

            <div id="contAreaCh" style="margin-left: -10%; margin-top: -1%; width: 120%" >

            </div>
        </div>         
    </div>    
</body>